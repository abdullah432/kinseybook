import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class LivingWithArt3 extends StatefulWidget {
  String name = "family1";
  @override
  _LivingWithArt3State createState() => _LivingWithArt3State();
}

class _LivingWithArt3State extends State<LivingWithArt3> {
  @override
  Widget build(BuildContext context) {
    final textFiles = [
      "\nof Shirley’s response to streets in the Caribbean that reminded her so much of her childhood neighborhood, of their interactions with people in various parts of the world, who greeted them with great appreciation of their generosity and their willingness to travel long distances to learn about different cultures.\n\nBernard and Shirley’s story includes people who share their passion, and have contributed to the collection through gifts. The Malawi chief’s chair,",
      "with intricately carved figures in relief, was a gift from the Xerox Black Employees Association in gratitude for Bernard’s leadership. Their portrait by California artist and friend Artis Lane was another gift from generous friends. Lane’s sculptures are prominent in the Kinsey home, alongside her portrait of Khalil.\n\nImages of the Kinseys created by both famous and emerging artists can be found throughout their home. They are modest about these pieces, although it is clear that they hold a special place in their hearts. Shirley is constantly discovering new ways of seeing, through her interactions with artists such as Phoebe Beasley, Bill Pajaud, Richard Mayhew, and Ed Dwight. These encounters extend her awareness and push her to explore new media.\n\nFor Bernard, much of the power of collecting lies in the tangibility of objects: holding Equiano’s 18th-century writings in his hand; studying at the source and expand his understanding of the cultural capital he  shares with the world through the exhibition,“The Kinsey Collection: Shared Treasures of Bernard and Shirley Kinsey,” and publications highlighting the collection.\n\nBernard and Shirley share deep intellectual curiosity and hunger for exploration, and a profound commitment to their mission as both cultivators of knowledge and stewards of history. Their home is a testimony to their passion for this commitment, and is in itself a work of art.\n\nLiving with such a large and provocative collection ",
      "Kinseys, but the advantages for visitors who encounter the collection are also extraordinary. Those who see it there or in museums across America have had the opportunity to join them as stewards of a remarkable past of perseverance and accomplishment, and of a vibrant present filled with beauty and variety. The Kinseys’ daily interaction with such inspiration ensures that many more will share in their legacy."
    ];
    final fonts = [
      "Auto2-Regular",
      "Auto2-Regular",
    ];
    List<double> presetFonts = [27, 24, 20, 18, 14, 8];
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            flex: 6,
                            child: GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/living_with_art_03.jpg",)));
                              },
                              child: Image.asset(
                                "assets/images/living_with_art_03.jpg",
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 2,
                            child: AutoSizeText.rich(
                              TextSpan(
                                text: "Inuit Mother and Child, 1975\n",
                                children: [
                                  TextSpan(
                                      text: "fossilized whale bone",
                                      style: TextStyle(
                                          fontWeight: FontWeight.normal)),
                                ],
                              ),
                              style: TextStyle(fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                              presetFontSizes: presetFonts,
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: AutoSizeText(
                              textFiles[0],
                              textAlign: TextAlign.justify,
                              presetFontSizes: presetFonts,
                              style: TextStyle(fontFamily: fonts[0]),
                            ),
                          ),
                        ],
                      ),
                    )),
                Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: AutoSizeText(textFiles[1],
                          textAlign: TextAlign.justify,
                          presetFontSizes: presetFonts,
                          style: TextStyle(fontFamily: fonts[1])),
                    )),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: AutoSizeText(textFiles[2],
                              textAlign: TextAlign.justify,
                              presetFontSizes: presetFonts,
                              style: TextStyle(fontFamily: fonts[1])),
                        ),
                        Flexible(
                          flex: 4,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/living_with_art_04.jpg",)));
                              },
                              child: Image.asset(
                                "assets/images/living_with_art_04.jpg",
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),
                        AutoSizeText("Hopi Kachina Doll",
                            textAlign: TextAlign.center,
                            presetFontSizes: presetFonts,
                            style: TextStyle(
                                fontFamily: fonts[1],
                                fontWeight: FontWeight.bold))
                      ],
                    ),
                  ),
                )
              ],
            )));
  }
}
