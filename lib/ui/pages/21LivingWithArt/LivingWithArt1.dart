import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class LivingWithArt1 extends StatefulWidget {
  @override
  _LivingWithArt1State createState() => _LivingWithArt1State();
}

class _LivingWithArt1State extends State<LivingWithArt1> {
  // List<double> presetFonts =  [28, 24, 20, 16, 14];
  // presetFonts =  [28];
  @override
  Widget build(BuildContext context) {
  final textFiles = [
    "People collect art and artifacts for many different reasons. Some are aesthetes who aim to capture moments of beauty; others collect to possess, to own a body of work or a body of ideas. Like many collectors, Bernard and Shirley Kinsey began collecting objects and art to document their lives, as a way to reflect on their maturation as people, as parents, and as professionals. Bernard, as an executive at Xerox, gave prints of High Aspirations by Ernie Barnes to top",
    "performing sales people. His intention was both to honor and to educate his staff while bridging cultural differences, and to forge connections through mutual aesthetic appreciation. The Kinseys also collected art during their travels around the world, on their quest for knowledge of things larger than themselves. These beginnings evolved into a passion that became its own journey: to discover who and what came before them, and what could propel them further along their path. As the Kinseys identified themes emerging from their collection, they began to shape a larger purpose for the art and artifacts in",
    "their possession: to inspire and educate others through a repository of culturally important material.\n\nThe Kinseys’ vision—to document the past and the present through the collection of historical documents, and two- and three-dimensional art—requires an ongoing process of discovery. It also requires tenacity and courage, particularly because the collection contains many grim reminders of the subjugation of black people and black identity."
  ];
    List<double> presetFonts = [26, 24, 20, 16, 14];
    // presetFonts = [26];
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/living_with_art_01.jpg",)));
              },
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width ,
                child: FittedBox(
                  fit: BoxFit.cover,
                  child: Image.asset("assets/images/living_with_art_01.jpg"),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: AutoSizeText.rich(
                          TextSpan(
                              text: "Living with Art\n\n",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                              children: [
                                TextSpan(
                                  text: textFiles[0],
                                  style: TextStyle(fontWeight: FontWeight.normal),
                                )
                              ]),
                          textAlign: TextAlign.justify,
                          presetFontSizes: presetFonts)),
                ),
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: AutoSizeText(
                    textFiles[1],
                    presetFontSizes: presetFonts,
                    textAlign: TextAlign.justify,
                  ),
                )),
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: AutoSizeText(
                    textFiles[2],
                    presetFontSizes: presetFonts,
                    textAlign: TextAlign.justify,
                  ),
                ))
              ],
            ),
          )
        ],
      ),
    );
  }
}
