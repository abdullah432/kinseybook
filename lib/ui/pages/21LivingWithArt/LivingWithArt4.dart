import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class LivingWithArt4 extends StatefulWidget {
  String name = "family1";
  @override
  _LivingWithArt4State createState() => _LivingWithArt4State();
}

class _LivingWithArt4State extends State<LivingWithArt4> {
  @override
  Widget build(BuildContext context) {
    final textFiles = [
      "Bernard and Shirley Kinsey find that leading people down to the “history gallery” in their home is like taking them on a journey through time. Guests range in age from their son Khalil’s young friends to artists, friends, and family in the twilight years of life. But all who visit experience similar feelings of wonder and astonishment at the perseverance and hope that shines through these artifacts.\n\nThe Kinseys are continually educating themselves as well as their guests about what it means to be stewards of such a powerful legacy. They feel grounded by this work, surrounded by the accomplishments of generations who were without resources or protection. Their home is a vivid reminder of all those who forged the path to freedom. What could then be achieved along that path becomes apparent on the walls: the sheer number of artists and ephemera bears witness to an enormous variety of artistic expression across eras and continents. It is impressive, and humbling.\n\nThe weight of this ongoing interaction with history is lightened by the design of the house, constructed to showcase the collection. Light from floor-to-ceiling windows fills the house, with views onto the gleaming Pacific and the hillsides of Pacific Palisades. The design merges inside and outside, and this theme reflects the way in which the Kinseys live with art and cultural objects. A Dogon ladder from Mali greets visitors outside the front door, a natural composition based on their deep appreciation for their African roots. Inside the door, a seven-foot",
      "sculpture by California artist Matthew Thomas with elements of African ritualized carving, Native American symbolism, and Buddhist energy welcomes all visitors. It encourages an exploration of the intersections of space and place that signify the Kinseys’ passion for life and collecting in a global context.\n\nEverywhere in the Kinsey home, on all the walls and surfaces, one finds art from the far corners of the world. There are carved images from Northwest Coast Indian tribes and an Inuit polar bear carved from walrus tusk. A large painting, The 200 Year Old Tree by Ed Loper, stands as sentinel opposite an “ethnic wall” that includes African ritual masks, a beautifully woven breastplate from Papua New Guinea, and Kachina Dolls from the Hopi in Arizona. Each artifact carries equal merit and is linked to a personal story. Often, close by, one can find a book detailing work similar to that on display, such as George Swinton’s Sculptures of the Eskimo (1972), as evidence of the Kinseys’ quest to educate themselves about the contexts of these remarkably diverse works.\n\nThe art and artifacts that comprise the Kinsey Collection are varied, yet speak a single story.  A narrative emerges—from the striking face of the Haida carving, to the Steuben crystal with sculptural elements that complement the fine art around it, to the technically masterful Chinese jade sculptures and the large abstract paintings by Bill Dallas, harmoniously juxtaposed to Aboriginal dot paintings.  Each object and work of art is infused with the Kinseys’ own story as a couple, yet they are also individuals with different",
      "interests that sometimes intersect, sometimes diverge. Bernard says that he collected the early African American masters less for aesthetic reasons than for the historical interest and importance of art made by blacks at a time when most were enslaved. Shirley, however, has a strong connection with the rich colors and the brushstrokes that bring to life bucolic scenes mindful of her childhood in Florida, and of African American life in the South. She seeks visual reminders of her past, to sustain her in the face of the materialism of life in Los Angeles.\n\nBernard’s greatest passion is his love for Shirley; therefore, he, too, is made whole by reminders of her past. Together, they can recount their journeys to over 100 countries through the images and artifacts they have collected. There are stories of Bernard’s passion for polar bears, "
    ];
    final fonts = [
      "Auto2-Regular",
      "Auto2-Regular",
    ];
    List<double> presetFonts = [27, 24, 20, 18, 14, 8];
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/living_with_art_05.jpg",)));
                          },
                          child: Image.asset(
                            "assets/images/living_with_art_05.jpg",
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      AutoSizeText.rich(
                        TextSpan(text: " left: ", children: [
                          TextSpan(
                              text: "Young Maasai Woman,",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: " Ed Dwight              right: ",
                              style: TextStyle(fontWeight: FontWeight.normal)),
                          TextSpan(
                              text: "The Pipers,  ",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: "Ed Dwight  ",
                              style: TextStyle(fontWeight: FontWeight.normal))
                        ]),
                        presetFontSizes: presetFonts,
                      )
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      AutoSizeText.rich(
                        TextSpan(
                            text: "Cactus Flower Breastplate\n",
                            style: TextStyle(fontWeight: FontWeight.bold),
                            children: [
                              TextSpan(
                                text: "Papua New Guinea",
                                style: TextStyle(fontWeight: FontWeight.normal),
                              )
                            ]),
                        presetFontSizes: presetFonts,
                        textDirection: TextDirection.rtl,
                      ),
                      SizedBox(width: 20,),
                      GestureDetector(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/living_with_art_06.jpg",)));
                        },
                        child: Image.asset(
                          "assets/images/living_with_art_06.jpg",
                          fit: BoxFit.fill,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            )));
  }
}
