import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForwardTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 8;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
            height: ht,
            width: wt,
            decoration: template.viewportBorder(),
            child: Center(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    width: (ht / 300) * 7.5,
                  ),
                  Container(
                      height: template.textContainerheight(),
                      width: template.viewWidth() * 270,
                      child: Padding(
                        padding: template.textcontainerPadding(),
                        child: Column(children: <Widget>[
                          Container(
                            height: template.viewHeight() * 230,
                            width: template.viewWidth() * 200,
                            child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => GalleryView(
                                                url:
                                                    "assets/images/forward_2_main.jpg",
                                              )));
                                },
                                child: Image.asset(
                                  "assets/images/forward_2_main.jpg",
                                  fit: BoxFit.contain,
                                )),
                          ),
                          SizedBox(
                            height: (ht / 300) * 36,
                          ),
                          Container(
                              height: template.footerTextContainerHihght(),
                              width: template.textContainerwidth() * 3,
                              child: template.footerLeftText()),
                        ]),
                      )),
                  Expanded(
                    flex: 1,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "The Cultivators, 2000",
                          style: template.textheaderStyle(),
                        ),
                        Text(
                          "Samuel L. Dunson, Jr.",
                          style: template.bodyTextStyle(),
                        ),
                        Text(
                          "Oil on canvas",
                          style: template.bodyTextStyle(),
                        ),
                        Text(
                          "38 1⁄2 x 26 1⁄2 in",
                          style: template.bodyTextStyle(),
                        ),
                        SizedBox(
                          height: template.viewHeight() * 10,
                        ),
                        GestureDetector(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return Dialog(
                                    child: Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              2,
                                      width:
                                          MediaQuery.of(context).size.width / 2,
                                      child: Stack(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 10, vertical: 20),
                                            child: Column(
                                              children: <Widget>[
                                                SizedBox(
                                                  height: 30,
                                                ),
                                                AutoSizeText(
                                                    "Samuel L. Dunson, Jr. (1970–)",
                                                    style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                                                    presetFontSizes: [
                                                      28,
                                                      26,
                                                      24,
                                                      22,
                                                      20,
                                                      18,
                                                      14
                                                    ]),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Expanded(
                                                    child: AutoSizeText(
                                                  "Dunson showed a talent for art as a youth. He attended Tennessee State University, where he began to paint in earnest. He earned his bachelor’s degree in 1992 and continued his education at the Savannah College of Art and Design, earning an MFA in 2000. He returned to Tennessee State as an assistant professor. He continues to paint, and to exhibit his work throughout the United States.",
                                                  presetFontSizes: [
                                                    28,
                                                    26,
                                                    24,
                                                    22,
                                                    20,
                                                    18,
                                                    16,
                                                    14
                                                  ],
                                                  textAlign: TextAlign.justify,
                                                ))
                                              ],
                                            ),
                                          ),
                                          Positioned(
                                            child: IconButton(
                                              icon: Icon(
                                                Icons.clear,
                                                color: Colors.black,
                                                size: 30,
                                              ),
                                              onPressed: () =>
                                                  Navigator.pop(context),
                                            ),
                                            top: 10,
                                            right: 10,
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                });
                          },
                          child: Container(
                              height: template.viewHeight() * 15,
                              width: template.viewWidth() * 15,
                              child: Image.asset(
                                "assets/images/bio.png",
                                height: template.viewHeight() * 10,
                              )),
                        )
                      ],
                    ),
                  )
                ],
              ),
            )));
  }
}
