import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForwardOne extends StatefulWidget {
  @override
  _ForwardOneState createState() => _ForwardOneState();
}

class _ForwardOneState extends State<ForwardOne> {
  final textFiles = [
    "Not long after my book, Slavery by Another Name: The Re-Enslavement of Black Americans from the Civil War to World War II, was published in 2008, I received an unexpected call from Bernard Kinsey. He insisted that he was a direct relative of a woman whose poignant letter of 1903 to President Theodore Roosevelt appeared prominently in the book.\n\nThe letter, written by a then-young woman named Carrie Kinsey, described how her 14-year-old brother had been kidnapped and sold into slavery. It was the historical artifact that had haunted me more than any other in the eight-year process of writing my book, and I’d never been able to learn the fates of Carrie or James. In just a few desperate lines, written in the struggling hand of a barely literate, impoverished African American woman, was a story that distilled both the individual horror and the fantastic width and breadth of the American campaign to crush Southern blacks at the beginning of the 20th century. I later told Bernard that in the story of Carrie’s stolen brother James—his seizure and sale to a massive plantation owned by one of Georgia’s most powerful families, the abject refusal of authorities to assist her, the brutalization of thousands of other blacks on the plantation where he worked in chains, the heroism of Carrie Kinsey in seeking the aid of President Roosevelt, and finally in the futility of her never investigated letter—was the entire epic tragedy of black life in the rural South at that time. All the wrongs, all the valiance",
    "of African Americans who persevered against immeasurable odds, all the failings of America to the slaves emancipated by the Civil War and to generations of their descendants.\n\nBernard pleaded for a copy of the full text of the letter. He wanted to include it in exhibitions of the Kinsey Collection. But weeks passed without my fulfilling his request. I told myself that I was delaying because the letter was filed away with thousands of other things, then that I was swamped by the surprising response to my book—as I was eventually awarded a Pulitzer Prize. Weeks passed. Bernard called and e-mailed again. Finally I realized that I was avoiding the letter for another reason: my own personal sense of shame. Not shame for anything I had done personally. But shame for America—for white Americans first but also for all of America. Shame that an idealistic nation could expend so much of its own blood to give freedom and citizenship and opportunity to four million enslaved African Americans and then allow that achievement to be debased in barely the span of a generation.\n\nFinally one Saturday morning, I dug through the boxes, found the letter, and sent it to him. A few months later, I escorted him and his extraordinary wife Shirley to the National Archives. In an intensely emotional moment we each held in our hands the actual piece of paper that 106 years earlier had been in the hands of Carrie Kinsey, the paper on which she inked perhaps the only record of the life and loss of her brother James.",
    "Such humble artifacts are crucial to our understanding of the past, and of ourselves. Major documents and official histories almost always survive to tell the story of society’s rulers. But only through the miraculously preserved wisps of history like Carrie Kinsey’s letter can we defy the inexorable erasure of the full story of all our forebears. Bernard and Shirley Kinsey understand that. By the time we held the Carrie Kinsey letter in our hands together, I had come to know them as friends—and had heard the remarkable story of their lives and their fantastic achievements, professionally, personally, and as chroniclers of the African American experience.\n\nI learned from them that Carrie Kinsey’s family survived its ordeal to become key leaders among African Americans. The letter—like the entire Kinsey Collection—came also to rep- resent for me something altogether different. It is a totem of a story of the greatest kind of American triumph, of a family besieged but never destroyed by the worst forces of our society. Of a family that survived and then thrived—and now seeks to share a legacy of comprehension and appreciation of the African American experience.\n\nThere are few forces in America that more powerfully demonstrate how much can be achieved in our country—and has been achieved by African Americans—than Bernard and Shirley Kinsey and their extraordinary collection.\n\n"
  ];

  bool listView = false;
  ScrollController listViewController = ScrollController();
  double fontSize = 50;
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 7;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Center(
          child: listView
              ? Stack(
                  children: <Widget>[
                    ListView.builder(
                        controller: listViewController,
                        itemCount: textFiles.length,
                        itemBuilder: (BuildContext context, index) {
                          if (index == 0) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              child: AutoSizeText.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                        text: "Forward\n\n",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 24)),
                                    TextSpan(text: "Not long after my book,"),
                                    TextSpan(
                                        text:
                                            "Slavery by Another Name: The Re-Enslavement of Black Americans from the Civil War to World War II,",
                                        style: TextStyle(
                                            fontStyle: FontStyle.italic)),
                                    TextSpan(
                                        text:
                                            " was published in 2008, I received an unexpected call from Bernard Kinsey. He insisted that he was a direct relative of a woman whose poignant letter of 1903 to President Theodore Roosevelt appeared prominently in the book.\n\nThe letter, written by a then-young woman named Carrie Kinsey, described how her 14-year-old brother had been kidnapped and sold into slavery. It was the historical artifact that had haunted me more than any other in the eight-year process of writing my book, and I’d never been able to learn the fates of Carrie or James. In just a few desperate lines, written in the struggling hand of a barely literate, impoverished African American woman, was a story that distilled both the individual horror and the fantastic width and breadth of the American campaign to crush Southern blacks at the beginning of the 20th century. I later told Bernard that in the story of Carrie’s stolen brother James—his seizure and sale to a massive plantation owned by one of Georgia’s most powerful families, the abject refusal of authorities to assist her, the brutalization of thousands of other blacks on the plantation where he worked in chains, the heroism of Carrie Kinsey in seeking the aid of President Roosevelt, and finally in the futility of her never investigated letter—was the entire epic tragedy of black life in the rural South at that time. All the wrongs, all the valiance")
                                  ],
                                ),
                                presetFontSizes: [fontSize],
                                textAlign: TextAlign.justify,
                              ),
                            );
                          }
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            child: AutoSizeText(textFiles[index],
                                minFontSize: fontSize,
                                textAlign: TextAlign.justify),
                          );
                        }),
                    Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          color: Colors.white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              IconButton(
                                onPressed: () {
                                  if (listViewController
                                          .position.minScrollExtent ==
                                      listViewController.offset) {
                                    print("on the end");
                                  } else {
                                    listViewController.animateTo(
                                        listViewController.offset -
                                            listViewController
                                                    .position.maxScrollExtent /
                                                7,
                                        duration: Duration(milliseconds: 500),
                                        curve: Curves.easeIn);
                                  }
                                },
                                icon: Icon(
                                  Icons.arrow_back_ios,
                                  color: Colors.blue,
                                  size: 40,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  IconButton(
                                      icon: Icon(
                                        Icons.zoom_out,
                                        color: Colors.blue,
                                        size: 40,
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          fontSize = 30;
                                        });
                                      }),
                                  IconButton(
                                      icon: Icon(
                                        Icons.close,
                                        color: Colors.blue,
                                        size: 40,
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          listView = false;
                                        });
                                      })
                                ],
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.blue,
                                  size: 40,
                                ),
                                onPressed: () {
                                  if (listViewController
                                          .position.maxScrollExtent ==
                                      listViewController.offset) {
                                    print("on the end");
                                  } else {
                                    listViewController.animateTo(
                                        listViewController.offset +
                                            listViewController
                                                    .position.maxScrollExtent /
                                                7,
                                        duration: Duration(milliseconds: 500),
                                        curve: Curves.easeIn);
                                  }
                                },
                              )
                            ],
                          ),
                        ))
                  ],
                )
              : Column(
                  children: <Widget>[
                    SizedBox(height: template.viewHeight() * 7),
                    Container(
                        height: template.viewHeight() * 2,
                        width: wt,
                        child: Padding(
                          padding: EdgeInsets.only(left: (wt / 390) * 7),
                          child: Container(
                            color: template.boxColor(),
                          ),
                        )),
                    Container(
                        height: template.viewHeight() * 287,
                        width: wt,
                        child: Row(
                          children: <Widget>[
                            SizedBox(width: template.viewWidth() * 5),
                            Container(
                              height: template.viewHeight() * 287,
                              width: template.textContainerwidth(),
                              child: Padding(
                                padding: EdgeInsets.only(
                                    top: (ht / 300) * 2,
                                    left: (wt / 390) * 2.5,
                                    right: (wt / 390) * 2.5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: template.viewWidth() * 30,
                                      height: template.viewHeight() * 15,
                                      child: Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    fontSize = 50;
                                                    listView = true;
                                                  });
                                                },
                                                child: RotatedBox(
                                                  quarterTurns: 1,
                                                  child: Image.asset(
                                                    "assets/images/magnify.png",
                                                    height: template
                                                        .magnifyingGlass(),
                                                  ),
                                                )),
                                            left: -template.viewWidth() * 10,
                                          ),
                                        ],
                                        overflow: Overflow.visible,
                                      ),
                                    ),
                                    Container(
                                        height: template.viewHeight() * 260,
                                        width: template.textContainerwidth(),
                                        child: AutoSizeText.rich(
                                          TextSpan(
                                            children: [
                                              TextSpan(
                                                text: "Forward\n\n",
                                                style:
                                                    template.textheaderStyle(),
                                              ),
                                              TextSpan(
                                                text: "Not long after my book,",
                                                style: template.bodyTextStyle(),
                                              ),
                                              TextSpan(
                                                text:
                                                    "Slavery by Another Name: The Re-Enslavement of Black Americans from the Civil War to World War II,",
                                                style: template.bodyTextStyle(),
                                              ),
                                              TextSpan(
                                                text:
                                                    " was published in 2008, I received an unexpected call from Bernard Kinsey. He insisted that he was a direct relative of a woman whose poignant letter of 1903 to President Theodore Roosevelt appeared prominently in the book.\n\nThe letter, written by a then-young woman named Carrie Kinsey, described how her 14-year-old brother had been kidnapped and sold into slavery. It was the historical artifact that had haunted me more than any other in the eight-year process of writing my book, and I’d never been able to learn the fates of Carrie or James. In just a few desperate lines, written in the struggling hand of a barely literate, impoverished African American woman, was a story that distilled both the individual horror and the fantastic width and breadth of the American campaign to crush Southern blacks at the beginning of the 20th century. I later told Bernard that in the story of Carrie’s stolen brother James—his seizure and sale to a massive plantation owned by one of Georgia’s most powerful families, the abject refusal of authorities to assist her, the brutalization of thousands of other blacks on the plantation where he worked in chains, the heroism of Carrie Kinsey in seeking the aid of President Roosevelt, and finally in the futility of her never investigated letter—was the entire epic tragedy of black life in the rural South at that time. All the wrongs, all the valiance",
                                                style: template.bodyTextStyle(),
                                              )
                                            ],
                                          ),
                                          textAlign: TextAlign.justify,
                                        ))
                                  ],
                                ),
                              ),
                            ),
                            Container(
                                height: template.viewHeight() * 287,
                                width: template.textContainerwidth(),
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      top: (ht / 300) * 26,
                                      left: (wt / 390) * 2.5,
                                      right: (wt / 390) * 2.5),
                                  child: Text(
                                    textFiles[1],
                                    style: template.bodyTextStyle(),
                                    textAlign: TextAlign.justify,
                                  ),
                                )),
                            Container(
                                height: template.viewHeight() * 287,
                                width: template.textContainerwidth(),
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      top: (ht / 300) * 25,
                                      left: (wt / 390) * 2.5,
                                      right: (wt / 390) * 2.5),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        height: template.viewHeight() * 240,
                                        width: template.textContainerwidth(),
                                        child: Text(
                                          textFiles[2],
                                          textAlign: TextAlign.justify,
                                          style: template.bodyTextStyle(),
                                        ),
                                      ),
                                      Container(
                                        height: template.viewHeight() * 10,
                                        width: template.textContainerwidth(),
                                        child: Text(
                                          "— Douglas A. Blackmon",
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                            fontFamily: 'Auto2-Light',
                                            fontSize: (ht / 300) * 6,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: (ht / 300) * 2,
                                      ),
                                      Container(
                                          height: template
                                              .footerTextContainerHihght(),
                                          width: template.textContainerwidth(),
                                          child: template.footerRightText())
                                    ],
                                  ),
                                ))
                          ],
                        )),
                  ],
                )),
    ));
  }
}
