import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class AfterThoughtsTwo extends StatefulWidget {
  @override
  _AfterThoughtsTwoState createState() => _AfterThoughtsTwoState();
}

class _AfterThoughtsTwoState extends State<AfterThoughtsTwo> {
  final textFiles=["stature declined, until the Smithsonian Museum of American Art exhibited several of his paintings.\n\nBefore World War I, 80 percent of African Americans lived in the South in mostly rural areas. There was a large migration north after the war, which created new African American communities in Washington, D.C., Chicago, Philadelphia, Detroit, and New York City. In the 1920s the phrase “New Negro” was used to characterize the advancing social and cultural position of the African American people. In 1922 James V. Herring established the Art Department at Howard University in Washington, D.C., and Alma W. Thomas became its first graduate in 1924.\n\nDuring the mid-1920s, African American artists gained wide recognition as part of the movement called the Harlem Renaissance. These artists included Aaron Douglas, Archibald Motley, Hale Woodruff, Palmer Hayden, May Howard Jackson, William H. Johnson, Melvin Gray Johnson, Henry Bozeman Jones, Richmond Barthe, James A. Porter, and James Wells. The Harmon Foundation was also founded in New York in 1922, and Alain Locke, America’s first black Rhodes Scholar, helped the Harmon Foundation to select African American artists, who did not have access to exhibitions, for awards and scholarships. Controversy surrounded these shows, however, because recognizing the achievements of African American artists separated them from white artists. In the early 1930s, artist Charles Alston",
    "and James Wells started an art program in Harlem for youths, and one of its most celebrated students was Jacob Lawrence. Also in the 1930s, sculptor Augusta Savage started the first African American private gallery to be founded by a black person, and one of her first students was Norman Lewis. In 1940 Alain Locke and Alonzo Aden curated The Negro Exposition in Chicago in celebration of the seventy-fifth anniversary of the Thirteenth Amendment. Charles White won first prize for his painting There Would Be No Crops This Year. In 1943 James V. Herring and Alonzo Aden founded the Barnett-Aden Collection— not a black gallery, but it did give opportunities to African American artists who did not have the opportunity to exhibit their art. Herring and Aden placed their art alongside the works of Henry O. Tanner, Robert S. Duncanson, and Archibald Motley in these exhibitions. The artists who had their first solo exhibitions were I. Rice Pereria, Thedores Stamos, Lois M. Jones, Romare Bearden, Alma Thomas, and Charles White. Romare Bearden said that the first time he had seen the work of artist Henri Matisse in person anywhere in America was in the Barnett-Aden Gallery in the 1940s.\n\nIn 1947 Hale Woodruff, along with James V. Herring, artist Romare Bearden, and sculptor Richmond Barthe, persuaded IBM to remove all racial references in the catalog of their corporate art collection and to abolish all racial references in their art acquisition policies. Woodruff stated",
    "that there was no need for “all-negro exhibitions, which tend to isolate the African American artist and segregate him from the mainstream American artist.” Social realism in African American art fell out of demand in the 1950s; modern abstract art was what galleries and museums were favoring. Works of Norman Lewis and Alma Thomas became more widely accepted in museums and galleries. In 1971 Alma Thomas was the first African American woman to have a solo exhibition at the Whitney Museum in New York. In 2015 Norman Lewis had a major retrospective at the Philadelphia Fine Arts Academy, and Alma Thomas’s paintings were acquired by Crystal Bridges in Arkansas, the Museum of Modern Art in New York, The Kinsey Collection, and President Obama and the White House.\n\nPresident Lyndon Johnson signed legislation in 1965 establishing the National Endowment for the Arts, declaring that any “advanced civilization must fully value the arts, humanities, and cultural activity.” In order to have a truly democratic system for all artists in America, one that fully values the arts, galleries and museums should apply criteria equally: that works are to be valued solely for their quality, not on the basis of the artist’s gender or race. Because of racial prejudice in America, many gifted artists have been hidden in obscurity. We have come a long way, but we still have a long way to go.\n\n"
  ];
  static bool listView=false;
  ScrollController listViewController=ScrollController();
  static double fontSize=50;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
        child: listView?Stack(
          children: <Widget>[
            ListView.builder(
                controller: listViewController,
                itemCount: textFiles.length,
                itemBuilder: (BuildContext context,index){
                  if(index==0){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: "Early Genius\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                            TextSpan(text: textFiles[0])
                          ],

                        ),
                        presetFontSizes: [fontSize],
                        textAlign: TextAlign.justify,
                      ),
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: AutoSizeText(textFiles[index],minFontSize: fontSize,textAlign: TextAlign.justify),
                  );
                }),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height*0.1,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(onPressed: (){

                        if(listViewController.position.minScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset-listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },icon: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 40,),),
                      Row(children: <Widget>[
                        IconButton(icon: Icon(Icons.zoom_out,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            fontSize=30;
                          });
                        }),
                        IconButton(icon: Icon(Icons.close,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            listView=false;
                          });
                        })
                      ],),
                      IconButton(icon: Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 40,),onPressed: (){
                        if(listViewController.position.maxScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },)
                    ],),
                ))
          ],
        ):Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 30,),
                  Expanded(child: AutoSizeText(textFiles[0],presetFontSizes: [22,20,18,16,14],textAlign: TextAlign.justify,))
                ],
              ),
            ),),
            Expanded(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[1],presetFontSizes: [22,18,16,14],textAlign: TextAlign.justify,),
            )),
            Expanded(child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Flexible(
                      child: AutoSizeText(textFiles[2],presetFontSizes: [20,16,14],textAlign: TextAlign.justify,)),
                  AutoSizeText.rich(
                    TextSpan(text: "— Ed Pratt"),
                    textAlign: TextAlign.right,presetFontSizes: [20,16,14]
                  ),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
