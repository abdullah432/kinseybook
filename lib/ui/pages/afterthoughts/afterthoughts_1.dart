import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class AfterThoughtsOne extends StatefulWidget {
  @override
  _AfterThoughtsOneState createState() => _AfterThoughtsOneState();
}

class _AfterThoughtsOneState extends State<AfterThoughtsOne> {
  final textFiles=["The culture of black people, who were transported across the vast waters of the Atlantic Ocean in the holds of slave ships, did not die when they reached the shores of America. It is therefore important that African Americans in America revere their past and form connections to it. Art is the spiritual utterance of a people. Beginning with our primary duty as a people to know ourselves, what follows is that our artistic traditions will become more important to us. The objective of the Kinsey Collection is to encourage the development of a true American art, where all people are fairly represented with respect and dignity. We must acknowledge that all cultures are interrelated and that they will nourish one another.\n\nThe Kinsey Collection has a reason for being: it is to mirror and reflect the total African American artistic experience in America. African American artists overcame overwhelming odds to achieve success. They faced constant social, mental, and economic struggles. The art and accomplishments of these artists have been lost from our nation’s history, and this collection will help give their achievements their rightful place in America’s historical record.",
    "⁂ James A. Porter graduated from Howard University’s art school in 1924. Known as the father of African American art history, he also was an acclaimed artist, the winner of a Harmon Foundation gold medal for A Woman and Her Jug in 1933. Porter found himself at odds, however, with the famous African American philosopher Alain Locke, and in 1937 published “Art Front,” an attack on Locke. At issue was whether African American artists should consider themselves Americans first or look toward the heritage of African art, as Locke advocated. In Porter’s opinion, Locke was advancing the “defeatist philosophy of the Segregationist.” In 1943 Porter published “Modern Negro Art,” the first comprehensive history of African American art in America. His mentor, James V. Herring, had taught art at Howard University for over forty years. In an article on the Civil War period, Porter encountered artist Robert S. Duncanson, and he began to investigate the neglected story of this artist. Research into other overlooked artists and artisans became the basis for his master’s thesis. He published an article in Art in America in 1953, the first systematic study of these artists to appear in a leading art magazine. This article rescued Duncanson and other African American artists from obscurity.\n\nIn the 1860s Robert S. Duncanson arrived at the home of Alfred, Lord Tennyson, England’s poet laureate. He brought with him his most celebrated painting, Land of the Lotus Eaters, based on",
    "Tennyson’s poem of that name. Tennyson was very impressed with the painting and proclaimed it “the land in which one loves to wander and linger.” In a review of the painting in the Cincinnati Gazette, the writer introduced Duncanson as the greatest landscape painter in the West.\n\nIn 1876, when Edward Mitchell Bannister won the Bronze Medal at the Philadelphia Centennial for his prize winning painting Under the Oaks, he was asked to leave the line of award-winners. Not only was it inconceivable that a black man could be a painter, it was absolutely impossible that he could be one of the leading artists of that time.\n\nHenry O. Tanner began two years of formal study under Thomas Eakins at the Philadelphia Academy of Fine Arts in 1888—the only black student in the school. He left after only two years, when the other students tied him up in a mock crucifixion and left him in the middle of Broad Street. In 1891 he went to Paris to further his studies, and in 1897 his painting The Raising of Lazarus won a medal at the Paris Salon, a rare feat for an American artist. Later that year the French government purchased the painting. When the editor of a Baltimore newspaper heard this story, he wanted to report on this rare award for an American artist, and he did not have a photo of Tanner to run with the story. He instructed the staff photographer to go down to the docks and take a photo of any black man to substitute for one of Tanner. After his death in 1937, Tanner’s"
  ];
  static bool listView=false;
  ScrollController listViewController=ScrollController();
  static double fontSize=50;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
        child: listView?Stack(
          children: <Widget>[
            ListView.builder(
                controller: listViewController,
                itemCount: textFiles.length,
                itemBuilder: (BuildContext context,index){
                  if(index==0){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: "Afterthoughts: The Tradition of African American Art\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                            TextSpan(text: textFiles[0])
                          ],

                        ),
                        presetFontSizes: [fontSize],
                        textAlign: TextAlign.justify,
                      ),
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: AutoSizeText(textFiles[index],minFontSize: fontSize,textAlign: TextAlign.justify),
                  );
                }),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height*0.1,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(onPressed: (){

                        if(listViewController.position.minScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset-listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },icon: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 40,),),
                      Row(children: <Widget>[
                        IconButton(icon: Icon(Icons.zoom_out,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            fontSize=30;
                          });
                        }),
                        IconButton(icon: Icon(Icons.close,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            listView=false;
                          });
                        })
                      ],),
                      IconButton(icon: Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 40,),onPressed: (){
                        if(listViewController.position.maxScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },)
                    ],),
                ))
          ],
        ):Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 30,),
                  Container(width:100,height: 50,child: Stack(children: <Widget>[Positioned(child: GestureDetector(
                      onTap: (){
                        setState(() {
                          fontSize=50;
                          listView=true;
                        });
                      },
                      child: Image.asset("assets/images/magnify.png",scale: 4,)),left: -30,),],overflow: Overflow.visible,)),
                  AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(text: "Afterthoughts: The Tradition of African American Art\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                      ],

                    ),
                    textAlign: TextAlign.justify,
                  ),
                  Expanded(child: AutoSizeText(textFiles[0],presetFontSizes: [22,16,14],textAlign: TextAlign.justify,))
                ],
              ),
            ),),
            Expanded(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[1],presetFontSizes: [22,18,16,14],textAlign: TextAlign.justify,),
            )),
            Expanded(child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: AutoSizeText(textFiles[2],presetFontSizes: [20,16,14],textAlign: TextAlign.justify,)),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
