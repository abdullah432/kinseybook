import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageFourteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/lineage14_1.jpg",
                                      )));
                        },
                        child: Image.asset("assets/images/lineage14_1.jpg")),
                  ),
                  Expanded(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => GalleryView(
                                              url:
                                                  "assets/images/lineage14_2.jpg",
                                            )));
                              },
                              child:
                                  Image.asset("assets/images/lineage14_2.jpg")),
                        ),
                        Expanded(
                          child: Column(
                            children: <Widget>[
                              Text(
                                "The History of the Rise, Progress, and",
                                style: template.textheaderStyle(),
                                maxLines: 1,
                              ),
                              Text(
                                "Accomplishment of the Abolition of the",
                                style: template.textheaderStyle(),
                                maxLines: 1,
                              ),
                              Text(
                                "African Slave-Trade by the",
                                style: template.textheaderStyle(),
                                maxLines: 1,
                              ),
                              Text(
                                "British Parliament, 1808",
                                style: template.textheaderStyle(),
                                maxLines: 1,
                              ),
                              Text(
                                "Thomas Clarkson, M.A.",
                                style: template.bodyTextStyle(),
                                maxLines: 1,
                              ),
                              Text(
                                "Book ( 2 volumes )",
                                style: template.bodyTextStyle(),
                                maxLines: 1,
                              ),
                              Text(
                                "with 2 (one in each volume)",
                                style: template.bodyTextStyle(),
                                maxLines: 1,
                              ),
                              Text(
                                "bound and folded nine panel diagrams",
                                style: template.bodyTextStyle(),
                                maxLines: 1,
                              ),
                              Text(
                                "83⁄4 x 51⁄2 in.",
                                style: template.bodyTextStyle(),
                                maxLines: 1,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "Thomas Clarkson (1760–1846) born in Wisbech, England, became a leading campaigner against the slave trade in the British Empire. Allied with William Wilberforce, Granville Sharp and Josiah Westwood, he was a tireless abolitionist. He helped form the Committee for the Abolition of the Slave Trade in 1787. In 1789 Clarkson was able to promote the Committee’s cause by encouraging the sale of the recently published autobiography by the African Olaudah Equiano. Equiano’s first-hand account of the slave trade, slavery abroad, and the horrors of the “Middle Passage” demonstrated both literary skill and an unanswerable case against slavery that helped capture the public consciousness.\n\nIn three further decades, the slave trade was eliminated in the British Empire and slavery was completely abolished there in 1833. Not until 1865 was slavery ended in America, after what is still the bloodiest war in our history.\n\nClarkson’s book revealed the inhumane treatment Africans suffered in the Transatlantic trade. The diagram shows in dramatic detail the virtual torture that prevailed aboard slave trade ships. Clarkson traveled 35,000 miles on horseback, as he interviewed over 20,000 sailors, seeking the evidence that was to doom slavery and lecturing on its evils.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ),
                  GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GalleryView(
                                      url: "assets/images/lineage14_3.jpg",
                                    )));
                      },
                      child: Image.asset("assets/images/lineage14_3.jpg"))
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
