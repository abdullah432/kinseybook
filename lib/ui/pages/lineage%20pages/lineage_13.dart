import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageThirteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    "Mexican Slave Trade Bill of Sale, 1790",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Jose Maria Montano",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Ink on paper ( 4 pages)",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "12 x 81⁄2 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Expanded(
                    child: Text(
                      "A scarce, early contract of sale in New Spain’s slave trade, in which a “mulatto” slave named Jose Maria Montano is sold for sixty pesos.\n\nSince 1492 the history of the Americas has been forged by three cultures: Indigenous, European, and African. It was Africans who established the third root of the Americas, according to Gonzalo Aquirre Beltrán of the University of Veracruz, author of The Black Population in Mexico (1964). The first pioneer from Europe to explore what would become the entire southern tier of the United States was an African named Esteban Dorantes (1500–1539). Blacks in the Americas were much more than slaves—they were explorers and co-founders of settlements as far north as Los Angeles, where twenty-six of the forty-four founders were of African descent. The second president of Mexico, Vicente Guerrero, was of African ancestry. He officially abolished slavery in 1822.\n\nAlong the Atlantic Coast of the Americas today, 25 percent of the Spanish and Portuguese speaking Latino population is of African descent—a reminder for both communities of their similarities, and for all of us, that racial categorizing does not represent the true facts of human history.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/lineage13.jpg",
                              )));
                },
                child: Image.asset("assets/images/lineage13.jpg")),
          ),
        ],
      ),
    ));
  }
}
