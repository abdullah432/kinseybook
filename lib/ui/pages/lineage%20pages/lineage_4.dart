import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        children: <Widget>[
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/lineage4.jpg",
                                      )));
                        },
                        child: Image.asset("assets/images/lineage4.jpg")),
                  ),
                  Text(
                    "Baptismal Document, Archives of the",
                    style: template.textheaderStyle(),
                  ),
                  Text(
                    "Diocese of St. Augustine,1595",
                    style: template.textheaderStyle(),
                  ),
                  Text(
                    "Handwritten on paper",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "10 x 8 in.",
                    style: template.bodyTextStyle(),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Translation:",
                    style: template.bodyTextStyle(),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "[margin:]",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "Estebana",
                        style: template.bodyTextStyle(),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "On the fifth day of January, (15)95, I, Father Marron, priest and vicar of this Holy Church, baptized Estebana. She is the daughter of Gratia, slave of [broken]-talita. This being true, I sign my name, Friar Francisco de Marron",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.center,
                      maxLines: 3,
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Expanded(
                        child: Text(
                      "of St. Augustine, rediscovered them and began negotiations to obtain them in 1871. Not until 1906 were the majority returned; the last volume, in Spanish, “Book of Spanish Baptisms of the City of St. Augustine,” was finally returned in 1939. Later in 1939, the documents were sent to the National Archives for preservation, each page encapsulated in transparent cellulose-acetate material. In 1942, in anticipation of German U-boat shelling off St. Augustine, the archives were shipped to the University of Notre Dame. They were rediscovered in 1961 in a library attic, and again returned to St. Augustine. At first stored in a bank vault, the archives were later found stacked in a hallway opposite the boiler room in the cathedral parish rectory. Not until 2009 were the archives made safe in a new facility that is waterproof, fireproof, and climate-controlled.\n\nThe institution of slavery in Spain, which became part of Spanish Florida’s history, was very different from its counterpart in the American antebellum South. In Spain, not all slaves were black and not all blacks were slaves. You could be enslaved if captured in a “just war,” or if you committed certain crimes—regardless of race. You could also sell yourself into slavery. And there were many free blacks in Spain: some came to trade and do business, others purchased their freedom, and still others were freed when their masters died. One significant example is Juan Garrido, a free black man from West Africa,",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    )),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              )),
        ],
      ),
    ));
  }
}
