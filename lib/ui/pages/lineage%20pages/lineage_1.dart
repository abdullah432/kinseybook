// TODO Implement this library.
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageOne extends StatefulWidget {
  @override
  _LineageOneState createState() => _LineageOneState();
}

class _LineageOneState extends State<LineageOne> {
  final textFiles = [
    "\nThe Bernard and Shirley Kinsey Collection contains a wide range of art and artifacts that bear witness to the multi-layered intersections of the past and present. From a curatorial perspective, the correlations of seemingly disparate objects and their historical moments yield a holistic understanding of the Kinseys’ mission. In aggregate these intersections, though sometimes confrontational, become a celebration of the diversity of African and African American experience in the world.\n\nYet the artifacts represented in this volume tell a story that is far from comforting. The context of each piece is complex, shedding light on the difficulties of being black but also illuminating the brilliance of exceptional people whose contributions add enduring force to the often undervalued collective history of a people. The Kinseys themselves are included in this oeuvre",
    "because their vision supports this agenda: amassing objects that can be juxtaposed, sometimes in surprising ways, in order to illuminate the past.\n\nThe Kinseys value roots. Florida is the beginning of their personal stories, where they were raised, met, and began their journey together. The beginning of their cultural cosmology, however, lies in Africa, in the extraordinary stories of men and women who overcame the harsh realities of discrimination and slavery to prosper, or simply to survive. The Kinseys are determined to preserve the earliest documentations of the collision between Africans and those who sought to possess them.\n\nDuring much of modern American history, people were taught that African slaves were from a “dark” continent and lacked both technical expertise and culture. But these myths, created to ensure subservience and racial stratification, could not be further from the truth. The same myths served to misrepresent the true impact of the vast numbers of Africans brought from the continent to Europe and the New World.\n\nBernard and Shirley Kinsey collect ephemera,",
    "including books, ledgers, accounts, and artifacts used in the transatlantic slave trade. These provide tangible evidence of both the single- mindedness of the European imperialist agenda and the intellectual acuity of the unfortunate Africans who were victims of that brutal agenda. The African and European artifacts shown here exemplify an intersection of worlds at the historical moments that brought black people to the shores of Europe and the New World, where they were considered assets and commodities to be counted, traded, and sold.\n\nAfrican men and women, however, understood and accommodated the complex and often hostile world around them, in ways that allowed them to produce and prosper. Their work provides telling evidence of their dedication to the pursuit of freedom and knowledge, and examples of an extraordinary aesthetics that captivated those who sought to capture and control them.\n\nThe Kinseys’ personal journey began over a century after the official cessation of the transatlantic slave trade, but the history of the legacy of courage in the face of disenfranchisement begins here."
  ];

  bool listView = false;

  ScrollController listViewController = ScrollController();

  double fontSize = 50;

  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: listView
            ? Stack(
                children: <Widget>[
                  ListView.builder(
                      controller: listViewController,
                      itemCount: textFiles.length,
                      itemBuilder: (BuildContext context, index) {
                        if (index == 0) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Lineage",
                                  style: template.textheaderStyle(),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "History matters. It provides our identity, it structures our relationships, and it defines\nthe terms of our debates.\n",
                                  textAlign: TextAlign.justify,
                                ),
                                Container(
                                    width: double.infinity,
                                    child: Text(
                                      "— Dr. James Oliver Horton",
                                      textAlign: TextAlign.right,
                                    )),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(textFiles[index],
                                    textAlign: TextAlign.justify)
                              ],
                            ),
                          );
                        }
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          child: Text(textFiles[index],
                              textAlign: TextAlign.justify),
                        );
                      }),
                  Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        color: Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            IconButton(
                              onPressed: () {
                                if (listViewController
                                        .position.minScrollExtent ==
                                    listViewController.offset) {
                                  print("on the end");
                                } else {
                                  listViewController.animateTo(
                                      listViewController.offset -
                                          listViewController
                                                  .position.maxScrollExtent /
                                              7,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.easeIn);
                                }
                              },
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.blue,
                                size: 40,
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                IconButton(
                                    icon: Icon(
                                      Icons.zoom_out,
                                      color: Colors.blue,
                                      size: 40,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        fontSize = 30;
                                      });
                                    }),
                                IconButton(
                                    icon: Icon(
                                      Icons.close,
                                      color: Colors.blue,
                                      size: 40,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        listView = false;
                                      });
                                    })
                              ],
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.blue,
                                size: 40,
                              ),
                              onPressed: () {
                                if (listViewController
                                        .position.maxScrollExtent ==
                                    listViewController.offset) {
                                  print("on the end");
                                } else {
                                  listViewController.animateTo(
                                      listViewController.offset +
                                          listViewController
                                                  .position.maxScrollExtent /
                                              7,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.easeIn);
                                }
                              },
                            )
                          ],
                        ),
                      ))
                ],
              )
            : Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              width: 100,
                              height: 50,
                              child: Stack(
                                children: <Widget>[
                                  Positioned(
                                    child: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            fontSize = 50;
                                            listView = true;
                                          });
                                        },
                                        child: Image.asset(
                                          "assets/images/magnify.png",
                                          scale: 4,
                                        )),
                                    left: -30,
                                  ),
                                ],
                                overflow: Overflow.visible,
                              )),
                          Text(
                            "Lineage",
                            style: template.bodyTextStyle(),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Flexible(
                              flex: 1,
                              child: Text(
                                "History matters. It provides our identity, it structures our relationships, and it defines\nthe terms of our debates.\n",
                                textAlign: TextAlign.justify,
                                style: template.bodyTextStyle(),
                              )),
                          Container(
                              width: double.infinity,
                              child: Text(
                                "— Dr. James Oliver Horton",
                                textAlign: TextAlign.right,
                                style: template.bodyTextStyle(),
                              )),
                          Flexible(
                              flex: 5,
                              child: Text(
                                textFiles[0],
                                textAlign: TextAlign.justify,
                                style: template.bodyTextStyle(),
                              ))
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      textFiles[1],
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  )),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      textFiles[2],
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ))
                ],
              ),
      ),
    );
  }
}
