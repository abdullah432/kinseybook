import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageEight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    "Ioannis Leonis Africani Africae, 1632",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "De totius Africae descriptione libri IX",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Book",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "4 3⁄4 x 3 x 2 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    child: Text(
                      "Al Hassan ibn Mohammed al Wazzan al Fassi was born in 1485 in Granada, Spain. He returned with his parents to their native Morocco and studied at the University of Al-Karaouine, founded in Fez in 859 A.D. In 1518, returning to Morocco from a diplomatic venture, he was captured by Spanish Corsairs and taken to Rome, where he was received and baptized by Pope Leo X and renamed Leo Africanus. He was freed when his captors realized he was from a prominent diplomatic family and could be a useful ally. Africanus traveled extensively throughout Italy for several years, then returned to Rome under the protection of Pope Clement VII. In 1526, he completed Ioannis Leonis Africani Africae, detailing African geography, with much of the information gathered from his studies at university and his interaction with African travelers in Egypt.The book is considered the first descriptive work on Africa authored by a person of African descent. Popular long after its original publication, it was translated in the early seventeenth century from Arabic into several languages, including Latin and French. This edition, of 1632, is in Latin.",
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  Container(
                    height: (ht / 300) * 15,
                    width: template.textContainerwidth(),
                    child: FittedBox(
                      child: AudioViewer(url: "assets/audio/lineage8.mp3"),
                      fit: BoxFit.contain,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/lineage8_1.jpg",
                                )));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    width: MediaQuery.of(context).size.width * 0.2,
                    child: FittedBox(
                      fit: BoxFit.fill,
                      child: Image.asset(
                        "assets/images/lineage8_1.jpg",
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                Text(
                  "Not pictured, but in the\nKinsey Collection, is an\nEnglish language version\npublished in 1600",
                  style: template.bodyTextStyle(),
                )
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/lineage8_2.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/lineage8_2.jpg",
                    fit: BoxFit.fill,
                  )),
            ),
          )
        ],
      ),
    ));
  }
}
