import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageSixteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/lineage16.jpg",
                              )));
                },
                child: Image.asset("assets/images/lineage16.jpg")),
          ),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Text(
                  "Accounts Presented to the",
                  style: template.textheaderStyle(),
                  maxLines: 1,
                ),
                Text(
                  "House of Commons, 1806",
                  style: template.textheaderStyle(),
                ),
                Text(
                  "William Irving, Inspector General of the Imports",
                  style: template.bodyTextStyle(),
                  maxLines: 1,
                ),
                Text(
                  "and Exports of Great Britain",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "Printed paper",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "13 x 8 in.",
                  style: template.bodyTextStyle(),
                ),
                Flexible(
                  child: Text(
                    "The African slave trade was a lucrative business that sustained the power of the British Empire into the 19th century. Yet the headlong rush of British businessmen into international trade was regulated by Parliament—ironically, to ensure that the wealth garnered by the sale of black slaves was fairly shared. Accounts Presented to the House of Commons confirms that ships could carry well over 3.8 million slaves during a ten-year period, from 1796 to 1805—shattering the myth that during the three centuries of transatlantic slave trading, only ten to twelve million Africans were transported to the New World. In one year alone, 149 ships transported 53,021 slaves from the ports along the west coast of Africa to the British West Indies.",
                    textAlign: TextAlign.justify,
                    style: template.bodyTextStyle(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
