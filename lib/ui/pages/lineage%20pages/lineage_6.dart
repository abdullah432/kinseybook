import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageSix extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/lineage6.jpg",
                              )));
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
//                height: MediaQuery.of(context).size.height*0.5,
                  child: FittedBox(
                    fit: BoxFit.fill,
                    child: Image.asset("assets/images/lineage6.jpg"),
                  ),
                ),
              ),
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Gorée Island Rock, n.d.",
                  style: template.textheaderStyle(),
                ),
                Text(
                  "Gift of Ed Dwight to Bernard and",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "Shirley Kinsey",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "3 x 10 x 6 1⁄2 in.",
                  style: template.bodyTextStyle(),
                ),
              ],
            ),
            Expanded(
              flex: 2,
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      "This rock was a gift to the Kinseys from an artist who traveled to Gorée, a small island located off the coast of Senegal in western Africa. Although the island was only a minor slave port, it has become famous for La Maison des Esclaves (The Slave House), constructed in 1786 by French African Creoles. The house continues to stand as a testament to the history of the slave trade",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  )),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      "in the region. Now a museum displaying slavery artifacts, it contains the “Door of No Return,” an example of the portal through which millions of slaves left African shores bound for a life of servitude in the New World. Bernard traces the family’s ancestors to Senegal and this rock represents the passage of those ancestors to the Americas.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  )),
                  Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: IconButton(
                        icon: Icon(
                          Icons.play_circle_outline,
                          color: Colors.black,
                        ),
                        iconSize: 80,
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VideoView(
                                        videoLink: "assets/videos/lineage6.MP4",
                                      )));
                        },
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
