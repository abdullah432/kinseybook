import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageTwelve extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => GalleryView(
                              url: "assets/images/lineage12.jpg",
                            )));
              },
              child: Container(
                  height: MediaQuery.of(context).size.height * 0.8,
                  width: MediaQuery.of(context).size.width * 0.6,
                  child: FittedBox(
                      fit: BoxFit.fill,
                      child: Image.asset("assets/images/lineage12.jpg"))),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Text(
                  "The Interesting Narrative of the Life of",
                  style: template.textheaderStyle(),
                  maxLines: 1,
                ),
                Text(
                  "Olaudah Equiano, or Gustavus Vassa,",
                  style: template.textheaderStyle(),
                  maxLines: 1,
                ),
                Text(
                  "The African, 1789",
                  style: template.textheaderStyle(),
                ),
                Text(
                  "Olaudah Equiano",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "Book",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "7 1⁄4 x 4 1⁄4 x 1 1⁄4 in.",
                  style: template.bodyTextStyle(),
                ),
                Expanded(
                  child: Text(
                    "A contemporary of Sancho, Olaudah Equiano was captured by African slavers near his home on the Niger River and sold into the trans-atlantic slave trade. He was purchased by an officer in the British Royal Navy, who taught him seamanship and navigation. Later, Equiano was sold to a Quaker who helped him become more literate and allowed him to buy his freedom. Equiano moved to England to work with William Wilberforce in the abolitionist movement. To promote the cause he wrote of his life as a slave in The Interesting Narrative of the Life of Olaudah Equiano, or Gustavus Vassa, the African. This work is one of the few first- person narratives documenting the life of those destined for slavery on the transatlantic slave routes. The book was so popular that six editions quickly followed the initial publication. This book was purchased from Ann Willis, the widow of John Willis, a professor at Princeton, whom Bernard Kinsey never had the chance to meet in person.",
                    textAlign: TextAlign.justify,
                    style: template.bodyTextStyle(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
