// TODO Implement this library.
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageTen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/lineage10_1.jpg",
                                      )));
                        },
                        child: Image.asset("assets/images/lineage10_1.jpg")),
                  ),
                  Text(
                    "Letters of the Late Ignatius Sancho,",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "an African, in Two Volumes, 1782",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Ignatius Sancho",
                    style: template.bodyTextStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Book",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "8 x 5 1⁄2 x 2 in.",
                    style: template.bodyTextStyle(),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      flex: 2,
                      child: Center(
                          child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30, vertical: 20),
                        child: Text(
                          "Ignatius Sancho (1729–1780) was born a slave on a ship crossing the Atlantic from Africa to the West Indies. He was taken to Greenwich, England, where he became a butler to the Montagu family, eventually retiring to run a grocery in Westminster. He acted in several plays on the London stage, and is thought to have performed in Othello, making him the first black actor to play the Moorish king.\n\nSancho also composed music and wrote poems and essays, posthumously published in this volume in 1782. The engraved frontispiece is after the portrait of Sancho painted by Thomas Gainsborough. Sancho, known as “The Extra- ordinary Negro,” became a symbol of the humanity of Africans to 18th-century British abolitionists. He was also the first African to vote in a British election.",
                          textAlign: TextAlign.justify,
                          style: template.bodyTextStyle(),
                        ),
                      ))),
                  Expanded(
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url: "assets/images/lineage10_2.jpg",
                                        )));
                          },
                          child: Image.asset("assets/images/lineage10_2.jpg")))
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
