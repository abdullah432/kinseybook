import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageNine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
            height: ht,
            width: wt,
            decoration: template.viewportBorder(),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        "Treaty of Utrecht, 1714",
                        style: template.textheaderStyle(),
                        maxLines: 1,
                      ),
                      Text(
                        "John Baskett, printer",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "Excerpt from book",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "8 3⁄4 x 7 in.",
                        style: template.bodyTextStyle(),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30, vertical: 10),
                        child: Text(
                          "The Treaties of Utrecht ended the War of Spanish Succession (1701–13) and divided the spoils among Europe’s monarchies, following the détente between the English and the French. This treaty grants the United Kingdom a 30-year asiento to supply slaves to the Spanish colonies. France would ultimately gain the coveted contract; however, her government, born of a bloody revolution, would in 1794 become the first to declare an official end to slavery and the slave trade.",
                          textAlign: TextAlign.justify,
                          style: template.bodyTextStyle(),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/lineage_9.jpg",
                                  )));
                    },
                    child: Container(
                        height: MediaQuery.of(context).size.height * 0.8,
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: FittedBox(
                            fit: BoxFit.contain,
                            child: Image.asset("assets/images/lineage_9.jpg"))),
                  ),
                )
              ],
            )));
  }
}
