import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageEleven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                    flex: 1,
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 30, left: 30, right: 30),
                      child: Text(
                        "Born in Gambia around 1753, Phillis Wheatley was sold into slavery and transported to the New World on the slave ship Phillis, after which she was named. In Boston, she was purchased by John Wheatley, a merchant. Wheatley and his wife Susanna educated Phillis and she excelled under their tutelage. She mastered Latin and Greek as well as history and geog-raphy, and began writing poetry when she was 13 years old. In 1768, she wrote “To the King’s Most Excellent Majesty,” dedicated to King George III on the occasion of the repeal of the Stamp Act. Her poems were well received in Boston and London, and she was encouraged by the Countess of Huntingdon to publish a compilation of her work. Many doubted Wheatley’s abilities, however, and she had to endure inspection by Boston notables, including John Hancock. In 1773, she finally received her due as a poet, earning recognition from George Washington and Benjamin Franklin.\n\nWheatley, the “first published African American woman poet, is regarded as a founding figure of black literature. Her portrait printed in the book is the only surviving work by the African American slave artist Scipio Moorhead.",
                        textAlign: TextAlign.justify,
                        style: template.bodyTextStyle(),
                      ),
                    )),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    icon: Icon(
                      Icons.play_circle_outline,
                      color: Colors.black,
                    ),
                    iconSize: 80,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => VideoView(
                                    videoLink: "assets/videos/lineage11.MP4",
                                  )));
                    },
                  ),
                ),
                AudioViewer(url: "assets/audio/lineage11.m4a")
              ],
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/lineage11.jpg",
                                      )));
                        },
                        child: Image.asset("assets/images/lineage11.jpg")),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Poems on Various Subjects",
                          style: template.textheaderStyle(),
                          maxLines: 1,
                        ),
                        Text(
                          "Religious and Moral, 1773",
                          style: template.textheaderStyle(),
                          maxLines: 1,
                        ),
                        Text(
                          "Phillis Wheatley",
                          style: template.bodyTextStyle(),
                          maxLines: 1,
                        ),
                        Text(
                          "Book",
                          style: template.bodyTextStyle(),
                          maxLines: 1,
                        ),
                        Text(
                          "7 3⁄4 x 5 1⁄4 x 1 1⁄2 in.",
                          style: template.bodyTextStyle(),
                          maxLines: 1,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
