import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageTwo extends StatefulWidget {
  @override
  _LineageTwo createState() => _LineageTwo();
}

class _LineageTwo extends State<LineageTwo> {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/lineage2.jpg",
                                  )));
                    },
                    child: Container(
                      height: (ht / 100) * 99,
                      width: (wt / 100) * 99,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: ht / 15, horizontal: wt / 40),

                        // fit: BoxFit.fill,
                        child: Image.asset("assets/images/lineage2.jpg"),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Text(
                        "Door of No Return,2007",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Auto2-Bold',
                          fontSize: ht / 25,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                      Text(
                        "Courtesy of documentary",
                        style: TextStyle(
                            fontSize: ht / 25, fontFamily: 'Auto2-Light'),
                      ),
                      Text(
                        "photographer Kwesi Hutchful",
                        style: TextStyle(
                            fontSize: ht / 25, fontFamily: 'Auto2-Light'),
                      ),
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
