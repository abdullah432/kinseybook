import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';


class HeartAndSoulOne extends StatefulWidget {
  @override
  _HeartAndSoulOneState createState() => _HeartAndSoulOneState();
}

class _HeartAndSoulOneState extends State<HeartAndSoulOne> {
  final textFiles=["\nThe period from 1875 to 1910 saw enormous change in America. America’s population doubled in a single generation, from 1875 to 1900. Black Congressmen and Senators were elected to the 41st and 42nd Congresses and were instrumental in passing the 1875 Civil Rights Bill, which guaranteed equal access to public accommodations, including inns,transportation, and entertainment venues. The 1883 ruling by the Supreme Court deemed the act unconstitutional, however, arguing that Congress had no power to regulate the conduct of individuals. This ruling led to the withdrawal of federal troops from the south, effectively re-enslaving African Americans. The Supreme Court ruling Plessy v. Ferguson established in 1896 the doctrine of “Separate but Equal,” which further oppressed African Americans. The Ku Klux Klan and the Chain Gang System thrived in the South through to the Civil Rights laws of the 1960s.\n\nDespite these nearly insurmountable obstacles to achievement, African Americans made landmark contributions to American life in the",
    "fields of science, medicine, literature, sports, education, arts, entertainment, manufacturing, and law. Some were great inventors. There were nearly fifty thousand professional positions held by African Americans in the U.S. even by the end of the nineteenth century, including 21,000 teachers, 15,000 clergymen, 1,700 physicians, and nearly 1,000 lawyers. Distinguished and influential figures include Hiram Rhodes Revels, W.E.B. Du Bois, George Washington Carver, Booker T. Washington, Carter J. Woodson, Alain Locke, Marcus Garvey, Paul Robeson, Mathew Henson, Paul Laurence Dunbar, James Weldon Johnson, Fredrick Douglas Patterson (the automobile manufacturer), Dr. Rebecca Lee Crumpler, Charlotte Ray, Dr. James Durham, Edward Bannister (an artist), and George Franklin Grant (a dentist). Among athletes were the Kentucky Derby winner Oliver Lewis, baseball pioneer Moses Fleetwood Walker, world champion boxer George Dixon, and world champion cyclist Marshall “Major”Taylor.\n\nAfrican Americans were breaking new ground in professional sports and quickly dominated the fields of horse racing, cycling, and boxing. In William Rhoden’s book, Forty Million Dollar Slaves, he identifies accomplished men who died in obscurity, despite excellence and achievement in sports, even during the worst Jim Crow times around the turn of the twentieth century. The “Jockey Syndrome,” which changed rules retroactively, suppressed African American dominance in horse racing, and later spread",
    "to baseball, cycling, boxing, football, and basketball. Blacks have continued to participate in professions such as sports, the military, and entertainment, where there are perhaps the clearest measures of success. There is a level playing field—a 100-yard dash is a 100-yard dash for all runners.\n\nThe winning rider at the first Kentucky Derby in 1875 was Oliver Lewis, a black man; in that race, thirteen of the fifteen riders were black. Isaac Murphy rode and won every major stake race for the next fifteen years. His income ranged above \$20,000 per year when the average worker’s annual salary was about \$500. He died in obscurity in 1891, at only thirty-five. Jimmy Winkfield won the 1901 and 1902 Kentucky Derby races, but was chased out of racing by the Jockey Syndrome. He was invited to eastern Europe to race and became a wealthy star. He later moved to France, bought a farm, and ran a successful stable business. He was invited back to the Kentucky Derby in 1960 to be honored for his earlier wins, but he was refused entrance to the hotel.\n\nThere were many African Americans who, while living under great oppression during this period, led lives of pride and dignity. They worked hard, raised families, and remained optimistic about their future. In this chapter, “Hearts and Souls,” we honor them with the eloquent images of their faces, though their names are lost to history."
  ];
  static bool listView=false;
  ScrollController listViewController=ScrollController();
  static double fontSize=50;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
        child: listView?Stack(
          children: <Widget>[
            ListView.builder(
                controller: listViewController,
                itemCount: textFiles.length,
                itemBuilder: (BuildContext context,index){
                  if(index==0){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: "Hearts and Souls\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                            TextSpan(text: textFiles[0])
                          ],

                        ),
                        presetFontSizes: [fontSize],
                        textAlign: TextAlign.justify,
                      ),
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: AutoSizeText(textFiles[index],minFontSize: fontSize,textAlign: TextAlign.justify),
                  );
                }),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height*0.1,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(onPressed: (){

                        if(listViewController.position.minScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset-listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },icon: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 40,),),
                      Row(children: <Widget>[
                        IconButton(icon: Icon(Icons.zoom_out,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            fontSize=30;
                          });
                        }),
                        IconButton(icon: Icon(Icons.close,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            listView=false;
                          });
                        })
                      ],),
                      IconButton(icon: Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 40,),onPressed: (){
                        if(listViewController.position.maxScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },)
                    ],),
                ))
          ],
        ):Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(width:100,height: 50,child: Stack(children: <Widget>[Positioned(child: GestureDetector(
                      onTap: (){
                        setState(() {
                          fontSize=50;
                          listView=true;
                        });
                      },
                      child: Image.asset("assets/images/magnify.png",scale: 4,)),left: -30,),],overflow: Overflow.visible,)),
                  AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(text: "Hearts and Souls\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                      ],

                    ),
                    textAlign: TextAlign.justify,
                  ),
                 Column(
                   crossAxisAlignment: CrossAxisAlignment.end,
                   children: <Widget>[
                     AutoSizeText("“I am an invisible man... of substance, of flesh",presetFontSizes: [22,21,20,19,18,17,16,14],maxLines: 1,textAlign: TextAlign.right,),
                     AutoSizeText("and bone, fiber and liquid – and I might even",presetFontSizes: [22,21,20,19,18,17,16,14],maxLines: 1,textAlign: TextAlign.right,),
                     AutoSizeText("be said to possess a mind. I am invisible,",presetFontSizes: [22,21,20,19,18,17,16,14],maxLines: 1,textAlign: TextAlign.right,),
                     AutoSizeText("understand, simply because people",presetFontSizes: [22,21,20,19,18,17,16,14],maxLines: 1,textAlign: TextAlign.right,),
                     AutoSizeText("refuse to see me.”",presetFontSizes: [22,21,20,19,18,17,16,14],maxLines: 1,textAlign: TextAlign.right,),
                     AutoSizeText("— Ralph Ellison",presetFontSizes: [22,21,20,19,18,17,16,14],maxLines: 1,textAlign: TextAlign.right,),
                     AutoSizeText("from the Invisible Man, 1852",presetFontSizes: [22,21,20,19,18,17,16,14],maxLines: 1,textAlign: TextAlign.right,),
                   ],
                 ),
                  Flexible(child: AutoSizeText(textFiles[0],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,))
                ],
              ),
            ),),
            Expanded(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[1],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
            )),
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[2],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
            ))
          ],
        ),
      ),
    );
  }
}
