import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class HeartAndSoulTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: template.viewHeight() * 280,
            width: template.viewWidth() * 220,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight() * 10),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/heartandsoul2.jpg",
                              )));
                },
                child: Container(
                    height: template.viewHeight() * 280,
                    width: template.viewWidth() * 220,
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: Image.asset("assets/images/heartandsoul2.jpg"))),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: (ht / 300) * 135,
                ),
                Text(
                  "Young Boy, ca. 1900",
                  style: template.textheaderStyle(),
                  maxLines: 1,
                ),
                Text(
                  "Maker unknown",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "Hand-colored silver print",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "12 x 8 in.",
                  style: template.bodyTextStyle(),
                ),
                SizedBox(
                  height: (ht / 300) * 120,
                ),
                Container(
                    height: template.footerTextContainerHihght(),
                    width: template.textContainerwidth() * 2,
                    child: Padding(
                        padding:
                            EdgeInsets.only(right: template.viewWidth() * 7),
                        child: template.footerRightText()))
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
