import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class HeartAndSoulSix extends StatefulWidget {
  @override
  _HeartAndSoulSixState createState() => _HeartAndSoulSixState();
}

class _HeartAndSoulSixState extends State<HeartAndSoulSix> {
  int _current = 0;
  List<String> _imagesList = [
    "assets/images/heartandsoul6_1.jpg",
    "assets/images/heartandsoul6_2.jpg",
    "assets/images/heartandsoul6_3.jpg",
    "assets/images/heartandsoul6_4.jpg",
    "assets/images/heartandsoul6_5.jpg",
    "assets/images/heartandsoul6_6.jpg",
  ];
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          children: <Widget>[
            SizedBox(
              width: template.viewWidth() * 6,
            ),
            Container(
                height: template.textContainerheight(),
                width: template.textContainerwidth(),
                child: Padding(
                  padding: template.textcontainerPadding(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: template.viewHeight() * 15,
                      ),
                      Text(
                        "Postcards, ca. early 20th century",
                        style: template.textheaderStyle(),
                        maxLines: 1,
                      ),
                      Text(
                        "Makers unknown",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "Photographic post cards",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "3 1⁄2 x 5 1⁄2 in.",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "\nThese photographic postcards are literally snapshots of the harsh realities of life for African Americans in the south in the early 20th century. After the failure of the post Civil War “era of reconstruction” in the late 19th century, life for African Americans in the south returned to near slave-like conditions.\n\nSegregation was legalized. The Ku Klux Klan used threats and violence to control much of the politics. Share cropping, a system where tenant farmers paid rent by giving a portion of their harvest to the land owner, became the primary livelihood for African Americans. African American men were convicted and sentenced to jail for all kinds of minor charges where they were then hired out for labor on “chain gangs.” Douglas Blackmon descibes this period of “neo slavery” in his Pulitzer Prize winning book Slavery by Another Name.",
                        textAlign: TextAlign.justify,
                        style: template.bodyTextStyle(),
                      ),
                      SizedBox(
                        height: template.viewHeight() * 75,
                      ),
                      Container(
                          height: template.footerTextContainerHihght(),
                          width: template.textContainerwidth() * 3,
                          child: template.footerLeftText()),
                    ],
                  ),
                )),
            SizedBox(
              width: template.viewWidth() * 6,
            ),
            Expanded(
                flex: 3,
                child: Stack(children: [
                  Center(
                    child: CarouselSlider(
                      items: _imagesList.map((i) {
                        return Builder(
                          builder: (BuildContext context) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => GalleryView(
                                              url: i,
                                            )));
                              },
                              child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.6,
                                  height:
                                      MediaQuery.of(context).size.height * 0.75,
                                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                                  child: FittedBox(
                                    fit: BoxFit.contain,
                                    child: Image.asset(i),
                                  )),
                            );
                          },
                        );
                      }).toList(),
                      onPageChanged: (index) {
                        setState(() {
                          _current = index;
                        });
                      },
                      viewportFraction: 1.0,
//                      aspectRatio: 16/9,
                      height: MediaQuery.of(context).size.height * 0.8,
                      enableInfiniteScroll: false,
                    ),
                  ),
                  Positioned(
                      bottom: 40,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        height: 40,
                        color: Colors.black54,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: _imagesList.map((f) {
                              var index = _imagesList.indexOf(f);
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  height: index == _current ? 14 : 10,
                                  width: index == _current ? 14 : 10,
                                  decoration: BoxDecoration(
                                      color: index == _current
                                          ? Colors.white
                                          : Colors.white30,
                                      shape: BoxShape.circle),
                                ),
                              );
                            }).toList()),
                      ))
                ])),
          ],
        ),
      ),
    );
  }
}
