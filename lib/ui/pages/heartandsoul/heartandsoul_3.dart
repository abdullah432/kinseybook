import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class HeartAndSoulThree extends StatefulWidget {
  @override
  _HeartAndSoulThreeState createState() => _HeartAndSoulThreeState();
}

class _HeartAndSoulThreeState extends State<HeartAndSoulThree> {
  int _current = 0;
  List<String> _imagesList = [
    "assets/images/heartandsoul3_1.jpg",
    "assets/images/heartandsoul3_2.jpg",
    "assets/images/heartandsoul3_3.jpg",
    "assets/images/heartandsoul3_4.jpg",
    "assets/images/heartandsoul3_5.jpg",
    "assets/images/heartandsoul3_6.jpg",
    "assets/images/heartandsoul3_7.jpg",
  ];
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          children: <Widget>[
            SizedBox(width: template.viewWidth() * 5),
            Container(
                height: template.textContainerheight(),
                width: template.textContainerwidth(),
                child: Padding(
                  padding: template.textcontainerPadding(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Tintypes, ca. 1855-1900",
                        style: template.textheaderStyle(),
                        maxLines: 1,
                      ),
                      Text(
                        "Makers unknown",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "Tintype",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "2 1⁄2 x 3 1⁄2 in. - 3 1⁄2 x 5 1⁄2 in.",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "\nThe first permanent photograph was \nproduced in 1826 by a French inventor, Joseph Niépce. Tintypes were first described in 1853, a derivative form in which a wet photographic emulsion on a thin sheet of blackened iron (not tin, as the name would imply) was exposed to light through a lens and camera and then processed in chemicals to make the image permanent. Because of the speed of the process, which took only a few minutes, the low cost, and the short exposure times, tintypes were the most popular form of portrait photography after 1850, until the invention of negative film displaced it around 1890. Tintypes were very durable and many, like the ones shown on these pages, have survived to present day.\n\nThese tintypes have come to us without any documentation, and the names of the people have long been lost to history, but these proud and dynamic portraits suggest scenarios of achievement and accomplishment. As a group they provide a collective snapshot of the accomplishments of African Americans after the end of the Civil War, until their newfound freedoms were extinguished by repressive legislation and Supreme Court decisions toward the end of the nineteenth century.",
                        textAlign: TextAlign.justify,
                        style: template.bodyTextStyle(),
                      ),
                      SizedBox(
                        height: template.viewHeight() * 25,
                      ),
                      Container(
                          height: template.footerTextContainerHihght(),
                          width: template.textContainerwidth() * 3,
                          child: template.footerLeftText()),
                    ],
                  ),
                )),
            Expanded(
                flex: 3,
                child: Stack(overflow: Overflow.visible, children: [
                  Center(
                    child: CarouselSlider(
                      items: _imagesList.map((i) {
                        return Builder(
                          builder: (BuildContext context) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => GalleryView(
                                              url: i,
                                            )));
                              },
                              child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.6,
                                  height:
                                      MediaQuery.of(context).size.height * 0.75,
                                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                                  child: FittedBox(
                                    fit: BoxFit.fill,
                                    child: Image.asset(i),
                                  )),
                            );
                          },
                        );
                      }).toList(),
                      onPageChanged: (index) {
                        setState(() {
                          _current = index;
                        });
                      },
                      viewportFraction: 1.0,
//                      aspectRatio: 16/9,
                      height: MediaQuery.of(context).size.height * 0.8,
                      enableInfiniteScroll: false,
                    ),
                  ),
                  Positioned(
                      bottom: 40,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        height: 40,
                        color: Colors.black54,
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: _imagesList.map((f) {
                              var index = _imagesList.indexOf(f);
                              return Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  height: index == _current ? 14 : 10,
                                  width: index == _current ? 14 : 10,
                                  decoration: BoxDecoration(
                                      color: index == _current
                                          ? Colors.white
                                          : Colors.white30,
                                      shape: BoxShape.circle),
                                ),
                              );
                            }).toList()),
                      ))
                ])),
          ],
        ),
      ),
    );
  }
}
