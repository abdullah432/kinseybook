import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class HeartAndSoulFour extends StatefulWidget {
  @override
  _HeartAndSoulFourState createState() => _HeartAndSoulFourState();
}

class _HeartAndSoulFourState extends State<HeartAndSoulFour> {
  int _current = 0;

  List<String> _imagesList = [
    "assets/images/heartandsoul4_1.jpg",
    "assets/images/heartandsoul4_2.jpg",
    "assets/images/heartandsoul4_3.jpg",
    "assets/images/heartandsoul4_4.jpg",
    "assets/images/heartandsoul4_5.jpg",
    "assets/images/heartandsoul4_6.jpg",
    "assets/images/heartandsoul4_7.jpg",
  ];

  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Stack(overflow: Overflow.visible, children: [
              Center(
                child: CarouselSlider(
                  items: _imagesList.map((i) {
                    return Builder(
                      builder: (BuildContext context) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url: i,
                                        )));
                          },
                          child: Container(
                              width: MediaQuery.of(context).size.width * 0.6,
                              height: MediaQuery.of(context).size.height * 0.75,
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              child: FittedBox(
                                fit: BoxFit.contain,
                                child: Image.asset(i),
                              )),
                        );
                      },
                    );
                  }).toList(),
                  onPageChanged: (index) {
                    setState(() {
                      _current = index;
                    });
                  },
                  viewportFraction: 1.0,
//                      aspectRatio: 16/9,
                  height: MediaQuery.of(context).size.height * 0.8,
                  enableInfiniteScroll: false,
                ),
              ),
              Positioned(
                  bottom: 40,
                  left: 0.0,
                  right: 0.0,
                  child: Container(
                    height: 40,
                    color: Colors.black54,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: _imagesList.map((f) {
                          var index = _imagesList.indexOf(f);
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              height: index == _current ? 14 : 10,
                              width: index == _current ? 14 : 10,
                              decoration: BoxDecoration(
                                  color: index == _current
                                      ? Colors.white
                                      : Colors.white30,
                                  shape: BoxShape.circle),
                            ),
                          );
                        }).toList()),
                  ))
            ]),
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: (ht / 300) * 109,
                  ),
                  Text(
                    "Tintypes, ca. 1855-1900",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Makers unknown",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Tintype",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "2 1⁄2 x 3 1⁄2 in.",
                    style: template.bodyTextStyle(),
                  ),
                  SizedBox(
                    height: (ht / 300) * 127,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
