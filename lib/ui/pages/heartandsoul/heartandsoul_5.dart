import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class HeartAndSoulFive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          children: <Widget>[
            Expanded(
                child: Row(
              children: <Widget>[
                Container(
                  height: template.viewHeight() * 150,
                  width: template.viewWidth() * 120,
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: template.viewHeight() * 10,
                        left: template.viewWidth() * 5),
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url:
                                            "assets/images/heartandsoul5_1.jpg",
                                      )));
                        },
                        child: Image.asset(
                          "assets/images/heartandsoul5_1.jpg",
                          fit: BoxFit.contain,
                        )),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              SizedBox(height: template.viewHeight() * 10),
                              Text(
                                "Young girl, ca. 1855",
                                style: template.textheaderStyle(),
                                maxLines: 1,
                              ),
                              Text(
                                "Maker unknown",
                                style: template.bodyTextStyle(),
                              ),
                              Text(
                                "Ambrotype",
                                style: template.bodyTextStyle(),
                              ),
                              Text(
                                "3 3⁄4 x 3 1⁄4 x 3⁄4 in.",
                                style: template.bodyTextStyle(),
                              ),
                              Container(
                                height: template.viewHeight() * 20,
                                width: template.viewWidth() * 50,
                                child: IconButton(
                                  icon: Icon(
                                    Icons.play_circle_outline,
                                    color: Colors.black,
                                  ),
                                  iconSize: template.videoIconSize(),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => VideoView(
                                                  videoLink:
                                                      "assets/videos/heartandsoul5_1.MP4",
                                                )));
                                  },
                                ),
                              ),
                            ],
                          ),
                          Spacer(),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Spacer(),
                          Column(
                            children: <Widget>[
                              Text(
                                "Well-to-do black couple, ca. 1860",
                                style: template.textheaderStyle(),
                                maxLines: 1,
                              ),
                              Text(
                                "Maker unknown",
                                style: template.bodyTextStyle(),
                              ),
                              Text(
                                "Hand-colored tintype",
                                style: template.bodyTextStyle(),
                              ),
                              Text(
                                "3 3⁄4 x 3 1⁄4 x 3⁄4 in.",
                                style: template.bodyTextStyle(),
                              ),
                              Container(
                                height: template.viewHeight() * 20,
                                width: template.viewWidth() * 50,
                                child: IconButton(
                                  icon: Icon(
                                    Icons.play_circle_outline,
                                    color: Colors.black,
                                  ),
                                  iconSize: template.videoIconSize(),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => VideoView(
                                                  videoLink:
                                                      "assets/videos/heartandsoul5_2.MP4",
                                                )));
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                    height: template.viewHeight() * 150,
                    width: template.viewWidth() * 120,
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: template.viewHeight() * 10,
                          right: template.viewWidth() * 5),
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url:
                                              "assets/images/heartandsoul5_2.jpg",
                                        )));
                          },
                          child:
                              Image.asset("assets/images/heartandsoul5_2.jpg")),
                    ))
              ],
            )),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: template.viewWidth() * 30,
                    vertical: template.viewHeight() * 10),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => GalleryView(
                                                  url:
                                                      "assets/images/heartandsoul5_3.jpg",
                                                )));
                                  },
                                  child: Image.asset(
                                      "assets/images/heartandsoul5_3.jpg"))),
                          Expanded(
                              child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Portrait of young black gentleman,\nca. 1860",
                                style: template.textheaderStyle(),
                                maxLines: 2,
                                textAlign: TextAlign.center,
                              ),
                              Text(
                                "Maker unknown",
                                style: template.bodyTextStyle(),
                              ),
                              Text(
                                "Ambrotype",
                                style: template.bodyTextStyle(),
                              ),
                              Text(
                                "4 x 5 x 1 in..",
                                style: template.bodyTextStyle(),
                              ),
                            ],
                          ))
                        ],
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url:
                                              "assets/images/heartandsoul5_4.jpg",
                                        )));
                          },
                          child:
                              Image.asset("assets/images/heartandsoul5_4.jpg")),
                      flex: 2,
                    )
                  ],
                ),
              ),
            ),
            Container(
                height: template.footerTextContainerHihght(),
                width: template.textContainerwidth() * 3,
                child: template.footerLeftText()),
          ],
        ),
      ),
    );
  }
}
