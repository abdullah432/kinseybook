import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class ReflectionOne extends StatefulWidget {

  @override
  _ReflectionOneState createState() => _ReflectionOneState();
}

class _ReflectionOneState extends State<ReflectionOne> {
  final textFiles=["Beauty is in the eyes, heart, and soul of the collector. . .\n\nWe think of ourselves not as owners of art but as caretakers, sharing a story, not possessions. Many of the artists’ works we collect have caught my eye, captured my heart, and remain in my soul. Our journey of discovery is remarkable, as we have been blessed to make and maintain relationships along the way. Artists and friends such as Ernie Barnes, Phoebe Beasley, Artis Lane, Ed Dwight, Jonathan Green, and Tina Allen: all touched our lives in profound ways, very early on and very directly.\n\nThe themes in Ernie Barnes’ figurative painting style captured both of us, with the elongated limbs of his young subjects playing in dirt yards, boys playing basketball and girls jumping rope. His images inspired us and brought back childhood memories at the same time. His High Aspirations shows a young man reaching high and dunking a basketball into a hoop made from a peach basket attached to a plywood backboard, mounted on a pole. It became our symbol of someone striving to do their best and attaining that goal.\n\nIn 1977, Bernard began to give a Barnes print to his top performing sales people. Instead of the usual plaque, they proudly took home to hang on their walls a work of art: Ernie Barnes’ High Aspirations, framed and inscribed with their",
    "name and stats. Some years later we met Ernie and became friends, and he let us know that his inspiration for Homecoming was our alma mater’s band, the Florida A&M University Marching 100. Bernard had played in this band, and we are both so very proud of their work. Needless to say, that strengthened our connection to Ernie, and he will always have a place in our hearts and in our home. . . We miss him.\n\nWe knew of Phoebe Beasley in the early 1980s and immediately felt drawn to her work. We attended an auction held by the Museum of African American Art, and when our son Khalil saw one of her paintings, he asked to bid. He was about eight years old at the time, and he acquired the piece: a 1973 watercolor with eight young African American boys and girls with words that spoke to him even then. That led to a lifelong friendship.\n\nLoving Phoebe’s images of grandmotherly figures was a big part of getting to know and love her as a friend. The sensitivity she shows in the themes of her work, her use of fabric, paper, ribbon, cardboard, and paint gives an insight into the spirit that lives in her creativity. In 1987, we commissioned her to do a special piece for our dear friends Joe and Julie Johnson, upon his retirement from Xerox. In Phoebe’s creative style, she asked for and used fabric and other family items to personalize a unique work of art. I continue to collect her work for us as well. Her print Holding Court hangs in my personal space; Piano, Bass and Drums is in the exhibit; and my",
    "own portrait, Shirley, hangs at home.\n\nArtis Lane . . . what a fortuitous name. We were first exposed to her work in 1985, and loved her portraits and sculptures immediately. Soon bonds were formed with her and her husband, Vince. Imagine how delighted we were to have her commissioned by the Xerox Black Employees to do a portrait of our son, Khalil, as a gift to us when Bernard retired in 1991. Little did we know that 11 years later we would be given a portrait of us as a 35th wedding anniversary gift from our FAMU best friend, Thomas L. Mitchell, with friends Terry and Thelma Harris. This highly spiritual and creative lady recently had the honor of having her bust of Sojourner Truth placed in the U.S. Capitol by our First Lady, Michelle Obama. Artis is keeping our history alive.\n\nEd Dwight came to us in 1986 by way of a gallery in Maui. We saw his magnificent sculpture of Charlie Parker, and upon hearing his story, that he was an astronaut in training, we wondered out loud how it was that we didn’t know this man. Bernard called him in Denver, invited him to the Urban League Dinner in Los Angeles, and the rest is living history. His sculptures of strong African women spoke so directly to me and his jazz pieces and Revolutionary soldiers spoke to Bernard. We decided our friends had to know him and collect his work, so, in 1987 and in 1998, we hosted anexhibition of his work at our home. And the beat goes on. . ."
  ];
  bool listView=false;
  double fontSize=50;
  ScrollController listViewController=ScrollController();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
        child: listView?Stack(
          children: <Widget>[
            ListView.builder(
                controller: listViewController,
                itemCount: textFiles.length,
                itemBuilder: (BuildContext context,index){
                  if(index==0){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: "Reflections\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                            TextSpan(text: textFiles[0])
                          ],

                        ),
                        presetFontSizes: [fontSize],
                        textAlign: TextAlign.justify,
                      ),
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: AutoSizeText(textFiles[index],minFontSize: fontSize,textAlign: TextAlign.justify),
                  );
                }),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height*0.1,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(onPressed: (){

                        if(listViewController.position.minScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset-listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },icon: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 40,),),
                      Row(children: <Widget>[
                        IconButton(icon: Icon(Icons.zoom_out,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                           fontSize=30;
                          });
                        }),
                        IconButton(icon: Icon(Icons.close,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            listView=false;
                          });
                        })
                      ],),
                      IconButton(icon: Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 40,),onPressed: (){
                        if(listViewController.position.maxScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },)
                    ],),
                ))
          ],
        ):Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 30,),
                  Container(width:100,height: 50,child: Stack(children: <Widget>[Positioned(child: GestureDetector(
                      onTap: (){
                        setState(() {
                          fontSize=50;
                          listView=true;
                        });
                      },
                      child: Image.asset("assets/images/magnify.png",scale: 4,)),left: -30,),],overflow: Overflow.visible,)),
                  AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(text: "Reflections\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                      ],

                    ),
                    textAlign: TextAlign.justify,
                  ),
                  Expanded(child: AutoSizeText(textFiles[0],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,))
                ],
              ),
            ),),
            Expanded(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[1],presetFontSizes:[22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
            )),
            Expanded(child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: AutoSizeText(textFiles[2],presetFontSizes:[22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,)),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
