import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class ReflectionTwo extends StatelessWidget {
  final textFiles=["We met Jonathan Green in 1988, and had the marvelous pleasure of seeing him work on Field Hands, the painting we ultimately acquired. Of course, that made it even more special. . . I love his “sheets on the line”—that takes me home—as well as his “swing” series. His use of bold, brilliant colors led to other acquisitions and an exhibition at our home to introduce him to friends. One of our Green paintings was selected as a backdrop for the ballet Off the Wall that the Charleston Ballet Theater conceived of and toured throughout the South.\n\nAnd then there’s Tina Allen. . . we always loved her beautiful bronze sculptures and could never decide on which one to acquire until we saw the magnificent Frederick Douglass. His elegant, stately presence sat on our dining room table for some time until his pedestal arrived. Imagine having dinner with his piercing eyes watching you! Oh, the stories he could tell. . . I recently discovered that he spoke in my home town in 1889 before seven hundred people. Tina is gone now, but her presence will always be with us. Her public sculptures of Alex Haley, Martin Luther King, Jr., Tupac Shakur, and many others will educate, inspire, and motivate generations to come.\n\nAll of these artists became friends, some before and others after we were proud caretakers of their creative output. Together we have shared many special moments in each others’ lives— small personal gatherings, art openings, New Year’s celebrations, Fourth of July, birthdays,",
    "anniversaries, unveilings, memorials, book signings. These moments have shaped our own history.\n\nOur collecting reflects our self-discovery, our individual and personal history, and memories of growing up in Florida. We have never strayed too far emotionally from those roots. I grew up in St. Augustine, with Mama (my grandmother), and I can now remember her as a collector of sorts. She would quietly and lovingly save swatches of fabric from dresses she made for me and my sister/cousin and other family members. I can remember her piecing those little squares or triangles together for the next quilt she would masterfully make. It was always a treat to try and find “my dress” while the quilting frame was up or after the quilt was finished and on the bed. To this day I have “pieces” done by her as well as some done by my mother as a young woman, before she passed away in 1948 at the age of 21.\n\nMy sister Barbara was an art major in college and during the summer she always used me as her model. I would have to sit on the porch rail and not move as she sketched my profile from both sides and all angles. Thus my close relationships with artists began when I was quite young.\n\nMy uncle, James A. Webster, who was also my elementary school principal, taught art during the summers at Florida Memorial College, in St. Augustine. There were always paintings in his home, some by him and some by his students. Little did I know at the time that he was friends",
    "with Jacob Lawrence, Gwendolyn Knight, Zora Neale Hurston, and Sarah Vaughn.\n\nWhat a remarkable, creative journey this has been and still is. As we share experiences and information, we learn so much about ourselves. This process of collecting our history evolves and becomes more focused, directed, and intentional, and as it exposes us to new work and new artists, it nourishes our souls.\n\nBernard and I have been blessed immeasurably, and we encourage those who have joined us on this journey to start their own narrative. Invest time, energy and resources to learn more about our collective history. Invest in an artist when you can, visit museums and become a patron of the arts. Begin your own journey to further educate, inspire, and motivate our young people. Find that special something that catches your eye, captures your heart, and remains in your soul.\n\n"
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
        child: Row(
          children: <Widget>[
            Expanded(child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: AutoSizeText(textFiles[0],textAlign: TextAlign.justify,presetFontSizes: [22,21,20,19,18,17,16,14])
            ),),
            Expanded(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[1],presetFontSizes:[22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
            )),
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flexible(

                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: AutoSizeText(textFiles[2],presetFontSizes: [24,23,22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
                      )),
                  Container(
                      width: double.infinity,
                      child: AutoSizeText("— Shirley Pooler Kinsey",textAlign: TextAlign.right,minFontSize: 20,))
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
