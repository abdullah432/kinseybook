import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusSix extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 36;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
            height: ht,
            width: wt,
            decoration: template.viewportBorder(),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: template.viewHeight() * 120,
                      ),
                      Text(
                        "Landscape, Autumn, ca. 1865",
                        style: template.textheaderStyle(),
                      ),
                      Text(
                        "Robert Scott Duncanson",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "Oil on board",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "16 ¾ x 13 ¼ in.",
                        style: template.bodyTextStyle(),
                      ),
                      SizedBox(
                        height: template.viewHeight() * 15,
                      ),
                      GestureDetector(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return Dialog(
                                  child: Container(
                                    height:
                                        MediaQuery.of(context).size.height / 2,
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Stack(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 20),
                                          child: Column(
                                            children: <Widget>[
                                              Text(
                                                "Robert Scott Duncanson (1817–1872)",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 20,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              Expanded(
                                                  child: Text(
                                                "Duncanson, born in Seneca, New York, spent his early years in Canada with his Scottish father. He later moved to Ohio to live with his black mother, where he pursued his love of painting. He began his professional life as a portrait painter but was drawn to landscape, and his work was greatly influenced by the Hudson River School. He was commissioned to do several large murals in Cincinnati, and these were well received, enabling him to live off the proceeds of his work. Later, he traveled to Britain to study and paint landscapes. The London Art Journal declared him a master painter. After his death, however, his work fell into obscurity. It was not until the 1950s, when James Porter dedicated himself to reclaiming Duncanson’s contribution to African American art, that his work was rediscovered by the public.",
                                                textAlign: TextAlign.justify,
                                              ))
                                            ],
                                          ),
                                        ),
                                        Positioned(
                                          child: IconButton(
                                            icon: Icon(
                                              Icons.clear,
                                              color: Colors.black,
                                              size: 30,
                                            ),
                                            onPressed: () =>
                                                Navigator.pop(context),
                                          ),
                                          top: 10,
                                          right: 10,
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              });
                        },
                        child: template.bioImg(),
                      ),
                      SizedBox(
                        height: template.viewHeight() * 100,
                      ),
                      Container(
                          height: template.footerTextContainerHihght(),
                          width: template.textContainerwidth() * 3,
                          child: Padding(
                              padding: EdgeInsets.only(left: 8),
                              child: template.footerLeftText())),
                    ],
                  ),
                ),
                Container(
                  height: template.viewHeight() * 280,
                  width: template.viewWidth() * 260,
                  child: Padding(
                    padding: EdgeInsets.only(top: template.viewHeight() * 5),
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/earlygenius6.jpg",
                                      )));
                        },
                        child: Image.asset(
                          "assets/images/earlygenius6.jpg",
                          fit: BoxFit.contain,
                        )),
                  ),
                )
              ],
            )));
  }
}
