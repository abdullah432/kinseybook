import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusOne extends StatefulWidget {
  @override
  _EarlyGeniusOneState createState() => _EarlyGeniusOneState();
}

class _EarlyGeniusOneState extends State<EarlyGeniusOne> {
  final textFiles = [
    "\n\nThe Kinsey Collection contains remarkable examples of African American works of genius. From these, the Kinseys forged their journey as stewards, tending, educating, and cultivating the landscape of social justice to yield evidence of continued excellence.\n\nThe Kinseys endeavored to become stewards of the work of prominent early black painters who earned both recognition and financial freedom by exercising technical expertise. These artists painted portraits and landscapes, as the style of the times required.",
    "The beauty of their images demonstrates their interpretation of a tradition studied and mastered in Europe at a time when most African Americans were toiling as slaves in fields and kitchens across America.\n\nIt is important to note the role that Europe played in the complex intersections of inclusion and prejudice that shaped the lives of Africans and African Americans for centuries. Under the umbrella of European imperialism, Africans became chattel, while African American intellectual and creative talent was encouraged and valued. Many prominent Europeans supported the development of individual black artists, musicians, and scholars while ignoring the plight of brown and black people throughout the world. This trend began upon first contact in ancient times, was resurrected when the Portuguese discovered the West African coast, and continued with the French and British examples shown here. This conflicted approach to black identity would significantly affect African American cultural commodification globally, and for generations.",
    ""
  ];
  static bool listView = false;
  ScrollController listViewController = ScrollController();
  static double fontSize = 50;
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 31;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: listView
            ? Stack(
                children: <Widget>[
                  ListView.builder(
                      controller: listViewController,
                      itemCount: textFiles.length,
                      itemBuilder: (BuildContext context, index) {
                        if (index == 0) {
                          return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              child: Column(
                                children: <Widget>[
                                  Text.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                            text: "Early Genius\n\n",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 24)),
                                        TextSpan(
                                          text:
                                              "But in our time, as in every time, the impossible is the least that one can demand—and one is, after all, emboldened by the spectacle of human history in general, and American Negro history in particular, for it testifies to nothing less than the perpetual achievement of the impossible.\n",
                                        ),
                                      ],
                                    ),
                                    textAlign: TextAlign.justify,
                                  ),
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: Text(
                                      "— James Baldwin",
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 20),
                                    child: Text(
                                      textFiles[0],
                                      textAlign: TextAlign.justify,
                                    ),
                                  )
                                ],
                              ));
                        }
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          child: Text(textFiles[index],
                              textAlign: TextAlign.justify),
                        );
                      }),
                  Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        color: Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            IconButton(
                              onPressed: () {
                                if (listViewController
                                        .position.minScrollExtent ==
                                    listViewController.offset) {
                                  print("on the end");
                                } else {
                                  listViewController.animateTo(
                                      listViewController.offset -
                                          listViewController
                                                  .position.maxScrollExtent /
                                              7,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.easeIn);
                                }
                              },
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.blue,
                                size: 40,
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                IconButton(
                                    icon: Icon(
                                      Icons.zoom_out,
                                      color: Colors.blue,
                                      size: 40,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        fontSize = 30;
                                      });
                                    }),
                                IconButton(
                                    icon: Icon(
                                      Icons.close,
                                      color: Colors.blue,
                                      size: 40,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        listView = false;
                                      });
                                    })
                              ],
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.blue,
                                size: 40,
                              ),
                              onPressed: () {
                                if (listViewController
                                        .position.maxScrollExtent ==
                                    listViewController.offset) {
                                  print("on the end");
                                } else {
                                  listViewController.animateTo(
                                      listViewController.offset +
                                          listViewController
                                                  .position.maxScrollExtent /
                                              7,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.easeIn);
                                }
                              },
                            )
                          ],
                        ),
                      ))
                ],
              )
            : Column(
                children: <Widget>[
                  SizedBox(height: template.viewHeight() * 20),
                  Container(
                    height: template.viewHeight() * 2,
                    width: wt,
                    child: Padding(
                      padding: EdgeInsets.only(right: (wt / 390) * 8),
                      child: Container(
                        color: template.boxColor(),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(width: template.viewWidth() * 5),
                        Container(
                          height: template.viewHeight() * 270,
                          width: template.textContainerwidth(),
                          child: Padding(
                            padding: EdgeInsets.only(
                                top: (ht / 300) * 20,
                                left: (wt / 390) * 2.5,
                                right: (wt / 390) * 2.5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                    width: (wt / 390) * 100,
                                    height: (ht / 300) * 15,
                                    child: Stack(
                                      children: <Widget>[
                                        Positioned(
                                          child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  fontSize = 50;
                                                  listView = true;
                                                });
                                              },
                                              child: RotatedBox(
                                                quarterTurns: 1,
                                                child: Image.asset(
                                                  "assets/images/magnify.png",
                                                  height: template
                                                      .magnifyingGlass(),
                                                ),
                                              )),
                                          left: -template.viewWidth() * 8,
                                        ),
                                      ],
                                      overflow: Overflow.visible,
                                    )),
                                Text.rich(
                                  TextSpan(
                                    children: [
                                      TextSpan(
                                        text: "Early Genius\n\n",
                                        style: template.textheaderStyle(),
                                      ),
                                      TextSpan(
                                        text:
                                            "But in our time, as in every time, the impossible is the least that one can demand—and one is, after all, emboldened by the spectacle of human history in general, and American Negro history in particular, for it testifies to nothing less than the perpetual achievement of the impossible.",
                                        style: template.bodyTextStyle(),
                                      ),
                                    ],
                                  ),
                                  textAlign: TextAlign.justify,
                                ),
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "— James Baldwin",
                                    style: template.bodyTextStyle(),
                                  ),
                                ),
                                Expanded(
                                    child: Padding(
                                        padding: EdgeInsets.only(
                                            left: (wt / 390) * 2.5,
                                            right: (wt / 390) * 2.5),
                                        child: Text(
                                          textFiles[0],
                                          textAlign: TextAlign.justify,
                                          style: template.bodyTextStyle(),
                                        )))
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                            child: Padding(
                          padding: EdgeInsets.only(
                              left: (wt / 390) * 2.5, right: (wt / 390) * 2.5),
                          child: Text(
                            textFiles[1],
                            textAlign: TextAlign.justify,
                            style: template.bodyTextStyle(),
                          ),
                        )),
                        Expanded(
                            child: Padding(
                          padding: EdgeInsets.only(
                              top: (ht / 300) * 20,
                              left: (wt / 390) * 2.5,
                              right: (wt / 390) * 2.5),
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                  child: Text(
                                textFiles[2],
                                textAlign: TextAlign.justify,
                                style: template.bodyTextStyle(),
                              )),
                            ],
                          ),
                        ))
                      ],
                    ),
                  ),
                  Container(
                    height: template.viewHeight() * 2,
                    width: wt,
                    child: Padding(
                      padding: EdgeInsets.only(left: (wt / 390) * 8),
                      child: Container(
                        color: template.boxColor(),
                      ),
                    ),
                  ),
                  SizedBox(height: template.viewHeight() * 20),
                  SizedBox(
                    height: (ht / 300),
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth() * 3,
                      child: Padding(
                          padding: EdgeInsets.only(right: (wt / 390) * 2.5),
                          child: template.footerRightText()))
                ],
              ),
      ),
    );
  }
}
