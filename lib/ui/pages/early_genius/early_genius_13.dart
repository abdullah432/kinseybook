import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusThirteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 43;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 6,
          ), /////  SINGLE IMAGE CONTAINER////
          Container(
            height: template.viewHeight() * 288,
            width: template.viewWidth() * 230,
            child: Padding(
                padding: EdgeInsets.only(top: template.viewHeight()),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url:
                                            "assets/images/earlygenius13_1.jpg",
                                      )));
                        },
                        child: Container(
                            height: MediaQuery.of(context).size.height * 0.8,
                            width: MediaQuery.of(context).size.width * 0.5,
                            child: FittedBox(
                                fit: BoxFit.contain,
                                child: Image.asset(
                                    "assets/images/earlygenius13_1.jpg"))),
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: template.viewWidth() * 45,
                        ),
                        Expanded(
                          flex: 2,
                          child: Column(
                            children: <Widget>[
                              Text(
                                "The Uncalled, 1898",
                                style: template.textheaderStyle(),
                                maxLines: 1,
                              ),
                              Text(
                                "Paul Laurence Dunbar",
                                style: template.bodyTextStyle(),
                              ),
                              Text(
                                "Book, First Edition",
                                style: template.bodyTextStyle(),
                              ),
                              Text(
                                "8 x 5 x 1 in.",
                                style: template.bodyTextStyle(),
                              ),
                            ],
                          ),
                        ),
                        Container(
                            height: template.viewHeight() * 60,
                            width: template.viewWidth() * 60,
                            child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => GalleryView(
                                                url:
                                                    "assets/images/earlygenius13_2.jpg",
                                              )));
                                },
                                child: Image.asset(
                                  "assets/images/earlygenius13_2.jpg",
                                  fit: BoxFit.contain,
                                )))
                      ],
                    )
                  ],
                )),
          ),
          SizedBox(
            width: template.viewWidth() * 27,
          ),
          Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Padding(
                padding: template.textcontainerPadding(),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        "Paul Laurence Dunbar was born in Kentucky to two Kentucky natives. His mother was a former slave and his father an escaped slave who went on to serve in the 55th Massachusetts Infantry Regiment and the 5th Massachusetts Colored Calvary Regiment during the Civil War. Paul’s mother instilled in her children a love for storytelling and literature. By the age of 6, Paul had written his first poem, and by the age of 9, he gave his first reading. His mother separated from his father and supported her poverty-stricken family by doing domestic work for white families, one of them the family of Orville and Wilbur Wright. The only African American student in his class, Paul attended school with the Wright brothers. He became the editor of the school newspaper, president of the school literary society, and class president. With the help of the Wright brothers, Paul published an African American newsletter in Dayton called TheTattler. He went on to publish his first collection of poetry, Oak and Ivy, in 1893, paying off his debt to his publisher by working as an elevator operator.\n\nHis second book, Majors and Minors, published in 1895 in both standard English and black dialect, brought him national fame. Paul traveled to London to give readings of his work, and on his return in 1898, married Alice Ruth Moore. They created companion poetry books. Paul took a job at the Library of Congress, but had to resign due to ill health, which led to depression and heavy drinking. He nonetheless produced twelve books of poetry, four of short stories, a play, and five novels. He died in 1906 at only 33.",
                        textAlign: TextAlign.justify,
                        style: template.bodyTextStyle(),
                      ),
                    ),
                    SizedBox(
                      height: (ht / 300) * 10,
                    ),
                    Container(
                        height: template.footerTextContainerHihght(),
                        width: template.textContainerwidth(),
                        child: template.footerRightText())
                  ],
                ),
              )),
        ],
      ),
    ));
  }
}
