import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 33;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 29,
          ),
          Container(
            height: template.viewHeight() * 270,
            width: template.viewWidth() * 230,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight() * 10),
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/earlygenius3.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/earlygenius3.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight() * 10),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 12),
                  Text(
                    "Porcupine’s Gazette, June 22, 1797",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "William Cobbett, publisher",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Printed paper",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "11 x 19 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    child: Text(
                      "William Cobbett began publishing this pro- British daily paper in Philadelphia. After battling with the anti-Federalist paper Aurora and losing a libel judgment against Benjamin Rush, the paper ceased publication in 1799.\n\nNewspapers were used extensively to advertise for the recapture of runaway slaves. In the upper right corner of this issue is a notice that reads: “One Hundred Dollars Reward” for a runaway negro man named Abraham, placed by Thomas Marsh Forman Sassafras Neck, Cecil County, Maryland.\n\nThe 1793 Fugitive Slave Act, designated in full as “An Act respecting fugitives from justice, and persons escaping from the service of their masters,” was the mechanism by which the government protected the property rights of slave owners. The government could pursue runaway slaves in any state or territory and ensure slave owners of their property rights as guaranteed in Article IV of the 1787 Constitution. The burden of proof was always on the enslaved person, to demonstrate their status as a free person. The Fugitive Slave Law therefore presumed guilt rather than innocence in the case of judicial proceedings involving black persons. This pernicious law still casts a shadow over the lives of blacks in this country, particularly the practice of racial profiling.\n\nNote: The New York Times did not begin capitalizing the word “Negro” until 1922.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ),
                  SizedBox(
                    height: (ht / 300) * 10,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
