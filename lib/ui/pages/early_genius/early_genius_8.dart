import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusEight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 38;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            SizedBox(
              width: 8,
            ),
            Container(
              height: template.viewHeight() * 218,
              width: template.viewWidth() * 310,
              child: Padding(
                padding: template.textcontainerPadding(),
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/earlygenius8.jpg",
                                  )));
                    },
                    child: Image.asset(
                      "assets/images/earlygenius8.jpg",
                      fit: BoxFit.cover,
                    )),
              ),
            ),
            SizedBox(
              height: (ht / 300) * 9,
            ),
            SizedBox(
              width: 8,
            ),
            Flexible(
              flex: 1,
              child: Column(
                children: <Widget>[
                  Text(
                    "Four Cows in a Meadow, 1893",
                    style: template.textheaderStyle(),
                  ),
                  Text(
                    "Edward Mitchell Bannister",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Oil on canvas",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "16 ¾	 x 20 ¾ in.",
                    style: template.bodyTextStyle(),
                  ),
                  SizedBox(
                    height: (ht / 300) * 5,
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height / 2,
                                width: MediaQuery.of(context).size.width / 2,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                            "Edward M. Bannister (1828–1901)",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 22,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Expanded(
                                              child: Text(
                                            "Bannister, born in Canada, was at a young age encouraged by his mother to paint. He moved to Boston in 1848 and became a barber, but pursued his love of art at the Lowell Institute, studying under sculptor William Rimmer. Influenced by the Barbizon School, Bannister painted idyllic landscapes with thick impasto. At the 1876 Centennial Exposition in Philadelphia, Bannister’s Under the Oaks was selected for the bronze medal, the major prize for oil painting. He became a founding member of the Providence Art Club in 1880. He received many commissions and lived on proceeds from his work.",
                                            textAlign: TextAlign.justify,
                                          ))
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.clear,
                                          color: Colors.black,
                                          size: 30,
                                        ),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                      top: 10,
                                      right: 10,
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                    child: Container(
                        height: template.viewHeight() * 20,
                        width: template.viewWidth() * 20,
                        child: Image.asset(
                          "assets/images/bio.png",
                        )),
                  ),
                  SizedBox(
                    height: (ht / 299) * 5,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth() * 3,
                      child: Padding(
                          padding:
                              EdgeInsets.only(left: template.viewHeight() * 3),
                          child: template.footerLeftText())),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
