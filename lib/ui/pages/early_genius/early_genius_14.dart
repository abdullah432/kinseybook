import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusFourteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 44;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        children: <Widget>[
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: EdgeInsets.only(
                  top: template.viewHeight() * 27,
                  left: template.viewWidth() * 8),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 30,
                  ),
                  Text(
                    "The Fastest Bicycle Rider In the World, 1928",
                    style: template.textheaderStyle(),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "Marshall W. “Major” Taylor",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Book, First Edition",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "9 x 6 ¼ x 1 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    child: Text(
                      "\n\nMarshall Walter Taylor (1878–1932) was an American cyclist who set several world records and then won the world one-mile track cycling championship in 1899. He was the first African American athlete to achieve the level of world champion in the sport, and only the second black man to win any world championship, after the Canadian boxer George Dixon.\n\nTaylor grew up with a well-to-do white family that his father worked for as a coachman, where he was treated like a member of the family. They gave young Taylor a Safety bicycle, which became his ticket to fame and fortune. Banned from racing in his home state of Indiana, he moved to the East Coast, where he turned professional. In 1903 he competed in Europe and was victorious in 40 of 57 races. When he retired in 1910 at only 32, he said it was because he could no longer tolerate the racism.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 40,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth() * 3,
                      child: template.footerLeftText()),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/earlygenius14_1.jpg",
                                  )));
                    },
                    child: Container(
                        height: template.viewHeight() * 220,
                        width: template.viewWidth() * 225,
                        child: FittedBox(
                            fit: BoxFit.contain,
                            child: Image.asset(
                                "assets/images/earlygenius14_1.jpg"))),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: template.viewWidth() * 10,
                              vertical: template.viewHeight() * 15),
                          child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => GalleryView(
                                              url:
                                                  "assets/images/earlygenius14_2.jpg",
                                            )));
                              },
                              child: Image.asset(
                                  "assets/images/earlygenius14_2.jpg")),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(
                              right: template.viewWidth() * 40,
                              left: template.viewWidth() * 10,
                              top: template.viewHeight() * 15),
                          child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => GalleryView(
                                              url:
                                                  "assets/images/earlygenius14_3.jpg",
                                            )));
                              },
                              child: Image.asset(
                                  "assets/images/earlygenius14_3.jpg")),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    ));
  }
}
