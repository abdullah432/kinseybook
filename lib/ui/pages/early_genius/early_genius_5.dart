import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusFive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: 9,
          ),
          Container(
            height: template.viewHeight() * 290,
            width: template.viewWidth() * 250,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight() * 2),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/earlygenius5.jpg",
                                      )));
                        },
                        child: Image.asset(
                          "assets/images/earlygenius5.jpg",
                          fit: BoxFit.contain,
                        )),
                  ),
                  Text(
                    "Narrative of the Life and Travels of",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Mrs. Nancy Prince, 1850",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Nancy Prince",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Book, First Edition",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "6 ½ x 4 ½ x ¼ in.",
                    style: template.bodyTextStyle(),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: EdgeInsets.only(
                top: template.viewHeight() * 2,
                left: template.viewWidth() * 2.5,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: (ht / 300) * 15,
                  ),
                  Container(
                    height: template.viewHeight() * 260,
                    child: Text(
                      "Nancy Gardener was born on September 15, 1779, in Newbury Port, Massachusetts, the granddaughter of slaves. Her father died when she was two months old; her mother suffered an emotional breakdown after she was widowed for the third time. Nancy and her siblings picked and sold berries to supplement their meager household income. Eventually, she left home and labored as a domestic servant for white families. With her marriage to Nero Prince in 1824, however, the course of her life changed dramatically. Prince was the founder of Prince Hall Free Masons in Boston. Together they traveled to St. Petersburg, Russia, and lived there for nine years, along with twenty paid black servants, at the court of the czar of Russia. Her husband worked as a footman at the court, and Nancy pursued an education, learning several languages. She established a business making fine clothing for babies and children, with the support of the empress herself. In 1833 she returned to Boston due to poor health, but her husband pre-deceased her before returning to the U.S.\n\nIn her biography, A Narrative of the Life and Travels of Mrs. Nancy Prince, first published in 1850, with two later editions, Prince narrates her experiences both in Russia and in the U.S. She recounts her journey from Jamaica back to Boston, during which the ship took a detour due to severe weather. The ship’s captain left her stranded in New Orleans with no money, and she feared she would be seized by a slave owner. While awaiting passage to New York, she witnessed the oppression experienced by her people in the South. Prince describes her missionary work in the British- controlled West Indies, and her abolitionist work both in America and in the West Indies. She died in 1856.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ),
                  SizedBox(
                    height: (ht / 300) * 10,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
