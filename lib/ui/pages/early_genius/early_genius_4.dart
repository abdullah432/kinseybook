import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 34;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewHeight() * 7,
          ),
          Column(
            children: <Widget>[
              Container(
                height: template.viewHeight() * 285,
                width: template.textContainerwidth(),
                child: Padding(
                  padding: EdgeInsets.only(
                      top: template.viewHeight() * 30,
                      left: template.viewWidth() * 2.5,
                      right: template.viewWidth() * 2.5),
                  child: Text(
                    "These woodblocks and their woodcut prints depict Haitian revolutionary leader Toussaint L’ Ouverture dictating a letter, and a group of Dominican rebels spying on French troops in Santo Domingo during the French campaign in Cuba and Santo Domingo. L’Ouverture led slave revolts against the French and established a new constitution during his peaceful and prosperous reign over Haiti. He and his well-trained military fended off attacks from both British and Spanish invaders. He was tricked by LeClerc, a proxy of Napoleon Bonaparte, and jailed in France, where he died in 1803.\n\nDespite L’Ouverture’s death, the independent republic of Haiti was formed in 1804, following a bloody revolution in which many French colonialists and plantation owners died. This did not sit well with either the French or the United States, a slave-owning country at the time. In 1825 a French armada arrived in the waters off Haiti, threatening invasion if Haiti did not agree to a ransom of 150 million gold francs, an astounding sum, more than ten times the total income of the nation at the time. It was not until 1948 that Haiti finished paying its nineteenth-century debts (estimated to have been \$19 billion in today’s currency). France has since acknowledged its responsibility for the irreparable damage done to the Haitian economy. Haiti remains one of the poorest nations in the Western Hemisphere, even though Haitians were the first primarily African enslaved peoples to gain their freedom from European colonialism.",
                    textAlign: TextAlign.justify,
                    style: template.bodyTextStyle(),
                  ),
                ),
              ),
              SizedBox(
                height: (ht / 300) * 2,
              ),
              Container(
                  height: template.footerTextContainerHihght(),
                  width: template.textContainerwidth(),
                  child: template.footerLeftText()),
            ],
          ),
          SizedBox(
            width: template.viewHeight() * 10,
          ),
          Container(
            height: template.viewHeight() * 290,
            width: template.viewWidth() * 240,
            child: Padding(
              padding: EdgeInsets.only(
                  top: template.viewHeight() * 10,
                  left: template.viewWidth() * 2.5,
                  right: template.viewWidth() * 2.5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GalleryView(
                                      url: "assets/images/earlygenius4.jpg",
                                    )));
                      },
                      child: Image.asset("assets/images/earlygenius4.jpg")),
                  SizedBox(
                    height: template.viewHeight() * 10,
                  ),
                  Text(
                    "Woodblocks, from Histoire de Napoléon by",
                    style: template.textheaderStyle(),
                  ),
                  Text(
                    "Jacques de Norvins, 1839",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Denis Auguste Marie Raffet, illustrator",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Wood",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "4 ½ x 3 ¾ in. each",
                    style: template.bodyTextStyle(),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
