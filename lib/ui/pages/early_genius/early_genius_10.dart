import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusTen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 40;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Container(
              height: template.viewHeight() * 225,
              width: template.viewWidth() * 340,
              child: Padding(
                padding: EdgeInsets.only(top: template.viewHeight() * 5),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/earlygenius10.jpg",
                                )));
                  },
                  child: Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: FittedBox(
                          fit: BoxFit.contain,
                          child:
                              Image.asset("assets/images/earlygenius10.jpg"))),
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Column(
                children: <Widget>[
                  Text(
                    "Street Scene, Tangier, 1913",
                    style: template.textheaderStyle(),
                  ),
                  Text(
                    "Henry O. Tanner",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Etching",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "18 ¾ x 20 in.",
                    style: template.bodyTextStyle(),
                  ),
                  SizedBox(
                    height: (ht / 300) * 10,
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height / 2,
                                width: MediaQuery.of(context).size.width / 2,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                            "Henry O. Tanner (1859–1925)",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 22,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Expanded(
                                              child: Text(
                                            "Tanner, born in Pittsburgh, was the first African American to attend the Pennsylvania Academy of the Fine Arts, where he studied with and became a favorite student of Thomas Eakins. The lack of opportunities in Philadelphia due to racial discrimination contributed to Tanner’s decision to leave the United States for Paris in 1891. With the help of renowned artist Jean Paul Lauren and others, Tanner gained considerable recognition for his religious paintings. The Resurrection of Lazarus in particular earned him a travel grant from art critic Rodman Wannamaker, and he went to the Middle East to further his study of religious art. Tanner’s work continued to garner international praise; W. E. B. DuBois and others tried to persuade him to return to the United States, to lead the black aesthetic movement, but Tanner remained in Europe and became host to many African American artists. The Banjo Lesson remains his most widely known work. Sand Dunes at Sunset, Atlantic City was the first work by an African American artist to be included in the White House permanent collection.",
                                            textAlign: TextAlign.justify,
                                          ))
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.clear,
                                          color: Colors.black,
                                          size: 30,
                                        ),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                      top: 10,
                                      right: 10,
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                    child: template.bioImg(),
                  ),
                  SizedBox(
                    height: (ht / 300) * 2,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth() * 3,
                      child: Padding(
                          padding: EdgeInsets.only(left: 4),
                          child: template.footerLeftText())),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
