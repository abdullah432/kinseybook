import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusSeven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 37;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 28,
          ),
          Container(
            height: template.viewHeight() * 280,
            width: template.viewWidth() * 240,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight()),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/earlygenius7.jpg",
                              )));
                },
                child: Container(
                  height: template.viewHeight() * 270,
                  width: template.viewWidth() * 150,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Image.asset("assets/images/earlygenius7.jpg"),
                  ),
                ),
              ),
            ),
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 68,
                  ),
                  Text(
                    "Lilacs, 1890",
                    style: template.textheaderStyle(),
                  ),
                  Text(
                    "Charles Ethan Porter",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Oil on canvas",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "21 ¾ x 18 in.",
                    style: template.bodyTextStyle(),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 15,
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height / 2,
                                width: MediaQuery.of(context).size.width / 2,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                            "Charles Ethan Porter (ca. 1847–1923)",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 22,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Expanded(
                                              child: Text(
                                            "Porter’s style skillfully merged Barbizon influences into an American perspective, and represents the best of American artistry in landscape, still life, and portraiture. He was one of the first black artists to study and exhibit at New York’s National Academy of Design. Porter traveled to Paris and was introduced to art circles through a letter of recommendation from Mark Twain. In France, he studied at L’Ecole des Arts Decoratifs and spent time studying landscapes near Barbizon. He returned to the U.S. and settled in Connecticut, where he continued to paint. In 1910, Porter became a charter member of the Connecticut Academy of Fine Arts.",
                                            textAlign: TextAlign.justify,
                                          ))
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.clear,
                                          color: Colors.black,
                                          size: 30,
                                        ),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                      top: 10,
                                      right: 10,
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                    child: Container(
                        height: template.viewHeight() * 20,
                        width: template.viewWidth() * 20,
                        child: Image.asset(
                          "assets/images/bio.png",
                        )),
                  ),
                  SizedBox(
                    height: (ht / 300) * 135,
                  ),
                  Container(
                    height: template.footerTextContainerHihght(),
                    width: template.textContainerwidth(),
                    child: Padding(
                        padding:
                            EdgeInsets.only(right: template.viewWidth() * 5),
                        child: template.footerRightText()),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
