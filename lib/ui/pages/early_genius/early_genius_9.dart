import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusNine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 39;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        children: <Widget>[
          /////  SINGLE IMAGE CONTAINER////
          SizedBox(
            width: template.viewWidth() * 15,
          ),
          Container(
            height: template.viewHeight() * 270,
            width: template.viewWidth() * 230,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight() * 10),
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/earlygenius9.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/earlygenius9.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: template.viewHeight() * 105,
                ),
                Text(
                  "Eagles Nest, Yellowstone,1890-91",
                  style: template.textheaderStyle(),
                  maxLines: 1,
                ),
                Text(
                  "Grafton Tyler Brown",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "Oil on canvas",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "36 x 25 ½ in.",
                  style: template.bodyTextStyle(),
                ),
                SizedBox(
                  height: template.viewHeight() * 10,
                ),
                GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height / 2,
                              width: MediaQuery.of(context).size.width / 2,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                          "Grafton Tyler Brown (1841–1918)",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 22,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Expanded(
                                            child: Text(
                                          "Born in Harrisburg, Pennsylvania, Brown migrated to San Francisco in search of opportunities. He worked as a lithographer for C. C. Kuchel and in 1867 started his own company producing maps, plats, and bank notes for Wells Fargo, Levi Strauss Company, and other firms. In addition to lithography, Brown mastered classic landscape painting. His paintings of western landscapes are technically precise, yet they also reveal Brown’s strong desire to document the threatened natural environment he loved. He sold his company to travel the West Coast, maintaining studios in Portland, Oregon, and then in British Columbia. In 1893, Brown moved east to St. Paul, Minnesota, to work as a draftsman for the U.S. Army Corps of Engineers. Much of his commercial work is a testament to both black craftsmanship and entrepreneurial spirit.",
                                          textAlign: TextAlign.justify,
                                        ))
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.clear,
                                        color: Colors.black,
                                        size: 30,
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                    top: 10,
                                    right: 10,
                                  )
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  child: template.bioImg(),
                ),
                SizedBox(
                  height: (ht / 300) * 120,
                ),
                Container(
                    height: template.footerTextContainerHihght(),
                    width: template.textContainerwidth(),
                    child: template.footerRightText())
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
