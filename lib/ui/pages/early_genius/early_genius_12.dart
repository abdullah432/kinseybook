import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusTwelve extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 42;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 7,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 90,
                  ),
                  Text(
                    "Incidents in the Life of a Slave Girl, 1861",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Harriet Ann Jacobs",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Book",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "8 x 5 ½ x 1 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "This book was written by Harriet Jacobs (1813– 1897) under the pen name Linda Brent. It describes the horrors of slavery and Jacobs’ daring pursuit of freedom. Her story, mindful of the well-known diary of Anne Frank, tells a similar tale of fear and confinement. Jacobs hid from her master for seven years in a “coffin-like space” before finding a path to freedom.",
                    textAlign: TextAlign.justify,
                    style: template.bodyTextStyle(),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 91,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth() * 3,
                      child: template.footerLeftText()),
                ],
              ),
            ),
          ),
          SizedBox(
            width: template.viewWidth() * 27,
          ),
          Container(
            height: template.viewHeight() * 260,
            width: template.viewWidth() * 200,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight() * 2),
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/earlygenius12.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/earlygenius12.jpg",
                    fit: BoxFit.contain,
                  )),
            ),
          )
        ],
      ),
    ));
  }
}
