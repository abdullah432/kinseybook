import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusEleven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 41;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: (ht / 300) * 10,
            ),
            Container(
              height: template.viewHeight() * 190,
              width: template.viewWidth() * 280,
              child: Padding(
                padding: EdgeInsets.only(top: template.viewHeight() * 5),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/earlygenius11_1.jpg",
                                )));
                  },
                  child: Image.asset("assets/images/earlygenius11_1.jpg"),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                children: <Widget>[
                  Text(
                    "My Country ’Tis of Thee, 1895",
                    style: template.textheaderStyle(),
                  ),
                  Text(
                    "Samuel Francis Smith",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Image 7 x 4 ½ in, letter 6 x 4 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                      child: Text(
                    "\nSmith, an American clergyman and poet, was the\nauthor of the national hymn “America” (1832),\noften known as “My Country ’Tis of Thee.” Smith\nwas asked by music educator Lowell Mason to\ntranslate lyrics from a German patriotic hymn.\nInstead, Smith wrote new lyrics for the hymn that\nbecame known as “America.”",
                    style: template.bodyTextStyle(),
                  )),
                ],
              ),
            ),
            SizedBox(
              height: (ht / 300) * 5,
            ),
            Container(
                height: template.footerTextContainerHihght(),
                width: template.textContainerwidth() * 3,
                child: Padding(
                    padding: EdgeInsets.only(right: 6),
                    child: template.footerRightText())),
          ],
        ),
      ),
    );
  }
}
