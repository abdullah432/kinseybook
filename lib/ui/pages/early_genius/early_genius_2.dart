import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class EarlyGeniusTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 32;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 6,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Bannaker’s Almanack, 1796",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "né Benjamin Banneker",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Extract from book",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "7 x 4 ½ x ¼ in.",
                    style: template.bodyTextStyle(),
                  ),
                  Expanded(
                    child: Text(
                      "Benjamin Banneker was born in Maryland in 1731 to a mulatto mother, Mary Banaky, and her former slave Robert, whom she married. Benjamin demonstrated great intellectual curiosity and by 1753 had constructed a working wooden clock that kept accurate time for over 50 years. At age 58, Banneker began the study of astronomy and was soon predicting solar and lunar eclipses. His almanacs were published annually from 1792 through 1797 and became best sellers in several states.\n\nIn 1791, Banneker served as a technical assistant in the first survey of Washington, D.C. Often referred to as the “Sable Astronomer,” he was regarded as proof that African Americans were not intellectually inferior to Europeans. In 1791, Banneker and Thomas Jefferson, then Secretary of State, had a famous exchange of letters. Jefferson, while having extreme reservations about the institution of slavery, believed that blacks were mentally inferior. Banneker implored Jefferson to reconsider this opinion in light of the language of the Declaration of Independence itself: “all men are created equal.” With this request, Banneker sent a copy of his almanac. Within two weeks Jefferson replied, advising Banneker that he was sending the copy of the almanac to the French Society of Science (and is still there today) as proof that they should reconsider their view that people of color were inferior intellectually.",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  SizedBox(
                    height: (ht / 299),
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth() * 3,
                      child: template.footerLeftText()),
                ],
              ),
            ),
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth() * 2,
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                children: <Widget>[
                  Container(
                    width: template.viewWidth() * 270,
                    height: template.viewHeight() * 240,
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/earlygenius2.jpg",
                                      )));
                        },
                        child: Image.asset(
                          "assets/images/earlygenius2.jpg",
                          fit: BoxFit.contain,
                        )),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 5,
                  ),
                  Container(
                    height: template.viewHeight() * 30,
                    width: template.viewWidth() * 152,
                    child: FittedBox(
                        child:
                            AudioViewer(url: "assets/audio/early_genius.mp3"),
                        fit: BoxFit.contain),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
