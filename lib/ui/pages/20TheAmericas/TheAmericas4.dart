import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class TheAmericas4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 4,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/the_americas3.jpg",)));
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width*.7,
                  child: FittedBox(
                    fit: BoxFit.fill,
                    child: Image.asset("assets/images/the_americas3.jpg"),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  AutoSizeText(
                    "Haitian Voodoo Flag, ca. 1990",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: "MyriadPro-BoldIt"),
                    presetFontSizes: presetFonts,
                  ),
                  AutoSizeText(
                    "Maxon Scylla\nMixed media",
                    presetFontSizes: presetFonts,
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  AutoSizeText(
                    "beads, sequins, on fabric",
                    presetFontSizes: presetFonts,
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  AutoSizeText(
                    "36 ½ x 30 in.",
                    presetFontSizes: presetFonts,
                    style: TextStyle(
                        fontStyle: FontStyle.italic, fontFamily: "Auto2-Regular"),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height / 2,
                                width: MediaQuery.of(context).size.width / 2,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          AutoSizeText(
                                            "Maxon Scylla (?– )",
                                            presetFontSizes: presetFonts,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontFamily: "MyriadPro-BoldIt",
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Expanded(
                                              child: AutoSizeText(
                                            "Maxon Scylla creates sequined banners, “Drapo Vodou,” an art form unique to Haiti. Scylla’s brother-in-law, famous Haitian artist Antoine Oleyant, brought Haitian ceremonial flags out of the Vodou Temple and into the realm of fine art. When Oleyant decided to devote full-time to his art, he opened his workshop with brother-in-law flag-maker Scylla. After Oleyant’s death in 1992, Scylla has continued the art with his own uniquely beautiful color palette perfecting Oleyant’s drawings.",
                                            style: TextStyle(
                                                fontFamily: "Auto2-Regular"),
                                            presetFontSizes: presetFonts,
                                            textAlign: TextAlign.justify,
                                          ))
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.clear,
                                          color: Colors.black,
                                          size: 30,
                                        ),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                      top: 10,
                                      right: 10,
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                    child: Container(
                        height: 80,
                        width: 80,
                        child: Image.asset(
                          "assets/images/bio.png",
                          scale: 3,
                        )),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
