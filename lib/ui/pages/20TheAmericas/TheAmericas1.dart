import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class TheAmericas1 extends StatefulWidget {
  @override
  _TheAmericas1State createState() => _TheAmericas1State();
}

class _TheAmericas1State extends State<TheAmericas1> {
  bool listView = false;

  ScrollController listViewController = ScrollController();

  double fontSize = 50;

  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    // presetFonts = [24];
    final textFiles = [
      "Travel has been a big part of our lives since the beginning of our marriage. Our first trip, to the Grand Canyon, led to further exploration in the southwest that was the beginning of our appreciation for Native American culture. We decided to explore all of the national parks of the West, and soon we realized that we had visited forty-three. We then began to spread our wings, and we visited the other Americas: South and Central America and the Caribbean islands. One of our first trips out of the U.S. was a three-week journey to South America in 1976. There we saw Machu Picchu, Iguazu Falls, Rio de Janeiro, the Amazon, Chile, Columbia, and Argentina. Each time we return home after experiencing other cultures, our desire to discover more about our own is heightened.\n\nEarly on we began looking for art on our travels and have discovered some wonderful images. One of our favorite pieces is a painting by Belizean artist Papo that we call Middle Street, San Pedro Town, Ambergris Caye. On a trip to Cuba we were introduced to an artist named Ricardo Losado, who depicts the Cuban landscape and experience, that draws you into the scene. During that same visit to Cuba, we had the opportunity to meet Fidel Castro.",
      "Works of art have also marked personal  milestones and relationships for us. On my fortieth birthday, my close friend and mentor Guy Dobbs, a vice president of a major Xerox division in the 1980s, gave me a painting by Brazilian artist Terciliano Jr. (pictured on page 168.) The great talent Carl Anderson, also a very close friend, gave us the beautiful Haitian Vodou flag by Maxon Scylla.\n\nAlthough this chapter is devoted to the Americas, we wanted to include one of Shirley’s favorite genres of art, Australian Aboriginal dot painting. We have traveled to Australia twice and have fallen in love with the Aboriginal culture and artwork. The Aboriginal story is similar to the story of Native Americans: conquest, massacre, and forced acculturation. But through this long ordeal they have maintained their cultural practices. Aboriginal paintings are based on the myth of the Dreamtime, which dates back over 40,000 years to some of the oldest rock paintings in the world. The piece that we are highlighting is an oil painting, Bush Plum by Colin Bird, which we purchased in 1996. The bush plum, which grows less than three feet high, is used for “smoke medicine” —the ancient holistic medical beliefs practiced by the Aboriginal people that recognizes the social, physical and spiritual dimensions of health and life.",
      "The  African American poet Langston Hughes gives voice to these themes in “Let America be America Again” (circa 1930’s)",
      "“O, let America be America again—\nThe land that never has been yet—\nAnd yet must be—the land where every man is free.\nThe land that’s mine—the poor man’s,\nIndian’s, Negro’s, ME—\nWho made America,\nWhose sweat and blood, whose faith and pain,\nWhose hand at the foundry, whose plow in the rain,\nMust bring back our mighty dream again.”\n\n“O, yes,\nI say it plain,\nAmerica never was America to me,\nAnd yet I swear this oath—America will be!”"
    ];
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
        child: listView
            ? Stack(
                children: <Widget>[
                  ListView.builder(
                      controller: listViewController,
                      itemCount: textFiles.length,
                      itemBuilder: (BuildContext context, index) {
                        if (index == 1) {
                          return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              child: AutoSizeText.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                        text:
                                            "Travel has been a big part of our lives since the beginning of our marriage. Our first trip, to the Grand Canyon, led to further exploration in the southwest that was the beginning of our appreciation for Native American culture. We decided to explore all of the national parks of the West, and soon we realized that we had visited forty-three. We then began to spread our wings, and we visited the other Americas: South and Central America and the Caribbean islands. One of our first trips out of the U.S. was a three-week journey to South America in 1976. There we saw Machu Picchu, Iguazu Falls, Rio de Janeiro, the Amazon, Chile, Columbia, and Argentina. Each time we return home after experiencing other cultures, our desire to discover more about our own is heightened."),
                                    TextSpan(
                                      text:
                                          "Early on we began looking for art on our travels and have discovered some wonderful images. One of our favorite pieces is a painting by Belizean artist Papo that we call Middle Street, San Pedro Town, Ambergris Caye. On a trip to Cuba we were introduced to an artist named Ricardo Losado, who depicts the Cuban landscape and experience, that draws you into the scene. During that same visit to Cuba, we had the opportunity to meet Fidel Castro.",
                                    ),
                                    TextSpan(
                                        text:
                                            "Although this chapter is devoted to the Americas, we wanted to include one of Shirley’s favorite genres of art, Australian Aboriginal dot painting. We have traveled to Australia twice and have fallen in love with the Aboriginal culture and artwork. The Aboriginal story is similar to the story of Native Americans: conquest, massacre, and forced acculturation. But through this long ordeal they have maintained their cultural practices. Aboriginal paintings are based on the myth of the Dreamtime, which dates back over 40,000 years to some of the oldest rock paintings in the world. The piece that we are highlighting is an oil painting, Bush Plum by Colin Bird, which we purchased in 1996. The bush plum, which grows less than three feet high, is used for “smoke medicine” —the ancient holistic medical beliefs practiced by the Aboriginal people that recognizes the social, physical and spiritual dimensions of health and life."),
                                    TextSpan(text: textFiles[3]),
                                  ],
                                ),
                                presetFontSizes:
                                    listView ? [fontSize] : presetFonts,
                                textAlign: TextAlign.justify,
                              ));
                        }
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          child: AutoSizeText(textFiles[index],
                              minFontSize: fontSize,
                              textAlign: TextAlign.justify),
                        );
                      }),
                  Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        color: Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            IconButton(
                              onPressed: () {
                                if (listViewController
                                        .position.minScrollExtent ==
                                    listViewController.offset) {
                                  print("on the end");
                                } else {
                                  listViewController.animateTo(
                                      listViewController.offset -
                                          listViewController
                                                  .position.maxScrollExtent /
                                              7,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.easeIn);
                                }
                              },
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.blue,
                                size: 40,
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                IconButton(
                                    icon: Icon(
                                      Icons.zoom_out,
                                      color: Colors.blue,
                                      size: 40,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        fontSize = 30;
                                      });
                                    }),
                                IconButton(
                                    icon: Icon(
                                      Icons.close,
                                      color: Colors.blue,
                                      size: 40,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        listView = false;
                                      });
                                    })
                              ],
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.blue,
                                size: 40,
                              ),
                              onPressed: () {
                                if (listViewController
                                        .position.maxScrollExtent ==
                                    listViewController.offset) {
                                  print("on the end");
                                } else {
                                  listViewController.animateTo(
                                      listViewController.offset +
                                          listViewController
                                                  .position.maxScrollExtent /
                                              7,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.easeIn);
                                }
                              },
                            )
                          ],
                        ),
                      ))
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              width: 100,
                              height: 50,
                              child: Stack(
                                children: <Widget>[
                                  Positioned(
                                    child: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            fontSize = 50;
                                            listView = true;
                                          });
                                        },
                                        child: Image.asset(
                                          "assets/images/magnify.png",
                                          scale: 4,
                                        )),
                                    left: -30,
                                  ),
                                ],
                                overflow: Overflow.visible,
                              )),
                          AutoSizeText(
                            "The Americas +1",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                            minFontSize: 24,
                            maxFontSize: 28,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Expanded(
                              child: AutoSizeText(textFiles[0],
                                  textAlign: TextAlign.justify,
                                  presetFontSizes: presetFonts))
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: AutoSizeText(textFiles[1],
                          textAlign: TextAlign.justify,
                          presetFontSizes: presetFonts),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            flex: 1,
                            child: AutoSizeText(
                              textFiles[2],
                              presetFontSizes: presetFonts,
                              textAlign: TextAlign.justify,
                            )),
                        Expanded(
                          flex: 6,
                          child: AutoSizeText(
                            textFiles[3],
                            textAlign: TextAlign.left,
                            presetFontSizes: presetFonts,
                          ),
                        )
                      ],
                    ),
                  ))
                ],
              ),
      ),
    );
  }
}
