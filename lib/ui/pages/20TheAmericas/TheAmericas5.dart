import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class TheAmericas5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 6,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/the_americas4.jpg",)));
                },
                child: Container(
                  padding: EdgeInsets.only(top: 30),
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                      fit: BoxFit.contain,
                      child: Column(
                        children: <Widget>[
                          // SizedBox(height: 5, width: getWidth()),  //加一个默认宽高的组件就好了， 不谢！看不懂中文就翻译去吧
                          Image.asset("assets/images/the_americas4.jpg"),
                          // Image.asset("assets/images/china.png")
                        ],
                      )),
                ),
              ),
            ),
            Column(
              children: <Widget>[
                AutoSizeText(
                  "Brazilian Abstract, 1984",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: "MyriadPro-BoldIt"),
                  presetFontSizes: presetFonts,
                ),
                AutoSizeText(
                  "Domingo Terciliano Jr.",
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  "Oil on canvas",
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  "15 ¼ x 23 ¼ in.",
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height / 2,
                              width: MediaQuery.of(context).size.width / 2,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        AutoSizeText(
                                          "Domingo Terciliano, Jr. (1939– )",
                                          presetFontSizes: presetFonts,
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontFamily: "Auto2-Regular",
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Expanded(
                                            child: AutoSizeText(
                                          "Terciliano, Jr., of Afro-Brazilian heritage, creates large abstract paintings devoted to the ritual arts of the African diasporic religion Candomblé. He paints the “ineffable qualities of the gods and their spiritual essence, or ase.” He explains, “I try to do work that is truly representative of Black people, not caricatures of Black culture.” Terciliano’s work has evolved in major stylistic ways, but always “celebrates his family, his ancestors, and the generations of Afro-Brazilians who have kept the gods of Africa alive and well in the hearts and minds of Brazilians today”",
                                          presetFontSizes: presetFonts,
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              fontFamily: "Auto2-Regular",
                                          ),
                                        ))
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.clear,
                                        color: Colors.black,
                                        size: 30,
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                    top: 10,
                                    right: 10,
                                  )
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  child: Container(
                      height: 80,
                      width: 80,
                      child: Image.asset(
                        "assets/images/bio.png",
                        scale: 3,
                      )),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
