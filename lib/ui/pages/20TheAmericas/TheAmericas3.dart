import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class TheAmericas3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 7,
              child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/the_americas2.jpg",)));
                  },
                  child: Image.asset("assets/images/the_americas2.jpg")),
            ),
            Column(
              children: <Widget>[
                AutoSizeText(
                  "Middle Street San Pedro Town, Ambergris Caye, 2001",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: "MyriadPro-BoldIt"),
                  presetFontSizes: presetFonts,
                ),
                AutoSizeText(
                  "Eduardo “Papo” Alamilla",
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  "Oil on canvas",
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  "32 x 38 in. ",
                  presetFontSizes: presetFonts,
                  style: TextStyle(
                      fontStyle: FontStyle.italic, fontFamily: "Auto2-Regular"),
                ),
                SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height / 2,
                              width: MediaQuery.of(context).size.width / 2,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        AutoSizeText(
                                          "Eduardo “Papo” Alamilla (1965– )",
                                          presetFontSizes: presetFonts,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontFamily: "MyriadPro-BoldIt",
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Expanded(
                                            child: AutoSizeText(
                                          "Born on the Island of San Pedro, Belize, Eduardo Alamilla, better known as “Papo,” began painting as a young boy. Papo is a humble man, who feels he inherited his grandmother’s painting talent. Papo calls his style “realism”. He depicts history, scenes, and stories of the people, culture, lifestyle, and mood of Belize. Although he never attended art school, he is studied by many artists and passes his ability on to others. Papo is considered the pride of San Pedro, Ambergris Caye, Belize.",
                                          style: TextStyle(
                                              fontFamily: "Auto2-Regular"),
                                          presetFontSizes: presetFonts,
                                          textAlign: TextAlign.justify,
                                        ))
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.clear,
                                        color: Colors.black,
                                        size: 30,
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                    top: 10,
                                    right: 10,
                                  )
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  child: Container(
                      height: 80,
                      width: 80,
                      child: Image.asset(
                        "assets/images/bio.png",
                        scale: 3,
                      )),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
