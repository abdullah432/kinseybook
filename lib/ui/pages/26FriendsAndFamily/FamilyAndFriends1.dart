import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class FamilyAndFriends1 extends StatefulWidget {
  @override
  _FamilyAndFriends1State createState() => _FamilyAndFriends1State();
}

class _FamilyAndFriends1State extends State<FamilyAndFriends1> {
  final textFiles = [
    "\nOur gratitude and warmest appreciation is extended to the many individuals who have been generous with advice, support, and love. You are our strongest supporters and have been with us on this journey from the beginning.",
    "\nAndrea Adams\nDr. James Ammons\nCarmen & Kent Amos\nCarl Anderson *\nSandra Dee Anderson\nJacqueline & Clarence Avant\nChucha Barber\nSherry & Tom Barrat\nCongresswoman Karen Bass\nPhoebe Beasley & Don Alberti\nDeeDee & Bob Billingslea\nBishop Charles & Mae Blake\nBeatrice & Andrea Bossi\nSylvia Wynette Bradwell\nBlaun Eva & Hiram Brewton\nEula & Charles Broadnax\nDarrell Brown\nDoris T. & Reginald L. Brown Jr.\nAnne H. & Wren T. Brown\nLonnie G. Bunch III\nLaphonza Butler\nReuben Cannon\nReginald Carey\nTrish Cerrone\nJohn Clayton\nMarian & Ted Craver",
    "\nDiane Neely Curtright\nDerrick Delaney\nMoneese de Lara\nSharon Drake\nEd Dwight\nDr. James Eaton *\nWillis Edwards\nSandra Evers-Manley\nJudy Pace Flood\nChristopher Floyd\nDr. William P. Foster *\nSheila Frazier\nCassandra F. Freeman\nLisa Frison\nBrenda & Bill Galloway\nGeorge Gibbs\nAlonzetta Tinker Gibson\nRichard E Glover\nDanny Godde\nRosalie Gordon-Mills *\nArnold Hackett\nThelma & Terry Harris\nRichard Hartnack\nDonald Henderson\nStan Henderson\nDr. Mary Jane Hewitt\nDr. James Hill\nRosalie Hamm Hines\nCarol Jackson\nDierdre & Russell Jackson\nTaydra Mitchell Jackson\nBenjamin Todd Jealous\n",
    "\nGrace Jenkins\nLenita & Ronald Joe\nBarbara Richardson Johnson\nJulie & Joseph Johnson\nStella & Harry Jones\nAlitash Kebede\nJohn Kennedy\nAllison V. Kinsey\nKhalil B. Kinsey\nLinda M. Kinsey\nPenelope J. & Bradshaw P. Kinsey\nSheryl C. Kinsey\nCassandra Kinsey-Scott & Richard Scott \nArtis Lane\nMattie & Michael Lawson\nCongresswoman Barbara Lee\nDr. Samella Lewis\nBishop Wallace E. Lockett *\nJohn E. Martin\nNoel Massie\nFaye McClure\nChery Mc Donald\nKisten McGowan-Sims\nIrv Miller\nT. Damian Mitchell\nThomas L. Mitchell\nRev. Cecil L. Murray\nNaomi Nelson\nAdrienne Newsom\nEugenia Nicholas\nDanita Patterson\nKwaku Person-Lynn\n"
  ];
  final fonts = [
    "Auto2-Regular",
    "Auto2-Regular",
    "Auto2-Regular",
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(left: 20, right: 20, top: 50),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: AutoSizeText(
                        "Family and Friends",
                        presetFontSizes: [24, 20, 18, 14, 8],
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: AutoSizeText(
                        textFiles[0],
                        presetFontSizes: [24, 20, 18, 14, 8],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: AutoSizeText(
                        textFiles[1],
                        presetFontSizes: [24, 20, 18, 14, 8],
                        textAlign: TextAlign.left,
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8, top: 40),
                  child: AutoSizeText(
                    textFiles[2],
                    presetFontSizes: [24, 20, 18, 14, 8],
                    textAlign: TextAlign.left,
                  ),
                )),
            Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8, top: 40),
                  child: AutoSizeText(
                    textFiles[3],
                    presetFontSizes: [24, 20, 18, 14, 8],
                    textAlign: TextAlign.left,
                  ),
                ))
          ],
        ));
  }
}
