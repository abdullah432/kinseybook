import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class FamilyAndFriends2 extends StatefulWidget {
  String name = "family1";
  @override
  _FamilyAndFriends2State createState() => _FamilyAndFriends2State();
}

class _FamilyAndFriends2State extends State<FamilyAndFriends2> {
  final textFiles = [
    "Ruth Plummer\nGwen Plummer-Sermon\nThe Albertha & Eddie Pooler Family\nJulieanna Richardson\nLaura Richardson\nDr. Larry Rivers\nEvelyn Robertson\nMichael Rouse\nEugene Ruffin\nKatrina & Christopher Schauble\nGilbert & Brenda Scott\nBridget Sermon\nAngelia & Robert Singleton\nDawnyelle Singleton\nCarmen Smith\nLisa Stein\nAlbert Stiles\nDawn Sutherland\nLyn & David Talbert\nDavid Thomas\nNancy & Larry Thompson\nMichelle Thornhill\nPhyllis & Kevin Toney\nPaulette H. & Walter Tucker\nPamela & Nicholas Walker\nCongresswoman Diane Watson\nJames A. Webster *\nEric D. White\nDr. Julian White\nKelley White-Riles\nTerry & Bill Whitaker\nAlfred Williams\nGloria & Michael Williams\nShawn Williams-Kinsey",
    "Belinda Wilson\nAngela Robinson Witherspoon\nCarolene J. & Harry Wood\nMia W. Wright\nNoah Wright\n\n* Deceased\n",
  ];
  final fonts = [
    "Auto2-Regular",
    "Auto2-Regular",
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 50),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                    child: AutoSizeText(
                  textFiles[0],
                  textAlign: TextAlign.left,
                        presetFontSizes: [24, 20, 18, 14, 8],
                  style: TextStyle(fontFamily: fonts[0]),
                )),
                Flexible(
                  flex:1,
                    child: AutoSizeText(textFiles[1],
                        textAlign: TextAlign.left,
                        presetFontSizes: [24, 20, 18, 14, 8],
                        style: TextStyle(fontFamily: fonts[1]))),
                Expanded(
                  flex: 1,
                  child: Container(),
                )
              ],
            )));
  }
}
