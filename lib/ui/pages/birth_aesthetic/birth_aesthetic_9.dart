import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class BirthAestheticNine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(children: <Widget>[

              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText("200 Year Old Tree, 2004",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                    AutoSizeText("Edward Loper",style: TextStyle(fontSize: 24),),
                    AutoSizeText("Oil on canvas",style: TextStyle(fontSize: 24),),
                    AutoSizeText("30 x 36 in.",style: TextStyle(fontSize: 24),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("Ed Loper (1916–2011 )",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("At 20 years of age, Loper began working for the Index of American Design of the Works Progress Administration. Albert Barnes invited him to take classes at the Barnes Foundation in Merion, Pennsylvania, and he subsequently became a full-time artist and art teacher. His technique of “fractured realism” closely resembles figurative abstraction; his boldly colored images look as if refracted through a prism. Loper recently donated his personal papers to the Historical Society of Delaware’s Research Library. His paintings can be found in many private collections and in the permanent collections of the Delaware Art Museum, the Philadelphia Museum of Art, the Corcoran Gallery of Art, the Museum of American Art at the Pennsylvania Academy of Fine Arts, and Howard University Gallery of Art.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_9.jpg",)));
                    },
                    child: Image.asset("assets/images/birth_aesthetic_9.jpg")),
              ),
            ],)
      ),
    );
  }
}
