import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class BirthAestheticFourteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Row(
        children: <Widget>[
          Expanded(flex: 2,child:GestureDetector(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_14.jpg",)));
            },
            child: Container(
            height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: FittedBox(
      fit: BoxFit.contain,
      child: Image.asset("assets/images/birth_aesthetic_14.jpg"),
      ),
      ),
          ),),
          Expanded(flex: 1,
          child: Column(children: <Widget>[
            Expanded(child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AutoSizeText("The Couple, 1940",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                AutoSizeText("Charles White",style: TextStyle(fontSize: 24),),
                AutoSizeText("Ink on paper",style: TextStyle(fontSize: 24),),
                AutoSizeText("18 x 14 in.",style: TextStyle(fontSize: 24),),
                SizedBox(height: 20,),
                GestureDetector(
                  onTap: (){
                    showDialog(context: context,builder: (BuildContext context){
                      return Dialog(
                        child: Container(
                          height: MediaQuery.of(context).size.height/1.5,
                          width: MediaQuery.of(context).size.width/1.5,
                          child: Stack(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(height: 30,),
                                    AutoSizeText("Charles White (1918–1979)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                    SizedBox(height: 20,),
                                    Expanded(child: AutoSizeText("White, born in Chicago, began his career at the Chicago Community Arts Center before receiving a full scholarship to the school of the Art Institute. In 1939, he worked as a mural painter for the Illinois Federal Arts Project, and later studied at the Art Students League in New York. White made prints at the renowned graphics workshop Taller de Grafica in Mexico City, where he lived for two years. After serving as artist in residence at Howard University, he moved to New York and worked with the Graphic Workshop. He said of his art, “I am a Negro in America. I relate to images that are meaningful to me, images that are closest to me. I use that as a springboard to deal with the more broad and the more all-encompassing.” White was awarded two Rosenwald fellowships, and in 1972 was elected a member of the National Academy of Design. He taught at the Otis Art Institute in Los Angeles from 1965 until his death.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                  ],
                                ),
                              ),
                              Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                            ],
                          ),
                        ),
                      );
                    });
                  },
                  child: Container(
                      height: 80,
                      width: 80,
                      child: Image.asset("assets/images/bio.png",scale: 3,)),
                )
              ],
            )),
            Expanded(child: GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_14_1.jpg",)));
              },
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image.asset("assets/images/birth_aesthetic_14_1.jpg"),
                ),
              ),
            ),)
          ],),),
          Expanded(child: Column(
            children: <Widget>[
              Expanded(child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_14_2.jpg",)));
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Image.asset("assets/images/birth_aesthetic_14_2.jpg"),
                  ),
                ),
              ),),
              Expanded(child: Column(
                children: <Widget>[
                  AutoSizeText("Charles White, 1943",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize:24),),
                  AutoSizeText("Gordon Parks",style: TextStyle(fontSize: 22),),
                  AutoSizeText("Gelatin silver print",style: TextStyle(fontSize: 22),),
                  AutoSizeText("8 x 10 in.",style: TextStyle(fontSize: 22),),
                ],
              ),),
              Expanded(child: Column(
                children: <Widget>[
                  AutoSizeText("The Dreamer, a Portrait of\nDorothy Dandridge, 1951",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24,),maxLines: 2,),
                  AutoSizeText("Charles White",style: TextStyle(fontSize: 22),),
                  AutoSizeText("Ink on paper",style: TextStyle(fontSize: 22),),
                  AutoSizeText("16 1⁄2 x 24 in.",style: TextStyle(fontSize: 22),),
                ],
              ),)
            ],
          ),)
        ],
      ),
    );
  }
}
