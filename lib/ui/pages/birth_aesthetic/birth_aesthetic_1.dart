import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class BirthAestheticOne extends StatefulWidget {
  @override
  _BirthAestheticOneState createState() => _BirthAestheticOneState();
}

class _BirthAestheticOneState extends State<BirthAestheticOne> {
  final textFiles=["\n\nThe power of the Kinsey Collection lies in the representation of cultural cross-pollination. The most visually compelling era for these influences was the early 20th century, when a movement was underway in Harlem, at Howard University and Dunbar High in Washington, D.C., and in urban centers throughout the United States. Artists, writers, poets, actors, musicians, and scholars came together to immerse themselves in a new discourse on identity, and a black renaissance was born. The Harlem Renaissance centered on the idea that black artists should not have to follow the canon of western art, literature, and music, but could establish their own aesthetic, interpreting the world around them in a uniquely African American way.\n\nDuring this time, many artists and scholars, among them W. E. B. DuBois, Alain Locke, and Carter G. Woodson, who had an assertive and celebratory approach to discussing and promulgating black culture, began to call for a more visible and more definitive voice. Many artists answered the call, and then reached out to the younger generation to ensure that the momentum of the movement was sustainable.",
    "They became teachers—notably among them Aaron Douglas and Augusta Savage, who helped spread a new black paradigm across the country and around the world.\n\nBernard Kinsey is extremely interested in Americans and others who recognized black brilliance. While the birth of a profound black aesthetic was supported by many, one man in particular piques Kinsey’s interest: Julius Rosenwald. In 1917, Rosenwald, President of Sears, Roebuck and Co., established the Julius Rosenwald Fund to support the most pressing issues of the day: racial inequality and black mobility. He donated over \$60 million to African American social, educational, and cultural initiatives. From 1928 to 1948, the fund awarded stipends to hundreds of prominent and emerging African American artists, writers, and scholars, including DuBois, Ralph Ellison, Zora Neale Hurston, Elizabeth Catlett, Eldzier Cortor, Aaron Douglas, Katherine Dunham, Jacob Lawrence, Gordon Parks, Marion Perkins, Augusta Savage, Charles White, and Hale Woodruff.\n\nAnother major, though unlikely, supporter of the Harlem Renaissance was a white real estate developer, William Harmon. He established the Harmon Foundation to celebrate black achievement in the arts and sciences. The foundation sponsored competitions to encourage artists nationwide to join those in Harlem who were creating this new, strong",
    "vision of black identity.\n\nOne Harmon Foundation winner was Hale Woodruff, the first African American to attend Indiana University’s prestigious Herron Art School. Other foundation award recipients include Palmer Hayden, Charles Alston, and Aaron Douglas. The Kinseys collect these renowned artists, as well as the Harmon Foundation catalogues that played such an important role in the artists’ national and international success. They view these documents as a testament to their own goal of cultivation: recognizing greatness, fostering and sharing it with people who want to broaden their knowledge and understanding of the world around them.\n\nOthers involved in the production and promotion of the African American artistic tradition worked at a grass roots level to ensure that artists had venues to present their work. James Herring, founder of Howard University’s department of art, and Alonzo Aden, curator of the university’s art gallery, founded a private gallery in their Washington, D.C., home, collecting works by African American artists who are featured in the Kinsey Collection. Herring and Aden supported the artists who inspired a movement, and they built the first African American gallery, which became the Barnett-Aden Collection."
  ];

  bool listView=false;

  double fontSize=50;

  ScrollController listViewController=ScrollController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: listView?Stack(
        children: <Widget>[
          ListView.builder(
              controller: listViewController,
              itemCount: textFiles.length,
              itemBuilder: (BuildContext context,index){
                if(index==0){
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: Column(
                      children: <Widget>[
                        AutoSizeText.rich(
                          TextSpan(
                            children: [
                              TextSpan(text: "The Birth of an Aesthetic\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                              TextSpan(text: "The pulse of the Negro world has begun to beat in Harlem.\n",),
                            ],

                          ),
                          presetFontSizes: [fontSize],
                          textAlign: TextAlign.justify,
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: AutoSizeText("— Alain Locke",minFontSize: fontSize,),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                          child: AutoSizeText(textFiles[0],minFontSize: fontSize,textAlign: TextAlign.justify,),
                        )
                      ],
                    )
                  );
                }
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                  child: AutoSizeText(textFiles[index],minFontSize: fontSize,textAlign: TextAlign.justify),
                );
              }),
          Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                height: MediaQuery.of(context).size.height*0.1,
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(onPressed: (){

                      if(listViewController.position.minScrollExtent==listViewController.offset){
                        print("on the end");
                      }
                      else{
                        listViewController.animateTo(listViewController.offset-listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                      }
                    },icon: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 40,),),
                    Row(children: <Widget>[
                      IconButton(icon: Icon(Icons.zoom_out,color: Colors.blue,size: 40,), onPressed: (){
                        setState(() {
                          fontSize=30;
                        });
                      }),
                      IconButton(icon: Icon(Icons.close,color: Colors.blue,size: 40,), onPressed: (){
                        setState(() {
                          listView=false;
                        });
                      })
                    ],),
                    IconButton(icon: Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 40,),onPressed: (){
                      if(listViewController.position.maxScrollExtent==listViewController.offset){
                        print("on the end");
                      }
                      else{
                        listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                      }
                    },)
                  ],),
              ))
        ],
      ):Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 30,),
                Container(width:100,height: 50,child: Stack(children: <Widget>[Positioned(child: GestureDetector(
                    onTap: (){
                      setState(() {
                        fontSize=50;
                        listView=true;
                      });
                    },
                    child: Image.asset("assets/images/magnify.png",scale: 4,)),left: -30,),],overflow: Overflow.visible,)),
                AutoSizeText.rich(
                  TextSpan(
                    children: [
                      TextSpan(text: "The Birth of an Aesthetic\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20)),
                      TextSpan(text: "The pulse of the Negro world has begun to beat in Harlem.\n",),
                    ],

                  ),
                  textAlign: TextAlign.justify,
                  presetFontSizes: [22,21,20,19,18,17,16,14],
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: AutoSizeText("— Alain Locke",presetFontSizes: [22,21,20,19,18,17,16,14],),
                ),
                Expanded(child: AutoSizeText(textFiles[0],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,))
              ],
            ),
          ),),
          Expanded(child:Padding(
            padding: const EdgeInsets.all(8.0),
            child: AutoSizeText(textFiles[1],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
          )),
          Expanded(child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 10),
            child: AutoSizeText(textFiles[2],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),

          ))
        ],
      ),
    );
  }
}
