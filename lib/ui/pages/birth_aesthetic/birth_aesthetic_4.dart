import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';


class BirthAestheticFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
        Expanded(
          flex: 3,
          child: GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_4.jpg",)));
              },
              child: Image.asset("assets/images/birth_aesthetic_4.jpg")),
        ),
        Flexible(
            flex: 1,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
              SizedBox(height: 30,),
              AutoSizeText("Westchester County, 1935",style: TextStyle(fontWeight: FontWeight.bold),maxLines: 1,
                presetFontSizes:[22,20],textAlign: TextAlign.center,),
              AutoSizeText("Palmer Haydene",presetFontSizes: [22,20],textAlign: TextAlign.center,),
              AutoSizeText("Oil on canvas",presetFontSizes: [22,20],textAlign: TextAlign.center,),
              AutoSizeText("18 x 24 in.",presetFontSizes: [22,20],textAlign: TextAlign.center,),
            ],)),
        SizedBox(height: 20,),
        GestureDetector(
          onTap: (){
            showDialog(context: context,builder: (BuildContext context){
              return Dialog(
                child: Container(
                  height: MediaQuery.of(context).size.height/1.5,
                  width: MediaQuery.of(context).size.width/1.5,
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 30,),
                            AutoSizeText("Palmer Hayden (1890–1973)",style: TextStyle(color: Colors.black,fontSize: 24,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                            SizedBox(height: 20,),
                            Expanded(child: AutoSizeText("Born Peyton Cole Hedgeman in Wide Water, Virginia, Hayden served in the military from 1914 to 1919. In 1925, he began studying with Asa G. Randall at the art colony in Boothbay, Maine. He won a Harmon Foundation Gold Award in 1926 for a Maine seascape. In Paris subsequently, he experimented with African motifs and black American subjects in his work. He then returned to New York, and in 1933 Fetiche et Fleurs, a work produced in Paris, was shown in the Exhibition of the Works of Negro Artists at the Harmon Foundation, and won the Mrs. John D. Rockefeller Prize. Hayden worked for the WPA from 1934 to 1938, producing a series of paintings depicting the legendary John Henry. Many of his works shed light on the experiences of African Americans in the rural South, including Hog Killing Time in Virginia. In 1973, Hayden was commissioned to paint a series on black American soldiers through a fellowship from the Creative Artist Public Service Program of New York, but he died before he could begin work on it.",presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,))
                          ],
                        ),
                      ),
                      Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                    ],
                  ),
                ),
              );
            });
          },
          child: Container(
              height: 80,
              width: 80,
              child: Image.asset("assets/images/bio.png",scale: 3,)),
        ),
        SizedBox(height: 30,)
      ],),
    );
  }
}
