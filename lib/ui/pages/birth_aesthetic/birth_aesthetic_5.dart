import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class BirthAestheticFive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child:  Row(children: <Widget>[
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText("Ping Pong, 1934",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),),
                    AutoSizeText("Allan Rohan Crite",style: TextStyle(fontSize: 22),),
                    AutoSizeText("Graphite on paper",style: TextStyle(fontSize: 22),),
                    AutoSizeText("21 1⁄2 x 19 in.",style: TextStyle(fontSize: 22),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("Allan Rohan Crite (1910–2007)",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Crite’s mother, Annamae, was a poet who encouraged her son to draw. He studied at the school of the Museum of Fine Arts in Boston and later attended Harvard University Extension. His drawings and paintings depict religious themes, African American daily life, and street scenes of his Boston neighborhood. In 1936, his work was exhibited at the Museum of Modern Art in New York and at Harvard University’s Fogg Museum. Other galleries and museums that featured major exhibitions of his work include the Harmon Foundation and the Corcoran Gallery of Art. Crite’s work can be found in the permanent collections of the Boston Museum of Fine Arts, the Boston Public Library, the Library of the Boston Athenaeum, the Museum of the National Center of Afro-American Artists, the Phillips Collection, the Smithsonian Institution, and the Art Institute of Chicago.",presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_5.jpg",)));
                    },
                    child: Image.asset("assets/images/birth_aesthetic_5.jpg",fit: BoxFit.fitHeight,)),
              ),
          ],
        ),
      ),
    );
  }
}
