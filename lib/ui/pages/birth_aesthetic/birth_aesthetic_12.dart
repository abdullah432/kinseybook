import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class BirthAestheticTwelve extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(children: <Widget>[
              Expanded(
                flex: 2,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_12.jpg",)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/birth_aesthetic_12.jpg"),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText("African Women, 1942",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                    AutoSizeText("Eldzier Cortor",style: TextStyle(fontSize: 24),),
                    AutoSizeText("Pen and ink on woven paper",style: TextStyle(fontSize: 24),),
                    AutoSizeText("16 x 11 1⁄2 in.",style: TextStyle(fontSize: 24),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("Eldzier Cortor (1916–2015 )",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Cortor grew up on the south side of Chicago and in 1935 attended drawing classes at the Art Institute. In 1940, he worked for the Works Projects Administration in Chicago with Charles Sebree, Charles White, and Bernard Goss. In the 1940s, he studied woodblock techniques at Columbia University. He was very interested in African art and explored the residual cultural elements of African origins in Georgia’s Sea Islands. He was awarded a Rosenwald Fellowship and later, a Guggenheim, which underwrote his study of African artistic and cultural influences in Haiti, Cuba, and Jamaica. African American women are the thematic focus of Cortor’s work because he believed the black woman is the essential spirit of the race, conveying “a feeling of eternity and continuance of life.” Although Cortor is best known for his early work, in the 1970s his art enjoyed resurgent interest, shown in major exhibitions at the Boston Museum of Fine Arts, the Studio Museum of Harlem, and the National Center of Afro-American Artists in Boston.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    )
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
