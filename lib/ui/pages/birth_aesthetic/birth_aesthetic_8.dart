import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';

class BirthAestheticEight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(children: <Widget>[
              Expanded(
                flex: 2,
                child: GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_8.jpg",)));
                    },
                    child: Image.asset("assets/images/birth_aesthetic_8.jpg",fit: BoxFit.fitHeight,)),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText("Portrait of Girl, 1940",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                    AutoSizeText("Charles Alston",style: TextStyle(fontSize: 24),),
                    AutoSizeText("Oil on canvas",style: TextStyle(fontSize: 24),),
                    AutoSizeText("28 1⁄2 x 24 3⁄4 in.",style: TextStyle(fontSize: 24),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("Charles Alston (1907–1977)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Alston earned bachelor’s and master’s degrees from Columbia University. Like many of his contemporaries, including Augusta Savage, he was both artist and educator, teaching many of the most talented African American artists of the 20th century, such as Jacob Lawrence and Romare Bearden. Alston was influenced by ritual African art as well as Mexican muralists Diego Rivera and Jose Clemente Orozco, who combined art with social activism. This influence is evidenced in Alston’s larger- than-life murals depicting Harlem Hospital. Alston was one of the first black supervisors for the Works Projects Administration and the first African American instructor at the Art Students League in New York; he also taught at City College from 1970 to 1977. His work is represented in the permanent collections of the Metropolitan Museum of Art and the Whitney Museum of American Art.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    ),
                    SizedBox(height: 40,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: AudioViewer(url:"assets/audio/birth_aesthetic_8.mp3"),
                    ),
                    SizedBox(height: 20,),
                    IconButton(
                      icon: Icon(
                        Icons.play_circle_outline,
                        color: Colors.black,
                      ),
                      iconSize: 80,
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VideoView(
                                  videoLink:
                                  "assets/videos/birth_aesthetic_8.MP4",
                                )));
                      },
                    )
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
