import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class BirthAestheticTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Column(children: <Widget>[
        Expanded(
          flex: 2,
          child: GestureDetector(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_2.jpg",)));
            },
            child: Container(
                height: MediaQuery.of(context).size.height*0.8,
                width: MediaQuery.of(context).size.width,
                child: FittedBox(
                    fit: BoxFit.contain,
                    child: Image.asset("assets/images/birth_aesthetic_2.jpg"))),
          ),
        ),
        Expanded(
          flex: 1,
            child: Column(children: <Widget>[
              SizedBox(height: 30,),
              AutoSizeText("The Negro in Art, 1941",style: TextStyle(fontWeight: FontWeight.bold),maxLines: 1,
                presetFontSizes:[24,22,20],textAlign: TextAlign.center,),
              AutoSizeText("Alain Locke",presetFontSizes: [22,20],textAlign: TextAlign.center,),
              AutoSizeText("Book",presetFontSizes: [22,20],textAlign: TextAlign.center,),
              AutoSizeText("12 1⁄4 x 9 1⁄4 x 1 in.",presetFontSizes: [22,20],textAlign: TextAlign.center,),
        ],))
      ],),
    );
  }
}
