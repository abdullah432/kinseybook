import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';


class BirthAestheticThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_3.jpg",)));
              },
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width*0.7,
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image.asset("assets/images/birth_aesthetic_3.jpg"),
                ),
              ),
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40,vertical: 20),
                  child: Column(
                    children: <Widget>[
                      AutoSizeText("Exhibition of the Work of Negro Artists,\n1931",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30,),maxLines: 2,textAlign: TextAlign.center,),
                      AutoSizeText("Harmon Foundation",presetFontSizes: [24,22,20,18],maxLines: 1,),
                      AutoSizeText("Exhibition catalogue",presetFontSizes: [24,22,20,18],maxLines: 1,),
                      AutoSizeText("8 x 5 in.",presetFontSizes: [24,22,20,18],maxLines: 1,),
                    ],
                  ),
                ),),
              Expanded(flex: 1,child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40,vertical: 20),
                child: Column(
                  children: <Widget>[
                    AutoSizeText("Negro Artists: An Illustrated Review of\nTheir Achievements, 1935",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30,),maxLines: 2,textAlign: TextAlign.center,),
                    AutoSizeText("Harmon Foundation",presetFontSizes: [24,22,20,18],maxLines: 1,),
                    AutoSizeText("Exhibition catalogue",presetFontSizes: [24,22,20,18],maxLines: 1,),
                    AutoSizeText("8 x 5 in.",presetFontSizes: [24,22,20,18],maxLines: 1,),

                  ],
                ),
              ),)
            ],
          ),
          Expanded(
            flex: 2,
            child:
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.2,vertical: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      AutoSizeText("Exhibition of the Productions by Negro Artists,\n1933",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30,),maxLines: 2,textAlign: TextAlign.center,),
                      AutoSizeText("Harmon Foundation",presetFontSizes: [22,20,18],maxLines: 1,),
                      AutoSizeText("Exhibition catalogue",presetFontSizes: [22,20,18],maxLines: 1,),
                      AutoSizeText("8 x 5 in.",presetFontSizes: [22,20,18],maxLines: 1,),
                      AudioViewer(url:"assets/audio/birth_aesthetic_3.mp3"),
                      SizedBox(height: 20,),
                    ],
                  ),
                ),
          )
        ],
      ),
    );
  }
}
