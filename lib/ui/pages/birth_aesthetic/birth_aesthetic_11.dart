import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class BirthAestheticEleven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_11.jpg",)));
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Image.asset("assets/images/birth_aesthetic_11.jpg"),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                children: <Widget>[
                  AutoSizeText("The Farm House, 1942",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                  AutoSizeText("Dox Thrash",style: TextStyle(fontSize: 24),),
                  AutoSizeText("Pen and ink on paper",style: TextStyle(fontSize: 24),),
                  AutoSizeText("9 x 6 in.",style: TextStyle(fontSize: 24),),
                  SizedBox(height: 20,),
                  GestureDetector(
                    onTap: (){
                      showDialog(context: context,builder: (BuildContext context){
                        return Dialog(
                          child: Container(
                            height: MediaQuery.of(context).size.height/1.5,
                            width: MediaQuery.of(context).size.width/1.5,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                  child: Column(
                                    children: <Widget>[
                                      SizedBox(height: 30,),
                                      AutoSizeText("Dox Thrash (1892–1965)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                      SizedBox(height: 20,),
                                      Expanded(child: AutoSizeText("Thrash, born in Georgia, left at age 15 in the Great Migration to find work in Chicago. He worked as a janitor by day and attended classes at the Art Institute at night. In 1917, he joined the army and was assigned to the 365th Infantry Regiment, 183rd Brigade, 92nd Division, famously known as the “Buffalo Soldiers.” After the war, he returned to Chicago but traveled extensively, finally settling in Philadelphia, where he worked for the Federal Art Project from 1936 to 1939. Working with artists Michael Gallagher and Hubert Mesibov, Thrash invented the process of carborundum mezzotint, a revolutionary technique that employed carborundum instead of tools or other materials to etch copper plates. The grainy finish allowed Thrash to create provocative images, which he employed to depict scenes of African American life. Thrash made this process his primary medium and through it produced his greatest works. In 2002, the Philadelphia Museum of Art mounted a major retrospective exhibit featuring over one hundred of his drawings, watercolors, and prints.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                    ],
                                  ),
                                ),
                                Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                              ],
                            ),
                          ),
                        );
                      });
                    },
                    child: Container(
                        height: 80,
                        width: 80,
                        child: Image.asset("assets/images/bio.png",scale: 3,)),
                  )
                ],
              ),
            ),


          ],
        ),
      ),
    );
  }
}
