import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class BirthAestheticTen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_10.jpg",)));
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Image.asset("assets/images/birth_aesthetic_10.jpg"),
                  ),
                ),
              ),
            ),
              Expanded(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    AutoSizeText("River Landscape, 1940",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                    AutoSizeText("John Wesley Hardwick",style: TextStyle(fontSize: 24),),
                    AutoSizeText("Oil on board",style: TextStyle(fontSize: 24),),
                    AutoSizeText("24 x 34 in.",style: TextStyle(fontSize: 24),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("John Wesley Hardwick (1891–1968)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Hardwick first exhibited his art at the Negro Business League convention and the Indiana State Fair in 1904 when he was only 13 years old. Later he attended Herron School of Art in Indianapolis and studied with William Forsyth. In the early 1920s, Hardwick shared a studio with Hale Woodruff, and their work was exhibited together at the Art Institute of Chicago. In 1927, Hardwick received a bronze award from the Harmon Foundation for his painting Portrait of a Young Girl. He continued to receive support from the Harmon Foundation for five more years, and his work was exhibited in museums around the country, including the Indianapolis Museum of Art and the Smithsonian. In 1934, Hardwick was awarded a Works Projects Administration commission to paint a mural for Crispus Attucks High School in Indianapolis. The mural, Workers, portrayed three African American foundry workers pouring molten metal; it was never mounted, however.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    )
                  ],
                ),
              ),


          ],
        ),
      ),
    );
  }
}
