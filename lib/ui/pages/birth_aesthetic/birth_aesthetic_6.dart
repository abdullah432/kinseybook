import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class BirthAestheticSix extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(children: <Widget>[
              Expanded(
                flex: 2,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_6.jpg",)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/birth_aesthetic_6.jpg"),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText("Boy’s Head, 1931",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                    AutoSizeText("James Lesesne Wells",style: TextStyle(fontSize: 24),),
                    AutoSizeText("Linocut",style: TextStyle(fontSize: 24),),
                    AutoSizeText("19 1⁄4 x 15 1⁄2 in.",style: TextStyle(fontSize: 24),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("James Lesesne Wells (1902–1993)",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Wells earned bachelor’s and master’s degrees from Columbia University, and studied at the National Academy of Design under George Laurence Nelson. In 1929, he joined the faculty at Howard University as a crafts teacher, where he created the graphics art department. His innovative approach to lithography and woodcut printmaking was driven by his desire to facilitate appreciation of the arts by ordinary people. His work often focused on religious themes, reflecting his upbringing as the son of a Baptist minister, and he was often inspired by African and cubist design. In 1931, Wells won the Harmon Foundation Gold Award for Flight into Egypt, and two years later he was awarded the foundation’s George E. Haynes prize for the best black-and-white woodcut. In 1980, Wells was honored by President Jimmy Carter for achievements throughout his life.",presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    )
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
