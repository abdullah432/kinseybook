import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class BirthAestheticThirteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/birth_aesthetic_13.jpg",)));
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Image.asset("assets/images/birth_aesthetic_13.jpg"),
                  ),
                ),
              ),
            ),
            Column(
              children: <Widget>[
                AutoSizeText("Five Letters, 1942–1943",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                AutoSizeText("Zora Neale Hurston"),
                AutoSizeText("Ink on paper"),
                AutoSizeText("Various sizes"),
                SizedBox(height: 20,),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.2),
                  child: AudioViewer(url:"assets/audio/birth_aesthetic_13.mp3"),
                ),
              ],
            ),
            Expanded(child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: <Widget>[
                  Expanded(child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: AutoSizeText("Shirley Kinsey discovered these letters among her uncle’s papers after his death in 1993. James A. Webster, her uncle, had befriended many artists and scholars of the Harlem Renaissance while he was a student at Columbia University. He established a deep friendship with the writer and anthropologist Zora Neale Hurston, who was also from Florida. They continued their friendship after university, spending time",textAlign: TextAlign.justify,presetFontSizes:[22,21,20,19,18,17,16,14])
                  ),),
                  Expanded(child:Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AutoSizeText("together in St. Augustine, where Webster was a professor at Florida Memorial College. In 1960, a St. Augustine elementary school was named in honor of his contributions to teaching. Hurston was raised in Eatonville, Florida, one of the first all-black towns to be formed after the Emancipation Proclamation in 1863, and in 1887 it was the first such town to be incorporated. Her novel Their Eyes Were",presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
                  )),
                  Expanded(child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AutoSizeText("Watching God (1937) provides a brief overview of the founding of the town through the eyes of Janie Crawford, the main character.\n\nThe Kinseys have two other Hurston books in their collection, Jonah‘s Gourd Vine and Tell My Horse. The later is signed and inscribed to Shirley’s uncle, “To “Dick” Webster With that old–time feeling.”",presetFontSizes:[22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
                  ))
                ],
              ),
            ),)

          ],
        ),
      ),
    );
  }
}
