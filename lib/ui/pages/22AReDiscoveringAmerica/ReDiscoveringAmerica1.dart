import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class ReDiscoveringAmerica1 extends StatefulWidget {
  String name = "family1";
  @override
  _ReDiscoveringAmerica1State createState() => _ReDiscoveringAmerica1State();
}

class _ReDiscoveringAmerica1State extends State<ReDiscoveringAmerica1> {
  bool listView = false;

  ScrollController listViewController = ScrollController();

  double fontSize = 50;
  @override
  Widget build(BuildContext context) {
    final textFiles = [
      "\nOur collection began personally, but it became a traveling exhibition in 2006 and has taken us on a journey we could never have imagined. There were nearly thirty exhibits along the way and many milestones, including awards, articles in the press, educational curriculums, and international showings. In 2010, the exhibit enjoyed a seven-month stay at the National Museum of American History, part of the Smithsonian, under the auspices of the National Museum of African American History and Culture. It was said that our exhibit was a preview of the 2016 opening of that museum on the Mall in Washington, D.C.\n\nOur partnership with Walt Disney World began thanks to our friend Bob Billingslea, who visited",
      "the exhibit during its showing at the Norton Museum of Art in West Palm Beach, Florida, in 2008. Bob, an executive at Disney World at the time, decided to bring our collection to the attention of the company, thinking that it was essential that Disney mount an exhibition presenting it. Bob thought that Carmen Smith, vice president of Disney Imagineering, was the one who could make it happen, and Carmen immediately saw the possibilities. On a bimonthly basis over the next five years, meetings were held at our home to work out how best to proceed. At one point, fourteen Disney executives flew in from Orlando to meet with us and to share in what Carmen regularly referred to as “the Kinsey experience.” An official agreement was reached and planning began.\n\nMarch 8, 2013, marked the opening of ",
      "Re-Discovering America: Family Treasures from The Kinsey Collection ",
      "at Epcot, Walt Disney World, in Orlando, Florida. The menu for this event was prepared entirely from recipes in ",
      "Mrs. Fisher Knows Southern Cooking",
      ", the first cookbook by an African American woman, published in 1881. This cookbook is highlighted in the exhibit at Epcot, and serving Mrs. Fisher’s recipes was our way of celebrating her contributions. Her peach cobbler became a permanent favorite on the menu at the American Pavilion restaurant. The exhibit at Epcot contains roughly fifty objects from the larger Kinsey exhibit, which are rotated on a six-month to yearly basis. The objects are grouped by theme—Hope, Courage, Belief, Imagination, and Heritage—and are enhanced by holographic animated lanterns featuring celebrity narrators",
      "such as Whoopie Goldberg, Kerry Washington, Zendaya, Diane Sawyer, and James Pickens. A historical timeline serves as a thread between exhibit cases to contextualize significant eras in American history. Interactive tablets are placed throughout to encourage visitors to dive deeper. It is estimated that 20 million guests from all over the world will experience the Kinsey Collection during its five year tenure at Epcot.\n\n",
      "Re-discovering America: Family Treasures from The Kinsey Collection",
      " marked the first time that Epcot has dedicated a gallery to a private family collection, and the exhibit will be on view longer than any other. We are ever appreciative of Disney Corporation for recognizing the importance of telling American history more accurately, and for sharing the stories of African American achievement with its millions of visitors. The partnership with Disney is one that we will celebrate forever.",
    ];
    final fonts = [
      "Auto2-Regular",
      "Auto2-Regular",
    ];
    List<double> presetFonts = [28, 24, 20, 16, 14];
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
            child: listView
                ? Stack(
                    children: <Widget>[
                      ListView.builder(
                          controller: listViewController,
                          itemCount: textFiles.length,
                          itemBuilder: (BuildContext context, index) {
                            // if (index == 1) {
                            //   return Padding(
                            //       padding: const EdgeInsets.symmetric(
                            //           horizontal: 20, vertical: 20),
                            //       child: AutoSizeText.rich(
                            //         TextSpan(
                            //           text: ""
                            //           // children: [
                            //           //   TextSpan(
                            //           //       text:
                            //           //           "Travel has been a big part of our lives since the beginning of our marriage. Our first trip, to the Grand Canyon, led to further exploration in the southwest that was the beginning of our appreciation for Native American culture. We decided to explore all of the national parks of the West, and soon we realized that we had visited forty-three. We then began to spread our wings, and we visited the other Americas: South and Central America and the Caribbean islands. One of our first trips out of the U.S. was a three-week journey to South America in 1976. There we saw Machu Picchu, Iguazu Falls, Rio de Janeiro, the Amazon, Chile, Columbia, and Argentina. Each time we return home after experiencing other cultures, our desire to discover more about our own is heightened."),
                            //           //   TextSpan(
                            //           //     text:
                            //           //         "Early on we began looking for art on our travels and have discovered some wonderful images. One of our favorite pieces is a painting by Belizean artist Papo that we call Middle Street, San Pedro Town, Ambergris Caye. On a trip to Cuba we were introduced to an artist named Ricardo Losado, who depicts the Cuban landscape and experience, that draws you into the scene. During that same visit to Cuba, we had the opportunity to meet Fidel Castro.",
                            //           //   ),
                            //           //   TextSpan(
                            //           //       text:
                            //           //           "Although this chapter is devoted to the Americas, we wanted to include one of Shirley’s favorite genres of art, Australian Aboriginal dot painting. We have traveled to Australia twice and have fallen in love with the Aboriginal culture and artwork. The Aboriginal story is similar to the story of Native Americans: conquest, massacre, and forced acculturation. But through this long ordeal they have maintained their cultural practices. Aboriginal paintings are based on the myth of the Dreamtime, which dates back over 40,000 years to some of the oldest rock paintings in the world. The piece that we are highlighting is an oil painting, Bush Plum by Colin Bird, which we purchased in 1996. The bush plum, which grows less than three feet high, is used for “smoke medicine” —the ancient holistic medical beliefs practiced by the Aboriginal people that recognizes the social, physical and spiritual dimensions of health and life."),
                            //           //   TextSpan(text: textFiles[3]),
                            //           // ],
                            //         ),
                            //         presetFontSizes:
                            //             listView ? [fontSize] : presetFonts,
                            //         textAlign: TextAlign.justify,
                            //       ));
                            // }
                            return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              child: AutoSizeText(
                                textFiles[index],
                                minFontSize: fontSize,
                                textAlign: TextAlign.justify,
                              ),
                            );
                          }),
                      Positioned(
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.1,
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                IconButton(
                                  onPressed: () {
                                    if (listViewController
                                            .position.minScrollExtent ==
                                        listViewController.offset) {
                                      print("on the end");
                                    } else {
                                      listViewController.animateTo(
                                          listViewController.offset -
                                              listViewController.position
                                                      .maxScrollExtent /
                                                  7,
                                          duration: Duration(milliseconds: 500),
                                          curve: Curves.easeIn);
                                    }
                                  },
                                  icon: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.blue,
                                    size: 40,
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    IconButton(
                                        icon: Icon(
                                          Icons.zoom_out,
                                          color: Colors.blue,
                                          size: 40,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            fontSize = 30;
                                          });
                                        }),
                                    IconButton(
                                        icon: Icon(
                                          Icons.close,
                                          color: Colors.blue,
                                          size: 40,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            listView = false;
                                          });
                                        })
                                  ],
                                ),
                                IconButton(
                                  icon: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.blue,
                                    size: 40,
                                  ),
                                  onPressed: () {
                                    if (listViewController
                                            .position.maxScrollExtent ==
                                        listViewController.offset) {
                                      print("on the end");
                                    } else {
                                      listViewController.animateTo(
                                          listViewController.offset +
                                              listViewController.position
                                                      .maxScrollExtent /
                                                  7,
                                          duration: Duration(milliseconds: 500),
                                          curve: Curves.easeIn);
                                    }
                                  },
                                )
                              ],
                            ),
                          ))
                    ],
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  flex: 2,
                                  child: GestureDetector(
                                    onTap: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/rediscoveringamerica01.jpg",)));
                                    },
                                    child: Image.asset(
                                      "assets/images/rediscoveringamerica01.jpg",
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                                Flexible(
                                  flex: 1,
                                  child: AutoSizeText(
                                    "\nRe-Discovering America:\nFamily Treasures from The Kinsey Collection",
                                    presetFontSizes: presetFonts,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "MyriadPro-Bold"),
                                  ),
                                ),
                                Flexible(
                                    flex: 1,
                                    child: AutoSizeText(
                                      "\nInside the American Heritage Gallery of the American Adventure\nPavilion at Epcot, Walt Disney World, Orlando, Florida\nMarch 2013 – March 2018",
                                      presetFontSizes: presetFonts,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "MyriadPro-Regular"),
                                    )),
                                Container(
                                    width: 30,
                                    height: 30,
                                    // color: Colors.red,
                                    child: Stack(
                                      children: <Widget>[
                                        Positioned(
                                          left: -10,
                                          child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  fontSize = 50;
                                                  listView = true;
                                                });
                                              },
                                              child: Image.asset(
                                                "assets/images/magnify2.png",
                                                scale: 6,
                                              )),
                                        ),
                                      ],
                                      overflow: Overflow.visible,
                                    )),
                                Expanded(
                                  flex: 4,
                                  child: AutoSizeText(
                                    textFiles[0],
                                    textAlign: TextAlign.justify,
                                    presetFontSizes: presetFonts,
                                    style: TextStyle(fontFamily: fonts[0]),
                                  ),
                                ),
                              ],
                            ),
                          )),
                      Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: AutoSizeText.rich(
                                TextSpan(children: [
                                  TextSpan(
                                      text: textFiles[1],
                                      style: TextStyle(
                                          fontStyle: FontStyle.normal)),
                                  TextSpan(
                                      text: textFiles[2],
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic)),
                                  TextSpan(
                                      text: textFiles[3],
                                      style: TextStyle(
                                          fontStyle: FontStyle.normal)),
                                  TextSpan(
                                      text: textFiles[4],
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic)),
                                  TextSpan(
                                      text: textFiles[5],
                                      style: TextStyle(
                                          fontStyle: FontStyle.normal))
                                ]),
                                textAlign: TextAlign.justify,
                                presetFontSizes: presetFonts,
                                style: TextStyle(fontFamily: fonts[1])),
                          )),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                flex: 4,
                                child: AutoSizeText.rich(
                                    TextSpan(children: [
                                      TextSpan(
                                          text: textFiles[6],
                                          style: TextStyle(
                                              fontStyle: FontStyle.normal)),
                                      TextSpan(
                                          text: textFiles[7],
                                          style: TextStyle(
                                              fontStyle: FontStyle.italic)),
                                      TextSpan(
                                          text: textFiles[8],
                                          style: TextStyle(
                                              fontStyle: FontStyle.normal)),
                                    ]),
                                    textAlign: TextAlign.justify,
                                    presetFontSizes: presetFonts,
                                    style: TextStyle(fontFamily: fonts[1])),
                              ),
                              Flexible(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: GestureDetector(
                                    onTap: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/rediscoveringamerica02.jpg",)));
                                    },
                                    child: Image.asset(
                                      "assets/images/rediscoveringamerica02.jpg",
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  )));
  }
}
