import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class ReDiscoveringAmerica2 extends StatefulWidget {
  String name = "family1";
  @override
  _ReDiscoveringAmerica2State createState() => _ReDiscoveringAmerica2State();
}

class _ReDiscoveringAmerica2State extends State<ReDiscoveringAmerica2> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Expanded(
                    flex: 1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/rediscoveringamerica03.jpg",)));
                            },
                            child: Image.asset(
                              "assets/images/rediscoveringamerica03.jpg",
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/rediscoveringamerica04.jpg",)));
                            },
                            child: Image.asset(
                              "assets/images/rediscoveringamerica04.jpg",
                              fit: BoxFit.fill,
                            ),
                          ),
                        )
                      ],
                    )),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/rediscoveringamerica05.jpg",)));
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width*0.7,
                        child: Image.asset(
                          "assets/images/rediscoveringamerica05.jpg",
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )));
  }
}
