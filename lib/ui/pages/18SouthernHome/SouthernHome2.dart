import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
// image on top, text on bottom
class SouthernHome2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Untitled, 1951",
      "Hughie Lee-Smith",
      "Oil on masonite",
      "18 x 24 in.",
      "Hughie Lee-Smith (1915–1999)",
      "Lee-Smith, born in Eustis, Florida, attended the Cleveland Institute of Art and later earned an arts education degree from Wayne State University. In 1940, his work was exhibited at the American Negro Exposition in Chicago. Lee-Smith painted murals during his service in the Navy, and for the Works Projects Administration in Ohio and Illinois. The pointed social commentary expressed in his work limited its appeal to a wide audience, and his figurative paintings of isolated youth did not conform to the abstract expressionism dominant at the time, yet he enjoyed a position of respect within the African American arts community. In 1994, he was commissioned to paint New York Mayor David Dinkins’ portrait. Lee-Smith’s work has been shown in the Whitney Museum of American Art and the Museum of Modern Art. In 1988, the New Jersey State Museum mounted a retrospective of his work.",
    ];
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 6,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/southern_home_01.jpg",)));
                },
                child: Container(
                  padding: EdgeInsets.only(top: 30),
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/southern_home_01.jpg")),
                ),
              ),
            ),
            Column(
              children: <Widget>[
                AutoSizeText(
                  textFiles[0],
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: "MyriadPro-BoldIt"),
                  presetFontSizes: [24,22,20],
                ),
                AutoSizeText(
                  textFiles[1],
                  presetFontSizes: [22,20],
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[2],
                  presetFontSizes: [22,20],
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[3],
                  presetFontSizes:[22,20],
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                SizedBox(
                  height: 2,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return Dialog(
                                child: Container(
                                  height:
                                      MediaQuery.of(context).size.height / 1.5,
                                  width: MediaQuery.of(context).size.width / 1.5,
                                  child: Stack(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 20),
                                        child: Column(
                                          children: <Widget>[
                                        AutoSizeText(
                                          textFiles[4],
                                          presetFontSizes: presetFonts,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontFamily: "Auto2-Regular",
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Expanded(
                                            child: AutoSizeText(
                                          textFiles[5],
                                          presetFontSizes: [28,26,24,22,21,20,19,18,17,16,14],
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              fontFamily: "Auto2-Regular",
                                          ),
                                        ))
                                      ],
                                        ),
                                      ),
                                      Positioned(
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.clear,
                                            color: Colors.black,
                                            size: 30,
                                          ),
                                          onPressed: () =>
                                              Navigator.pop(context),
                                        ),
                                        top: 10,
                                        right: 10,
                                      )
                                    ],
                                  ),
                                ),
                              );
                            });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset(
                            "assets/images/bio.png",
                            scale: 3,
                          )),
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.play_circle_outline,
                        color: Colors.black,
                      ),
                      iconSize: 80,
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VideoView(
                                      videoLink:
                                          "assets/videos/southern_home_01.MP4",
                                    )));
                      },
                    )
                  ],
                ),
                SizedBox(height: 20,)
              ],
            ),
          ],
        ),
      ),
    );
  }
}
