import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';

// image on top, text on bottom
class SouthernHome4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Field Hands, 1988",
      "Jonathan Green",
      "Oil on masonite",
      "37 ¼ x 29 ¼ in.",
      "Jonathan Green (1955– )",
      "Green earned his bachelor of fine arts degree from the Art Institute of Chicago in 1982. The scenes he depicts in his work focus on themes of work, love, belonging, and spirituality in the African American experience, and are heavily influenced by his childhood immersion in the Gullah culture of South Carolina. Gullah Images: The Art of Jonathan Green was published by University of South Carolina Press in 1996. Green’s work has been exhibited in major museums and is held in many private and public collections, including the Philharmonic Center for the Arts in Naples, Florida; the Morris Museum of Art in Augusta, Georgia; the Norton Museum of Art in West Palm Beach, Florida; the Gibbes Museum of Art in Charleston, South Carolina; and the McKissick Museum of the University of South Carolina, in Columbia.",
    ];
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 6,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/southern_home_03.jpg",)));
                },
                child: Container(
                  padding: EdgeInsets.only(top: 30),
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/southern_home_03.jpg")),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  AutoSizeText(
                    textFiles[0],
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: "MyriadPro-BoldIt"),
                    presetFontSizes: [24,22],
                  ),
                  AutoSizeText(
                    textFiles[1],
                    presetFontSizes: [22,20],
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  AutoSizeText(
                    textFiles[2],
                    presetFontSizes:[22,20],
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  AutoSizeText(
                    textFiles[3],
                    presetFontSizes: [22,20],
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height / 2,
                                width: MediaQuery.of(context).size.width / 2,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          AutoSizeText(
                                            textFiles[4],
                                            presetFontSizes: presetFonts,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontFamily: "Auto2-Regular",
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Expanded(
                                              child: AutoSizeText(
                                            textFiles[5],
                                            presetFontSizes: presetFonts,
                                            textAlign: TextAlign.justify,
                                            style: TextStyle(
                                                fontFamily: "Auto2-Regular",
                                            ),
                                          ))
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.clear,
                                          color: Colors.black,
                                          size: 30,
                                        ),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                      top: 10,
                                      right: 10,
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                    child: Container(
                        height: 80,
                        width: 80,
                        child: Image.asset(
                          "assets/images/bio.png",
                          scale: 3,
                        )),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.play_circle_outline,
                      color: Colors.black,
                    ),
                    iconSize: 80,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => VideoView(
                                    videoLink:
                                        "assets/videos/southern_home_02.MP4",
                                  )));
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
