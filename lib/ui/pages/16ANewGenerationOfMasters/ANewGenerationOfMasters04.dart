import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
// image on left, text on right
class ANewGenerationOfMasters04 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Falling Star, 1979",
      "Romare Bearden",
      "Lithograph",
      "34 ½ x 29 in.",
      "Romare Bearden (1911–1988)",
      "Bearden’s arts education began with Harlem Renaissance artists and scholars. He graduated from New York University, earning a degree in education. He also studied philosophy in Paris at the Sorbonne and art at the Art Students League in New York. Bearden, a social worker with the New York City Department of Social Services for over 25 years, translated this calling into social activism, which he carried out through both art and community involvement. In 1964, Bearden was appointed the first art director of the Harlem Cultural Council, which advocated for the African American community. He was a founding member of the Studio Museum in Harlem, and with fellow artists Norman Lewis and Ernest Crichlow created the Cinque Gallery to promote minority artists. He was a prolific writer, and his History of African American Artists: From 1792 to the Present, published posthumously, is a landmark in the scholarship on African American art. Bearden’s work is represented in the Metropolitan Museum of Art, the Whitney Museum of American Art, the Philadelphia Museum of Art, the Museum of Fine Arts, Boston, and the Studio Museum in Harlem. He was elected to the National Institute of Arts and Letters in 1972, and in 1987 President Ronald Reagan awarded Bearden the National Medal of Arts.",
    ];
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          
          Expanded(
            flex: 6,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 30),
              height: MediaQuery.of(context).size.height, 
              width: MediaQuery.of(context).size.width,
              child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image.asset("assets/images/a_new_generation_of_masters_03.jpg")),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AutoSizeText(
                  textFiles[0],
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: "MyriadPro-BoldIt"),
                  presetFontSizes: presetFonts,
                ),
                AutoSizeText(
                  textFiles[1],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[2],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[3],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                SizedBox(
                  height: 2,
                ),
                GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height / 1.5,
                              width: MediaQuery.of(context).size.width / 2,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        AutoSizeText(
                                          textFiles[4],
                                          presetFontSizes: presetFonts,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontFamily: "Auto2-Regular",
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Expanded(
                                            child: AutoSizeText(
                                          textFiles[5],
                                          presetFontSizes: presetFonts,
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              fontFamily: "Auto2-Regular",
                                          ),
                                        ))
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.clear,
                                        color: Colors.black,
                                        size: 30,
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                    top: 10,
                                    right: 10,
                                  )
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  child: Container(
                      height: 50,
                      width: 50,
                      child: Image.asset(
                        "assets/images/bio.png",
                        scale: 3,
                      )),
                ), 
              ],
            ),
          ),
        ],
      ),
    );
  }
}
