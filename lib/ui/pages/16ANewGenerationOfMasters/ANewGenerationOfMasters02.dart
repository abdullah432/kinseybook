import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
// image on left, text on right
class ANewGenerationOfMasters02 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Slim, 1933",
      "Henry Bozeman Jones",
      "Oil on board",
      "10 x 7 in.",
      "Henry Bozeman Jones (1889–1963)",
      "Jones, an illustrator of children’s books, was born in Philadelphia. He exhibited at the Harmon Foundation from 1929 to 1932 and was named the winner in 1933. His works are in the permanent collection of the Metropolitan Museum of Art in New York.",
    ];
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          
          Expanded(
            flex: 6,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 30),
              height: MediaQuery.of(context).size.height, 
              width: MediaQuery.of(context).size.width,
              child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image.asset("assets/images/a_new_generation_of_masters_01.jpg")),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AutoSizeText(
                  textFiles[0],
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: "MyriadPro-BoldIt"),
                  presetFontSizes: presetFonts,
                ),
                AutoSizeText(
                  textFiles[1],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[2],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[3],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                SizedBox(
                  height: 2,
                ),
                GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height / 3,
                              width: MediaQuery.of(context).size.width / 3,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        AutoSizeText(
                                          textFiles[4],
                                          presetFontSizes: presetFonts,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontFamily: "Auto2-Regular",
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Expanded(
                                            child: AutoSizeText(
                                          textFiles[5],
                                          presetFontSizes: presetFonts,
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              fontFamily: "Auto2-Regular",
                                          ),
                                        ))
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.clear,
                                        color: Colors.black,
                                        size: 30,
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                    top: 10,
                                    right: 10,
                                  )
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  child: Container(
                      height: 50,
                      width: 50,
                      child: Image.asset(
                        "assets/images/bio.png",
                        scale: 3,
                      )),
                ), 
              ],
            ),
          ),
        ],
      ),
    );
  }
}
