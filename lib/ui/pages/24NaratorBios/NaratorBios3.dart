import 'package:auto_size_text/auto_size_text.dart';
import 'package:drop_cap_text/drop_cap_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class NaratorBios3 extends StatefulWidget {
  @override
  _NaratorBios3State createState() => _NaratorBios3State();
}

class _NaratorBios3State extends State<NaratorBios3> {
  final textFiles = [
    "\nNichols began her entertainment career as a singer for Duke Ellington and Lionel Hampton. Later, she was among the first African American actresses to have a major role in a prime time television show when she was cast as Uhura in Star Trek 1966. She played this role on television for three years and in several of the Star Trek movies that were made after the series ended. Life imitated art when, from the late 1970s until the late 1980s, Nichols was employed by NASA as a recruiter of potential astronauts; she recruited several women and men of diverse backgrounds, including Guion Bluford and Sally Ride.",
  ];
  final fonts = [
    "Auto2-Light",
  ];
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [24, 20, 18, 14, 8];
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: GestureDetector(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/narator_bios_09.jpg",)));
                                },
                                child: Image.asset(
                                  "assets/images/narator_bios_09.jpg",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: AutoSizeText.rich(
                                  TextSpan(
                                      text: "Nichelle  Nichols",
                                      style:
                                          TextStyle(fontWeight: FontWeight.bold),
                                      children: [
                                        TextSpan(
                                            text: textFiles[0],
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontFamily: "Auto2-Regular"))
                                      ]),
                                  presetFontSizes: presetFonts,
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                      ),
                      Expanded(flex: 1, child: Column())
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(12),
                ),
                Expanded(
                  flex: 1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(flex: 1, child: Column()),
                      Padding(
                        padding: EdgeInsets.all(20),
                      ),
                      Expanded(flex: 1, child: Column()),
                    ],
                  ),
                ),
              ],
            )));
  }
}
