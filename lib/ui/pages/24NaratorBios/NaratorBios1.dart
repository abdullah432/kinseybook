import 'package:auto_size_text/auto_size_text.dart';
import 'package:drop_cap_text/drop_cap_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class NaratorBios1 extends StatefulWidget {
  @override
  _NaratorBios1State createState() => _NaratorBios1State();
}

class _NaratorBios1State extends State<NaratorBios1> {
  final textFiles = [
    "\nBeginning her career on stage, this Yale School of Drama graduate completed several productions on and off Broadway, which include Ma Rainey’s Black Bottom. Bassett also starred opposite Alec Baldwin in Macbeth. Bassett recently starred in the critically-acclaimed Akeelah and the Bee, and in the leading role in Meet the Browns. Perhaps best known for her portrayal of Tina Turner in What’s Love Got To Do With It, Bassett earned the Golden Globe for Best Actress in a Drama, an NAACP Image Award for Outstanding Lead Actress in a Motion Picture and an Academy Award nomination for her performance.",
    "\nGossett emerged as a young actor through stage and film versions of the groundbreaking drama A Raisin in the Sun. This led to numerous appearances on television in the 1960s and 1970s, culminating in his Emmy-winning portrayal of Fiddler in the landmark ABC miniseries Roots. As the drill sergeant in An Officer and a Gentleman (1982), he won a Best Supporting Actor Oscar. The recipient of multiple Golden Globes, Emmy’s and People’s Choice Awards, Gossett currently is developing a nonprofit foundation called the Eracism Foundation, aimed at developing entertainment that raises awareness of issues such as racism, ignorance and societal apathy.",
    "\nTodd learned the entertainment industry working with her mentor, Sidney Poitier. Benefiting from his tutelage, Todd began her professional career in the Off-Broadway hit, Deep Are The Roots. Beverly broke ground for actresses of color in the critically acclaimed PBS Television Special, Six Characters In Search of An Author. She won the People’s Choice Award and received her second NAACP Image Award nomination for her starring role in the phenomenally successful television mini-series, Roots. Recently she has played a recurring role in Six Feet Under. She has starred in many films, including Clara’s Heart and Lean on Me, and has received critical acclaim most recently for her role in The Bucket List.",
    "\nToussaint studied theater at New York’s renowned High School of Performing Arts and The Juilliard School. She spent 12 years working in New York theater prior to moving to Los Angeles. Toussaint starred as Rene Jackson in Any Day Now. As a result, she was nominated for four NAACP Image Awards for Best Actress in a Drama Series and a TV Guide Award for Best Actress in a Drama Series. In addition to recurring roles on Law & Order, Murder One and other shows, Toussaint’s feature-film credits include Point of No Return and Hudson Hawk. Currently, she is producing the cable television movie The Joyce Ann Brown Story.",
  ];
  final fonts = [
    "Auto2-Regular",
    "Auto2-Regular",
    "Auto2-Regular",
    "Auto2-Regular",
  ];

  @override
  Widget build(BuildContext context) {
    double sectionWidth = MediaQuery.of(context).size.width * .11;
    double sectionHeight = MediaQuery.of(context).size.height * .22;
    double fontSize = 20;
    List<double> presetFonts = [24, 20, 18, 14, 8];
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: AutoSizeText(
                    "Narrator Biographies",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontFamily: "Auto2-Regular",
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                    minFontSize: 34,
                    maxFontSize: 38,
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Flexible(
                              flex: 1,
                              child: GestureDetector(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/narator_bios_01.jpg",)));
                                },
                                child: Image.asset(
                                  "assets/images/narator_bios_01.jpg",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: AutoSizeText.rich(
                                  TextSpan(
                                      text: "Angela  Bassett",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontFamily: fonts[0]),
                                      children: [
                                        TextSpan(
                                            text: textFiles[0],
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal))
                                      ]),
                                  presetFontSizes: presetFonts,
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                      ),
                      Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Flexible(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/narator_bios_02.jpg",)));
                                  },
                                  child: Image.asset(
                                    "assets/images/narator_bios_02.jpg",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: AutoSizeText.rich(
                                    TextSpan(
                                        text: "Lou Gossett, Jr.",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: fonts[1]),
                                        children: [
                                          TextSpan(
                                              text: textFiles[1],
                                              style: TextStyle(
                                                  fontWeight: FontWeight.normal))
                                        ]),
                                    presetFontSizes: presetFonts,
                                    textAlign: TextAlign.justify,
                                  ),
                                ),
                              )
                            ],
                          ))
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(12),
                ),
                Expanded(
                  flex: 1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Flexible(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/narator_bios_03.jpg",)));
                                  },
                                  child: Image.asset(
                                    "assets/images/narator_bios_03.jpg",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: AutoSizeText.rich(
                                    TextSpan(
                                        text: "Beverly  Todd",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: fonts[2]),
                                        children: [
                                          TextSpan(
                                              text: textFiles[2],
                                              style: TextStyle(
                                                  fontWeight: FontWeight.normal))
                                        ]),
                                    presetFontSizes: presetFonts,
                                    textAlign: TextAlign.justify,
                                  ),
                                ),
                              )
                            ],
                          )),
                      Padding(
                        padding: EdgeInsets.all(20),
                      ),
                      Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Flexible(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/narator_bios_04.jpg",)));
                                  },
                                  child: Image.asset(
                                    "assets/images/narator_bios_04.jpg",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: AutoSizeText.rich(
                                    TextSpan(
                                        text: "Lorraine  Toussaint",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: fonts[3]),
                                        children: [
                                          TextSpan(
                                              text: textFiles[3],
                                              style: TextStyle(
                                                  fontWeight: FontWeight.normal))
                                        ]),
                                    presetFontSizes: presetFonts,
                                    textAlign: TextAlign.justify,
                                  ),
                                ),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
              ],
            )));
  }
}
