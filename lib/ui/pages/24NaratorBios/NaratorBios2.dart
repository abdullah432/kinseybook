import 'package:auto_size_text/auto_size_text.dart';
import 'package:drop_cap_text/drop_cap_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class NaratorBios2 extends StatefulWidget {
  @override
  _NaratorBios2State createState() => _NaratorBios2State();
}

class _NaratorBios2State extends State<NaratorBios2> {
  final textFiles = [
    "\nA graduate of Pasadena’s American Academy of Dramatic Arts, Haysbert made his earliest appearances on such television shows as Laverne & Shirley, Buck Rogers in the 25th Century, and The Incredible Hulk. Haysbert established himself as an actor equally comfortable on the big and small screens, in films ranging from comedies to thrillers during the 1990s. When the hit series 24 debuted in 2001, Haysbert’s performance as presidential hopeful David Palmer proved so effective that he was not only nominated for a Golden Globe for the role in 2003, but an Image Award and a Screen Actor’s Guild Award as well.",
    "\nKhalil Kinsey is the son of Bernard and Shirley Kinsey. Currently residing in Los Angeles, California, Khalil is the co-owner of the Diamond Supply Co. Skate Shop as well as The StevensonGibran Placement Agency, a sales and marketing firm for clothing lines. As the only child of Bernard and Shirley, Khalil developed an early appreciation for the arts and all things creative. An artist in his own right, Khalil is an up and coming hip-hop artist that performs regularly alongside notable musicians both stateside and abroad.",
    "\nVance graduated from Harvard and received a MFA degree from Yale School of Drama. After moving to New York, he distinguished himself onstage, earning two Tony Award nominations. Vance may be best known for his five-year run on Law & Order: Criminal Intent, where his role as New York Assistant District Attorney Ron Carver won nomination for the NAACP Image Award (Outstanding Actor in a Drama Series). Vance’s talents also translate to the big screen, where he has appeared in films such as Space Cowboys and The Preacher’s Wife opposite Whitney Houston and Denzel Washington, and where he received critical acclaim for his portrayal of Bobby Seale in Panther",
    "\nAfter attending Howard University in Washington, D.C., Winston moved to New York to become an actress. During the 1970s she had a role on the Emmy-award winning PBS-TV series The Electric Company. In the theater, she starred in the acclaimed Broadway play The Tap Dance Kid (1983). Her roles in To Take Up Arms and Up the Mountain earned her two Los Angeles Critics’ Drama-Logue awards. She has received two Obie Awards, and an Audelco Award for her contributions to the world of theater. In addition to many roles on television and in film, she revived Langston Hughes’ Black Nativity off-Broadway and has worked as an independent producer and director.",
  ];
  final fonts = [
    "Auto2-Regular",
    "Auto2-Regular",
    "Auto2-Regular",
    "Auto2-Regular",
  ];
  @override
  Widget build(BuildContext context) {
    double sectionWidth = MediaQuery.of(context).size.width * .11;
    double sectionHeight = MediaQuery.of(context).size.height * .22;
    double fontSize = 20;
    List<double> presetFonts = [24, 20, 18, 14, 8];
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start, 
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: GestureDetector(
                                onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/narator_bios_05.jpg",)));
                                },
                                child: Image.asset(
                                  "assets/images/narator_bios_05.jpg",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: AutoSizeText.rich(
                                  TextSpan(
                                      text: "Dennis  Haysbert",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontFamily: fonts[0]),
                                      children: [
                                        TextSpan(
                                            text: textFiles[0],
                                            style: TextStyle(
                                                fontWeight: FontWeight.normal))
                                      ]),
                                  presetFontSizes: presetFonts,
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(20),
                      ),
                      Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/narator_bios_06.jpg",)));
                                  },
                                  child: Image.asset(
                                    "assets/images/narator_bios_06.jpg",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: AutoSizeText.rich(
                                    TextSpan(
                                        text: "Khalil  Kinsey",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: fonts[1]),
                                        children: [
                                          TextSpan(
                                              text: textFiles[1],
                                              style: TextStyle(
                                                  fontWeight: FontWeight.normal))
                                        ]),
                                    presetFontSizes: presetFonts,
                                    textAlign: TextAlign.justify,
                                  ),
                                ),
                              )
                            ],
                          ))
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(12),
                ),
                Expanded(
                  flex: 1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/narator_bios_07.jpg",)));
                                  },
                                  child: Image.asset(
                                    "assets/images/narator_bios_07.jpg",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: AutoSizeText.rich(
                                    TextSpan(
                                        text: "Courtney B. Vance",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: fonts[2]),
                                        children: [
                                          TextSpan(
                                              text: textFiles[2],
                                              style: TextStyle(
                                                  fontWeight: FontWeight.normal))
                                        ]),
                                    presetFontSizes: presetFonts,
                                    textAlign: TextAlign.justify,
                                  ),
                                ),
                              )
                            ],
                          )),
                      Padding(
                        padding: EdgeInsets.all(20),
                      ),
                      Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/narator_bios_08.jpg",)));
                                  },
                                  child: Image.asset(
                                    "assets/images/narator_bios_08.jpg",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: AutoSizeText.rich(
                                    TextSpan(
                                        text: "Hattie Winston",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontFamily: fonts[3]),
                                        children: [
                                          TextSpan(
                                              text: textFiles[3],
                                              style: TextStyle(
                                                  fontWeight: FontWeight.normal))
                                        ]),
                                    presetFontSizes: presetFonts,
                                    textAlign: TextAlign.justify,
                                  ),
                                ),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
              ],
            )));
  }
}
