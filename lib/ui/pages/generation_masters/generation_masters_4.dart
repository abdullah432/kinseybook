import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class GenerationMastersFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(children: <Widget>[
              Expanded(
                flex: 2,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/generation_masters_4.jpg",)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.fitHeight,
                      child: Image.asset("assets/images/generation_masters_4.jpg"),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText("Falling Star, 1979",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                    AutoSizeText("Romare Bearden",style: TextStyle(fontSize: 24),),
                    AutoSizeText("Lithograph",style: TextStyle(fontSize: 24),),
                    AutoSizeText("34 1⁄2 x 29 in.",style: TextStyle(fontSize: 24),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("Romare Bearden (1911–1988)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Bearden’s arts education began with Harlem Renaissance artists and scholars. He graduated from New York University, earning a degree in education. He also studied philosophy in Paris at the Sorbonne and art at the Art Students League in New York. Bearden, a social worker with the New York City Department of Social Services for over 25 years, translated this calling into social activism, which he carried out through both art and community involvement. In 1964, Bearden was appointed the first art director of the Harlem Cultural Council, which advocated for the African American community. He was a founding member of the Studio Museum in Harlem, and with fellow artists Norman Lewis and Ernest Crichlow created the Cinque Gallery to promote minority artists. He was a prolific writer, and his History of African American Artists: From 1792 to the Present, published posthumously, is a landmark in the scholarship on African American art. Bearden’s work is represented in the Metropolitan Museum of Art, the Whitney Museum of American Art, the Philadelphia Museum of Art, the Museum of Fine Arts, Boston, and the Studio Museum in Harlem. He was elected to the National Institute of Arts and Letters in 1972, and in 1987 President Ronald Reagan awarded Bearden the National Medal of Arts.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    )
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
