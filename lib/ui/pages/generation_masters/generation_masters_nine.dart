import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class GenerationMastersNine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Column(children: <Widget>[
        Expanded(
          flex: 2,
          child: GestureDetector(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/generation_masters_9.jpg",)));
            },
            child: Image.asset("assets/images/generation_masters_9.jpg",fit: BoxFit.cover,),
          ),
        ),
        Flexible(
            flex: 1,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 30,),
                AutoSizeText("Ka’ena Point, 2009",style: TextStyle(fontWeight: FontWeight.bold),maxLines: 1,
                 textAlign: TextAlign.center,),
                AutoSizeText("Keith Morris Washington",textAlign: TextAlign.center,),
                AutoSizeText("Oil on panelh",textAlign: TextAlign.center,),
                AutoSizeText("6 x 20 in.",textAlign: TextAlign.center,),
              ],)),
        SizedBox(height: 20,),
        GestureDetector(
          onTap: (){
            showDialog(context: context,builder: (BuildContext context){
              return Dialog(
                child: Container(
                  height: MediaQuery.of(context).size.height/1.5,
                  width: MediaQuery.of(context).size.width/1.5,
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 30,),
                            AutoSizeText("Keith Morris Washington (1956 – )",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                            SizedBox(height: 20,),
                            Expanded(child: AutoSizeText("Born in Gary, Indiana, Washington earned a BFA from Massachusetts College of Art and an MFA from Tufts University/School of the Museum of Fine Art, in Boston. His natural progression of artistic exploration took him “from realism, to geometric abstraction, to self- discovery via semi-abstract African iconography, back to my (his) first love, landscapes”. Influenced by 19th century Hudson River, Barbizon and Luminist painting, Washington has developed several on going “dramatically thematic series of landscape paintings...the most logical mode to express my commitments to peace, justice, and my identity as both American.. and all that implies, and as an African American with all those ramifications...” Washington lives in Boston and is on the faculty of Massachusetts College of Art.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                          ],
                        ),
                      ),
                      Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                    ],
                  ),
                ),
              );
            });
          },
          child: Container(
              height: 50,
              width: 50,
              child: Image.asset("assets/images/bio.png",scale: 3,)),
        ),
        SizedBox(height: 30,)
      ],),
    );
  }
}
