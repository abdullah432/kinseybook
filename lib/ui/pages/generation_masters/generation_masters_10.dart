import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class GenerationMAstersTen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(children: <Widget>[
              Expanded(
                flex: 2,
                child: GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/generation_masters_10.jpg",)));
                    },
                    child: Image.asset("assets/images/generation_masters_10.jpg",fit: BoxFit.contain,)),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  AutoSizeText("Wonder, 2005",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),),
                  AutoSizeText("Sam Gilliam",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 22),),
                  AutoSizeText("Mixed media",style: TextStyle(fontSize: 22),),
                  AutoSizeText("40 x 44 x 4 in.",style: TextStyle(fontSize: 22),),
                  SizedBox(height: 20,),
                  GestureDetector(
                    onTap: (){
                      showDialog(context: context,builder: (BuildContext context){
                        return Dialog(
                          child: Container(
                            height: MediaQuery.of(context).size.height/1.5,
                            width: MediaQuery.of(context).size.width/1.5,
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                  child: Column(
                                    children: <Widget>[
                                      SizedBox(height: 30,),
                                      AutoSizeText("Sam Gilliam (1933– )",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                      SizedBox(height: 20,),
                                      Expanded(child: AutoSizeText("Born in Tupelo, Mississippi, Gilliam received his formal art education at the University of Louisville. He moved to Washington, D.C., and established a connection with prominent color-field artists. In 1967 he received a National Endowment for the Arts grant. Gilliam is best known for his draped cloth paintings, using both color and gravity to shape the freed canvas. He has produced a number of outdoor sculptures, including an installation at Philadelphia Museum of Art in 1975 and another at the California African American Museum in Los Angeles. In the 1970s, he experimented with white paintings, covering richly textured color-field work with impasto and glaze. In his later work, he has turned to solidly three-dimensional structures made of wood and mixed media. In 2005, the Corcoran Gallery of Art in Washington, D.C., held a major retrospective of Gilliam’s work.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                    ],
                                  ),
                                ),
                                Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                              ],
                            ),
                          ),
                        );
                      });
                    },
                    child: Container(
                        height: 80,
                        width: 80,
                        child: Image.asset("assets/images/bio.png",scale: 3,)),
                  )
                ],
              ),
          ],
        ),
      ),
    );
  }
}
