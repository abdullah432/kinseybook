import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class GenerationMasterOne extends StatefulWidget {
  @override
  _GenerationMasterOneState createState() => _GenerationMasterOneState();
}

class _GenerationMasterOneState extends State<GenerationMasterOne> {
  final textFiles=["Art is not simply works of art; it is the spirit that knows beauty; that has music in its soul and the color of the sunset in its handkerchief, that can dance on a flaming world and make the world dance too.\n— W. E. B. DuBois\n\nBernard and Shirley Kinsey’s collection strongly represents artists who were the progeny of the Harlem Renaissance masters. This second generation followed their mentors’ example, conjoining formal education with unique points of view from African American culture. These artists are of particular importance not only for their technical and cultural contributions but also because they were the first to be truly integrated into modern American art culture.\n\nJacob Lawrence captured the imagination of a nation with his profound and haunting renderings of the African American life he saw growing up in Harlem. He employed bold color in a decisively new style. Lawrence was the first",
    "African American artist to be elected a member of the National Institute of Arts and Letters and the first black president of the organization.\n\nNorman Lewis was elected the first president of the Spiral Group, which sought to advance the goals of the civil rights movement through the visual arts. Although his early work was grounded in social realism, he later worked beside Willem de Kooning, Jackson Pollack, and Mark Rothko in the abstract expressionist movement born in New York in the 1950s. Since his death in 1979, he has assumed a major place in histories of American art and has been featured in several important exhibits.\n\nBob Blackburn’s printmaking workshop in Chelsea Village, Manhattan, established him as both an American print master and one of the most generous artists of his time. Artists from all walks of life, including Robert Rauschenberg and Roy DeCarava, worked with Blackburn in his workshop and went on to critical acclaim around the world. Blackburn’s altruism and dedication to community coincide with the very best of",
    "what the Kinsey Collection represents, values shared by many of the artists and contributors to the collection.\n\nEncouraged by artist Alma Thomas, a young Sam Gilliam extended the possibilities of modern art with innovative abstract works on different media, including loose, draped canvas, wood, and steel. Today, Gilliam is internationally known as the premier African American painter of “color- field” works, large canvases usually occupied by a single block of color, the successor to abstract expressionism as a dominant form.\n\nThe youngest artist to exhibit at the 1962 Seattle World’s Fair, which included a major international survey of modern art, Chicagoan Richard Hunt has garnered national attention for his sculpture, which exists mainly as public art around the country. His work is also in the permanent collection of the Museum of Modern Art in New York."
  ];
  static bool listView=false;
  ScrollController listViewController=ScrollController();
  static double fontSize=50;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
        child: listView?Stack(
          children: <Widget>[
            ListView.builder(
                controller: listViewController,
                itemCount: textFiles.length,
                itemBuilder: (BuildContext context,index){
                  if(index==0){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: "A New Generation of Masters\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                            TextSpan(text: textFiles[0])
                          ],

                        ),
                        presetFontSizes: [fontSize],
                        textAlign: TextAlign.justify,
                      ),
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: AutoSizeText(textFiles[index],minFontSize: fontSize,textAlign: TextAlign.justify),
                  );
                }),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height*0.1,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(onPressed: (){

                        if(listViewController.position.minScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset-listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },icon: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 40,),),
                      Row(children: <Widget>[
                        IconButton(icon: Icon(Icons.zoom_out,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            fontSize=30;
                          });
                        }),
                        IconButton(icon: Icon(Icons.close,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            listView=false;
                          });
                        })
                      ],),
                      IconButton(icon: Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 40,),onPressed: (){
                        if(listViewController.position.maxScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },)
                    ],),
                ))
          ],
        ):Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 30,),
                  Container(width:100,height: 50,child: Stack(children: <Widget>[Positioned(child: GestureDetector(
                      onTap: (){
                        setState(() {
                          fontSize=50;
                          listView=true;
                        });
                      },
                      child: Image.asset("assets/images/magnify.png",scale: 4,)),left: -30,),],overflow: Overflow.visible,)),
                  AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(text: "A New Generation of Masters\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                      ],

                    ),
                    textAlign: TextAlign.justify,
                  ),
                  Expanded(child: AutoSizeText(textFiles[0],presetFontSizes: [22,20,18,16,14],textAlign: TextAlign.justify,))
                ],
              ),
            ),),
            Expanded(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[1],presetFontSizes: [22,20,18,16,14],textAlign: TextAlign.justify,),
            )),
            Expanded(child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: AutoSizeText(textFiles[2],presetFontSizes: [22,20,18,16,14],textAlign: TextAlign.justify,)),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
