import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class GenerationMasterThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
        Expanded(
          flex: 2,
          child: GestureDetector(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/generation_masters_3.jpg",)));
            },
            child: Image.asset("assets/images/generation_masters_3.jpg"),
          ),
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 30,),
            AutoSizeText("John Brown Series, #8, 1977",style: TextStyle(fontWeight: FontWeight.bold),maxLines: 1,
              textAlign: TextAlign.center,),
            AutoSizeText("Jacob Lawrence",textAlign: TextAlign.center,),
            AutoSizeText("Serigraph",textAlign: TextAlign.center,),
            AutoSizeText("22 1⁄2 x 28 1⁄4 in.",textAlign: TextAlign.center,),
          ],),
        SizedBox(height: 20,),
        GestureDetector(
          onTap: (){
            showDialog(context: context,builder: (BuildContext context){
              return Dialog(
                child: Container(
                  height: MediaQuery.of(context).size.height/1.5,
                  width: MediaQuery.of(context).size.width/1.5,
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 30,),
                            AutoSizeText("Jacob Lawrence (1917–2000)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                            SizedBox(height: 20,),
                            Expanded(child: AutoSizeText("Schooled by Charles Alston and other Harlem Renaissance notables, Lawrence received his early education in Harlem. Growing up, he observed the daily life of African Americans, and these scenes comprise much of Lawrence’s subject matter. In 1937, at the age of 21, Lawrence created 41 paintings documenting the rise and fall of Toussant L’Ouverture, Haiti’s revolutionary leader. His series on the Great Migration, which combines stylistic interpretation with a historically grounded perspective on black life, was acquired by the Metropolitan Museum of Art in New York, making Lawrence the first African American artist represented in that institution’s permanent collection. He served on the faculty at the Art Students League, the Skowhegan School in Maine, and Pratt Institute, and in 1970, moved to Seattle with his wife, painter Gwendolyn Knight, to take up an appointment as a professor in the School of Art at the University of Washington. He retired in 1980. Lawrence’s work earned him a National Medal of Arts as well as election to the National Academy of Arts and Letters and the National Academy of Design.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                          ],
                        ),
                      ),
                      Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                    ],
                  ),
                ),
              );
            });
          },
          child: Container(
              height: 50,
              width: 50,
              child: Image.asset("assets/images/bio.png",scale: 3,)),
        ),
        SizedBox(height: 30,)
      ],),
    );
  }
}
