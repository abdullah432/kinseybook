import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class GenerationMastersSeven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
        Expanded(
          flex: 2,
          child: GestureDetector(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/generation_masters_7.jpg",)));
            },
            child: Image.asset("assets/images/generation_masters_7.jpg"),
          ),
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 30,),
            AutoSizeText("Untitled, 1960",style: TextStyle(fontWeight: FontWeight.bold),maxLines: 1,
              textAlign: TextAlign.center,),
            AutoSizeText("Bob Blackburn",textAlign: TextAlign.center,),
            AutoSizeText("Lithograph",textAlign: TextAlign.center,),
            AutoSizeText("29 x 34 in.",textAlign: TextAlign.center,),
          ],),
        SizedBox(height: 20,),
        GestureDetector(
          onTap: (){
            showDialog(context: context,builder: (BuildContext context){
              return Dialog(
                child: Container(
                  height: MediaQuery.of(context).size.height/1.5,
                  width: MediaQuery.of(context).size.width/1.5,
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 30,),
                            AutoSizeText("Bob Blackburn (1920–2003)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                            SizedBox(height: 20,),
                            Expanded(child: AutoSizeText("Blackburn grew up in Harlem, where he attended art classes taught by Harlem Renaissance artists, and he studied lithography, etching, woodblock, and silk-screening at the Art Students League. In 1948, he began experimenting with innovative lithographic techniques. In the late ’50s and early ’60s, he worked as a master printer at Universal Limited Art Editions. With Will Barnet and Thomas Laidman, Blackburn founded and became the director of the Printmaking Workshop in New York. There he printed works with artists Robert Rauschenberg, Charles White, and Eldzier Cortor, among others. Blackburn taught at Pratt Institute, Columbia University, and Rutgers University. In 1992, he received a John T. and Catherine D. MacArthur Foundation Fellowship, and in 2002, lifetime achievement awards from the College Art Association and the National Fine Print Association.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                          ],
                        ),
                      ),
                      Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                    ],
                  ),
                ),
              );
            });
          },
          child: Container(
              height: 50,
              width: 50,
              child: Image.asset("assets/images/bio.png",scale: 3,)),
        ),
        SizedBox(height: 30,)
      ],),
    );
  }
}
