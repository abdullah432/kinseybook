import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class GenerationMastersEight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child:Row(children: <Widget>[
              Expanded(
                flex: 2,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/generation_masters_8.jpg",)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/generation_masters_8.jpg"),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText("Love Letter, 1995",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),),
                    AutoSizeText("Dane Tilghman",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),),
                    AutoSizeText("Watercolor on paper",style: TextStyle(fontSize: 22),),
                    AutoSizeText("20 1⁄2 x 14 3⁄4 in.",style: TextStyle(fontSize: 22),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("Dane Tilghman (1957– )",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Tilghman, a native Pennsylvanian, earned his BFA from Kutztown University, PA. His art has evolved from realism to his own interpretation of surrealism, impressionism, and primitive elongation. His images are inspired by African Americans in daily life, popular entertainment, and the world of sports. Tilghman is a premier painter of American Golf Art and Negro League Baseball images. His work has been widely exhibited since 1979. In 1999, a Tilghman baseball image was incorporated into the Baseball Hall of Fame Museum in Cooperstown. He has painted numerous commissioned murals at sports stadiums, and his art is included in many art products. “I emphasize Black Americana, but my work crosses over to a broad audience,” says Tilghman. “I think everyone can relate to the old times and traditions and that all people can connect with the work.”",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    )
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
