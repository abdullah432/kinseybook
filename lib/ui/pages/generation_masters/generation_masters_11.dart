import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class GenerationMastersEleven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(children: <Widget>[

              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText("Inside Out Series, ca. 1986",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),),
                    AutoSizeText("Richard Hunt",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),),
                    AutoSizeText("Cast bronze",style: TextStyle(fontSize: 22),),
                    AutoSizeText("13 3⁄4 x 13 x 12 in.",style: TextStyle(fontSize: 22),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("Richard Hunt (1935– )",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Hunt developed his skills at the Junior School of the Art Institute and later at the University of Illinois, Chicago. Early in his career, he experimented with various materials and sculpting techniques but is best known for his works in bronze and stainless steel. He was the youngest artist represented in the survey of modern art exhibition that was presented at the 1962 Seattle World’s Fair. Hunt’s best-known works include Jacob’s Ladder, now at the Carter G. Woodson Library in Chicago, and Flintlock Fantasy, now in Detroit. President Lyndon Johnson appointed him to serve on the governing board of the National Endowment for the Arts; he was the first artist to hold this position. Hunt’s work is included in the permanent collection of the Museum of Modern Art.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: GestureDetector(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/generation_masters_11.jpg",)));
                        },
                        child: Image.asset("assets/images/generation_masters_11.jpg")),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
