import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class WomenInArtThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child:Row(children: <Widget>[
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_3_1.jpg",)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/women_in_art_3_1.jpg"),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding:  EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.1),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              AutoSizeText("Woman, 1989",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                              AutoSizeText("Artis Lane",style: TextStyle(fontSize: 24),),
                              AutoSizeText("Bronze sculpture",style: TextStyle(fontSize: 24),),
                              AutoSizeText("31 x 15 1⁄2 x 8.0 in.",style: TextStyle(fontSize: 24),),
                              SizedBox(height: 20,),
                            ],
                          ),
                          Spacer()
                        ],
                      ),
                      Spacer(),
                      GestureDetector(
                        onTap: (){
                          showDialog(context: context,builder: (BuildContext context){
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height/1.5,
                                width: MediaQuery.of(context).size.width/1.5,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          SizedBox(height: 30,),
                                          AutoSizeText("Artis Lane (1927– )",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                          SizedBox(height: 20,),
                                          Expanded(child: AutoSizeText("A descendent of renowned abolitionist Mary Ann Shadd,Lane was awarded a scholarship to the Ontario College of Art in Toronto and was the first woman admitted to Cranbrook Art Academy. Her early career as a portrait painter began with Michigan Governor George Romney as her subject. Lane continued her portraiture in Los Angeles, depicting dignitaries Nelson Mandela, Gordon used artistic expression to address social and spiritual Getty, Ronald Reagan, Barbara Bush, and others. She issues with such works as The Beginning, depicting a young Rosa Parks seated on the bus. She was to become Parks’ personal portrait artist. Lane eventually shifted her focus to sculpture, creating lithe dancers, pregnant women, and her generic man/woman series. Lane’s latest series,Emerging into Spirit,includes fragmented pieces of ceramic shell left on her bronzes that symbolize man’s spiritual journey to enlightenment. Most recently in 2009,her bust of Sojourner Truth was unveiled by Michelle Obama in the Capitol Visitors Center and is now on permanent display.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                        ],
                                      ),
                                    ),
                                    Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                  ],
                                ),
                              ),
                            );
                          });
                        },
                        child: Container(
                            height: 80,
                            width: 80,
                            child: Image.asset("assets/images/bio.png",scale: 3,)),
                      ),
                      Spacer(),
                      Row(
                        children: <Widget>[
                          Spacer(),
                          Column(
                            children: <Widget>[
                              SizedBox(height: 30,),
                              AutoSizeText("Classic Head 1, 1994",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                              AutoSizeText("Artis Lane",style: TextStyle(fontSize: 24),),
                              AutoSizeText("Bronze sculpture",style: TextStyle(fontSize: 24),),
                              AutoSizeText("16 1⁄2 x 8 1⁄2 x 9.0 in.",style: TextStyle(fontSize: 24),),
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_3_2.jpg",)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/women_in_art_3_2.jpg"),
                    ),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
