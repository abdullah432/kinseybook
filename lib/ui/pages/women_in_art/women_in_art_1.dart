import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class WomenInArtOne extends StatefulWidget {
  @override
  _WomenInArtOneState createState() => _WomenInArtOneState();
}

class _WomenInArtOneState extends State<WomenInArtOne> {
  final textFiles=["I think I’m just lucky at a time when it’s fashionable or it’s necessary to do something about a black woman.\n — Elizabeth Catlett\n\nOne of the things that most attracted Bernard and Shirley Kinsey to the work of the early African American masters (featured in the “Early Genius” section of this catalogue) was that these painters succeeded as professional artists while the majority of African Americans were still enslaved. The five artists featured in that section were instrumental in establishing an American aesthetic in the 19th century. The artists of the Harlem Renaissance and the broader Negritude Movement further contributed to that aesthetic, redefining the notion of the primitive in modern art and working in a wide variety of media and genres, including literature and music as well as visual art.\n\nThe early masters and others featured in the preceding pages were not the only artists to gain praise and recognition during the 20th century. Black women artists proved their talents through technical skill and aesthetic daring, challenging conventions and experimenting in various media and styles. These innovators hold a special place in the lexicon of African American artists, and the Kinseys have singled out some",
    "of those who have made the load lighter and the road more interesting to travel.\n\nMay Howard Jackson’s Portrait Bust of an African serves as a visual reminder of the transatlantic slave trade for Bernard, and it is one of his most valued works. Jackson was influential in American sculpture and African American artistic pedagogy. Born to a middle-class family in Philadelphia, she was the first African American woman to attend the Pennsylvania Academy of the Fine Arts, where she was enrolled from 1895 to 1898 and again from 1900 to 1902. She was mentored by pioneer African American sculptor Meta Warrick Fuller, but decided against following Fuller to Paris to continue her studies. Instead, Jackson married, settled in Washington, D.C., and became a mentor and instructor to many prolific black artists of the 20th century, including her husband’s nephew, Sargent Johnson, and James Porter.\n\nLaura Wheeler Waring also attended the Pennsylvania Academy of the Fine Arts, beginning in 1908. She was a prominent portrait artist whose works were exhibited in museums around the world. Waring was commissioned to paint the Harmon Foundation’s series “Portraits of Outstanding American Citizens of Negro Origin,” which included the now famous portraits of W. E. B. Dubois and Marian Anderson.\n\nThe Kinseys are stewards of a beautiful rendering of quotidian existence painted by one",
    "of the most widely recognized women artists, Lois Mailou Jones. Born in Boston in 1905, Jones was also mentored by Meta Fuller and attended the Designers Art School of Boston and later the Acade`mie Julien in Paris. She was a teacher and prolific artist at Howard University, earning recognition from President Jimmy Carter at the White House in 1980.\n\nBorn in Georgia in 1891, Alma Thomas and her family moved to Washington, D.C., after the 1906 race riots. Thomas became an art teacher in the public school system and studied design at Howard University. After joining Jones’ Little Paris group of artists, Thomas was introduced to abstract expressionism, which changed her life and her art forever. While abstract, her work is alive with emotion and color.\n\nAnother Jones student at Howard was Elizabeth Catlett, who fell in love with sculpture and the work of Mexican muralists under the tutelage of James Porter. Later, Catlett earned the first master’s degree awarded in fine arts at the State University of Iowa. She won a Rosenwald fellowship in 1946 and traveled to Mexico, where she was befriended by Diego Rivera, Rufino Tamayo, and Francisco Mora (whom she later married). Catlett, who still lives in Mexico, continues to be one of America’s most renowned sculptors."
  ];
  static bool listView=false;
  ScrollController listViewController=ScrollController();
  static double fontSize=50;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
        child: listView?Stack(
          children: <Widget>[
            ListView.builder(
                controller: listViewController,
                itemCount: textFiles.length,
                itemBuilder: (BuildContext context,index){
                  if(index==0){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: "African American Women in the Arts\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                            TextSpan(text: textFiles[0])
                          ],

                        ),
                        presetFontSizes: [fontSize],
                        textAlign: TextAlign.justify,
                      ),
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: AutoSizeText(textFiles[index],minFontSize: fontSize,textAlign: TextAlign.justify),
                  );
                }),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height*0.1,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(onPressed: (){

                        if(listViewController.position.minScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset-listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },icon: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 40,),),
                      Row(children: <Widget>[
                        IconButton(icon: Icon(Icons.zoom_out,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            fontSize=30;
                          });
                        }),
                        IconButton(icon: Icon(Icons.close,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            listView=false;
                          });
                        })
                      ],),
                      IconButton(icon: Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 40,),onPressed: (){
                        if(listViewController.position.maxScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },)
                    ],),
                ))
          ],
        ):Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 30,),
                  Container(width:100,height: 50,child: Stack(children: <Widget>[Positioned(child: GestureDetector(
                      onTap: (){
                        setState(() {
                          fontSize=50;
                          listView=true;
                        });
                      },
                      child: Image.asset("assets/images/magnify.png",scale: 4,)),left: -30,),],overflow: Overflow.visible,)),
                  AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(text: "African American Women in the Arts\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                      ],

                    ),
                    textAlign: TextAlign.justify,
                  ),
                  Expanded(child: AutoSizeText(textFiles[0],presetFontSizes: [22,20,18,16,14],textAlign: TextAlign.justify,))
                ],
              ),
            ),),
            Expanded(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[1],presetFontSizes: [22,20,18,16,14],textAlign: TextAlign.justify,),
            )),
            Expanded(child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: AutoSizeText(textFiles[2],presetFontSizes: [22,20,18,16,14],textAlign: TextAlign.justify,)),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
