import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class WomenInArtTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child:  Row(children: <Widget>[

              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText("Portrait Bust of an African, 1899–1900",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),textAlign: TextAlign.center,),
                    AutoSizeText("May Howard Jackson",style: TextStyle(fontSize: 22),),
                    AutoSizeText("Bronze",style: TextStyle(fontSize: 22),),
                    AutoSizeText("21 x 12 3⁄4 in.",style: TextStyle(fontSize: 22),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("May Howard Jackson (1877–1931)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Jackson was born and educated in Philadelphia. In 1895, she became the first African American woman to be accepted to the Pennsylvania Academy of the Fine Arts, where she studied with William Merritt Chase and John Joseph Boyle. African Americans, particularly those of mixed ancestry, are the focus of her sculptures. Jackson created portrait busts of Paul Lawrence Dunbar and W. E. B. Dubois, among others. She also painted abstract portraits, such as Head of Negro Child and Mulatto Mother and Child. She declined invitations to study in Paris, and while her work reflects the expressive style of the times, her perspective as a black woman is evident. Jackson joined the art department at Howard University in 1922. Her work was exhibited at the Corcoran Gallery in Washington, D.C., in 1915, and at the National Academy of Design in 1916 and 1928.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_2.jpg",)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/women_in_art_2.jpg"),
                    ),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
