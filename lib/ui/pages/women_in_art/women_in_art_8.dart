import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class WomenInArtEight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child:
        Row(children: <Widget>[
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(flex: 1,child: GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_8_1.jpg",)));
                      },
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        child: FittedBox(
                          fit: BoxFit.contain,
                          child: Image.asset("assets/images/women_in_art_8_1.jpg"),
                        ),
                      ),
                    ),),
                    Column(children: <Widget>[
                      SizedBox(height: 30,),
                      AutoSizeText("Jackie, 1974",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                      AutoSizeText("Elizabeth Catlett",style: TextStyle(fontSize: 24),),
                      AutoSizeText("Lithograph",style: TextStyle(fontSize: 24),),
                      AutoSizeText("34 x 27 in.",style: TextStyle(fontSize: 24),),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                        child: AudioViewer(url:"assets/audio/women_in_art_8.mp3"),
                      )
                    ],)

                  ],
                )
              ),
              Expanded(
                flex: 1,
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 20,),
                    Spacer(),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("Elizabeth Catlett (1915– 2012)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Catlett, one of the most famous and prolific women sculptors of the 20th century, was the first African American to earn an MFA from the University of Iowa. In 1940, her thesis project, a limestone sculpture entitled Mother and Child, won first prize in sculpture at the the Chicago Institute of Art and lithography at the Art Negro Exposition in Chicago. She studied ceramics at Students League. She received a Rosenwald Fellowship to study wood and ceramic sculpture in Mexico, where she befriended many muralists and worked with the People’s Graphic Arts Workshop. She married artist Francisco Mora and became a Mexican citizen. Her work is included in the permanent collections of Museum of Modern Art and the Metropolitan Museum of Art in New York, as well as the National Institute of Fine Arts in Mexico City and the National Museum in Prague.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    ),
                    Spacer(),
                    AutoSizeText("Untitled, ca. 1980",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                    AutoSizeText("Elizabeth Catlett",style: TextStyle(fontSize: 24),),
                    AutoSizeText("Bronze sculpture",style: TextStyle(fontSize: 24),),
                    AutoSizeText("19 x 9 x 6 in.",style: TextStyle(fontSize: 24),),
                    SizedBox(height: 30,),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_8_2.jpg",)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.fitHeight,
                      child: Image.asset("assets/images/women_in_art_8_2.jpg"),
                    ),
                  ),
                ),
              ),
            ],)
      ),
    );
  }
}
