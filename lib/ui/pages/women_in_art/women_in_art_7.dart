import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class WomenInArtSeven extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
      return Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
          Expanded(
            flex: 2,
            child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_7.jpg",)));
                },
                child: Image.asset("assets/images/women_in_art_7.jpg")),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 30,),
              AutoSizeText("The Faces of My People, 1990",style: TextStyle(fontWeight: FontWeight.bold),maxLines: 1,
                presetFontSizes:[24,22,20],textAlign: TextAlign.center,),
              AutoSizeText("Margaret Burroughs",presetFontSizes: [22,20],textAlign: TextAlign.center,),
              AutoSizeText("Woodcut",presetFontSizes: [22,20],textAlign: TextAlign.center,),
              AutoSizeText("28 x 32 3⁄4 in.",presetFontSizes: [22,20],textAlign: TextAlign.center,),
            ],),
          SizedBox(height: 20,),
          GestureDetector(
            onTap: (){
              showDialog(context: context,builder: (BuildContext context){
                return Dialog(
                  child: Container(
                    height: MediaQuery.of(context).size.height/1.5,
                    width: MediaQuery.of(context).size.width/1.5,
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 30,),
                              AutoSizeText("Margaret Burroughs (1917–2010 )",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                              SizedBox(height: 20,),
                              Expanded(child: AutoSizeText("SBurroughs was born in St. Rose, Louisiana, in 1917, and moved with her family to the south side of Chicago in 1922. She graduated from Chicago Teachers College in 1937, studied at Chicago State, and became an art teacher at DuSable High School. She married fellow artist Bernard Goss in 1939, and their coach-house flat became a social center, dubbed “little Bohemia,” for a wide and interracial circle of friends and colleagues. Burroughs worked tirelessly to establish the South Side Community Art Center, which opened in 1940, and became the youngest member of its board. She later studied at the Art Institute of Chicago, receiving the MFA in 1948. Burroughs worked in many media, showing special facility in watercolors and linocut printmaking. An accomplished poet, she has also written children’s books. In 1961, with Charles Burroughs, her second husband, she founded the DuSable Museum of African American History in Chicago, which stands as one of the premier institutions for documenting and displaying the African American experience.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                            ],
                          ),
                        ),
                        Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                      ],
                    ),
                  ),
                );
              });
            },
            child: Container(
                height: 80,
                width: 80,
                child: Image.asset("assets/images/bio.png",scale: 3,)),
          ),
          SizedBox(height: 30,)
        ],),
      );
  }
}
