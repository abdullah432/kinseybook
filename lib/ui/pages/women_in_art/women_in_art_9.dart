import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';

class WomenInArtNine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Expanded(flex: 2,child: Row(children: <Widget>[
              Expanded(flex: 1,child: Column(
                children: <Widget>[
                      Expanded(flex: 1,child: GestureDetector(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_9_1.jpg",)));
                        },
                        child: Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        child: FittedBox(
                        fit: BoxFit.contain,
                        child: Image.asset("assets/images/women_in_art_9_1.jpg"),
                        ),
                        ),
                      ),),
                      Expanded(
                      flex: 1,
                      child: Column(children: <Widget>[
                      SizedBox(height: 30,),
                      AutoSizeText("Piano, Bass and Drums, 2000",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),textAlign: TextAlign.center,),
                      AutoSizeText("Phoebe Beasley",style: TextStyle(fontSize: 22),),
                      AutoSizeText("Mixed media",style: TextStyle(fontSize: 22),),
                      AutoSizeText("25 1⁄2 x 25 1⁄2 in.",style: TextStyle(fontSize: 22),),
                ],
              ),),

            ],),),
              Expanded(flex: 2,child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_9_2.jpg",)));
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Image.asset("assets/images/women_in_art_9_2.jpg"),
                  ),
                ),
              ),)
          ],
        ),
      ),Expanded(
              flex: 1,
              child: Row(
                children: <Widget>[
                  Expanded(child: GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_9_3.jpg",)));
                    },
                    child: Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: FittedBox(
                        fit: BoxFit.contain,
                        child: Image.asset("assets/images/women_in_art_9_3.jpg"),
                      ),
                    ),
                  )),
                  Expanded(child: Column(
                    children: <Widget>[
                      SizedBox(height: 30,),
                      AutoSizeText("Shirley at Sixty, 2006",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),),
                      AutoSizeText("Phoebe Beasley",style: TextStyle(fontSize: 22),),
                      AutoSizeText("Mixed media",style: TextStyle(fontSize: 22),),
                      AutoSizeText("20 x 24 in.",style: TextStyle(fontSize: 22),),
                      SizedBox(height: 10,),
                      IconButton(
                        icon: Icon(
                          Icons.play_circle_outline,
                          color: Colors.black,
                        ),
                        iconSize: 80,
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VideoView(
                                    videoLink:
                                    "assets/videos/women_in_art_9.MP4",
                                  )));
                        },
                      )
                    ],
                  )),
                  Expanded(child: Column(
                    children: <Widget>[
                      SizedBox(height: 30,),
                      AutoSizeText("As Violence, 1973",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),),
                      AutoSizeText("Phoebe Beasley",style: TextStyle(fontSize: 22),),
                      AutoSizeText("Watercolor on paper",style: TextStyle(fontSize: 22),),
                      AutoSizeText("20 1⁄2 x 28 1⁄2 in.",style: TextStyle(fontSize: 22),),
                      SizedBox(height: 10,),
                      GestureDetector(
                        onTap: (){
                          showDialog(context: context,builder: (BuildContext context){
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height/1.5,
                                width: MediaQuery.of(context).size.width/1.5,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          SizedBox(height: 30,),
                                          AutoSizeText("Phoebe Beasley (1943– )",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                          SizedBox(height: 20,),
                                          Expanded(child: AutoSizeText("Beasley was born in Cleveland and earned a BFA from Ohio State University. After moving to Los Angeles, she sustained dual careers as an artist and a media executive. Beasley is the only artist to have earned a Presidential Seal on two occasions, for commissions from Presidents, Clinton and George H. W. Bush. Beasley’s work has been shown at the Kansas African American Museum and in a solo exhibition at the M. Hanks Gallery in Santa Monica, California. Another project was a collaboration with Maya Angelou, in which Beasley created several serigraphs based on poems by Langston Hughes for a limited edition book, Sunrise is Coming after While. Beasley’s work has recently been featured in the touring exhibition “Portraying Lincoln: Man of Many Faces.”",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                        ],
                                      ),
                                    ),
                                    Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                  ],
                                ),
                              ),
                            );
                          });
                        },
                        child: Container(
                            height: 80,
                            width: 80,
                            child: Image.asset("assets/images/bio.png",scale: 3,)),
                      ),
                    ],
                  ))
                ],
              ),
            )])),
    );
  }
}
