import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class WomenInArtFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child:Row(children: <Widget>[

              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText("Mr. Bernard Kinsey, 2004",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),),
                    AutoSizeText("Buena Johnson",style: TextStyle(fontSize:22),),
                    AutoSizeText("Watercolor, pencil, collage",style: TextStyle(fontSize:22),),
                    AutoSizeText("20 x 16 in.",style: TextStyle(fontSize: 22),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("Buena Johnson (1954 – )",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Johnson received her BFA in Communications Design from the Pratt Institute of Art in New York. She is a trained illustrator, teacher, photographer and fine artist. Inspired by the intricate line drawings of German Renaissance artist Albrecht Durer, Johnson draws with a photographic realism style that captures minute detail to intensify reality. Using symbolic imagery, her aim is “to present a message that will uplift, encourage and inspire the spirit of the viewer”. Johnson worked for several years in advertising and editorial work for major corporations, book publishers, magazines, and newspapers. She has lectured, done art demonstrations, and exhibited throughout the United States and Japan, and currentlyresides in Southern California. This painting was commissioned by the Torrance Chamber of Commerce as a gift to Mr. Kinsey for his outstanding community service in 2004.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_4.jpg",)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/women_in_art_4.jpg"),
                    ),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
