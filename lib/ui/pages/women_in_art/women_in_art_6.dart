import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';


class WomenInArtSix extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child:
            Row(children: <Widget>[
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Expanded(
                        flex: 3,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_6_1.jpg",)));
                              },
                              child: Image.asset("assets/images/women_in_art_6_1.jpg")),
                        ),),
                      SizedBox(height: 20,),
                      AutoSizeText("Untitled, n.d.",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                      AutoSizeText("Alma Thomas",style: TextStyle(fontSize: 24),),
                      AutoSizeText("Oil on canvas",style: TextStyle(fontSize: 24),),
                      AutoSizeText("30 x 25.1⁄2 in.",style: TextStyle(fontSize: 24),),
                      SizedBox(height: 20,),
                      GestureDetector(
                        onTap: (){
                          showDialog(context: context,builder: (BuildContext context){
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height/1.5,
                                width: MediaQuery.of(context).size.width/1.5,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          SizedBox(height: 30,),
                                          AutoSizeText("Alma Thomas (1891–1978)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                          SizedBox(height: 20,),
                                          Expanded(child: AutoSizeText("Alma Thomas was the first graduate of the Howard University art department, and the first black woman to earn an MFA from Columbia University. After teaching in the Washington, D.C., public schools for 35 years, she began her professional career in earnest. She was among the color-field painters active in Washington and shared a collaborative relationship with friends Morris Louis, Kenneth Nolan and Gene Davis. In 1943, she was asked by James Vernon Herring and Alonzo Aden to join them in establishing the Barnett-Aden Gallery, the first African American gallery in Washington. In 1971, when she was 80 years old, she was the first female African American artist to be recognized in a solo exhibition at the Whitney Museum of American Art.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                                        ],
                                      ),
                                    ),
                                    Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                  ],
                                ),
                              ),
                            );
                          });
                        },
                        child: Container(
                            height: 80,
                            width: 80,
                            child: Image.asset("assets/images/bio.png",scale: 3,)),
                      )
                    ],
                  ),
                )
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      AutoSizeText("Woman Wearing Orange Scarf, ca. 1940",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                      AutoSizeText("Laura Wheeler Waring",style: TextStyle(fontSize: 24),),
                      AutoSizeText("Oil on canvas",style: TextStyle(fontSize: 24),),
                      AutoSizeText("17 x 12 in.",style: TextStyle(fontSize: 24),),
                      SizedBox(height: 20,),
                      Expanded(
                        flex: 3,
                        child: GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/women_in_art_6_2.jpg",)));
                          },
                          child: Container(
                          height: MediaQuery.of(context).size.height*0.6,
                          width: MediaQuery.of(context).size.width,
                          child: FittedBox(
                            fit: BoxFit.contain,
                            child: Image.asset("assets/images/women_in_art_6_2.jpg"),
                          ),
                      ),
                        ),),
                    ],
                  ),
                ),
              ),

            ],)
      ),
    );
  }
}
