import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class UnderstandingThree extends StatelessWidget {
  final textFiles = [
    "about telling a story of the African American experience. We want to grant our ancestors voices, names, and personalities, so that we and others can better understand their triumphs and accomplishments, despite the challenges and obstacles they faced. Since 2008, we have presented our lecture series, ”What You Didn’t Learn in High School History,” to more than 30,000 people across the country and abroad. We have witnessed the transformative power of the objects and underlying stories contained in the Kinsey Collection. In the course of our work, we have come to employ a different lexicon from that commonly used in the study of our history and race. These words enhance our personal connection to individuals and events. Instead of “slave” we use “enslaved person,” recognizing that our ancestors were human beings forced into slavery. Instead of “mercantilism” we use “kidnapping,” in place of “wilderness,” “home”— for America was already inhabited by millions of native Americans before the arrival of Europeans. Instead of “discovered” we use “conquered,” instead of “plantation,” “prison”—for what was a plantation to whites was a plot of land that our ancestors were born on, confined to, and forced to work on until their deaths.\n\nOur collection began through the gift of a historical document, a bill of sale dated 1832 for William Johnson, a slave sold for \$550, the equivalent of \$14,000 today. As I held this document in my hands, my life changed: it was as if I was holding not only the fate of that man but all that went before and after him in this terrible",
    "destiny called slavery. That document inspired my quest to learn all I could about my ancestors and African American history.\n\nLerone Bennett, Jr. has explained a concept he calls “the myth of absence”:\n\nIn American history, as in American life, Black Americans are invisible presences. They are not seen, not because of their absence but because of the presence of a myth that prepares and requires their absence. The myth of absence, which expresses this idea and intention, operates not by misinterpretation and slander but by silence and exclusion.\n\nThe way to counter the “myth of absence” is to dig underneath the traditional definitions of historical events and documents in order to provide an alternative point of view. By displaying artifacts that show African American perspectives, I hope to dispel the myths that still prevail about black culture and history—about family, hard work, and genius; about Jim Crow and the courage of our people. It is essential to be able to connect scholarship to tangible artifacts that document this history, so that it can be seen and known in another way.\n\nOur collecting has shed light on compelling facts and figures in African American history that deserve to be much better known. Here are some highlights from our discoveries.\n\nOne little-known and fascinating story concerns",
    "Antonio and Isabela Tucker, who came to this continent in 1619 as indentured servants rather than slaves. They earned their freedom, married, and had a son, William, born in 1625, who was the first child of African parents baptized in Jamestown. Black family life has its roots in an era that long preceded the institution of slavery and its effects on the black family. We are proud to have permission from the St. Augustine Diocese to show the earliest known document of African American existence in America, twelve years before Jamestown. We now have a 1595 baptismal record of an African American girl named Estabana, a 1598 marriage certificate of an African American couple named Augustine and Francisca—demonstrating that African Americans were an integral part of this country beginning in 1565.\n\nResearching in various media, I learned that in 1640, farmer Hugh Gwyn’s servants escaped to Maryland. A Dutchman, a Scot, and a black man, John Punch, were all punished for the same crime. The Dutchman and the Scot had their indentures extended by one year while John Punch was sentenced to slavery for life.It was John Punch’s color that doomed him to a life of enslavement.\n\nAn important contribution to life in early America that needs more attention is the work of Phillis Wheatley. Many people don’t know that she was a brilliant writer, as popular in her time as Oprah Winfrey is today. She arrived on a slave ship and became a literary force who read for the Founding Fathers, including Benjamin Franklin."
  ];
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: template.viewWidth() * 5,
            ),
            Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Padding(
                padding: template.textcontainerPadding(),
                child: Text(
                  textFiles[0],
                  textAlign: TextAlign.justify,
                  style: template.bodyTextStyle(),
                ),
              ),
            ),
            Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Padding(
                padding: template.textcontainerPadding(),
                child: Text(
                  textFiles[1],
                  textAlign: TextAlign.justify,
                  style: template.bodyTextStyle(),
                ),
              ),
            ),
            Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Padding(
                padding: template.textcontainerPadding(),
                child: Text(
                  textFiles[2],
                  textAlign: TextAlign.justify,
                  style: template.bodyTextStyle(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
