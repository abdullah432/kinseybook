import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class UnderstandingTwo extends StatefulWidget {
  @override
  _UnderstandingTwoState createState() => _UnderstandingTwoState();
}

class _UnderstandingTwoState extends State<UnderstandingTwo> {
  final textFiles = [
    "that I realized only much later, Dr. James Eaton, a professor of history. The founder of the Black Archives on campus, he was the keeper of all things FAMU, and also documented the struggle and achievements of people of color in Florida and elsewhere in the South. In class he often said, “There would be no history in this country if it weren’t for black folks. African American history is American history.” My college experience in the 1960s helped shape my values and my attitudes about being black. For many years I struggled with the reality of American segregation and racism, and always wanted to know how black people in this country got into this predicament. Dr. Eaton’s saying stuck with me for many years, and through travel, study, and reading I came to know what he meant. We have been here from the beginning, and we have a story to tell that all Americans must know. Each of us must engage with that history and take the narrative forward and outward.\n\nMy commitment to civil rights has taken many forms over the past four decades, from my days in college protesting segregated conditions, to Xerox, where I helped shape the company and hired thousands of African Americans, Latinos, and women. My work as co-chair of Rebuild LA attracted nearly \$400 million in new capital to rebuild the city after the 1992 uprising, and Shirley and I have worked together to raise over \$26 million for community organizations and for educational scholarships to historically black colleges.\n\nNow we are sharing the wonderful story",
    "of our ancestors’ struggles, triumphs, and accomplishments through The Kinsey African American Art & History Collection. My understanding of my parents’ efforts and Dr. Eaton’s lessons is embodied here in this book, through my personal history, my collecting, and my own words.\n",
    "Shirley and I have been married for fifty years. Our journey together has been filled with experiences we cherish: the birth of our son Khalil, and travel, learning, and sharing our personal history with friends and our community. Although we enjoy discovering new places and ideas, we have focused our educational efforts on our personal past as well as its connection to history. Shirley documents our personal history through visual and written momentos. I concentrate on exploring books and historical artifacts that affirm the perseverance of people of color and their contributions to the world.\n\nHistorians and scholars have taught us a great deal about what we collect. Bibliophile Charles Blockson compiled A Commented Bibliography of 101 Influential Books by and about People of African Descent (1556–1982), a work that became my bible in discovering stories of our people. For example,",
    "Leo Africanus’s Description of Africa (1550)— represented in our collection in an English edition published in 1600—supports the view that Africa was not a “dark” continent but a very sophisticated civilization that made discoveries in astronomy, medicine, and science. We owe deep gratitude to Henry Proctor Slaughter, Dr. Dorothy Porter Wesley, Arthur Schomburg, Carter G. Woodson, Alain Locke, and Lerone Bennett Jr. for preserving our heritage through books, pamphlets, and documents. Edward Pratt, an artist and art historian, guided my discovery of the connection between art and history, alerting me in particular to the role of the Barnett Aden Collection in the development of African American culture.\n\nKenneth M. Stampp’s The Peculiar Institution: Slavery in the Ante-Bellum South has profoundly influenced the modern understanding of slavery. John Hope Franklin’s groundbreaking book From Slavery to Freedom and Herbert Apetheker’s writings documenting the slave revolts beginning in the seventeenth century have exploded the myth that black people accepted their fate. Through these works, we encountered people who resisted their bondage through cunning, courage, and even genius, longing to be free. Until I read Franklin’s book, I viewed our history backwards, as most people do, seeing slavery as an isolated event essentially occurring in the South, when in truth slavery dominated every aspect of American life: politics, economy, religion, social mores, and laws in both North and South.\nAgainst this backdrop, Shirley and I have set"
  ];

  bool listView = false;

  ScrollController listViewController = ScrollController();

  double fontSize = 50;

  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 11;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: listView
            ? Stack(
                children: <Widget>[
                  ListView.builder(
                      controller: listViewController,
                      itemCount: textFiles.length,
                      itemBuilder: (BuildContext context, index) {
                        if (index == 1) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: AutoSizeText(
                                    textFiles[1],
                                    presetFontSizes: [fontSize],
                                    textAlign: TextAlign.justify,
                                  ),
                                ),
                                Container(
                                    width: double.infinity,
                                    child: AutoSizeText(
                                      "— Bernard W. Kinsey",
                                      textAlign: TextAlign.right,
                                      presetFontSizes: [fontSize],
                                    )),
                                SizedBox(
                                  height: 20,
                                ),
                                AutoSizeText(
                                  "Understanding the Past Part II:\nCollecting History",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                  presetFontSizes: [fontSize],
                                ),
                              ],
                            ),
                          );
                        }
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          child: AutoSizeText(textFiles[index],
                              minFontSize: fontSize,
                              textAlign: TextAlign.justify),
                        );
                      }),
                  Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        color: Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            IconButton(
                              onPressed: () {
                                if (listViewController
                                        .position.minScrollExtent ==
                                    listViewController.offset) {
                                  print("on the end");
                                } else {
                                  listViewController.animateTo(
                                      listViewController.offset -
                                          listViewController
                                                  .position.maxScrollExtent /
                                              7,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.easeIn);
                                }
                              },
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.blue,
                                size: 40,
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                IconButton(
                                    icon: Icon(
                                      Icons.zoom_out,
                                      color: Colors.blue,
                                      size: 40,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        fontSize = 30;
                                      });
                                    }),
                                IconButton(
                                    icon: Icon(
                                      Icons.close,
                                      color: Colors.blue,
                                      size: 40,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        listView = false;
                                      });
                                    })
                              ],
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.blue,
                                size: 40,
                              ),
                              onPressed: () {
                                if (listViewController
                                        .position.maxScrollExtent ==
                                    listViewController.offset) {
                                  print("on the end");
                                } else {
                                  listViewController.animateTo(
                                      listViewController.offset +
                                          listViewController
                                                  .position.maxScrollExtent /
                                              7,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.easeIn);
                                }
                              },
                            )
                          ],
                        ),
                      ))
                ],
              )
            : Column(
                children: <Widget>[
                  Expanded(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: template.sizeBoxWidth(),
                        ),
                        Container(
                          height: template.viewHeight() * 282,
                          width: template.textContainerwidth(),
                          child: Padding(
                            padding: template.textcontainerPadding(),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  textFiles[0],
                                  textAlign: TextAlign.justify,
                                  style: template.bodyTextStyle(),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                            height: template.viewHeight() * 282,
                            width: template.textContainerwidth(),
                            child: Padding(
                              padding: template.textcontainerPadding(),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Text(
                                      textFiles[1],
                                      style: template.bodyTextStyle(),
                                      textAlign: TextAlign.justify,
                                    ),
                                  ),
                                  Container(
                                      width: template.textContainerwidth(),
                                      child: Text(
                                        "— Bernard W. Kinsey",
                                        textAlign: TextAlign.right,
                                        style: template.bodyTextStyle(),
                                      )),
                                  SizedBox(
                                    height: template.viewHeight() * 8,
                                  ),
                                  Container(
                                      width: template.viewWidth() * 100,
                                      height: template.viewHeight() * 15,
                                      child: Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  fontSize = 50;
                                                  listView = true;
                                                });
                                              },
                                              child: RotatedBox(
                                                quarterTurns: 1,
                                                child: Image.asset(
                                                  "assets/images/magnify.png",
                                                  height: template
                                                      .magnifyingGlass(),
                                                ),
                                              ),
                                            ),
                                            left: -template.viewWidth() * 8,
                                          ),
                                        ],
                                        overflow: Overflow.visible,
                                      )),
                                  Text(
                                    "Understanding the Past Part II:\nCollecting History",
                                    style: template.textheaderStyle(),
                                    maxLines: 2,
                                  ),
                                  SizedBox(
                                    height: template.viewHeight() * 3,
                                  ),
                                  Container(
                                      height: template.viewHeight() * 155,
                                      width: template.textContainerwidth(),
                                      child: Text(
                                        textFiles[2],
                                        style: template.bodyTextStyle(),
                                        textAlign: TextAlign.justify,
                                      )),
                                ],
                              ),
                            )),
                        Container(
                          height: template.viewHeight() * 282,
                          width: template.textContainerwidth(),
                          child: Padding(
                            padding: template.textcontainerPadding(),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Text(
                                    textFiles[3],
                                    textAlign: TextAlign.justify,
                                    style: template.bodyTextStyle(),
                                  ),
                                ]),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: template.viewHeight() * 2,
                    width: wt,
                    child: Padding(
                      padding: EdgeInsets.only(left: (wt / 390) * 8),
                      child: Container(
                        color: template.boxColor(),
                      ),
                    ),
                  ),
                  SizedBox(height: template.viewHeight() * 2.5),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth() * 3,
                      child: Padding(
                          padding:
                              EdgeInsets.only(right: template.viewWidth() * 4),
                          child: template.footerRightText()))
                ],
              ),
      ),
    );
  }
}
