import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class UnderstandingFour extends StatelessWidget {
  final textFiles = [
    "I am interested in the early work of black businessmen, particularly Philadelphian James Forten, who amassed an estate of over \$100,000. One of the earliest black abolitionists, as well as one of the earliest successful black entrepreneurs, he managed one of the first known integrated work forces in America, with an almost even division between black and white workers in a city that had experienced frequent racial violence. Although virtually unknown today, Forten made a tremendous contribution to the fabric of this country. We do not possess any artifacts that serve to tell his story, but it is one that very much needs to be told. I include him in the exhibition catalogue for this reason.\n\nThe Buffalo Soldiers were the most decorated unit in American military history. They received",
    "eighteen Congressional Medals of Honor, including five for the taking of San Juan Hill. Among them is Henry Flipper, the first black man to graduate from West Point, who left the institution in 1877—not a single word was spoken to him in four years. One of the first superintendents of Yosemite National Park was Buffalo soldier Colonel Charles Young. These are our heroes.\n\nYou can’t understand the Civil War without understanding slavery, or understand Civil Rights without understanding Jim Crow—because imbedded in both of these narratives is the near total domination and oppression of African Americans, save for the very brief period of Reconstruction from 1868 to 1878, when African Americans were allowed to vote and elected more",
    "han two thousand African American politicians from sheriffs to U.S. senators.\n\nThe record of enslavement dramatically changed the social, political, and economic landscape of America. Its effects can still be felt We know from our own experiences and study that learning about African American accomplishments and struggles contributes to better interracial understanding and tolerance. Share the wealth of African American experience in these pages and in our exhibition as it travels the nation. Use public libraries and universities to connect with history in a personal way and discover your own heroes.\n\n"
  ];
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 13;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/understanding4.jpg",
                              )));
                },
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.45,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                    fit: BoxFit.fill,
                    child: Image.asset("assets/images/understanding4.jpg"),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(
                    left: template.viewWidth() * 5,
                    right: template.viewWidth() * 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: template.viewHeight() * 200,
                      width: template.textContainerwidth(),
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: template.viewHeight() * 10,
                            left: template.viewWidth() * 2.5,
                            right: template.viewWidth() * 2.5),
                        child: Text(
                          textFiles[0],
                          textAlign: TextAlign.justify,
                          style: template.bodyTextStyle(),
                        ),
                      ),
                    ),
                    Container(
                      height: template.viewHeight() * 200,
                      width: template.textContainerwidth(),
                      child: Padding(
                          padding: EdgeInsets.only(
                              top: template.viewHeight() * 10,
                              left: template.viewWidth() * 2.5,
                              right: template.viewWidth() * 2.5),
                          child: Text(
                            textFiles[1],
                            textAlign: TextAlign.justify,
                            style: template.bodyTextStyle(),
                          )),
                    ),
                    Container(
                      height: template.viewHeight() * 200,
                      width: template.textContainerwidth(),
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: template.viewHeight() * 10,
                            left: template.viewWidth() * 2.5,
                            right: template.viewWidth() * 2.5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                textFiles[2],
                                textAlign: TextAlign.justify,
                                style: template.bodyTextStyle(),
                              ),
                            ),
                            Container(
                              height: template.viewHeight() * 16,
                              width: template.textContainerwidth(),
                              child: Text(
                                "— Bernard W. Kinsey",
                                textAlign: TextAlign.right,
                                style: template.bodyTextStyle(),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              height: template.viewHeight() * 2,
              width: wt,
              child: Padding(
                padding: EdgeInsets.only(left: (wt / 390) * 8),
                child: Container(
                  color: template.boxColor(),
                ),
              ),
            ),
            SizedBox(
              height: (ht / 300) * 2,
            ),
            Container(
                height: template.footerTextContainerHihght(),
                width: template.textContainerwidth() * 3,
                child: template.footerRightText())
          ],
        ),
      ),
    );
  }
}
