import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';
import 'package:auto_size_text/auto_size_text.dart';

class UnderstandingOne extends StatefulWidget {
  @override
  _UnderstandingOneState createState() => _UnderstandingOneState();
}

class _UnderstandingOneState extends State<UnderstandingOne> {
  final textFiles = [
    "l grew up in the South in the 1950s and 1960s in a solid middle-class family. With my dad, who was a school principal, and my stay-at-home mom and five siblings, I lived in West Palm Beach, Florida, in a totally segregated world. We lived in public housing, “The Projects,” as it was called. It was simply where everyone lived: the teachers, the postman, and the gardeners. Everyone was content with where they lived, regardless of their class. Everything that was necessary and important to me was right in our community, between 1st and 25th Streets and the two railroad tracks.\n\nMy family treasured music, books, and achievement, and valued thrift—all of which grounded my brothers and sisters and me. My dad, U .B. Kinsey, was a civil rights legend in Palm Beach County for refusing to accept the strictures of segregation, Jim Crow, or mediocrity. For thirty-nine years he was the principal at Palmview Elementary School, which now bears his name, and he was active in every fight for better conditions for blacks in West Palm Beach. ",
    "In 1941, Dad and a group of black educators sued the Palm Beach County school board in a case seeking equal pay for black teachers and equal education for black students. The traditional school calendar lasted from September to June, but in the South black students typically attended school only from January to June and were required to do farm labor for the rest of the year. Thurgood Marshall, the attorney who argued on their behalf and won, later relied on, the precedent to attack Plessy v. Ferguson, which led to the Brown v. Board of Education ruling of 1954.\n\nMy dad fought for the black community: for sewers, streetlights, sidewalks, landscaping, and, to his credit, the black community is still beautiful today, with well-kept lawns and homes. Growing up in a home with a strong, courageous, and loving father made a big impression on me, particularly watching Dad live by his motto, “Stand for Right.” He consistently did so, in our segregated community and beyond.\n\nMy mom remains the consummate homemaker. Juggling our family finances on a black teacher’s salary during the 1940s and 1950s taught me the value of a dollar— how to save and how to take care of what you own. Mom would go to three grocery stores with a coupon wallet to ensure that we got the best value for our purchases. These fiscal lessons anchor the way Shirley and I live our lives.\n\nEducation was very important in our family. Mom and Dad met in college, at Florida A&M",
    "University in Tallahassee. They later married and moved to West Palm Beach. Every year, one of the most important football games in America was the Orange Blossom Classic, played in Miami between Florida A&M and the top black college team for the Negro national championship. The game typically drew 40,000 fans, including Mom and Dad, who would take all of us. It was there that I saw the Florida A&M University “Marching 100,” and I decided on the spot that I would go to FAMU and play in the band. I attended Palmview Elementary and Roosevelt High School—my parents’ alma mater—and after a very successful career in high school, where I was class president, I entered FAMU in 1961. By 1964, Mom and Dad had four kids in college at the same time. Years later, I learned how much they sacrificed to send us to college, and grew to recognize the way education indelibly stamps young people with the promise of achieving the American dream, even more so for black Americans.\n\nFAMU had a profound impact on my life. I met Shirley, the best part of my day, as well as many people who would become close friends, including Nick Walker, Al Williams, Mike Williams, and Thomas Mitchell. My relationships with the directors of the Marching 100, Dr. William P. Foster and Dr. Julian White, and other faculty and administrators enabled me to develop the leadership and interpersonal skills I needed to compete effectively in the white world I soon entered.\n\nOne faculty member had an influence on me"
  ];

  bool listView = false;

  ScrollController listViewController = ScrollController();

  double fontSize = 50;

  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Center(
          child: listView
              ? Stack(
                  children: <Widget>[
                    ListView.builder(
                        controller: listViewController,
                        itemCount: textFiles.length,
                        itemBuilder: (BuildContext context, index) {
                          if (index == 0) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              child: AutoSizeText.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(
                                        text:
                                            "Understanding the Past Part I:\nA Personal Perspective\n\n",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 24)),
                                    TextSpan(text: textFiles[0])
                                  ],
                                ),
                                presetFontSizes: [fontSize],
                                textAlign: TextAlign.justify,
                              ),
                            );
                          }
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            child: AutoSizeText(textFiles[index],
                                minFontSize: fontSize,
                                textAlign: TextAlign.justify),
                          );
                        }),
                    Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          color: Colors.white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              IconButton(
                                onPressed: () {
                                  if (listViewController
                                          .position.minScrollExtent ==
                                      listViewController.offset) {
                                    print("on the end");
                                  } else {
                                    listViewController.animateTo(
                                        listViewController.offset -
                                            listViewController
                                                    .position.maxScrollExtent /
                                                7,
                                        duration: Duration(milliseconds: 500),
                                        curve: Curves.easeIn);
                                  }
                                },
                                icon: Icon(
                                  Icons.arrow_back_ios,
                                  color: Colors.blue,
                                  size: 40,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  IconButton(
                                      icon: Icon(
                                        Icons.zoom_out,
                                        color: Colors.blue,
                                        size: 40,
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          fontSize = 30;
                                        });
                                      }),
                                  IconButton(
                                      icon: Icon(
                                        Icons.close,
                                        color: Colors.blue,
                                        size: 40,
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          listView = false;
                                        });
                                      })
                                ],
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.blue,
                                  size: 40,
                                ),
                                onPressed: () {
                                  if (listViewController
                                          .position.maxScrollExtent ==
                                      listViewController.offset) {
                                    print("on the end");
                                  } else {
                                    listViewController.animateTo(
                                        listViewController.offset +
                                            listViewController
                                                    .position.maxScrollExtent /
                                                7,
                                        duration: Duration(milliseconds: 500),
                                        curve: Curves.easeIn);
                                  }
                                },
                              )
                            ],
                          ),
                        ))
                  ],
                )
              : Column(
                  children: <Widget>[
                    SizedBox(height: template.viewHeight() * 8),
                    Container(
                      height: template.viewHeight() * 2,
                      width: wt,
                      child: Padding(
                        padding: EdgeInsets.only(left: (wt / 390) * 8),
                        child: Container(
                          color: template.boxColor(),
                        ),
                      ),
                    ),
                    Container(
                      height: template.viewHeight() * 280,
                      width: template.viewWidth() * 395,
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: template.viewWidth() * 5,
                          ),
                          Container(
                            height: template.viewHeight() * 280,
                            width: template.textContainerwidth(),
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: (ht / 300) * 10,
                                  left: (wt / 390) * 2.5,
                                  right: (wt / 390) * 2.5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Understanding the Past Part I:\nA Personal Perspective",
                                    style: template.textheaderStyle(),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => VideoView(
                                                    videoLink:
                                                        "assets/videos/understanding_video.MP4",
                                                  )));
                                    },
                                    child: Image.asset(
                                      "assets/images/understanding1.jpg",
                                      height: template.viewHeight() * 50,
                                    ),
                                  ),
                                  Container(
                                    width: template.textContainerwidth(),
                                    height: template.viewHeight() * 12,
                                    child: Stack(
                                      children: <Widget>[
                                        Positioned(
                                          child: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  fontSize = 50;
                                                  listView = true;
                                                });
                                              },
                                              child: RotatedBox(
                                                quarterTurns: 1,
                                                child: Image.asset(
                                                  "assets/images/magnify.png",
                                                  height: template
                                                      .magnifyingGlass(),
                                                ),
                                              )),
                                          left: -template.viewWidth() * 10,
                                        ),
                                      ],
                                      overflow: Overflow.visible,
                                    ),
                                  ),
                                  Container(
                                    height: template.viewHeight() * 180,
                                    width: wt / 3.1,
                                    child: Text(textFiles[0],
                                        textAlign: TextAlign.justify,
                                        style: template.bodyTextStyle()),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            height: template.viewHeight() * 280,
                            width: template.textContainerwidth(),
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: (ht / 300) * 10,
                                  left: (wt / 390) * 2.5,
                                  right: (wt / 390) * 2.5),
                              child: Text(textFiles[1],
                                  textAlign: TextAlign.justify,
                                  style: template.bodyTextStyle()),
                            ),
                          ),
                          Container(
                            height: template.viewHeight() * 280,
                            width: template.textContainerwidth(),
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: (ht / 300) * 10,
                                  left: (wt / 390) * 2.5,
                                  right: (wt / 390) * 2.5),
                              child: Text(
                                textFiles[2],
                                textAlign: TextAlign.justify,
                                style: template.bodyTextStyle(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: template.viewHeight() * 5,
                      width: template.footerContainerWidth() * 3,
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        '10 Kisney|Book',
                        textAlign: TextAlign.center,
                        style: template.footerTextStyle(),
                      ),
                    )
                  ],
                ),
        ),
      ),
    );
  }
}
