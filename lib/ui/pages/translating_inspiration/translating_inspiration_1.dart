import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';


class TranslatingInspirationOne extends StatefulWidget {
  @override
  _TranslatingInspirationOneState createState() => _TranslatingInspirationOneState();
}

class _TranslatingInspirationOneState extends State<TranslatingInspirationOne> {
  final textFiles=["My introduction to art, and consequently my passion for it and all things creative, came from my parents. As far back as I can remember, there were vivid colors and profound words; powerful images and sculpture that seemed like they were larger than life. I remember being drawn to abstract paintings and forming my own interpretations. I learned at an early age that art could be used to escape, heal, tell a story, bridge gaps, and inspire. I learned by experiencing all of this, and so much more, firsthand.\n\nI was extremely fortunate to grow up in a household that emphasized education, both formal and nontraditional. Through travel to many parts of the world with my parents, I was exposed to the various ways people express themselves and celebrate their cultures, their individuality as well as their similarities. I learned that the world was bigger than my backyard, and that although we may have different approaches, we are far more alike than we are different: we experience the same emotions and desires and are all ultimately searching to live fulfilling lives. As a result of these observations and encounters, I saw that there are many ways to find that fulfillment, and I was fascinated with creative expression as a way not only to become successful but also to love, to give, and to find peace within oneself.",
    "My parents helped to give me a head start by making sure I got a well-rounded view of the world. I was often the only kid in a room full of adults, but instead of being forced to act older than my age, I was engaged by the discussions, invited to express my opinions and interests, and I felt that those opinions and interests were valued.\n\nAs my parents became more involved in the art world, I played a part in picking out works and in turn developed my own interest in and passion for art. Some of my favorite artists became like aunts and uncles to me. They took an interest in me and urged me to find my own creative outlets. I grew up surrounded by some of the greatest painters, sculptors, musicians, writers, intellectuals, and corporate moguls of our time. These encounters shaped my life.\n\nAlthough this book is focused on the collection of paintings, sculpture, and historical artifacts that my parents have amassed during a partnership of over fifty years, it is this collection that has given me a profound appreciation for the arts as a whole. Music, dance, poetry, and theater, along with the visual arts, have helped me to discover myself and my passions. As a teenager I discovered self-expression through poetry/hip- hop, graffiti, dance, and song. It was through my parents’ passion that I discovered my own, but it was also through their passion that I discovered my heritage. I’ve learned so much about African",
    "American history and the rich culture from which we come. Through this knowledge I have found pride and confidence, and have gained greater insight into the issues of race in America and the ways they affect my life.\n\nThe Kinsey African American Art & History Collection\n\ntells the story of the African and African American experience both in the motherland and in the Americas. It shows where we have been and where we are going. It shows our oppression and our progress, our resilience and perseverance, our creativity and our genius. To view the collection and not be overwhelmed with pride is impossible. I am ever proud that these works are being shared because they are true educational tools—and even more, in my opinion, they are inspirational tools.\n\n"
  ];

  bool listView=false;

  double fontSize=50;

  ScrollController listViewController=ScrollController();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
        child: listView?Stack(
          children: <Widget>[
            ListView.builder(
                controller: listViewController,
                itemCount: textFiles.length,
                itemBuilder: (BuildContext context,index){
                  if(index==0){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: "Translating Inspiration\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                            TextSpan(text: textFiles[0])
                          ],

                        ),
                        presetFontSizes: [fontSize],
                        textAlign: TextAlign.justify,
                      ),
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: AutoSizeText(textFiles[index],minFontSize: fontSize,textAlign: TextAlign.justify),
                  );
                }),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height*0.1,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(onPressed: (){

                        if(listViewController.position.minScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset-listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },icon: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 40,),),
                      Row(children: <Widget>[
                        IconButton(icon: Icon(Icons.zoom_out,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            fontSize=30;
                          });
                        }),
                        IconButton(icon: Icon(Icons.close,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            listView=false;
                          });
                        })
                      ],),
                      IconButton(icon: Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 40,),onPressed: (){
                        if(listViewController.position.maxScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },)
                    ],),
                ))
          ],
        ):Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 30,),
                  Container(width:100,height: 50,child: Stack(children: <Widget>[Positioned(child: GestureDetector(
                      onTap: (){
                        setState(() {
                          fontSize=50;
                          listView=true;
                        });
                      },
                      child: Image.asset("assets/images/magnify.png",scale: 4,)),left: -30,),],overflow: Overflow.visible,)),
                  AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(text: "Translating Inspiration\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                      ],

                    ),
                    textAlign: TextAlign.justify,
                  ),
                  Expanded(child: AutoSizeText(textFiles[0],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,))
                ],
              ),
            ),),
            Expanded(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[1],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
            )),
            Expanded(child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                      child: AutoSizeText(textFiles[2],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,)),
                  Container(
                      width: double.infinity,
                      child: AutoSizeText("— Khalil B. Kinsey\nChief Curator & General Manager",textAlign: TextAlign.right,minFontSize: 20,))
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
