import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class TranslatingInspirationTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(children: <Widget>[
              Expanded(
                flex: 2,
                child: GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/translating_inspiration_2.jpg",)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.fill,
                      child: Image.asset("assets/images/translating_inspiration_2.jpg"),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText("Sinalunga, 2001",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 30),),
                    AutoSizeText("Carl Anderson",style: TextStyle(fontSize: 24),),
                    AutoSizeText("Oil on canvas",style: TextStyle(fontSize: 24),),
                    AutoSizeText("20 x 20 in.",style: TextStyle(fontSize: 24),),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){
                        showDialog(context: context,builder: (BuildContext context){
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height/1.5,
                              width: MediaQuery.of(context).size.width/1.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        SizedBox(height: 30,),
                                        AutoSizeText("Carl Anderson (1945–2004)",style: TextStyle(color: Colors.black,fontSize: 24,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                                        SizedBox(height: 20,),
                                        Expanded(child: AutoSizeText("Born in Lynchburg, Virginia, Anderson was one of 12 children. He became a well-known film star and prolific recording artist beginning in the 1970s; he was also an artist. In film, he appeared as Judas in Jesus Christ Superstar (1974) and subsequently in The Black Pearl (1978) and Steven Spielberg’s The Color Purple (1985). On television he appeared in Cop Rock, Days of our Lives, and Hill Street Blues. As a recording artist, Anderson released nine jazz and soul albums. He collaborated with Stevie Wonder on the landmark double album Songs in the Key of Life (1976), and his influence can be heard in many recordings made in that decade. “Friends and Lovers,” a duet with Gloria Loring, was the title song from a 1986 album. In 1992, he reprised the role of Judas on a national tour that lasted for five years and grossed over \$100 million. A personal note: Anderson became a very close friend and traveling companion of the Kinseys. Sinalunga, Italy, a place they visited together, was the setting for his first painting.",presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,))
                                      ],
                                    ),
                                  ),
                                  Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                                ],
                              ),
                            ),
                          );
                        });
                      },
                      child: Container(
                          height: 80,
                          width: 80,
                          child: Image.asset("assets/images/bio.png",scale: 3,)),
                    )
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}
