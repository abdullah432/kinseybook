import 'package:auto_size_text/auto_size_text.dart';
import 'package:drop_cap_text/drop_cap_text.dart';
import 'package:flutter/material.dart';

class Bibliography4 extends StatefulWidget {
  @override
  _Bibliography4State createState() => _Bibliography4State();
}

class _Bibliography4State extends State<Bibliography4> {
  @override
  Widget build(BuildContext context) {
    dynamic leftRowText = [
      [
        "Sinnette, Elinor Des Verney.",
        " Arthur Alfonso Schomburg: Black Bibliophile & Collector.",
        " Detroit: Wayne State University Press, 1989. The life of Schomburg, a biracial Caribbean immigrant, who was at the center of the Harlem Renaissance.\n"
      ],
      [
        "Stampp, Kenneth M.",
        " The Peculiar Institution: Slavery in the Ante-Bellum South.",
        " New York: Vintage/Anchor, 1956.\n"
      ],
      [
        "Stuckey, Sterling.",
        " Slave Culture: Nationalist Theory and the Foundations of Black America.",
        " Oxford: Oxford University Press, 1987. African influences in slave life and American culture.\n"
      ],
      [
        "Taha, Halima.",
        " Collecting African American Art.",
        " New York: Crown, 1998. A guide to collecting, expanding, and investing in an African American art collection, including beautiful prints of artwork for fine-tuning one’s knowledge of African American art.\n"
      ],
      [
        "Taney, Roger.",
        " The Case of Dred Scott in the United States Supreme Court.",
        " New York: Greeley & McElrath, 1857. Contains the full opinions of Chief Justice Taney and Justice Curtis, abstracts of the opinions of other judges, and analyses of rulings.\n"
      ],
      [
        "Truth, Sojourner.",
        " Narrative of Sojourner Truth, A Northern Slave, Emancipated from Bodily Servitude by the State of New York in 1829.",
        " Boston: Edward O. Jenkins, 1850. The biography of an African American abolitionist and women’s rights activist.\n"
      ],
      [
        "Washington, Booker Taliaferro.",
        " Up From Slavery: An Autobiography.",
        " New York: Doubleday, 1901.\n"
      ],
      [
        "Wheatley, Phillis.",
        " Poems on Various Subjects, Religious and Moral.",
        " London: Act of Parliament, 1773. A compilation of poems by the first African American woman to publish a book.\n"
      ],
      [
        "Willis, Deborah.",
        " Reflections in Black: A History of Black Photographers, 1840 to the Present.",
        " New York: W. W. Norton, 2000."
      ]
    ];
    dynamic rightRowText = [
      [
        "Woodson, Carter.",
        " Various articles for The Journal of Negro History.",
        " Washington, D.C.: The Association for the Study of Negro Life and History, 1916–1940. Woodson founded this journal, still published as The Journal of African American History."
      ],
      [
        "—.",
        "  The Negro in Our History.",
        " Washington, D.C.: Associate Publishers, Inc., 1922.                        \n" // TODO: the spaces are needed to positon this line at the beginning, because we've used TextAlign Justified
      ],
      [
        "Woolman, John.",
        " Some Considerations on the Keeping of Negroes.",
        " Church-Alley: James Chattin, 1754. The journey of  a humanitarian, a tailor by trade, who led one of the first major crusades against slavery.\n"
      ],
      [
        "Wright, Richard.",
        " Native Son.",
        " New York; London: Harper & Brothers, 1940. A novel about Bigger Thomas, a victim of racism and oppression.\n"
      ],
      [
        "X, Malcolm and Alex Haley.",
        " The Autobiography of Malcolm X.",
        " New York: Grove Press, 1975. The fascinating saga of one of the most powerful leaders of the turbulent period of the 1950s and 1960s."
      ],
      
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      ["","",""], 
      //TODO: remove some of these lines to  positon the right side, as we've used Expanded and AutoSizeText, the AutoSizeText goes to the center
      
    ];
    double widgetFontSize = 24;
    Color fontColor = Colors.black;
    String widgetFont = "Auto2-Regular";
    List<double> presetFonts = [24, 20, 17, 13 ,10, 9, 5];
    // presetFonts = [10];
    // presetFonts = [18];
    _populateLeftSide() {
      List<TextSpan> children = new List<TextSpan>();
      for (int i = 0; i < leftRowText.length; i++) {
        children.add(
          TextSpan(
              text: leftRowText[i][0],
              style: TextStyle(
                  fontFamily: widgetFont,
                  color: fontColor,
                  fontSize: widgetFontSize),
              children: [
                TextSpan(
                    text: leftRowText[i][1],
                    style: TextStyle(fontStyle: FontStyle.italic)),
                TextSpan(
                  text: leftRowText[i][2] + "\n",
                )
              ]),
        );
      }
      return AutoSizeText.rich(
        TextSpan(children: children),
        textAlign: TextAlign.justify,
        presetFontSizes: presetFonts,
        style: TextStyle(
          fontFamily: "Auto2-Regular",
        ),
      );
    }

    _populateRightSide() {
      List<TextSpan> children = new List<TextSpan>();
      for (int i = 0; i < rightRowText.length; i++) {
        children.add(
          TextSpan(
              text: rightRowText[i][0],
              style: TextStyle(
                  fontFamily: widgetFont,
                  color: fontColor,
                  fontSize: widgetFontSize),
              children: [
                TextSpan(
                    text: rightRowText[i][1],
                    style: TextStyle(fontStyle: FontStyle.italic)),
                TextSpan(
                  text: rightRowText[i][2] + "\n",
                )
              ]),
        );
      }
      return AutoSizeText.rich(
        TextSpan(children: children),
        textAlign: TextAlign.justify,
        presetFontSizes: presetFonts,
        style: TextStyle(
          fontFamily: "Auto2-Regular",
        ),
      );
    }

    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: _populateLeftSide(), // Left Side COLUMN
                      ),
                      Padding(
                        padding: EdgeInsets.all(18),
                      ),
                      Expanded(
                        flex: 1,
                        child: _populateRightSide(),
                      )
                    ],
                  ),
                ),
              ],
            )));
  }
}
