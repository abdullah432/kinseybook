import 'package:auto_size_text/auto_size_text.dart';
import 'package:drop_cap_text/drop_cap_text.dart';
import 'package:flutter/material.dart';

class Bibliography3 extends StatefulWidget {
  @override
  _Bibliography3State createState() => _Bibliography3State();
}

class _Bibliography3State extends State<Bibliography3> {
  @override
  Widget build(BuildContext context) {
    dynamic leftRowText = [
      [
        "Lewis, Samella.",
        " African American Art and Artists.",
        " Berkeley: University of California Press, 1994. An impressive representation of African American visual artists’ work from the eighteenth century to the present.\n"
      ],
      [
        "Lincoln, Abraham.",
        " Emancipation Proclamations, Sept. 24, 1862, and Jan. 1, 1863.",
        " Two Presidential executive orders during the Civil War.\n"
      ],
      [
        "Locke, Alain.",
        " The Negro in Art: A Pictorial Record of the Negro Artist and of the Negro Theme in Art.",
        " Wash., D.C.: Associates in Negro Folk Education, 1940."
      ],
      [
        "—.",
        "  The New Negro.",
        " New York: A. & C. Boni, 1925. One of the most sought after works of the Harlem Renaissance.\n"
      ],
      [
        "Lusane, Clarence.",
        " The Black History of the White House.",
        " San Francisco: Open Media Series/ City Light Books, 2011. Provides an overview of the relationship between the African American community and the U.S. government, focusing on the White House from the founding of the country through the early years of the Obama administration.\n"
      ],
      [
        "Painter, Nell Irvin.",
        " The History Of White People.",
        " New York: W. W. Norton & Company, 2010. Detailed history showing the construction of “whiteness” and its built-in exclusions and repressiveness, from classical to modern times.\n\n"
      ],
      [
        "Patton, Sharon.",
        " African-American Art.",
        " Oxford: Oxford University Press, 1998. The synthesis of diverse cultures making up society as a whole, as told through African American artistic expression.\n"
      ],
      [
        "Porter, James A.",
        " Modern Negro Art.",
        " New York: Dryden Press, 1943. On the development of African American visual artists, with multiple bibliographic references.\n"
      ],
      [
        "Powell, Richard.",
        " Black Art: A Cultural History.",
        " New York: Thames & Hudson, 2002.                                  \n"
      ],
      [
        "Powell, Richard and Jock Reynolds.",
        " To Conserve a Legacy: American Art from Historically Black Colleges and Universities.",
        " Andover: Addison Gallery."
      ],
    ];
    dynamic rightRowText = [
      [
        "of American Art, and New York: Studio Museum of Harlem, 1999 Works from collections of six of the oldest, most prestigious, historically black universities (Clark Atlanta, Fisk, Hampton, Howard, North Carolina Central, and Tuskegee).\n",
        "",
        "",
      ],
      [
        "Rabby, Glenda.",
        " The Pain and the Promise: The Struggle for Civil Rights in Tallahassee, Florida.",
        " Athens: University of Georgia Press, 1999. Florida’s underappreciated involvement in the Civil Rights Movement.\n"
      ],
      [
        "Rediker, Marcus.",
        " The Slave Ship: A Human History.",
        " New York: Viking Penguin, 2007. Illuminates the greed, cruelty, and violence inflicted on Africans on cargo ships over four centuries.\n"
      ],
      [
        "Reynolds, Gary, Beryl Wright, and David Driskell.",
        " Against the Odds: African-American Artists and the Harmon Foundation.",
        " Newark, N.J.: Newark Museum, 1990. The 1990 Harmon Foundation Collection Exhibition.\n"
      ],
      [
        "Rhoden, William.",
        " Forty Million Dollar Slaves: The Rise, Fall, and Redemption of the Black Athlete.",
        " New York: Three Rivers Press, 2006.\n"
      ],
      [
        "Rivers, Larry.",
        " Slavery in Florida: Territorial Days to Emancipation.",
        " Gainesville: University Press of Florida, 2000. An important illustrated social history of slavery, depicting what life was like for bondservant in Florida from 1821 to 1865, with insights from slaves as well as masters.\n"
      ],
      [
        "Said, Omar Ibn.",
        " A Muslim American Slave: The Life of Omar Ibn Said.",
        " Wisconsin: University of Wisconsin Press, 2011. The autobiography of this Muslim scholar, born in what is now Senegal, who was captured in 1807 during a military conflict and enslaved in America, where he died in 1864, the year before the Civil War ended.\n"
      ],
      [
        "Sancho, Ignatius.",
        " Letters of the Late Ignatius Sancho, An African, in Two Volumes.",
        " London: J. Nichols, 1782. Memoir of a slave who became a notable actor and author among the English elite."
      ],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      
    ];
    double widgetFontSize = 24;
    Color fontColor = Colors.black;
    String widgetFont = "Auto2-Regular";
    List<double> presetFonts = [24, 20, 17, 13 ,9, 5];
    // presetFonts = [18];
    _populateLeftSide() {
      List<TextSpan> children = new List<TextSpan>();
      for (int i = 0; i < leftRowText.length; i++) {
        children.add(
          TextSpan(
              text: leftRowText[i][0],
              style: TextStyle(
                  fontFamily: widgetFont,
                  color: fontColor,
                  fontSize: widgetFontSize),
              children: [
                TextSpan(
                    text: leftRowText[i][1],
                    style: TextStyle(fontStyle: FontStyle.italic)),
                TextSpan(
                  text: leftRowText[i][2] + "\n",
                )
              ]),
        );
      }
      return AutoSizeText.rich(
        TextSpan(children: children),
        textAlign: TextAlign.justify,
        presetFontSizes: presetFonts,
        style: TextStyle(
          fontFamily: "Auto2-Regular",
        ),
      );
    }

    _populateRightSide() {
      List<TextSpan> children = new List<TextSpan>();
      for (int i = 0; i < rightRowText.length; i++) {
        children.add(
          TextSpan(
              text: rightRowText[i][0],
              style: TextStyle(
                  fontFamily: widgetFont,
                  color: fontColor,
                  fontSize: widgetFontSize),
              children: [
                TextSpan(
                    text: rightRowText[i][1],
                    style: TextStyle(fontStyle: FontStyle.italic)),
                TextSpan(
                  text: rightRowText[i][2] + "\n",
                )
              ]),
        );
      }
      return AutoSizeText.rich(
        TextSpan(children: children),
        textAlign: TextAlign.justify,
        presetFontSizes: presetFonts,
        style: TextStyle(
          fontFamily: "Auto2-Regular",
        ),
      );
    }

    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: _populateLeftSide(), // Left Side COLUMN
                      ),
                      Padding(
                        padding: EdgeInsets.all(18),
                      ),
                      Expanded(
                        flex: 1,
                        child: _populateRightSide(),
                      )
                    ],
                  ),
                ),
              ],
            )));
  }
}
