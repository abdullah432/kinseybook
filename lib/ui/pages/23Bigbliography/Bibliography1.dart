import 'package:auto_size_text/auto_size_text.dart';
import 'package:drop_cap_text/drop_cap_text.dart';
import 'package:flutter/material.dart';

class Bibliography1 extends StatefulWidget {
  @override
  _Bibliography1State createState() => _Bibliography1State();
}

class _Bibliography1State extends State<Bibliography1> {
  @override
  Widget build(BuildContext context) {
    dynamic leftRowText = [
      [
        "Africanus. Leo.",
        " De totius Africae descriptione libri IX. Translated by Jean Florian, Anvers, Jean Latium, 1556, and 1588.",
        " Re-edited in Leiden: Elzevir, 1632. African geography, history, important trade routes, and inhabitants.\n"
      ],
      [
        "Aptheker, Herbert.",
        " American Negro Slave Revolts.",
        " New York: Columbia University Press, 1943."
      ],
      [
        "—.",
        " A Documentary History of the Negro People, Volumes 1- 6.",
        " New York: Citadel Press, 1990.\n"
      ],
      [
        "Baldwin, James.",
        " The Fire Next Time. New York: Dial Press, 1963.",
        " Two masterful from-the-heart essays predicting the bloodshed retaliating against participants of the Civil Rights Movement.\n"
      ],
      [
        "Banneker, Benjamin.",
        " Bannaker’s Pennsylvania, Delaware, Maryland, Virginia, Kentucky, and North Carolina, Almanack and Ephemeris.",
        " Series of 6. Baltimore: Various, 1792–1796.\n"
      ],
      [
        "Baptist, Edward E.",
        " The Half Has Never Been Told: Slavery and the Making of American Capitalism. New York: Basic Books, 2014.",
        " Investigates the deep interplay of slavery and the making of the U.S. economy.\n"
      ],
      [
        "Bearden, Romare, and Harry Henderson.",
        " A History of African-American Artists: From 1792 to Present.",
        " New York: Pantheon, 1993.\n"
      ],
      [
        "Bennett, Lerone, Jr.",
        " Before the Mayflower: A History of the Negro in America, 1619–1962.",
        " Chicago: Johnson Publishing, 1962."
      ],
      [
        "",
        "Black Entrepreneurs of the Eighteenth and Nineteenth Centuries.",
        " Boston: Federal Reserve Bank of Boston, and Museum of African American History, Boston and Nantucket, 2009.\n"
      ],
      [
        "Blackmon, Douglas.",
        " Slavery by Another Name: The Re-enslavement of Black Americans from the Civil War to World War II.",
        " New York: Doubleday, 2008."
      ],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
      ["","",""],
    ];
    dynamic rightRowText = [
      [
        "Blockson, Charles.",
        " A Commented Bibliography on 101 Influential Books By and About People of African Descent (1556–1982).",
        " Netherlands: A. Gerits & Sons, 1989.\n"
      ],
      [
        "Campbell, Mary Schmidt.",
        " Harlem Renaissance: Art of Black America.",
        " New York: Harry N. Abrams, 1994.Campbell, Mary Schmidt. Harlem Renaissance: Art of Black America. New York: Harry N. Abrams, 1994.\n"
      ],
      [
        "Capitein, Jacobus Elisa Johannes.",
        " Staakundig-Godgeleerd Onderzoekschrift Over de Slaverny. Amsteldam: Gerrit de Groot ,1742.",
        " A rare dissertation on slavery by an African orphan, educated in Holland.\n"
      ],
      [
        "",
        "Captive Passage: The Transatlantic Slave Trade and the Making of the Americas.",
        " Newport News: Smithsonian Press, Mariners Museum, 2002. Exhibition catalogue.\n"
      ],
      [
        "Clarkson, Thomas.",
        " The History of the Rise, Progress and Accomplishment of the Abolition of the African Slave-Trade, by the British Parliament.",
        " 2 vols. London: Longman, Hurst, Rees, and Orme, 1808. A recounting of Clarkson’s long, active protest against the slave trade.\n"
      ],
      [
        "Cleaver, Eldridge.",
        " Soul on Ice. New York: McGraw-Hill, 1967.",
        " A deeply moving, intellectually wide-ranging portrayal of black life.\n"
      ],
      [
        "Diop, Cheikh Anta.",
        " The African Origin of Civilization: Myth or Reality.",
        " New York: L. Hill, 1974. On the theory that Egypt is a civilization of Black origin.\n"
      ],
      [
        "Dodson, Howard.",
        " Jubilee: The Emergence of African-American Culture. Wash.,",
        " D.C.: National Geographic, 2003. An illustrated study identifying the cultural, political, and economic factors that helped slaves from various African regions integrate their individual religions, artistic expressions, and languages into a distinctive African American culture.\n"
      ],
      [
        "Douglass, Frederick.",
        " Narrative of the Life of Frederick Douglass, An American Slave.",
        " Boston: Anti-Slavery Office, 1845. A fugitive slave who became a great orator and a leading abolitionist."
      ],
    ];

    double widgetFontSize = 24;
    Color fontColor = Colors.black;
    String widgetFont = "Auto2-Regular";
    List<double> presetFonts = [24, 20, 16, 14 ,8, 5];
    _populateLeftSide() {
      List<TextSpan> children = new List<TextSpan>();
      for (int i = 0; i < leftRowText.length; i++) {
        children.add(
          TextSpan(
              text: leftRowText[i][0],
              style: TextStyle(
                  fontFamily: widgetFont,
                  color: fontColor,
                  fontSize: widgetFontSize),
              children: [
                TextSpan(
                    text: leftRowText[i][1],
                    style: TextStyle(fontStyle: FontStyle.italic)),
                TextSpan(
                  text: leftRowText[i][2] + "\n",
                )
              ]),
        );
      }
      return AutoSizeText.rich(
        TextSpan(children: children),
        textAlign: TextAlign.justify,
        presetFontSizes: presetFonts,
        style: TextStyle(
          fontFamily: "Auto2-Regular",
        ),
      );
    }

    _populateRightSide() {
      List<TextSpan> children = new List<TextSpan>();
      for (int i = 0; i < rightRowText.length; i++) {
        children.add(
          TextSpan(
              text: rightRowText[i][0],
              style: TextStyle(
                  fontFamily: widgetFont,
                  color: fontColor,
                  fontSize: widgetFontSize),
              children: [
                TextSpan(
                    text: rightRowText[i][1],
                    style: TextStyle(fontStyle: FontStyle.italic)),
                TextSpan(
                  text: rightRowText[i][2] + "\n",
                )
              ]),
        );
      }
      return AutoSizeText.rich(
        TextSpan(children: children),
        textAlign: TextAlign.justify,
        presetFontSizes: presetFonts,
        style: TextStyle(
          fontFamily: "Auto2-Regular",
        ),
      );
    }

    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: AutoSizeText(
                    "Recommended Reading\n",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontFamily: "Auto2-Regular",
                        color: fontColor,
                        fontWeight: FontWeight.bold),
                    minFontSize: 28,
                    maxFontSize: 32,
                  ),
                ),
                Expanded(
                  flex: 6,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: _populateLeftSide(), // Left Side COLUMN
                      ),
                      Padding(
                        padding: EdgeInsets.all(18),
                      ),
                      Expanded(
                        flex: 1,
                        child: _populateRightSide(),
                      )
                    ],
                  ),
                ),
              ],
            )));
  }
}
