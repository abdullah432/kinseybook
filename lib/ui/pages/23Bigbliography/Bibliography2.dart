import 'package:auto_size_text/auto_size_text.dart';
import 'package:drop_cap_text/drop_cap_text.dart';
import 'package:flutter/material.dart';

class Bibliography2 extends StatefulWidget {
  @override
  _Bibliography2State createState() => _Bibliography2State();
}

class _Bibliography2State extends State<Bibliography2> {
  @override
  Widget build(BuildContext context) {
    dynamic leftRowText = [
      [
        "—.",
        "  Oration, Delivered in Corinthian Hall.",
        " Rochester: Anti-Slavery Office, 1852. The best-known narrative of the antebellum period, written seven years after Douglass escaped from slavery.\n"
      ],
      [
        "Dover, Cedric.",
        " American Negro Art.",
        " Greenwich: NY Graphic Society, 1960.                                            \n"
      ],
      [
        "Dray, Phillip.",
        " At The Hands of Persons Unknown: The Lynching of Black America.",
        " New York: Random House, 2002. A comprehensive analysis of the lynching of thousands of African Americans.\n"
      ],
      [
        "Driskell, David.",
        " Two Centuries of Black American Art.",
        " New York: A. Knopf, 1976. Los Angeles County Museum of Art’s 1976 exhibition, one of the first to recognize the contributions of African American artists."
      ],
      [
        "—.",
        "  The Other Side of Color: African American Art in the Collection of Camille O. and William H. Cosby, Jr.",
        " Rohnert Park: Pomegranate, 2001. Discusses prominent postcolonial African American artists and their struggles for cultural emancipation and acceptance into American mainstream.\n"
      ],
      [
        "Du Bois, W.E.B.",
        " The Souls of Black Folk.",
        " Chicago: A.C. McClurg, 1903.\nA timeless literary work highlighting the struggle that has dominated black and white Americans on the quest for dignity, justice, and equality.\n"
      ],
      [
        "Dumond, Dwight.",
        " Anti-Slavery: The Crusade for Freedom in America.",
        " Ann Arbor: University of Michigan Press, 1961. On  printed anti-slavery documents—one of the most exhaustive and accurate narratives on America’s victory for democracy.\n"
      ],
      [
        "Ellison, Ralph.",
        " Invisible Man.",
        " New York: Random House, 1952. The odyssey of one man’s search for his own identity.\n"
      ],
      [
        "Equiano, Olaudah.",
        " The Interesting Narrative of the Life of Olaudah Equiano, or Gustavus Vassa, The African.",
        " London: T. Wilkins, 1789. A slave who earned the price of his freedom, became a world-traveling seaman, and wrote this best-seller of its time, furthering the anti-slavery cause."
      ],
    ];
    dynamic rightRowText = [
      [
        "Fisher, Angela.",
        " Africa Adorned.",
        " New York: Harry N. Abrams, 1984. A visual narrative of ceremonial African jewelry, body art, and clothing, through photographs of natives.\n"
      ],
      [
        "Franklin, John Hope.",
        " From Slavery to Freedom: A History of American Negroes.",
        " New York: A. Knopf, 1947. The impact of African Americans on the formation of this nation.\n"
      ],
      [
        "Haley, Alex.",
        " Roots.",
        " New York: Double Day, 1976. Haley successfully traced his ancestry from 1750 in Gambia. His painstaking research, including oral histories, opened a historical perspective for millions of African Americans.\n"
      ],
      [
        "Hansberry, Lorraine.",
        " A Raisin in the Sun.",
        " New York: Random House, 1959. Exploring the courage, dignity and strength of a black family in crisis.\n"
      ],
      [
        "Holland, Jesse.",
        " Black Men Built the Capitol: Discovering African American History in and around Washington, D.C.\n",
        " Gilford: Globe Pequot Press, 2007."
      ],
      [
        "International Review of African American Art.",
        " Hampton, VA: Hampton University Museum, 1976 to present.",
        " Quarterly magazine published by the oldest African American museum (1868) in the U.S.\n"
      ],
      [
        "Johnson, Charles, Patricia Smith and WGBH Research Team.",
        " Africans in America: America’s Journey through Slavery.",
        " New York: Harcourt Base, 1998. The journey of enslaved Africans across the Atlantic “Middle Passage” to the Caribbean and America, and the history of African slavery and the involvement of Arab and European nations.\n"
      ],
      [
        "Johnson, Walter.",
        " Soul by Soul: Life Inside the Antebellum Slave Market.",
        " Cambridge: Harvard University Press, 1999. The New Orleans slave market, largest in the nation, where 100,000 men, women, and children were packaged, priced, and sold.\n"
      ],
      [
        "Kurin, Richard.",
        " The Smithsonian History of America in 101 Objects.",
        " New York: Penguin Press, 2013. Stories based on the many treasures in the Smithsonian."
      ],
    ];

    double widgetFontSize = 24;
    Color fontColor = Colors.black;
    String widgetFont = "Auto2-Regular";
    List<double> presetFonts = [24, 20, 17, 13 ,9, 5];
    // presetFonts = [18];
    _populateLeftSide() {
      List<TextSpan> children = new List<TextSpan>();
      for (int i = 0; i < leftRowText.length; i++) {
        children.add(
          TextSpan(
              text: leftRowText[i][0],
              style: TextStyle(
                  fontFamily: widgetFont,
                  color: fontColor,
                  fontSize: widgetFontSize),
              children: [
                TextSpan(
                    text: leftRowText[i][1],
                    style: TextStyle(fontStyle: FontStyle.italic)),
                TextSpan(
                  text: leftRowText[i][2] + "\n",
                )
              ]),
        );
      }
      return AutoSizeText.rich(
        TextSpan(children: children),
        textAlign: TextAlign.justify,
        presetFontSizes: presetFonts,
        style: TextStyle(
          fontFamily: "Auto2-Regular",
        ),
      );
    }

    _populateRightSide() {
      List<TextSpan> children = new List<TextSpan>();
      for (int i = 0; i < rightRowText.length; i++) {
        children.add(
          TextSpan(
              text: rightRowText[i][0],
              style: TextStyle(
                  fontFamily: widgetFont,
                  color: fontColor,
                  fontSize: widgetFontSize),
              children: [
                TextSpan(
                    text: rightRowText[i][1],
                    style: TextStyle(fontStyle: FontStyle.italic)),
                TextSpan(
                  text: rightRowText[i][2] + "\n",
                )
              ]),
        );
      }
      return AutoSizeText.rich(
        TextSpan(children: children),
        textAlign: TextAlign.justify,
        presetFontSizes: presetFonts,
        style: TextStyle(
          fontFamily: "Auto2-Regular",
        ),
      );
    }

    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: _populateLeftSide(), // Left Side COLUMN
                      ),
                      Padding(
                        padding: EdgeInsets.all(18),
                      ),
                      Expanded(
                        flex: 1,
                        child: _populateRightSide(),
                      )
                    ],
                  ),
                ),
              ],
            )));
  }
}
