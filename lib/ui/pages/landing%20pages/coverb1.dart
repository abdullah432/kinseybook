import 'package:flutter/material.dart';
import 'package:zoom_widget/zoom_widget.dart';

class CoverBOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: FittedBox(
          fit: BoxFit.fill, child: Image.asset("assets/images/coverb1.png")),
    );
  }
}
