import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class CoverBTwo extends StatefulWidget {
  @override
  _CoverBTwoState createState() => _CoverBTwoState();
}

class _CoverBTwoState extends State<CoverBTwo> {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: template.viewHeight() * 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/coverb2_1.jpg",
                                  )));
                    },
                    child: Container(
                      height: template.viewHeight() * 190,
                      width: template.viewWidth() * 140,
                      child: FittedBox(
                          fit: BoxFit.contain,
                          child: Image.asset("assets/images/coverb2_1.jpg")),
                    ),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 10,
                  ),
                  Text(
                    "Bernard and Shirley Kinsey, 2002",
                    textAlign: TextAlign.center,
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text("Artis Lane",
                      textAlign: TextAlign.center,
                      style: template.bodyTextStyle()),
                  Text("Oil on canvas",
                      textAlign: TextAlign.center,
                      style: template.bodyTextStyle()),
                  Text("41 ¾ x 31 ¾ in.",
                      textAlign: TextAlign.center,
                      style: template.bodyTextStyle())
                ],
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: template.viewHeight() * 205,
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.play_circle_outline,
                        color: Colors.black,
                      ),
                      iconSize: template.viewHeight() * 22,
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VideoView(
                                      videoLink:
                                          "assets/videos/coverb2_video.MP4",
                                    )));
                      },
                    )
                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/coverb2_2.jpg",
                                  )));
                    },
                    child: Container(
                      height: template.viewHeight() * 190,
                      width: template.viewWidth() * 140,
                      child: FittedBox(
                          fit: BoxFit.contain,
                          child: Image.asset("assets/images/coverb2_2.jpg")),
                    ),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 10,
                  ),
                  Text(
                    "Khalil Kinsey, 1991",
                    textAlign: TextAlign.center,
                    style: template.textheaderStyle(),
                  ),
                  Text("Artis Lane",
                      textAlign: TextAlign.center,
                      style: template.bodyTextStyle()),
                  Text("Oil on canvas",
                      textAlign: TextAlign.center,
                      style: template.bodyTextStyle()),
                  Text("40 x 30 in.",
                      textAlign: TextAlign.center,
                      style: template.bodyTextStyle())
                ],
              ),
            ],
          ),
        ],
      ),
    ));
  }
}
