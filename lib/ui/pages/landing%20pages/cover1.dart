// TODO Implement this library.
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class CoverOne extends StatefulWidget {
  @override
  _CoverOneState createState() => _CoverOneState();
}

class _CoverOneState extends State<CoverOne> {
  VideoPlayerController _controller;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.asset("assets/videos/cover1.mp4")
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {
          _controller.play();
          _controller.setLooping(true);
        });
      });
  }

  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.black,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Image.asset("assets/images/cover1.png"),
        ),
        Positioned(
            left: 0,
            right: 0,
            bottom: 15,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.3,
              child: _controller.value.initialized
                  ? AspectRatio(
                      aspectRatio: _controller.value.aspectRatio,
                      child: VideoPlayer(_controller),
                    )
                  : Container(),
            ))
      ],
    );
  }
}
