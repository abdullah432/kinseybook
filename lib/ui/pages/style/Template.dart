import 'package:flutter/material.dart';

class Template {
  double ht, wt, htl, wtl;
  int page;
  Template(this.ht, this.wt, this.page);
  footerTextStyle() {
    htl = (ht / 300) * 4;
    return TextStyle(
        fontFamily: 'auto', fontSize: htl, fontWeight: FontWeight.bold);
  }

  videoIconSize() {
    return (ht / 300) * 20;
  }

  footerRightText() {
    return Text(
      "THE KINSEY COLLECTION | $page",
      style: footerTextStyle(),
      textAlign: TextAlign.end,
    );
  }

  footerLeftText() {
    return Text(
      "$page | THE KINSEY COLLECTION",
      style: footerTextStyle(),
      textAlign: TextAlign.start,
    );
  }

  footerTextContainerHihght() {
    return (ht / 300) * 10;
  }

  bodyTextStyle() {
    htl = (ht / 300) * 6;
    return TextStyle(
      fontFamily: 'Auto2-Light',
      fontWeight: FontWeight.w600,
      fontSize: htl,
    );
  }

  textContainerheight() {
    htl = ht;
    return htl;
  }

  bioImg() {
    htl = ht;
    return Container(
        height: viewHeight() * 20,
        width: viewWidth() * 20,
        child: Image.asset(
          "assets/images/bio.png",
        ));
  }

  textContainerwidth() {
    wtl = wt / 3.1;
    return wtl;
  }

  textcontainerPadding() {
    return EdgeInsets.only(
        top: (ht / 300) * 20, left: (wt / 390) * 2.5, right: (wt / 390) * 2.5);
  }

  footerContainerHeight() {
    return (ht / 300) * 15;
  }

  footerContainerWidth() {
    return wt / 3.1;
  }

  viewportBorder() {
    return BoxDecoration(border: Border.all(width: 1));
  }

  textheaderStyle() {
    return TextStyle(
      color: Colors.black,
      fontFamily: 'Auto2-Bold',
      fontSize: (ht / 300) * 7,
    );
  }

  viewHeight() {
    return (ht / 300);
  }

  viewWidth() {
    return (wt / 399);
  }

  sizeBoxWidth() {
    return (wt / 390) * 5;
  }

  magnifyingGlass() {
    return (ht / 300) * 18;
  }

  magnifyGlassPosition() {
    return -(wt / 390) * 10;
  }

  leftSizedBox() {
    return (ht / 300) * 30;
  }

  downSizedBox() {
    return (ht / 300) * 30;
  }

  boxColor() {
    return Color.fromRGBO(205, 133, 63, 1);
  }
}
