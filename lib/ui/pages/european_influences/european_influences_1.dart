import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class EuropeanInfluenceOne extends StatefulWidget {
  @override
  _EuropeanInfluenceOneState createState() => _EuropeanInfluenceOneState();
}

class _EuropeanInfluenceOneState extends State<EuropeanInfluenceOne> {
  final textFiles=["The understanding of art depends finally upon one’s willingness to extend one’s humanity and one’s knowledge of human life.\n— Ralph Ellison\n\nAs noted previously, Europe was the site of many of the historical and artistic intersections that inspire the Kinseys to collect. Surprising and important vignettes come into focus through items in their collection, including Pope Clement VII’s protection of Leo Africanus and Robert Scott Duncanson’s privileged upbringing with his father in Scotland. These individuals contributed to the cultural movements that would transform artistic and intellectual canons. During the world wars, however, cultural intersections like these took place more broadly, as a large number of African Americans shared their cultural traditions with their European hosts. An African American aesthetic infused with a globalizing perspective now added to the complexity of black American identity.\n\nHenry O. Tanner, whose work is represented in the Kinsey Collection, left the United States to begin a thriving career in Paris, where he lived until his death in 1937. He hosted and befriended scores of black artists and scholars,including Alain Locke, Augusta Savage, Hale Woodruff, and",
    "his best friend in Paris, Palmer Hayden. These artists grappled with the tension in America between black ideals and white realities, between living as artists and acting as insurgents against the aggressive and powerful enemy, racism. They met to create and to discuss their ideas, and to mourn the intractability of discrimination.\n\nBut in Europe, they also fell in love. In Paris, the city of lights, they fell in love with the skyline and landscape, but also with its openness to black thinkers, who needed space to confront profound problems. That love is reflected in the work of many black artists whose aesthetic sensibilities reflect their complex position as “others,” both in Europe and in America.\n\nAlmost every artist who came of age during the Harlem Renaissance traveled to Europe. Some received Harmon Foundation awards for study in Paris. Many artists, including Richmond Barthé, Lois Mailou Jones, and Beauford Delaney, were invigorated in Europe and then returned to the United States to continue their work, despite harsh environments. Others became true expatriates and lived for the rest of their lives in Europe. The work of these artists was enriched by both worlds.",
    ""
  ];
  static bool listView=false;
  ScrollController listViewController=ScrollController();
  static double fontSize=50;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: listView?Stack(
          children: <Widget>[
            ListView.builder(
                controller: listViewController,
                itemCount: textFiles.length,
                itemBuilder: (BuildContext context,index){
                  if(index==0){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: "European Influences\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                            TextSpan(text: textFiles[0])
                          ],

                        ),
                        presetFontSizes: [fontSize],
                        textAlign: TextAlign.justify,
                      ),
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: AutoSizeText(textFiles[index],minFontSize: fontSize,textAlign: TextAlign.justify),
                  );
                }),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height*0.1,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(onPressed: (){

                        if(listViewController.position.minScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset-listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },icon: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 40,),),
                      Row(children: <Widget>[
                        IconButton(icon: Icon(Icons.zoom_out,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            fontSize=30;
                          });
                        }),
                        IconButton(icon: Icon(Icons.close,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            listView=false;
                          });
                        })
                      ],),
                      IconButton(icon: Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 40,),onPressed: (){
                        if(listViewController.position.maxScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },)
                    ],),
                ))
          ],
        ):Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 30,),
                  Container(width:100,height: 50,child: Stack(children: <Widget>[Positioned(child: GestureDetector(
                      onTap: (){
                        setState(() {
                          fontSize=50;
                          listView=true;
                        });
                      },
                      child: Image.asset("assets/images/magnify.png",scale: 4,)),left: -30,),],overflow: Overflow.visible,)),
                  AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(text: "European Influences\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                      ],

                    ),
                    textAlign: TextAlign.justify,
                  ),
                  Expanded(child: AutoSizeText(textFiles[0],presetFontSizes: [22,20,18,16,14],textAlign: TextAlign.justify,))
                ],
              ),
            ),),
            Expanded(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[1],presetFontSizes: [22,20,18,16,14],textAlign: TextAlign.justify,),
            )),
            Expanded(child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: AutoSizeText(textFiles[2],presetFontSizes: [22,20,18,16,14],textAlign: TextAlign.justify,)),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
