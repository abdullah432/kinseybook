import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class EuropeanInfluenceFive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
          Expanded(
            flex: 2,
            child: GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/europe_influence_5.jpg",)));
              },
              child: Image.asset("assets/images/europe_influence_5.jpg"),
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 30,),
              AutoSizeText("Child’s Play, 1950",style: TextStyle(fontWeight: FontWeight.bold),maxLines: 1,
               textAlign: TextAlign.center,presetFontSizes: [18,16],),
              AutoSizeText("Aaron Douglas",textAlign: TextAlign.center,),
              AutoSizeText("Watercolor",textAlign: TextAlign.center,),
              AutoSizeText("11 3⁄4 x 8 1⁄2 in.",textAlign: TextAlign.center,),
            ],),
          SizedBox(height: 20,),
          GestureDetector(
            onTap: (){
              showDialog(context: context,builder: (BuildContext context){
                return Dialog(
                  child: Container(
                    height: MediaQuery.of(context).size.height/1.5,
                    width: MediaQuery.of(context).size.width/1.5,
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 30,),
                              AutoSizeText("Aaron Douglas (1899–1979)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                              SizedBox(height: 20,),
                              Expanded(child: AutoSizeText("Douglas, born in Topeka, Kansas, earned a bachelor’s degree from the University of Nebraska. He came to New York in the 1920s and studied under German artist Winold Reiss. Douglas infused his drawings and paintings with elements of traditional African styles and design. His illustrations appeared in national magazines, the written work of Alain Locke and James Weldon Johnson, as well as in DuBois’ periodical The Crisis. In 1928, he became the Harlem Artist Guild’s first president, and he was instrumental in securing WPA projects for black artists. He founded the art department at Fisk University in 1939, where he worked until his retirement in the late 1960s.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                            ],
                          ),
                        ),
                        Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                      ],
                    ),
                  ),
                );
              });
            },
            child: Container(
                height: 50,
                width: 50,
                child: Image.asset("assets/images/bio.png",scale: 3,)),
          ),
          SizedBox(height: 30,)
        ],),
      ),
    );
  }
}
