import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class EuropeanInfluenceFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
        Expanded(
          flex: 2,
          child: GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/europe_influence_4.jpg",)));
              },
              child: Image.asset("assets/images/europe_influence_4.jpg",)),
        ),
        Flexible(
            flex: 1,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 30,),
                AutoSizeText("Martigues Coastal Scene, 1932",style: TextStyle(fontWeight: FontWeight.bold),maxLines: 1,
                  presetFontSizes:[24,22,20],textAlign: TextAlign.center,),
                AutoSizeText("William H. Johnson",presetFontSizes: [22,20],textAlign: TextAlign.center,),
                AutoSizeText("Watercolor",presetFontSizes: [22,20],textAlign: TextAlign.center,),
                AutoSizeText("20 1⁄2 x 26 3⁄4 x 2 in.",presetFontSizes: [22,20],textAlign: TextAlign.center,),
              ],)),
        SizedBox(height: 20,),
        GestureDetector(
          onTap: (){
            showDialog(context: context,builder: (BuildContext context){
              return Dialog(
                child: Container(
                  height: MediaQuery.of(context).size.height/1.5,
                  width: MediaQuery.of(context).size.width/1.5,
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 20),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 30,),
                            AutoSizeText("William H. Johnson (1901–1970)",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold,),presetFontSizes: [28,24,22,18,14]),
                            SizedBox(height: 20,),
                            Expanded(child: AutoSizeText("Born in Florence, South Carolina, Johnson moved to New York City when he was 17 to pursue a career as an artist. At 20, he enrolled at the National Academy of Design, where he was mentored by Charles Hawthorne. In 1926, Johnson moved to Paris, where he met artists Henry Tanner and Palmer Hayden and Danish artist Holche Krake, whom he would later marry. Johnson returned to New York in 1929 and won the Harmon Foundation Gold Award. He turned his attention toward a more primitive style of painting that employed bold colors and block figures. A large number of his paintings were gathered by the Harmon Foundation and given to the National Museum of American Art in Washington, D.C.",presetFontSizes: [28,26,24,22,20,18,16,14],textAlign: TextAlign.justify,))
                          ],
                        ),
                      ),
                      Positioned(child: IconButton(icon: Icon(Icons.clear,color: Colors.black,size: 30,),onPressed: ()=>Navigator.pop(context),),top: 10,right: 10,)
                    ],
                  ),
                ),
              );
            });
          },
          child: Container(
              height: 80,
              width: 80,
              child: Image.asset("assets/images/bio.png",scale: 3,)),
        ),
        SizedBox(height: 30,)
      ],),
    );
  }
}
