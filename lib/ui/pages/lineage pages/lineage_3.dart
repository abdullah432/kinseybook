import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 16;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 5,
          ),
          Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Padding(
                padding: template.textcontainerPadding(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: template.viewHeight() * 10,
                    ),
                    Text(
                      "Before Jamestown",
                      style: template.textheaderStyle(),
                    ),
                    SizedBox(
                      height: template.viewHeight() * 10,
                    ),
                    Text(
                      "Translation:",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "Today, Septuagesima Sunday (the third Sunday before Lent), 1598, Father Richard Arthur married Agustín and Francisca, negra of Doña Petronilla. Their sponsors were Simon, negro, and Maria, negra. This being true, I sign my name.",
                      textAlign: TextAlign.center,
                      style: template.bodyTextStyle(),
                    ),
                    SizedBox(
                      height: template.viewHeight() * 10,
                    ),
                    Expanded(
                      child: Text(
                        "Documents of marriage and baptism from the sixteenth and early seventeenth century show that the Spanish viewed and treated enslaved people quite differently from the way others did. These documents sanctify a marriage and record two baptisms of African people in Spanish St. Augustine (now Florida). As part of the Diocese of St. Augustine’s archives, the documents are not only historically revealing but also have quite a remarkable history themselves. The document dating from 1594 (none survived the previous twenty-nine years of parish life) is the oldest known document of people of African descent in America.\n\nIn 1763, in accordance with the Treaty of Paris ending the Seven Years’ War, Spanish Florida was ceded to Great Britain. Spanish settlers largely left Florida for Spanish colonies in the Caribbean, most notably Cuba. The archives were taken to Cuba and stored in the crypt of the cathedral of Havana. They were forgotten for 107 years until Augustin Verot, the first bishop",
                        textAlign: TextAlign.justify,
                        style: template.bodyTextStyle(),
                      ),
                    ),
                    Container(
                        height: template.footerTextContainerHihght(),
                        width: template.textContainerwidth() * 3,
                        child: template.footerLeftText()),
                  ],
                ),
              )),
          SizedBox(
            width: template.viewWidth() * 10,
          ),
          Container(
            height: template.viewHeight() * 270,
            width: template.viewWidth() * 230,
            child: Padding(
              padding: EdgeInsets.only(),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/lineage3.jpg",
                                      )));
                        },
                        child: Image.asset(
                          "assets/images/lineage3.jpg",
                          fit: BoxFit.contain,
                        )),
                  ),
                  Text(
                    "Marriage Document, Archives of the",
                    style: template.textheaderStyle(),
                  ),
                  Text(
                    "Diocese of St. Augustine,1598",
                    style: template.textheaderStyle(),
                  ),
                  Text(
                    "Handwritten on paper",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "7 ¾	x 11 ½	in.",
                    style: template.bodyTextStyle(),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
