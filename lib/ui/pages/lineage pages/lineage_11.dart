import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageEleven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 24;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: (ht / 299) * 5,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      height: template.viewHeight() * 200,
                      width: template.textContainerwidth(),
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: (ht / 300) * 5,
                            left: (wt / 390) * 2.5,
                            right: (wt / 390) * 2.5),
                        child: Text(
                          "Born in Gambia around 1753, Phillis Wheatley was sold into slavery and transported to the New World on the slave ship Phillis, after which she was named. In Boston, she was purchased by John Wheatley, a merchant. Wheatley and his wife Susanna educated Phillis and she excelled under their tutelage. She mastered Latin and Greek as well as history and geog-raphy, and began writing poetry when she was 13 years old. In 1768, she wrote “To the King’s Most Excellent Majesty,” dedicated to King George III on the occasion of the repeal of the Stamp Act. Her poems were well received in Boston and London, and she was encouraged by the Countess of Huntingdon to publish a compilation of her work. Many doubted Wheatley’s abilities, however, and she had to endure inspection by Boston notables, including John Hancock. In 1773, she finally received her due as a poet, earning recognition from George Washington and Benjamin Franklin.\n\nWheatley, the “first published African American woman poet, is regarded as a founding figure of black literature. Her portrait printed in the book is the only surviving work by the African American slave artist Scipio Moorhead.",
                          textAlign: TextAlign.justify,
                          style: template.bodyTextStyle(),
                        ),
                      )),
                  Container(
                    height: template.viewHeight() * 30,
                    width: template.viewWidth() * 20,
                    padding: EdgeInsets.only(left: template.viewWidth()),
                    child: IconButton(
                      icon: Icon(
                        Icons.play_circle_outline,
                        color: Colors.black,
                      ),
                      iconSize: template.viewHeight() * 20,
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VideoView(
                                      videoLink: "assets/videos/lineage11.MP4",
                                    )));
                      },
                    ),
                  ),
                  SizedBox(
                    height: (ht / 299) * 10,
                  ),
                  Container(
                      height: (ht / 300) * 15,
                      width: template.textContainerwidth(),
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: (wt / 390) * 2.5, right: (wt / 390) * 2.5),
                        child: FittedBox(
                          child: AudioViewer(url: "assets/audio/lineage11.m4a"),
                          fit: BoxFit.contain,
                        ),
                      )),
                  SizedBox(
                    height: (ht / 299) * 10,
                  ),
                  Container(
                    height: template.footerTextContainerHihght(),
                    width: template.textContainerwidth() * 3,
                    child: Padding(
                        padding: EdgeInsets.only(
                            left: (wt / 390) * 2.5, right: (wt / 390) * 2.5),
                        child: template.footerLeftText()),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/lineage11.jpg",
                                      )));
                        },
                        child: Image.asset("assets/images/lineage11.jpg")),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        Text(
                          "Poems on Various Subjects",
                          style: template.textheaderStyle(),
                          maxLines: 1,
                        ),
                        Text(
                          "Religious and Moral, 1773",
                          style: template.textheaderStyle(),
                          maxLines: 1,
                        ),
                        Text(
                          "Phillis Wheatley",
                          style: template.bodyTextStyle(),
                          maxLines: 1,
                        ),
                        Text(
                          "Book",
                          style: template.bodyTextStyle(),
                          maxLines: 1,
                        ),
                        Text(
                          "7¾ x 5¼ x 1½ in.",
                          style: template.bodyTextStyle(),
                          maxLines: 1,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
