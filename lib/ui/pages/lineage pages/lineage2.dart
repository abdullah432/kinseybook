import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageTwo extends StatefulWidget {
  @override
  _LineageTwo createState() => _LineageTwo();
}

class _LineageTwo extends State<LineageTwo> {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 15;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/lineage2.jpg",
                                  )));
                    },
                    child: Container(
                      height: template.viewHeight() * 295,
                      width: template.viewWidth() * 110,
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: template.viewHeight() * 5,
                            left: template.viewWidth() * 20),
                        child: Image.asset("assets/images/lineage2.jpg"),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: (ht / 300) * 130,
                      ),
                      Text(
                        "Door of No Return,2007",
                        style: template.textheaderStyle(),
                      ),
                      Text(
                        "Courtesy of documentary",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "photographer Kwesi Hutchful",
                        style: template.bodyTextStyle(),
                      ),
                      SizedBox(
                        height: (ht / 300) * 135,
                      ),
                      Container(
                          height: template.footerTextContainerHihght(),
                          width: template.textContainerwidth() * 2,
                          child: Padding(
                              padding: EdgeInsets.only(
                                  right: template.viewWidth() * 8),
                              child: template.footerRightText()))
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
