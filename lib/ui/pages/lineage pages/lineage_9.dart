import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageNine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 22;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(width: template.viewWidth() * 5),
            Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Padding(
                padding: template.textcontainerPadding(),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: template.viewHeight() * 30),
                    Text(
                      "Treaty of Utrecht, 1714",
                      style: template.textheaderStyle(),
                      maxLines: 1,
                    ),
                    Text(
                      "John Baskett, printer",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "Excerpt from book",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "8¾ x 7 in.",
                      style: template.bodyTextStyle(),
                    ),
                    Container(
                      height: template.viewHeight() * 200,
                      width: template.textContainerwidth(),
                      child: Padding(
                        padding: template.textcontainerPadding(),
                        child: Text(
                          "The Treaties of Utrecht ended the War of Spanish Succession (1701–13) and divided the spoils among Europe’s monarchies, following the détente between the English and the French. This treaty grants the United Kingdom a 30-year asiento to supply slaves to the Spanish colonies. France would ultimately gain the coveted contract; however, her government, born of a bloody revolution, would in 1794 become the first to declare an official end to slavery and the slave trade.",
                          textAlign: TextAlign.justify,
                          style: template.bodyTextStyle(),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: template.viewHeight() * 5,
                    ),
                    Container(
                        height: template.footerTextContainerHihght(),
                        width: template.textContainerwidth() * 3,
                        child: template.footerLeftText()),
                  ],
                ),
              ),
            ),
            SizedBox(width: template.viewWidth() * 20),
            Container(
              height: template.viewHeight() * 290,
              width: template.viewWidth() * 200,
              child: Padding(
                padding: EdgeInsets.only(top: template.viewHeight() * 10),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/lineage_9.jpg",
                                )));
                  },
                  child: Container(
                      height: template.viewHeight() * 240,
                      width: template.viewWidth() * 200,
                      child: FittedBox(
                          fit: BoxFit.fill,
                          child: Image.asset("assets/images/lineage_9.jpg"))),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
