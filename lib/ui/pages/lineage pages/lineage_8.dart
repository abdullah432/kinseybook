import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageEight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 21;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: (ht / 300) * 5,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: EdgeInsets.only(
                  top: (ht / 300),
                  left: (wt / 390) * 2.5,
                  right: (wt / 390) * 2.5),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Ioannis Leonis Africani Africae, 1632",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "De totius Africae descriptione libri IX",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Book",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "4⅓ x 3 x 2 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    child: Text(
                      "Al Hassan ibn Mohammed al Wazzan al Fassi was born in 1485 in Granada, Spain. He returned with his parents to their native Morocco and studied at the University of Al-Karaouine, founded in Fez in 859 A.D. In 1518, returning to Morocco from a diplomatic venture, he was captured by Spanish Corsairs and taken to Rome, where he was received and baptized by Pope Leo X and renamed Leo Africanus. He was freed when his captors realized he was from a prominent diplomatic family and could be a useful ally. Africanus traveled extensively throughout Italy for several years, then returned to Rome under the protection of Pope Clement VII. In 1526, he completed Ioannis Leonis Africani Africae, detailing African geography, with much of the information gathered from his studies at university and his interaction with African travelers in Egypt.The book is considered the first descriptive work on Africa authored by a person of African descent. Popular long after its original publication, it was translated in the early seventeenth century from Arabic into several languages, including Latin and French. This edition, of 1632, is in Latin.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ),
                  SizedBox(height: template.viewHeight() * 10),
                  Container(
                    height: (ht / 300) * 25,
                    width: template.textContainerwidth(),
                    child: FittedBox(
                      child: AudioViewer(url: "assets/audio/lineage8.mp3"),
                      fit: BoxFit.contain,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(width: template.viewWidth() * 10),
          Padding(
            padding: EdgeInsets.only(
                top: (ht / 300) * 20,
                left: (wt / 390) * 2.5,
                right: (wt / 390) * 2.5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/lineage8_1.jpg",
                                )));
                  },
                  child: Container(
                    height: template.viewHeight() * 80,
                    width: template.viewWidth() * 60,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset(
                        "assets/images/lineage8_1.jpg",
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
                Text(
                  "Not pictured, but in the\nKinsey Collection, is an\nEnglish language version\npublished in 1600",
                  style: template.bodyTextStyle(),
                )
              ],
            ),
          ),
          Column(children: <Widget>[
            SizedBox(
              height: template.viewHeight() * 8,
            ),
            Container(
              height: template.viewHeight() * 272,
              width: template.viewWidth() * 186,
              child: Padding(
                padding: EdgeInsets.only(top: template.viewHeight() * 10),
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/lineage8_2.jpg",
                                  )));
                    },
                    child: Image.asset(
                      "assets/images/lineage8_2.jpg",
                      fit: BoxFit.fill,
                    )),
              ),
            ),
            SizedBox(
              height: template.viewHeight() * 7,
            ),
            Container(
                height: template.footerTextContainerHihght(),
                width: template.viewWidth() * 168,
                child: template.footerRightText())
          ]),
        ],
      ),
    ));
  }
}
