import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageSeven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 20;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          children: <Widget>[
            SizedBox(width: template.viewWidth() * 6),
            Container(
                height: template.textContainerheight(),
                width: template.textContainerwidth(),
                child: Padding(
                  padding: template.textcontainerPadding(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                          child: Text(
                        "Official “Cedula,” or decree, ordering all Spaniards, Negroes, Mulattoes, Quadroons, and Octoroons who are not employed to report for work. Signed by Don Luis de Velasco, by order of the Viceroy, Don Alonso Fernandez de Cordova.\n\nAn example of the power of the Inquisition, this order is one of twelve ordinances issued on the same day by Luis de Velasco, and the only one to mention people of color. The ordinance stated that those who were enjoying the fruits of the labor of others, while doing nothing themselves to contribute to the general good in Lima, were ordered to work, and given only a few days to comply. The order was to be enforced “with all the rigor of physical as well as monetary punishments.”\n\nThis document establishes the fact that Africans have been a part of the development of the Americas from the earliest conquest by the Spanish, Portuguese, French, English, and Dutch. Blacks were with Pizarro in Peru, Cortes in Mexico, Menendez in Florida, and they accompanied DeSoto. The document also reveals that Africans were classified during the colonial occupation by the amount of white blood they carried in their veins:\n\nNegro: both parents of African descent\nMulatto: a white parent and a black parent \nQuadroon: a mulatto parent and a white parent \nOctoroon: a quadroon parent and a white parent",
                        textAlign: TextAlign.justify,
                        style: template.bodyTextStyle(),
                      )),
                      SizedBox(
                        height: (ht / 299),
                      ),
                      Container(
                          height: template.footerTextContainerHihght(),
                          width: template.textContainerwidth() * 3,
                          child: template.footerLeftText()),
                    ],
                  ),
                )),
            Container(
              height: template.viewHeight() * 280,
              width: template.viewWidth() * 230,
              child: Padding(
                padding: EdgeInsets.only(top: template.viewHeight() * 5),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url: "assets/images/lineage7.jpg",
                                        )));
                          },
                          child: Image.asset("assets/images/lineage7.jpg")),
                    ),
                    Text(
                      "Para Que Se Apremien, Los Espanoles,",
                      style: template.textheaderStyle(),
                    ),
                    Text(
                      "Baldios, Mestizos, Mulatos, Negros,",
                      style: template.textheaderStyle(),
                    ),
                    Text(
                      "y cambahijos se alquilen y siruan., 1604",
                      style: template.textheaderStyle(),
                    ),
                    Text(
                      "Printed paper",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "8½ x 12¼ in.",
                      style: template.bodyTextStyle(),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    ));
  }
}
