// TODO Implement this library.
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageTen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 23;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        children: <Widget>[
          SizedBox(width: template.viewWidth() * 14),
          Container(
            height: template.viewHeight() * 250,
            width: template.viewWidth() * 250,
            child: Padding(
              padding: EdgeInsets.only(left: 2.5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/lineage10_1.jpg",
                                      )));
                        },
                        child: Image.asset("assets/images/lineage10_1.jpg",
                            fit: BoxFit.fill)),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 10,
                  ),
                  Text(
                    "Letters of the Late Ignatius Sancho,",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "an African, in Two Volumes, 1782",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Ignatius Sancho",
                    style: template.bodyTextStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Book",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "8 x 5½ x 2 in.",
                    style: template.bodyTextStyle(),
                    maxLines: 1,
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 15,
                  ),
                  Container(
                    height: template.viewHeight() * 160,
                    width: template.textContainerwidth(),
                    child: Padding(
                      padding: template.textcontainerPadding(),
                      child: Text(
                        "Ignatius Sancho (1729–1780) was born a slave on a ship crossing the Atlantic from Africa to the West Indies. He was taken to Greenwich, England, where he became a butler to the Montagu family, eventually retiring to run a grocery in Westminster. He acted in several plays on the London stage, and is thought to have performed in Othello, making him the first black actor to play the Moorish king.\n\nSancho also composed music and wrote poems and essays, posthumously published in this volume in 1782. The engraved frontispiece is after the portrait of Sancho painted by Thomas Gainsborough. Sancho, known as “The Extra- ordinary Negro,” became a symbol of the humanity of Africans to 18th-century British abolitionists. He was also the first African to vote in a British election.",
                        textAlign: TextAlign.justify,
                        style: template.bodyTextStyle(),
                      ),
                    ),
                  ),
                  Container(
                      height: template.viewHeight() * 80,
                      width: template.textContainerwidth(),
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url: "assets/images/lineage10_2.jpg",
                                        )));
                          },
                          child: Image.asset("assets/images/lineage10_2.jpg"))),
                  SizedBox(
                    height: template.viewHeight() * 10,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
