import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageThirteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 26;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 7,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: EdgeInsets.only(
                  top: (ht / 300) * 14,
                  left: (wt / 390) * 2.5,
                  right: (wt / 390) * 2.5),
              child: Column(
                children: <Widget>[
                  Text(
                    "Mexican Slave Trade Bill of Sale, 1790",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Jose Maria Montano",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Ink on paper ( 4 pages)",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "12 x 8½	 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Container(
                    width: template.textContainerwidth(),
                    height: template.viewHeight() * 210,
                    child: Text(
                      "A scarce, early contract of sale in New Spain’s slave trade, in which a “mulatto” slave named Jose Maria Montano is sold for sixty pesos.\n\nSince 1492 the history of the Americas has been forged by three cultures: Indigenous, European, and African. It was Africans who established the third root of the Americas, according to Gonzalo Aquirre Beltrán of the University of Veracruz, author of The Black Population in Mexico (1964). The first pioneer from Europe to explore what would become the entire southern tier of the United States was an African named Esteban Dorantes (1500–1539). Blacks in the Americas were much more than slaves—they were explorers and co-founders of settlements as far north as Los Angeles, where twenty-six of the forty-four founders were of African descent. The second president of Mexico, Vicente Guerrero, was of African ancestry. He officially abolished slavery in 1822.\n\nAlong the Atlantic Coast of the Americas today, 25 percent of the Spanish and Portuguese speaking Latino population is of African descent—a reminder for both communities of their similarities, and for all of us, that racial categorizing does not represent the true facts of human history.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 33,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth() * 3,
                      child: template.footerLeftText()),
                ],
              ),
            ),
          ),
          SizedBox(
            width: template.viewWidth() * 20,
          ),
          Container(
            height: template.viewHeight() * 280,
            width: template.viewWidth() * 210,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight()),
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/lineage13.jpg",
                                )));
                  },
                  child: Image.asset("assets/images/lineage13.jpg",
                      fit: BoxFit.fill)),
            ),
          )
        ],
      ),
    ));
  }
}
