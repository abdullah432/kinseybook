import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageSeventeen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 30;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: template.viewWidth() * 6),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 47,
                  ),
                  Text(
                    "Schedule of over Five Hundred Slaves, 1820",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "William Law",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Ink on vellum",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "25½ x 28½ x 31½ in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    child: Text(
                      "\nWilliam Law was forced to sell his estate on the island of Grenada to settle his debts. This inventory of assets, presented and signed by the Lord Mayor of London, includes a listing of slaves by name, color, country, supposed age, and defining marks. Although the estate auction took place nearly 15 years after the abolition of the slave trade in Britain, the inventory documents younger slaves born in Africa and bought by Law. The document also shows that black slaves, even infants a few months old, were regarded as property to be bred and bartered.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 98,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth() * 3,
                      child: template.footerLeftText()),
                ],
              ),
            ),
          ),
          SizedBox(width: template.viewWidth() * 10),
          Container(
            height: template.viewHeight() * 280,
            width: template.viewWidth() * 230,
            child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/lineage17.jpg",
                              )));
                },
                child: Image.asset("assets/images/lineage17.jpg",
                    fit: BoxFit.contain)),
          ),
        ],
      ),
    ));
  }
}
