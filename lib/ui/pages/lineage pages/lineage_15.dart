import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageFifteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 28;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: template.viewHeight() * 200,
            width: template.viewWidth() * 350,
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Center(
                  child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GalleryView(
                                      url: "assets/images/lineage15_1.jpg",
                                    )));
                      },
                      child: Image.asset("assets/images/lineage15_1.jpg",
                          fit: BoxFit.fill))),
            ),
          ),
          Container(
            height: template.viewHeight() * 80,
            width: template.viewWidth() * 150,
            child: Padding(
              padding: template.textcontainerPadding(),
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/lineage15_2.jpg",
                                )));
                  },
                  child: Image.asset("assets/images/lineage15_2.jpg",
                      fit: BoxFit.fill)),
            ),
          ),
          SizedBox(
            height: (ht / 300) * 7,
          ),
          Container(
            height: template.footerTextContainerHihght(),
            width: template.textContainerwidth() * 3,
            child: Padding(
                padding: EdgeInsets.only(left: template.viewWidth() * 4),
                child: template.footerLeftText()),
          )
        ],
      ),
    ));
  }
}
