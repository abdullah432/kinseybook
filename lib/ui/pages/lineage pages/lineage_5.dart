import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class LineageFive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 18;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 6,
          ),
          Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Padding(
                padding: template.textcontainerPadding(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: template.viewHeight() * 10,
                    ),
                    Expanded(
                        child: Text(
                      "who traveled with Ponce De Leon as a Spanish conquistador, and was part of the venture in 1513 that discovered what is now Florida. He was probably the first African to set foot in what is now the United States, and could thus be called the first African American.\n\nSpanish religious and legal culture provided for a more humane view of slavery, if such a thing can be said. In the thirteenth century, the Castilian slave code acknowledged the “moral personality” of the slave and specified the rights and obligations of slaves. Enslaved people could own and transfer property. Castilian law considered slavery an unnatural condition and provided ways for enslaved people to become free, including purchasing their own or others’ freedom. This ultimately created a large free black community in Spain.\n\nWhen the Spanish began to conquer and colonize the Americas, they brought for the most part laws and beliefs regarding slavery with them. Many of the expeditions and settlements in the Spanish Caribbean, and later in Spanish Florida, included free blacks as well as slaves. In 1738 in Florida, Spanish rule even established the first free ex- slave community and sanctuary town, Gracia Real de Santa Teresa de Mose (now Fort Mose Historic State Park).\n\nThe history of Africans in Spanish Florida poses a tantalizing question: What if the antebellum South had followed Spanish traditions?",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    )),
                    SizedBox(
                      height: (ht / 299),
                    ),
                    Container(
                        height: template.footerTextContainerHihght(),
                        width: template.textContainerwidth() * 3,
                        child: template.footerLeftText()),
                  ],
                ),
              )),
          SizedBox(
            width: template.viewWidth() * 15,
          ),
          Container(
              height: template.viewHeight() * 280,
              width: template.viewWidth() * 230,
              child: Padding(
                padding: EdgeInsets.only(),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url: "assets/images/lineage5.jpg",
                                        )));
                          },
                          child: Image.asset("assets/images/lineage5.jpg",
                              fit: BoxFit.contain)),
                    ),
                    Text(
                      "Baptismal Document, Archives of the",
                      style: template.textheaderStyle(),
                    ),
                    Text(
                      "Diocese of St. Augustine,1606",
                      style: template.textheaderStyle(),
                    ),
                    Text(
                      "Handwritten on paper",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "7¾ x 10¾ in.",
                      style: template.bodyTextStyle(),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Translation:",
                      style: template.bodyTextStyle(),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "[margin:]",
                          style: template.bodyTextStyle(),
                        ),
                        Text(
                          "Agustín received holy oil and chrism",
                          style: template.bodyTextStyle(),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 2.5),
                      child: Text(
                        "Agustín was baptized today, Sunday, August 20, 1606, in this High Holy Church of Saint Augustine. He is the legitimate son of Agustín, slave, and Francisca, slave. His godfather is Diego López from Saint Augustine. Because this is true, I sign my name, Vicente Freire de Andrade.",
                        style: template.bodyTextStyle(),
                        textAlign: TextAlign.center,
                        maxLines: 4,
                      ),
                    )
                  ],
                ),
              )),
        ],
      ),
    ));
  }
}
