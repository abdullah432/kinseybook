import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class Acknowledgements3 extends StatefulWidget {
  @override
  _Acknowledgements3State createState() => _Acknowledgements3State();
}

class _Acknowledgements3State extends State<Acknowledgements3> {
  final textFiles = [
    "\n\nMany thanks to our corporate sponsors who \nbelieved in our mission and made this possible:\n\n",
    "The Walt Disney Company­\nEdison International– Southern California Edison\nToyota  Motor Sales, USA, Inc.\nUnited Parcel Service of America, Inc. \nSouthwest Airlines\nWells Fargo\nXerox Corporation\n\n\nThe Bernard & Shirley Kinsey Foundation \nfor Arts and Education",
    "Board members:\nBernard W. Kinsey, Executive Director & CEO\nShirley Pooler Kinsey, President\nPhoebe Beasley\nGeorge Gibbs\nTerry D. Harris\nKhalil B. Kinsey\nPamela T. Walker\n\nGreat appreciation is extended to the following friends whose voices\n gave life to the exhibition’s cell phone tour.\nAngela Bassett\nLouis Gossett Jr.\nDennis Haysbert\nKhalil B. Kinsey\nNichelle Nichols\nBeverly Todd\nLorraine Toussaint\nCourtney B. Vance\nHattie Winston\n",
    "\n\nSpecial thanks for support and help from:\nCharles & Doreen Allen, book producers with extraordinary patience and persistence\nDouglas Blackmon, author and writer\nHaili Francis, research assistant and writer\nSusan Green, editorial consulting\nTerry Harris, art consultant\nKhalil Kinsey, research, editing and \n      contributing writer\nJill Moniz, PhD, additional research, curatorial    \n      services, and preliminary copy\nEdward Pratt, art and history consultant\n\n",
    "\nMikel Alatza\nCharles Allen\nManuel Flores\nKwesi Hutchful\nKirk McKoy\nJohn Sullivan\nFrank Turner\n"
  ];
  final fonts = [
    "Auto2-Regular",
    "Auto2-Regular",
    "Auto2-Regular",
    "Auto2-Regular",
    "Auto2-Regular",
  ];

  @override
  Widget build(BuildContext context) {
    final leftSide = [
      "California African American Museum (Los Angeles, CA)",
      "The Freedom Center Underground Railroad (Cincinnati, OH)",
      "The DuSable Museum of History (Chicago, IL)",
      "Norton Museum of Art (West Palm Beach, FL)",
      "The Mary Brogan Museum of Art and Science (Tallahassee, FL)",
      "The California Museum - African American Treasures (Sacramento, CA)",
      "Smithsonian Museum of American History (Washington, DC)",
      "San Francisco Historical Society (San Francisco, CA)",
      "Vacaville Museum (Vacaville, CA)",
      "Museum of the African Diaspora “MoAD” (San Francisco, CA)",
      "Barrett Museum Santa Monica College  (Santa Monica, CA)",
      "American Pavilion at Epcot at Walt Disney World Resort (Orlando, FL)",
      "Harvey Gantt Center (Charlotte, NC)",
      "Pepperdine University Libraries (Malibu, CA)",
      "Reginald F. Lewis Museum (Baltimore, MD)",
      "Atlanta History Center (Atlanta, GA)",
      "Houston Museum of African American Culture (Houston, TX)",
      "Mississippi State University (Starkville, MS)",
      "Florida A & M University “Toyota Presents the Kinsey Collection”",
      "Santa Cruz Museum Art & History (Santa Cruz, CA)",
      "Mosaic Templars Cultural Center (Little Rock, AR)",
      "Hong Kong University Museum (Hong Kong, China)",
      "Epcot, Walt Disney World (Orlando,FL)",
      "Harvard University (Cambridge, MA)",
      "National Underground Railroad Freedom Center (Cincinnati, OH)",
      "Toyota North American Headquarters (Plano,TX)",
      "ArtCentre of Plano(Plano,TX)."
    ];
    final rightSide = [
      "Sept. 24, 2006 – Mar. 2007",
      "Apr. 13 – June 3, 2007",
      "July 13, 2007 – Mar. 2008",
      "Apr. 19, 2008 – July 20, 2008",
      "Sept. 11, 2009",
      "Feb.6 – May 2010",
      "Oct. 15, 2009 – May 1, 2010",
      "Jan. 14, 2011 – Apr. 22, 2011",
      "May 13 – July 22, 2011",
      "Feb. 8 – May 19, 2013",
      "Feb.12 – Mar. 9, 2013",
      "Mar. 9, 2013 – Feb. 2016",
      "June 17, – Oct. 1, 2013",
      "Sept. 3, – Sept. 27, 2013",
      "Nov. 1, 2013 – Jan.20, 2014",
      "Apr. 5, 2014 – July 13, 2014",
      "Aug. 2, 2014 – Oct. 26, 2014",
      "Mar. 19 – June 19, 2015",
      "Jan. 25 – Mar. 25, 2016",
      "Feb. 26 – May 22, 2016",
      "Apr. 8 – July 2, 2016",
      "Dec. 8 – Feb. 26, 2016",
      "March 8, 2013-March 30, 2018",
      "April, 2017",
      "Nov. 2, 2017-March 30, 2018",
      "June 2018",
      "July 23-Sept.23, 2018"
    ];
    String widgetFont = "Auto2-Regular";
    List<double> presetFonts = [24, 15, 14, 8, 5];
    // presetFonts = [8];
    _buildRows() {
      // List<TextSpan> children = new List<TextSpan>();
      List<Widget> children = new List<Widget>();
      for (int i = 0; i < leftSide.length; i++) {
        children.add(Expanded(
          flex: 1,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: AutoSizeText(
                  leftSide[i],
                  textAlign: TextAlign.left,
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: widgetFont),
                ),
              ),
              Expanded(
                flex: 1,
                child: AutoSizeText(
                  rightSide[i],
                  textAlign: TextAlign.right,
                  style: TextStyle(fontFamily: widgetFont),
                  presetFontSizes: presetFonts,
                ),
              ),
            ],
          ),
        ));
      }
      return Column(
        children: children,
      );
    }

    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 30),
            child: Row(
              children: <Widget>[
                Expanded(flex: 1, child: Container()),
                Expanded(
                  flex: 6,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: AutoSizeText(
                          "Museum Exhibit Openings\n",
                          textAlign: TextAlign.start,
                          minFontSize: 24,
                          maxFontSize: 32,
                          style: TextStyle(
                              fontFamily: widgetFont,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                          // minFontSize: 24,
                          // maxFontSize: 28,
                        ),
                      ),
                      Expanded(flex: 6, child: _buildRows())
                    ],
                  ),
                ),
                Expanded(flex: 1, child: Container()),
              ],
            )));
  }
}
