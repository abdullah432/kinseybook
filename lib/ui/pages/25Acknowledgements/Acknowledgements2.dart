import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class Acknowledgements2 extends StatefulWidget {
  @override
  _Acknowledgements2State createState() => _Acknowledgements2State();
}

class _Acknowledgements2State extends State<Acknowledgements2> {
  final textFiles = [
    "\nMany thanks to our corporate sponsors who \nbelieved in our mission and made this possible:\n\n",
    "The Walt Disney Company­\nEdison International– Southern California Edison\nToyota  Motor Sales, USA, Inc.\nUnited Parcel Service of America, Inc. \nSouthwest Airlines\nWells Fargo\nXerox Corporation\n\nThe Bernard & Shirley Kinsey Foundation \nfor Arts and Education",
    "Board members:\nBernard W. Kinsey, Executive Director & CEO\nShirley Pooler Kinsey, President\nPhoebe Beasley\nGeorge Gibbs\nTerry D. Harris\nKhalil B. Kinsey\nPamela T. Walker\n\nGreat appreciation is extended to the following friends whose voices\n gave life to the exhibition’s cell phone tour.\nAngela Bassett\nLouis Gossett Jr.\nDennis Haysbert\nKhalil B. Kinsey\nNichelle Nichols\nBeverly Todd\nLorraine Toussaint\nCourtney B. Vance\nHattie Winston\n",
    "\n\nSpecial thanks for support and help from:\nCharles & Doreen Allen, book producers with extraordinary patience and persistence\nDouglas Blackmon, author and writer\nHaili Francis, research assistant and writer\nSusan Green, editorial consulting\nTerry Harris, art consultant\nKhalil Kinsey, research, editing and \n      contributing writer\nJill Moniz, PhD, additional research, curatorial    \n      services, and preliminary copy\nEdward Pratt, art and history consultant\n\n",
    "\nMikel Alatza\nCharles Allen\nManuel Flores\nKwesi Hutchful\nKirk McKoy\nJohn Sullivan\nFrank Turner\n"
  ];
  final fonts = [
    "Auto2-Regular",
    "Auto2-Regular",
    "Auto2-Regular",
    "Auto2-Regular",
    "Auto2-Regular",
  ];

  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [18,15, 14,8];
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: AutoSizeText(
                    "Acknowledgments",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontFamily: "Auto2-Regular",
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                    minFontSize: 20,
                    maxFontSize: 32,
                  ),
                ),
                Expanded(
                  flex: 6,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: AutoSizeText.rich(
                          TextSpan(text: textFiles[0], children: [
                            TextSpan(
                                text: textFiles[1],
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            TextSpan(text: textFiles[2])
                          ]),
                          presetFontSizes: presetFonts,
                        ),
                      ),
                      Expanded(
                         flex: 1,
                        child: AutoSizeText.rich(
                          TextSpan(
                              text: "Contributors",
                              style: TextStyle( 
                                fontFamily: "Auto2-Regular",
                                fontWeight: FontWeight.bold,
                              ),
                              children: [
                                TextSpan(
                                    text: textFiles[3],
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal)),
                                TextSpan(
                                    text: "Photographers\n",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                TextSpan(
                                    text: textFiles[4],
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal)),
                              ]),
                          presetFontSizes: presetFonts,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )));
  }
}
