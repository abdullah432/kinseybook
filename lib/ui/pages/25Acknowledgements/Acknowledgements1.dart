import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class Acknowledgements1 extends StatefulWidget {
  @override
  _Acknowledgements1State createState() => _Acknowledgements1State();
}

class _Acknowledgements1State extends State<Acknowledgements1> {
  final textFiles = [
    "Individual three-dimensional portraits of \nBernard, Shirley, and Khalil Kinsey, 2008",
    "Mikel Alatza\nOil on board",
    "Our thanks to The Xerox Corporation,\n for their support and funding of the development of this\n application, with special heartfelt thanks to Vice Presidents\n Dawn Sutherland and Arnold Hackett.",
  ];
  final fonts = [
    "MyriadPro-BoldIt",
    "Auto2-Regular",
    "MyriadPro-It",
  ];

  @override
  Widget build(BuildContext context) {
    List<double> presetFonts1 = [14, 10, 8];
    List<double> presetFonts2 = [38, 38, 34];

    String widgetFont = "Auto2-Regular";
    double leftRightRatio = .12;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child:
                      Container(
                          height: MediaQuery.of(context).size.height*0.3,
                          child: GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/acknowledgements1.jpg",)));
                              },
                              child: Image.asset("assets/images/acknowledgements1.jpg"))),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(

                        height: MediaQuery.of(context).size.height*0.25,

                        child: GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/acknowledgements2.jpg",)));
                          },
                          child: Image.asset(
                              "assets/images/acknowledgements2.jpg"),
                        ),
                      ),
                      AutoSizeText(
                        textFiles[0],
                        textAlign: TextAlign.center,
                        presetFontSizes: presetFonts1,
                        style: TextStyle(
                            fontFamily: fonts[0],
                            fontWeight: FontWeight.bold),
                      ),
                      AutoSizeText(textFiles[1],
                          textAlign: TextAlign.center,
                          presetFontSizes: presetFonts1,
                          style: TextStyle(
                            fontFamily: fonts[1],
                          ))
                    ],
                  ),
                ),
                Expanded(
                  child:
                      GestureDetector(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/acknowledgements3.jpg",)));
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height*0.3,
                          child:
                              Image.asset("assets/images/acknowledgements3.jpg"),
                        ),
                      ),
                )
              ],
            ),
          ),
          Expanded(
            child: Center(
              child: AutoSizeText(
                textFiles[2],
                textAlign: TextAlign.center,
                presetFontSizes: presetFonts2,
                style: TextStyle(
                  fontFamily: fonts[2],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
