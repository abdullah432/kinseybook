import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';


class ForgingFreedomSeven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Row(
        children: <Widget>[
          Expanded(flex: 3,
          child: Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 30,top: 30,bottom: 30),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/forging_freedom_7_1.jpg",)));
                        },
                        child: Image.asset("assets/images/forging_freedom_7_1.jpg"),
                      ),
                    ),
                    SizedBox(height: 30,),
                    AutoSizeText("Letter to President Roosevelt, 1903",style: TextStyle(fontWeight: FontWeight.bold),maxLines: 1,
                      presetFontSizes:[24,22,20] ,),
                    AutoSizeText("Carrie Kinsey\nDigital scan",presetFontSizes: [22,20],maxLines: 2,textAlign: TextAlign.center,),
                    AutoSizeText("13 1⁄4 x 10 3⁄4 in.",presetFontSizes: [22,20],),
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                right: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height*0.6,
                  width: MediaQuery.of(context).size.width*0.2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/forging_freedom_7_2.jpg",)));
                            },
                            child: Image.asset("assets/images/forging_freedom_7_2.jpg")),
                      ),
                      SizedBox(height: 30,),
                      AutoSizeText("Receipt of letter, 1903",style: TextStyle(fontWeight: FontWeight.bold),maxLines: 1,
                        presetFontSizes:[22,20,18,16,14] ,),
                      AutoSizeText("Department of Justice\nDigital scan",presetFontSizes: [22,20,18,16,14],textAlign: TextAlign.center,maxLines: 2,),
                      AutoSizeText("10 x 2 1⁄2 in.",presetFontSizes: [22,20,18,16,14],),
                    ],
                  ),
                ),
              )
            ],
          ),),
          Expanded(
            flex: 2,
            child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex:2,
                child: AutoSizeText("This letter from Carrie Kinsey, Bernard Kinsey’s second cousin, demonstrates the Kinsey family traits of persistence and intelligence, and the passion for justice and mobility in the face of the brutal culture that existed particularly in the South after the Civil War and Reconstruction. Although she is largely illiterate, she understands that the President of the United States is her only redress.\nShe writes:\n\nMr. President I have a brother about 14 years old a colord man came hear a hird him from me and sad that he would take good care of him an pay me five dollars a month for him an I heard of him no more he went an sold him to Macree an tha tha [they] has bin working him in prison for 12 month and I has tried to get thim to sind him to me an tha wont let/ him go he has no mother and has no father they are both dead an I am his only friend an tha wont let me have him he hase not don nothing for thim to have him in chanes so I rite to yoo for yoo to help me get my poor brother – his name is James Robinson and the man that carried him of his name Darr Cal he sold him to McCra at Valdosta, Ga please let me hear from yoo at once.",presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: AutoSizeText("Carrie Kinsey.",presetFontSizes: [22,20],textAlign: TextAlign.right,),
              ),
              Flexible(
                child: AutoSizeText("Carrie Kinsey’s letter provided the impetus for Douglas Blackmon’s 2008 book, Slavery by Another Name, which won a Pulitzer Prize in 2009. In April of 2009, Bernard and Shirley traveled with Blackmon to the National Archives in Washington, D.C., to retrieve the letter. It is one of the most personal and important acquisitions in their collection.",presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
              ),
            ],
          ),)
        ],
      ),
    );
  }
}
