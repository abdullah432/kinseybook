import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_4.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_4.jpg",
                  )),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
              child: Column(
                children: <Widget>[
                  AutoSizeText(
                    "The First Colored Senator and Representatives in the 41st and 42nd US Congress, 1872",
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                    ),
                    maxLines: 1,
                  ),
                  AutoSizeText(
                    "Currier and Ives",
                    style: TextStyle(fontSize: 22),
                  ),
                  AutoSizeText(
                    "Lithograph",
                    style: TextStyle(fontSize: 22),
                  ),
                  AutoSizeText(
                    "24 x 18 in.",
                    style: TextStyle(fontSize: 22),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
            Expanded(
                flex: 1,
                child: Padding(
                  padding:
                      const EdgeInsets.only(right: 30, left: 30, bottom: 20),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: AutoSizeText(
                            "Left to right:Senator Hiram Revels of Mississippi, Representatives Benjamin Turner of Alabama, Robert DeLarge of South Carolina, Josiah Walls of Florida, Jefferson Long of Georgia, and Joseph Rainey and Robert Brown Elliot of South Carolina.\n\nOf the 22 African American men who served in the United States Congress between 1870 and",
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: AutoSizeText(
                            "1901, 13 of them had been born into slavery.Hiram Rhoades Revels, Josiah Walls, and Robert Brown Elliot were born to free parents. Regardless of their backgrounds and the discrimination they faced in securing their seats, these men championed the rights of all Americans, proving that African Americans could succeed in the framework of a democratically elected legislature.",
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: AudioViewer(
                                url: "assets/audio/forging_freedom_4.mp3"),
                          ))
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
