import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomEighteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Citation for Croix de Guerre, 1918",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      presetFontSizes: [24, 22, 20],
                    ),
                    AutoSizeText(
                      "Government of France",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "Printed document",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "8 1⁄2 x 11 in.",
                      presetFontSizes: [22, 20],
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "The French Government awarded the 369th Infantry Regiment, the Harlem Hellfighters, its Croix de Guerre (Cross of War) for specific acts of bravery in capturing the village of Séchault from German forces during the regiment’s service with French troops in World War I.\n\nThe 369th Infantry took part in many campaigns in Europe and was the first Allied force to reach the Rhine River in Germany. In addition, the 369th band introduced jazz to much of Europe, creating a musical phenomenon in both the cities and the countryside.\n\nBy the end of the war, many soldiers in the division were awarded French Medals of Honor; however, they would return to the United States to face increasing racial hostility and blatant acts of discrimination. They returned home changed men, by both the war and the cultural knowledge they gained. This knowledge would help create opportunities in both America and Europe for a new generation of black American artists and scholars, changing the global artistic and intellectual landscape forever.",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url:
                                      "assets/images/forging_freedom_eighteen.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_eighteen.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
