import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwentySeven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url:
                                            "assets/images/forging_freedom_27.jpg",
                                      )));
                        },
                        child: Container(
                            height: MediaQuery.of(context).size.height * 0.8,
                            width: MediaQuery.of(context).size.width,
                            child: FittedBox(
                                fit: BoxFit.fill,
                                child: Image.asset(
                                    "assets/images/forging_freedom_27.jpg"))),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Commemorative coins and stamps, 1948, 1956",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 1,
                      presetFontSizes: [24, 22, 20],
                      textAlign: TextAlign.center,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Column(
                          children: <Widget>[
                            AutoSizeText(
                              "Booker T. Washington",
                              style: TextStyle(fontWeight: FontWeight.bold),
                              maxLines: 1,
                              presetFontSizes: [24, 22, 20],
                              textAlign: TextAlign.center,
                            ),
                            AutoSizeText(
                              "Picture, 4 3⁄4 x 4 in",
                              presetFontSizes: [22, 20],
                              textAlign: TextAlign.center,
                            ),
                            AutoSizeText(
                              "Coin, 1 1⁄4 diam.",
                              presetFontSizes: [22, 20],
                              textAlign: TextAlign.center,
                            ),
                            AutoSizeText(
                              "Envelope, 6 1⁄4 x 3 1⁄2 in.",
                              presetFontSizes: [22, 20],
                            ),
                          ],
                        )),
                        Expanded(
                            child: Column(
                          children: <Widget>[
                            AutoSizeText(
                              "George Washington Carver",
                              style: TextStyle(fontWeight: FontWeight.bold),
                              maxLines: 1,
                              presetFontSizes: [24, 22, 20],
                              textAlign: TextAlign.center,
                            ),
                            AutoSizeText(
                              "Picture, 5 1⁄4 x 3 3⁄4 in",
                              presetFontSizes: [22, 20],
                              textAlign: TextAlign.center,
                            ),
                            AutoSizeText(
                              "Coin, 1 1⁄4 diam.",
                              presetFontSizes: [22, 20],
                              textAlign: TextAlign.center,
                            ),
                            AutoSizeText(
                              "Envelope, 6 1⁄4 x 3 1⁄2 in.",
                              presetFontSizes: [22, 20],
                            ),
                          ],
                        ))
                      ],
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30, vertical: 10),
                        child: AutoSizeText(
                          "Booker T. Washington was a renowned American educator, author, and orator. Born a slave, he was emancipated in 1865 and went on to become the first president of the Tuskegee Normal and Industrial Institute (now Tuskegee University). Washington was considered to be one of the most influential leaders of the African American community in the 1890s and early 1900s.\n\nGeorge Washington Carver was an African American agricultural chemist and agronomist. Carver is known for revolutionizing agriculture in the South by conducting groundbreaking research on alternative crops to replace cotton. His experimentation with peanuts, sweet potatoes, and soybeans enabled poor farmers to cultivate crops that could provide food for their families. In 1896, Carver became the director of agricultural research for the Tuskegee Normal and Industrial Institute, where he worked with Booker T. Washington. Carver’s many accomplishments include his election to Britain’s Society for the Encouragement of Arts, Manufactures, and Commerce in 1916 and the award of the Spingarn Medal in 1923 .",
                          presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
