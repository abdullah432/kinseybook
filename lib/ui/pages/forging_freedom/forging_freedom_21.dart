import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwentyOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Flexible(
                        child: AutoSizeText(
                      "\nRosewood: Chronology of Events",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      textAlign: TextAlign.left,
                      presetFontSizes: [24, 22, 20, 18, 16, 14],
                    )),
                    Flexible(
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text: "08/05/20\n",
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            TextSpan(
                                text:
                                    "Four black men in McClenny are removed from the local jail and lynched for the alleged rape of a white woman.\n")
                          ],
                        ),
                        presetFontSizes: [
                          24,
                          22,
                          20,
                          18,
                        ],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Flexible(
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text: "11/02/20\n",
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            TextSpan(
                                text:
                                    "Two whites and at least five blacks are killed in Ocoee in a dispute over voting rights. The black community of Ocoee is destroyed, twenty-five homes, two churches, and a Masonic Lodge.\n")
                          ],
                        ),
                        presetFontSizes: [24, 22, 21, 20],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Flexible(
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text: "2/12/21\n",
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            TextSpan(
                                text:
                                    "A black man in Wauchula is lynched for an alleged attack on a white woman.\n")
                          ],
                        ),
                        presetFontSizes: [
                          24,
                          22,
                          20,
                          18,
                        ],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Flexible(
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text: "12/09/22\n",
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            TextSpan(
                                text:
                                    "A black man in Perry is burned at the stake, accused of the murder of a white school teacher. A black church, school, Masonic Lodge, and meeting hall are burned.\n")
                          ],
                        ),
                        presetFontSizes: [
                          24,
                          22,
                          20,
                        ],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Flexible(
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text: "12/31/22\n",
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            TextSpan(
                                text:
                                    "On New Year’s Eve a large Ku Klux Klan parade is held in Gainesville.\n")
                          ],
                        ),
                        presetFontSizes: [
                          24,
                          22,
                          20,
                          18,
                        ],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Flexible(
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text: "01/01/23\n",
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            TextSpan(
                                text:
                                    "Early morning: Fannie Taylor reports an attack by an unidentified black man.\n")
                          ],
                        ),
                        presetFontSizes: [
                          24,
                          22,
                          20,
                          18,
                        ],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Flexible(
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text:
                                    "Monday afternoon: Aaron Carrier is apprehended by a posse and is taken out of the area by Sheriff Walker.\n")
                          ],
                        ),
                        presetFontSizes: [
                          24,
                          22,
                          20,
                          18,
                        ],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text:
                                "Late afternoon: A posse of white vigilantes apprehends and kills a black man named Sam Carter.\n")
                      ],
                    ),
                    presetFontSizes: [
                      24,
                      22,
                      20,
                      18,
                    ],
                    textAlign: TextAlign.justify,
                  ),
                ),
                Flexible(
                  child: AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: "01/02/23\n",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(
                            text: "Armed whites begin gathering in Sumner.\n")
                      ],
                    ),
                    presetFontSizes: [
                      24,
                      22,
                      20,
                      18,
                    ],
                    textAlign: TextAlign.justify,
                  ),
                ),
                Expanded(
                  child: AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: "01/04/23\n",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(
                            text:
                                "Late evening: White vigilantes attack the Carrier house. Two white men are killed, and several others wounded. A black woman, Sarah Carrier, is killed, and others inside the Carrier house are either killed or wounded. Rosewood’s black residents flee into the swamps.\n\nOne black church is burned, in addition to several unprotected homes.\n\nLexie Gordon is murdered.\n")
                      ],
                    ),
                    presetFontSizes: [
                      24,
                      22,
                      20,
                      18,
                    ],
                    textAlign: TextAlign.justify,
                  ),
                ),
                Expanded(
                  child: AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: "01/05/23\n",
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(
                            text:
                                "Approximately 200 to 300 whites from surrounding areas begin to converge on Rosewood.\nMingo Williams is murdered.\nGovernor Cary Hardee is notified, and Sheriff Walker reports that he fears “no further disorder.”\n\nThe sheriff of Alachua County arrives in Rosewood to assist Sheriff Walker.\n\nJames Carrier is murdered.\n")
                      ],
                    ),
                    presetFontSizes: [
                      26,
                      24,
                      22,
                      20,
                      18,
                    ],
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            )),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Column(
                children: <Widget>[
                  Flexible(
                    child: AutoSizeText.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                              text: "\n\n01/06/23\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text:
                                  "A train of evacuated refugees travels to Gainesville.\n")
                        ],
                      ),
                      presetFontSizes: [
                        24,
                        22,
                        20,
                        18,
                      ],
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  Flexible(
                    child: AutoSizeText.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                              text: "01/07/23\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text:
                                  "A mob of 100 to 150 whites returns to Rosewood and burns the remaining structures.\n")
                        ],
                      ),
                      presetFontSizes: [
                        24,
                        22,
                        20,
                        18,
                      ],
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  Flexible(
                    child: AutoSizeText.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                              text: "01/17/23\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text:
                                  "A black man in Newberry is convicted of stealing cattle. He is removed from his cell and lynched by local whites.\n")
                        ],
                      ),
                      presetFontSizes: [
                        24,
                        22,
                        20,
                        18,
                      ],
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  Flexible(
                    child: AutoSizeText.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                              text: "02/11/23\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text:
                                  "A grand jury convenes in Bronson to investigate the Rosewood riot.\n")
                        ],
                      ),
                      presetFontSizes: [
                        24,
                        22,
                        20,
                        18,
                      ],
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  Flexible(
                    child: AutoSizeText.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                              text: "02/15/23\n",
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text:
                                  "The grand jury finds “insufficient evidence” to prosecute.\n")
                        ],
                      ),
                      presetFontSizes: [
                        24,
                        22,
                        20,
                        18,
                      ],
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
