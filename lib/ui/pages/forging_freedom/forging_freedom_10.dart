import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Marcus Garvey, ca. 1920",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 1,
                      presetFontSizes: [24, 22, 20],
                    ),
                    AutoSizeText(
                      "Provisional President of Africa, Founder and\nPresident of the Universal Negro Improvement",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      'Association\nPoster',
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "19 1⁄2 x 15 1⁄2 in.",
                      presetFontSizes: [22, 20],
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "Marcus Mosiah Garvey was born in Jamaica on August 17, 1887, and at 14 became an apprentice in the printing trade. He moved to Kingston and helped form the Printer’s Union, the first trade union in Jamaica. In 1910, Garvey was transformed from an individual concerned about people with little opportunity to an African nationalist determined to lift an entire race from bondage.\n\nGarvey’s movement, headquartered in New York, was the greatest international movement of African peoples in modern times. From 1922 to 1924, the movement claimed to have over eight million followers. Garvey emphasized the belief in one God, the God of Africa, who should be seen through black eyes. He preached to black people, urging them to know their African history and their rich cultural heritage. He was the first voice to clearly demand black power: “A race without authority and power is a race without respect.” He was imprisoned for securities fraud and spent nearly three years in jail. He was deported back to Jamaica and died in London in 1940.",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_10.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_10.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
