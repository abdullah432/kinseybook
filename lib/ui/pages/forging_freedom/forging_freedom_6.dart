import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomSix extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Hiram Rhoades Revels, ca. 1870",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 1,
                      presetFontSizes: [24, 22, 20],
                    ),
                    AutoSizeText(
                      "Mathew Brady\nCabinet card photograph",
                      presetFontSizes: [22, 20],
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "13 1⁄4 x 10 3⁄4 in.",
                      presetFontSizes: [22, 20],
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "Hiram Rhoades Revels, who was of mixed African and American Indian descent, represented the state of Mississippi as the first black senator elected during Reconstruction. He founded Alcorn State College, still in existence today as Alcorn State University.",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: AudioViewer(
                          url: "assets/audio/forging_freedom_6.mp3"),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_6.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_6.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
