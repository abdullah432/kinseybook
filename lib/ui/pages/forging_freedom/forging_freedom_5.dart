import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomFive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 3,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_5_1.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_5_1.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Josiah Walls, Congressman\nrepresenting Florida",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      presetFontSizes: [24, 22, 20],
                    ),
                    AutoSizeText(
                      "GPhotograph",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "11 x 13 in.",
                      presetFontSizes: [22, 20],
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "Contemporary artist Gayle Hubbard has enhanced this Harper’s Weekly cover illustration, “The First Vote,” with watercolor paint. The power of the image was recognized and promoted by the publication’s editors, who wrote in 1867:\n\n“The good sense and discretion, and above all the modesty, which the freedmen have displayed in the exercise, for the first time, of the great privilege which has been bestowed upon them, and the vast power which accompanies the privilege, have been most noticeable. Admiration of their commendable conduct has suggested the admirable engraving which we give on the first page of this issue. The freedmen are represented marching to the ballot-box to deposit their first vote, not with expressions of exultation or of defiance of their old masters and present opponents depicted on their countenances, but looking serious and solemn and determined. The picture is one which should interest every colored loyalist in the country.”",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 0.45,
                        height: MediaQuery.of(context).size.height * 0.2,
                        child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => GalleryView(
                                            url:
                                                "assets/images/forging_freedom_5_2.jpg",
                                          )));
                            },
                            child: Image.asset(
                                "assets/images/forging_freedom_5_2.jpg")))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
