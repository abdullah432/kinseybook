import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class ForgingFreedomNine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.03, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[

            Expanded(
              flex: 2,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/forging_freedom_9.jpg",)));
                },
                child: Image.asset("assets/images/forging_freedom_9.jpg"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40,vertical: 20),
              child: Column(
                children: <Widget>[
                  AutoSizeText("North Carolina Law Providing for Separate\nAccommodation for White & Colored Passengers Upon Motor Busses,\nand for Other Purposes, 1907",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,),maxLines: 3,textAlign: TextAlign.center,),
                  AutoSizeText("Printed document"),
                  AutoSizeText("6 1⁄4 x 13 1⁄4 in."),
                  SizedBox(height: 20,),
                  Padding(
                    padding:  EdgeInsets.only(right: MediaQuery.of(context).size.width*0.15,left: MediaQuery.of(context).size.width*0.15,bottom: 20),
                    child: AudioViewer(url:"assets/audio/forging_freedom_9.mp3"),
                  )
                ],
              ),
            ),
            Expanded(
                flex: 1,
                child: Padding(
                  padding:  EdgeInsets.only(right: MediaQuery.of(context).size.width*0.15,left: MediaQuery.of(context).size.width*0.15,bottom: 20),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex:1,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                          child: AutoSizeText("The 1896 United States Supreme Court decision Plessy v. Ferguson established that “separate” facilities for blacks and whites were constitutional as long as they were “equal.” The “separate but equal” doctrine was extended through state legislation to cover many areas of public life, such as restaurants, theaters, restrooms, and public schools.",presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
                        ),
                      ),
                      Expanded(
                        flex:1,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                          child: AutoSizeText("The state laws were posted in public spaces to ensure that they were obeyed. Although separate accommodations for blacks were rarely equal to those of whites, the local, state, and federal governments upheld the Supreme Court ruling as a strategy for racial dominance and the continued subjugation of blacks.",presetFontSizes:[22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
                        ),
                      ),
                    ],
                  ),
                ))

          ],
        ),
      ),
    );
  }
}
