import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomThirtyOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url:
                                            "assets/images/forging_freedom_31.jpg",
                                      )));
                        },
                        child: Container(
                            height: MediaQuery.of(context).size.height * 0.8,
                            width: MediaQuery.of(context).size.width,
                            child: FittedBox(
                                fit: BoxFit.fill,
                                child: Image.asset(
                                    "assets/images/forging_freedom_31.jpg"))),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.play_circle_outline,
                        color: Colors.black,
                      ),
                      iconSize: 80,
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VideoView(
                                      videoLink:
                                          "assets/videos/forging_freedom_31.mp4",
                                    )));
                      },
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Woolworth Boycott Broadside by CORE, ca.\n1960",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      presetFontSizes: [24, 22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "Ink on paper",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "8 1⁄4 x 10 3⁄4 in.",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "The first sit-ins by the Congress of Racial Equality in Chicago in 1943 were intended to change the attitudes of business owners—as did this one of 1960, a national boycott of Woolworth’s.\n\nDuring the period 1955–68, acts of nonviolent protest and civil disobedience produced confrontations between activists and government authorities. Crisis situations that highlighted the inequities faced by African Americans often drew immediate attention from federal, state, and local governments, and from businesses and educational institutions. Forms of protest and/or civil disobedience included boycotts, such as the successful Montgomery Bus Boycott in Alabama (1955–1956); sit-ins such as the influential Greensboro one in North Carolina (1960); marches, such as the Selma to Montgomery marches in Alabama (1965); and a wide range of other nonviolent activities.\n\nMany of those who were active in the civil rights movement, with organizations such as NAACP, SNCC, CORE, and SCLC, prefer the term “Southern Freedom Movement” because the struggle was about far more than just civil rights under the law; it was also about fundamental issues of freedom, respect, dignity, and economic and social equality.",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
