import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomThirty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Letter from Rev. Dr. Martin Luther King, Jr.\nto his Literary Agent, 1957",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "Typed on paper",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "8 1⁄2 x 11 in.",
                      presetFontSizes: [22, 20],
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "King’s letterhead identifies him as Pastor of the Dexter Avenue Baptist Church and President of the Montgomery Improvement Association. Written to his literary agent, Mary Rodell, the letter refers to the contract for his first book, Strive Toward Freedom.",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_30.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_30.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
