import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

class ForgingFreedomSixteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.02),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[

            Expanded(
              flex: 3,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/forging_freedom_16.jpg",)));
                },
                child: Image.asset("assets/images/forging_freedom_16.jpg",fit: BoxFit.fill,),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40,vertical: 10),
              child: Column(
                children: <Widget>[
                  AutoSizeText("The Birth of a Race Photoplay Corporation\nCertificate of 10 shares of stock in the Corporation, 1918",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 18,),maxLines: 2,textAlign: TextAlign.center,),
                  AutoSizeText("Standard engraved certificate",presetFontSizes: [22,20,18],maxLines: 1,),
                  AutoSizeText("8 x 11 in.",maxLines: 1,),
                  AutoSizeText("10 x 7 in.",maxLines: 1,),
                ],
              ),
            ),
            Flexible(
                flex: 1,
                child: Padding(
                  padding:  EdgeInsets.only(right: MediaQuery.of(context).size.width*0.15,left: MediaQuery.of(context).size.width*0.15),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex:1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AutoSizeText("This is a stock certificate from one of the earli- est black film companies, formed to rebut D. W. Griffith’s Birth of a Nation. Emmett J. Scott, best known for Scott’s Official History of the American Negro in the World War, formed “The Birth of a",presetFontSizes:[22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
                        ),
                      ),
                      Expanded(
                        flex:1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AutoSizeText("Race Photoplay Corporation” in 1917 with a cap- ital stock of a million dollars. The company un- dertook one of the largest stock offerings ever, signing up approximately 7,000 shareholders for a total of \$140,000.",presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
                        ),
                      ),

                    ],
                  ),
                ))

          ],
        ),
      ),
    );
  }
}
