import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomEight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url:
                                              "assets/images/forging_freedom_8_1.jpg",
                                        )));
                          },
                          child: Image.asset(
                              "assets/images/forging_freedom_8_1.jpg")),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Slavery by Another Name:\nThe Re-Enslavement of Black Americans\nfrom the Civil War to World War II, 2008",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 3,
                      presetFontSizes: [24, 22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "Douglas A. Blackmon\nBook",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "19 x 6 1⁄2 x 1 1⁄2 in.",
                      presetFontSizes: [22, 20],
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url:
                                            "assets/images/forging_freedom_8_2.jpg",
                                      )));
                        },
                        child: Container(
                            height: MediaQuery.of(context).size.height * 0.8,
                            width: MediaQuery.of(context).size.width * 0.5,
                            child: FittedBox(
                                fit: BoxFit.fill,
                                child: Image.asset(
                                    "assets/images/forging_freedom_8_2.jpg"))),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Letter to Bernard Kinsey, 2008",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 1,
                      presetFontSizes: [24, 22, 20],
                    ),
                    AutoSizeText(
                      "Douglas A. Blackmon\nPhotomechanical reproduction",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "8 1⁄2 x 11 in.",
                      presetFontSizes: [22, 20],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
