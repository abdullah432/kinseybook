import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomThirteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_13.jpg",
                                )));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child:
                          Image.asset("assets/images/forging_freedom_13.jpg"),
                    ),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                child: Column(
                  children: <Widget>[
                    AutoSizeText(
                      "The Crisis: A Record of the Darker Race, 1911–27",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 24,
                      ),
                      maxLines: 1,
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "New York: National Association for the Advancement of Colored People",
                      presetFontSizes: [
                        22,
                      ],
                      maxLines: 1,
                    ),
                    AutoSizeText(
                      "132 issues, Printed on paper",
                      presetFontSizes: [
                        22,
                      ],
                      maxLines: 1,
                    ),
                    AutoSizeText(
                      "10 x 7 in.",
                      presetFontSizes: [22],
                      maxLines: 1,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
              Expanded(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AutoSizeText(
                            "This large collection of early issues of the official magazine of the NAACP, The Crisis, once belonged to Augustus Granville Dill, who served as business manager and the assistant editor to W.E.B. DuBois between 1913 and 1928. This periodical, originally subtitled A Crisis of the Darker Races, is one of the key sources for the African American history of that era. The Crisis (originally named for James Russell Lowell’s passionate anti-slavery poem “The Present Crisis”) is still being published.",
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AutoSizeText(
                            "Under DuBois’s editorship, the magazine promoted a progressive image of African American people and called for social justice and an end to violence against them. It also reported the accomplishments of individual African Americans. DuBois reviewed articles from other publications that he felt were incorrect, and wrote about controversial issues, including lynching and racism, and commented on the views of Booker T. Washington and Marcus Garvey. Although mainly concerned with political and social issues, The Crisis also covered the arts, and",
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AutoSizeText(
                            "had a major impact on the Harlem Rensaissance movement, especially from 1918 to 1926, when Jessie Redmon Fauset was the periodical’s literary editor. The Crisis also promoted the production of black cinema, with its focal point the Ethiopian Art Theatre in Chicago (1923–25). The theatre provided training and promotion of black actors as well as employment for black citizens of Chicago. The theatre attracted thousands of blacks from the South who saw it as an opportunity for success, and it fostered great pride among the black community.",
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
