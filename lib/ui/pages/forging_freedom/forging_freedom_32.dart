import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomThirtyTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/forging_freedom_32.jpg",
                              )));
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Image.asset("assets/images/forging_freedom_32.jpg"),
                  ),
                ),
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 40, vertical: 20),
                    child: Column(
                      children: <Widget>[
                        AutoSizeText(
                          "March on Washington for Jobs and Freedom,\nAugust 28, 1963",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 24,
                          ),
                          maxLines: 2,
                          textAlign: TextAlign.center,
                        ),
                        AutoSizeText(
                          "Lincoln Memorial program with news photograph of Martin Luther King, Jr.",
                          presetFontSizes: [22, 20, 18],
                          maxLines: 1,
                        ),
                        AutoSizeText(
                          "Ink on paper and black-and-white photograph",
                          presetFontSizes: [22, 20, 18],
                          maxLines: 1,
                        ),
                        AutoSizeText(
                          "8 1⁄2 x 18 3⁄4 in.",
                          presetFontSizes: [22, 20, 18],
                          maxLines: 1,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child:
                        AudioViewer(url: "assets/audio/forging_freedom_32.mp3"),
                  ),
                )
              ],
            ),
            Expanded(
                flex: 1,
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AutoSizeText(
                            "This is the original program from the March on Washington for Jobs and Freedom, calling for racial equality and an end to discrimination. The speech by Dr. King, famously known as “I Have a Dream,” was delivered from the steps of the Lincoln Memorial to over 200,000 rapt listeners. The speech galvanized the burgeoning civil rights movement and awakened the social conscience of the American people. It is without question one of the greatest speeches given in the history",
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AutoSizeText(
                            "of the United States.\n\nHere is an excerpt from the program:\n\n“It was conceived as an outpouring of the deep feeling of millions of white and colored American citizens that the time has come for the government of the United States of America, and particularly for the Congress of that government, to grant and guarantee complete equality and citizenship",
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AutoSizeText(
                            "of the Negro minority of our population. As such, the Washington March is a living petition— in the flesh—of the scores of thousands of citizens of both races who will be present from all parts of our country.”\n\nMajor legislative landmarks consequent on the movement are the Civil Rights Act of 1964 and the Voting Rights Act of 1965.",
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
