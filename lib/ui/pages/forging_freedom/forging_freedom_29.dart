import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwentyNine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 3,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_29.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_29.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Letter to Alex Haley, 1963",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      presetFontSizes: [24, 22, 20],
                    ),
                    AutoSizeText(
                      "Malcolm X",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "Typed on paper",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "8.5 x 11 in.",
                      presetFontSizes: [22, 20],
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "Bernard Kinsey is a student of history. The documents he collects are a conduit to an African American past to which he links both his mobility and his legacy. While some artifacts, such as this one, are written by iconic figures, many in the Kinsey Collection document the extraordinary courage of the common man—each contributing to the tapestry of black history that the Kinseys are assembling, in order to educate and inspire those who encounter it.",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
