import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwenty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "A Documented History of the Incident Which\nOccurred at Rosewood, Florida,\nin January 1923, 1993",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 3,
                      textAlign: TextAlign.center,
                      presetFontSizes: [24, 22, 20],
                    ),
                    AutoSizeText(
                      "Jones, Rivers, Colburn, Dye, and Rogers",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "Photomechanical reproduction",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "8 1⁄2 x 11 in.",
                      presetFontSizes: [22, 20],
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "In January 1923, black townspeople of Rosewood acted to defend themselves after one of them was lynched over the accusation that a white woman was attacked, and possibly raped, by a black man. When several hundred whites burned the community of Rosewood to the ground, the black residents fled, and they never returned.\n\nThis report provided the documentation necessary for the survivors and their descendants to be compensated for damages by the state of Florida. This is the only time reparations have been paid to black Americans for the atrocities committed against them. Florida has since declared Rosewood a Florida Heritage Landmark.\n\nThis document was a gift from Dr Larry E. Rivers.",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_20.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_20.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
