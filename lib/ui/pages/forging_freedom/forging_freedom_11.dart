import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomEleven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url:
                                              "assets/images/forging_freedom_11_1.jpg",
                                        )));
                          },
                          child: Image.asset(
                              "assets/images/forging_freedom_11_1.jpg")),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "A Negro Explorer at the North Pole, 1912",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 1,
                      presetFontSizes: [24, 22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "Matthew Henson\nBook",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "8 1⁄2 x 6 x 1 in.",
                      presetFontSizes: [22, 20],
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Explorer Matthew Henson, 1910",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 1,
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "Carte de visite, 7 x 5",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "letter, 6 1⁄2 x 5, envelope, 6 x 3 1⁄4 in.",
                      presetFontSizes: [22, 20],
                    ),
                    Expanded(
                      child: AutoSizeText(
                        "Matthew A. Henson was an explorer who became one of the first men to reach the North Pole. Robert E. Peary hired Henson on many expeditions, including the historic venture to the North Pole in 1909. Henson’s account of the expedition, A Negro Explorer at the North Pole, was published in 1912. In 1913, President William Howard Taft appointed him a clerk in the U.S. Customs House in New York City. Henson was awarded the Congressional Medal for the Peary Expedition in 1944.\n\nDr. S. Allen Counter, a high school classmate and close friend of Bernard Kinsey, worked to have Matthew Henson reburied with full honors in Arlington Cemetery .",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GalleryView(
                                      url:
                                          "assets/images/forging_freedom_11_2.jpg",
                                    )));
                      },
                      child: Container(
                          height: MediaQuery.of(context).size.height * 0.35,
                          width: MediaQuery.of(context).size.width * 0.25,
                          child: FittedBox(
                              fit: BoxFit.fitHeight,
                              child: Image.asset(
                                  "assets/images/forging_freedom_11_2.jpg"))),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
