import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwentyFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "American Beach Negro Ocean Playground,\nca. 1930",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      presetFontSizes: [24, 22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "Maker unknown",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "Steel plaque",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "11 1⁄4 x 4 1⁄2 in.",
                      presetFontSizes: [22, 20],
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "This plaque is an important artifact for Shirley Kinsey, as this was one of the only beaches where her family was allowed to visit and vacation. It also demonstrates the spirit common in the African American communities facing segregation up and down the Atlantic seaboard. Whites designated this beach as a segregated space, keeping their own beaches “whites only,” but blacks protected, built on, and fortified their beach communities, making them into thriving and celebratory spaces.\n\nAmerican Beach, founded in 1935 by Florida’s first black millionaire, Abraham Lincoln Lewis, was designated a historic site by the National Register of Historic Places in January 2002.",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
                flex: 3,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url:
                                              "assets/images/forging_freedom_24_1.jpg",
                                        )));
                          },
                          child: Container(
                              height: MediaQuery.of(context).size.height * 0.8,
                              width: MediaQuery.of(context).size.width * 0.6,
                              child: FittedBox(
                                  fit: BoxFit.fill,
                                  child: Image.asset(
                                      "assets/images/forging_freedom_24_1.jpg"))),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url:
                                              "assets/images/forging_freedom_24_2.jpg",
                                        )));
                          },
                          child: Container(
                              height: MediaQuery.of(context).size.height * 0.8,
                              width: MediaQuery.of(context).size.width * 0.6,
                              child: FittedBox(
                                  fit: BoxFit.fill,
                                  child: Image.asset(
                                      "assets/images/forging_freedom_24_2.jpg"))),
                        ),
                      ),
                      AutoSizeText(
                        "Separate Drinking Fountain, Montgomery, Alabama,",
                        style: TextStyle(fontWeight: FontWeight.bold),
                        maxLines: 1,
                        presetFontSizes: [24, 22, 20],
                      ),
                      AutoSizeText(
                        "1931",
                        style: TextStyle(fontWeight: FontWeight.bold),
                        maxLines: 1,
                        presetFontSizes: [24, 22, 20],
                      ),
                      AutoSizeText(
                        "Painted bronze plaque",
                        presetFontSizes: [22, 20],
                      ),
                      AutoSizeText(
                        "10 3⁄4 x 4 1⁄2 in",
                        presetFontSizes: [22, 20],
                      ),
                      AutoSizeText(
                        "A gift from friends Erika and Eusebius Williams.",
                        presetFontSizes: [22, 20],
                      ),
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
