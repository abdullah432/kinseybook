import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomFifteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          children: <Widget>[
            Expanded(
                flex: 2,
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: 30,
                              ),
                              AutoSizeText(
                                "“Colored Man is No Slacker”, 1918",
                                style: TextStyle(fontWeight: FontWeight.bold),
                                maxLines: 1,
                                presetFontSizes: [24, 22, 20],
                              ),
                              AutoSizeText(
                                "Recruiting poster",
                                presetFontSizes: [22, 20],
                              ),
                              AutoSizeText(
                                "Lithograph",
                                presetFontSizes: [22, 20],
                              ),
                              AutoSizeText(
                                "16 x 20 in.",
                                presetFontSizes: [22, 20],
                              ),
                              Flexible(
                                child: AutoSizeText(
                                  "This World War I recruiting poster was aimed at young African Americans, encouraging them to enlist. “Colored Man Is No Slacker” portrays a black soldier off to war against a backdrop depicting African American patriotism, self- sacrifice, and courage. It frames the war as a struggle for freedom akin to the Civil War and as an opportunity for African Americans to prove their patriotism.\n\nMore than 350,000 African Americans, trained and deployed in segregated units, served in the U.S. military during World War I, and more than 42,000 saw action in Europe. The most celebrated unit was the 369th Regiment, the “Harlem Hell Fighters,” who fought for the French, in French uniforms and using French weaponry—when the American command under General Pershing regarded black troops only as ditch-diggers and burial detail. The Harlem Hell Fighters received the Croix de guerre, the highest French military honor for bravery, in acknowledgment that they were the first regiment to reach the Rhine in World War I.",
                                  presetFontSizes: [
                                    22,
                                    21,
                                    20,
                                    19,
                                    18,
                                    17,
                                    16,
                                    14
                                  ],
                                  textAlign: TextAlign.justify,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => GalleryView(
                                            url:
                                                "assets/images/forging_freedom_15.jpg",
                                          )));
                            },
                            child: Image.asset(
                                "assets/images/forging_freedom_15.jpg")),
                      ),
                    ])),
            Expanded(
                flex: 1,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: AutoSizeText(
                          "Black servicemen during World War I challenged racial truths about black men by fighting with courage and pride in the face of virulently racist attitudes. Throughout our country’s history, African Americans have looked hopefully to military service as a way to achieve racial equality and opportunity, but this aspiration was in direct conflict with the status allowed them by white",
                          presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: AutoSizeText(
                          "society. Hailed as a hero during the war, Sergeant Henry Johnson returned almost completely disabled from his wounds, but along with many other black veterans, he was denied the medical care and other assistance to which veterans were entitled. After making his plight public, he was discharged with no disability pay. Henry Johnson, patriot and war hero, died penniless and alone in 1929, age thirty-two.",
                          presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: AutoSizeText(
                          "Upon their return from World War I, many black soldiers were hung in their uniforms, to signal to blacks as well as whites that nothing had changed. African Americans learned from World War I that they should never underestimate the perversion of the ideals the nation supposedly fought for.",
                          presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
