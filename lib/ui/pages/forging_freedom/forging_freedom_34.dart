import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomThirtyFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Muhammad Ali Wheaties box, 1999",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 1,
                      presetFontSizes: [24, 22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "Ink on board, signed",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "7 1⁄2 x 11 in.",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "So much can be said of Muhammad Ali - one of the greatest boxers of all time, one of the best known athletes of all time, one of the smartest athletes of all time, and one of the most eclectic self promoters of all time. Early in his boxing career, he claimed, “I am the greatest,” which at the time seemed to be youthful boasting and brashness, but it turned out to be true or very nearly true.\n\nIn this book, he is featured less for his boxing than for his major contributions to black independence and race relations during the civil rights movement of the 1960’s and 70’s. After winning the heavyweight title he converted to Islam and changed his name from Cassius Clay, “his slave name,” to Muhammad Ali. Later, he refused to be drafted during the Vietnam era for religious reasons. This caused him to be stripped of his boxing titles and his right to fight professionally for 3 years at the height of his skills and probably most productive years as an athlete. Ultimately, the Supreme Court ruled in his favor and he returned to boxing,regaining his heavyweight championship title.\n\nAfter his boxing career ended, Ali was struck with Parkinson’s disease, most likely resulting from the constant and consistent head trauma from his years in the ring. If anything, his popularity and fame continued to grow until his death in 2013.\n\nThe Kinseys had a long and personal friendship with the champion and his family.",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_34.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_34.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
