import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomNinteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 3,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_19.jpg",
                                )));
                  },
                  child: Image.asset("assets/images/forging_freedom_19.jpg")),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Broadside, For the Good of America, ca.1926",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 1,
                      presetFontSizes: [24, 22, 20],
                    ),
                    AutoSizeText(
                      "NAACP",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "Printed paper",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "14 1⁄4 x 10 3⁄4 in.",
                      presetFontSizes: [22, 20],
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "Violent acts against African Americans increased during the first quarter of the twentieth century. The National Association for the Advancement of Colored People (NAACP) began compiling lynching statistics in 1912, thirty years after the Chicago Tribune and twenty years after the Tuskegee Institute started tracking such crimes. In November 1922, the NAACP ran full-page ads in newspapers, pressing for the passage of the Dyer anti-lynching bill. Entitled “The Shame of America,” the ad presented shocking statistics covering more than two decades, 1899 through 1922. The House of Representatives passed the bill with a two-to-one margin, but it was defeated in the Senate. A few years later, the NAACP issued the statistics as a broadside. Entitled “For the Good of America,” it encouraged citizens to “aid the organization, which has been fighting for ten years to wipe out our shame.” Despite the NAACP’s vigorous efforts through the 1930s and the introduction of several subsequent bills, the U.S. Congress never outlawed lynching.",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
