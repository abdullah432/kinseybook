import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwentySix extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "Due to segregation, prejudice, and “Jim Crow” laws, traveling by car was a daunting prospect to black families, who might be turned away from lodging, restaurants, gas stations, and even restrooms along the way. The Negro Motorist Green-Book listed establishements where a friendly welcome might be expected by the traveler. These were located on every major and minor artery that families might be likely to traverse on their way from the Northeast to visit their hometowns in the “old south.” Well into the twentieth century, copies of a guide like this could be found in most automobiles belonging to black families. Although this edition of the guide was printed in 1941, the lastone appeared in 1966.",
                        presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GalleryView(
                                      url:
                                          "assets/images/forging_freedom_26_1.jpg",
                                    )));
                      },
                      child: Container(
                          height: MediaQuery.of(context).size.height * 0.4,
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: FittedBox(
                              fit: BoxFit.fitHeight,
                              child: Image.asset(
                                  "assets/images/forging_freedom_26_1.jpg"))),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url:
                                            "assets/images/forging_freedom_26_2.jpg",
                                      )));
                        },
                        child: Container(
                            height: MediaQuery.of(context).size.height * 0.8,
                            width: MediaQuery.of(context).size.width,
                            child: FittedBox(
                                fit: BoxFit.fill,
                                child: Image.asset(
                                    "assets/images/forging_freedom_26_2.jpg"))),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "The Negro Motorist Green-Book, 1941",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 1,
                      presetFontSizes: [24, 22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "Victor H. Green, Publisher\nBook",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "4.3⁄4 x 6 in.",
                      presetFontSizes: [22, 20],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
