import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomThirtyThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url:
                                              "assets/images/forging_freedom_33.jpg",
                                        )));
                          },
                          child: Image.asset(
                            "assets/images/forging_freedom_33.jpg",
                            fit: BoxFit.fitHeight,
                          )),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(left: 30, top: 30, bottom: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Honor King: End Racism!, 1968",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 1,
                      presetFontSizes: [24, 22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "Ink on paper",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "14 x 21 in.",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15),
                        child: AutoSizeText(
                          "This placard was carried during the memorial march in honor of Dr. Martin Luther King, Jr., held on April 8, 1968, in Memphis, Tennessee.",
                          presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                          textAlign: TextAlign.justify,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: AudioViewer(
                          url: "assets/audio/forging_freedom_33.mp3"),
                    ),
                    Container(
                        height: MediaQuery.of(context).size.height * 0.3,
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: FittedBox(
                            fit: BoxFit.contain,
                            child: Image.asset(
                                "assets/images/forging_freedom_33_1.jpg"))),
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Memorial March after the Assassination of\nMartin Luther King, Jr., 1968",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      presetFontSizes: [24, 22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "Ernest Withers",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "Photographic print",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "16 x 24 in.",
                      presetFontSizes: [22, 20],
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
