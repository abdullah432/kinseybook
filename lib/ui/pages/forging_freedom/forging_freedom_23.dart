import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwentyThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AutoSizeText(
                      "group of white liberals that included Mary White Ovington and Oswald Garrison Villard (both descendants of abolitionists), William English Walling, and Dr. Henry Moscowitz issued a call for a meeting to discuss racial justice. Some sixty people, seven of whom were African American (including W. E. B. Du Bois, Ida B. Wells-Barnett, and Mary Church Terrell), signed the call.\n\nThe NAACP established its national office in New York City in 1910 and named a board of directors as well as a president, Moorfield Storey, who was a white constitutional lawyer and former president of the American Bar Association. Du Bois, the only African American among the organization’s executives, was made director of publications and research and in 1910 established the official journal of the NAACP, The Crisis. With a strong emphasis on local organizing, by 1913 the NAACP had established branch offices in such cities as Boston, Baltimore, Kansas City, Washington, D.C., Detroit, and St. Louis. Joel Spingarn, a professor of literature who was one of the NAACP founders, formulated much of the strategy that led to the growth of the organization. He was elected board chairman of the NAACP in 1915 and served as president from 1929 to 1939. In 1938, the association continued its fight to end segregation at the Tennessee Valley Authority. Charles Houston and Thurgood Marshall conducted a third NAACP investigation of working conditions there and submitted a report to a joint committee of Congress that was appointed to study repeated discrimination. During that same year, Thurgood Marshall was appointed NAACP special counsel after the currentoffice holder, Charles H. Houston,",
                      presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AutoSizeText(
                      "returned to private practice. It was Houston who represented Lloyd Gaines before the Supreme Court in a landmark case later that year, where the Supreme Court ruled that the University of Missouri Law School could not exclude Gaines because of his color. It also ruled that each state must provide equal educational facilities for black students, and that states could not fulfill this requirement by providing scholarships to black students to attend college elsewhere.",
                      presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: SizedBox(),
                ),
              ],
            )),
      ),
    );
  }
}
