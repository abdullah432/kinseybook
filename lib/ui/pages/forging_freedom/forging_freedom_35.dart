import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomThirtyFive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => GalleryView(
                              url: "assets/images/forging_freedom_35_1.jpg",
                            )));
              },
              child: Container(
                  height: MediaQuery.of(context).size.height,
                  child: Image.asset(
                    "assets/images/forging_freedom_35_1.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 30,
              ),
              AutoSizeText(
                "Ulysses Bradshaw Kinsey\n(1914-2005)",
                style: TextStyle(fontWeight: FontWeight.bold),
                maxLines: 2,
                presetFontSizes: [24, 22, 20],
                textAlign: TextAlign.center,
              ),
              AutoSizeText(
                "21 1⁄2 x 13 in.",
                presetFontSizes: [22, 20],
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.07,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/forging_freedom_35_2.jpg",
                              )));
                },
                child: Container(
                    height: MediaQuery.of(context).size.height * 0.3,
                    width: MediaQuery.of(context).size.width * 0.3,
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: Image.asset(
                            "assets/images/forging_freedom_35_2.jpg"))),
              ),
            ],
          ),
          Expanded(
            flex: 1,
            child: AutoSizeText(
              "Bernard Kinsey’s father, Ulysses Bradshaw, “U.B.,” Kinsey was born in Fort White, in northern Florida, the third of ten children. His father sold the family farm and bought a grocery store in West Palm Beach about 1930. Kinsey graduated from all-black Industrial High School, where the principal, Clarence Walker, inspired him to aim high in life. Although Kinsey hoped to become a lawyer, the University of Florida accepted only whites at that time. Kinsey graduated in 1941 from Florida A&M University.\n\nHis first teaching position was at his alma mater, Industrial High School. When the Palm Beach County School Board raised the pay of the white teachers, Kinsey joined the newly established Palm Beach County Teachers Association for black teachers, which challenged this measure and lost. They then initiated and won a class-action lawsuit with the help of future U.S. Supreme Court Justice Thurgood Marshall, a precedent for Brown vs. Board of Education of Topeka.\n\nIndustrial High School was renamed Palmview Elementary in 1950, with Kinsey appointed principal. Because of the impact of segregation on his own life, Kinsey was a stickler for respect and a tireless advocate for his students. Each year, he acted early to secure new books and supplies for Palmview, rather than accepting the leftovers the all-black schools usually received. When Kinsey retired, the Palm Beach County Commission designated the week of September 16–22, 1989, as “Ulysses B. Kinsey Week,” and Palmview Elementary was renamed U. B. Kinsey Elementary School. Kinsey passed away in 2005",
              presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
              textAlign: TextAlign.justify,
            ),
          ),
        ],
      ),
    ));
  }
}
