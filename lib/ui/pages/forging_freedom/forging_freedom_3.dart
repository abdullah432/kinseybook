import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: template.viewWidth() * 33,
            ),
            Container(
              height: template.viewHeight() * 270,
              width: template.viewWidth() * 230,
              child: Padding(
                padding: EdgeInsets.only(top: template.viewHeight() * 10),
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/forging_freedom_3.jpg",
                                  )));
                    },
                    child: Image.asset(
                      "assets/images/forging_freedom_3.jpg",
                      fit: BoxFit.contain,
                    )),
              ),
            ),
            Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Padding(
                padding: template.textcontainerPadding(),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: template.viewHeight() * 28,
                    ),
                    Text(
                      "The First Vote, n.d.",
                      style: template.textheaderStyle(),
                      maxLines: 1,
                    ),
                    Text(
                      "Gayle Hubbard",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "Watercolor painted on facsimile of\nHarper’s Weekly, November 16, 1867",
                      style: template.bodyTextStyle(),
                    ),
                    Text("20 x 16 in.", style: template.bodyTextStyle()),
                    Container(
                      height: template.viewHeight() * 195,
                      width: template.textContainerwidth(),
                      child: Text(
                        "\nContemporary artist Gayle Hubbard has enhanced this Harper’s Weekly cover illustration, “The First Vote,” with watercolor paint. The power of the image was recognized and promoted by the publication’s editors, who wrote in 1867:\n\n“The good sense and discretion, and above all the modesty, which the freedmen have displayed in the exercise, for the first time, of the great privilege which has been bestowed upon them, and the vast power which accompanies the privilege, have been most noticeable. Admiration of their commendable conduct has suggested the admirable engraving which we give on the first page of this issue. The freedmen are represented marching to the ballot-box to deposit their first vote, not with expressions of exultation or of defiance of their old masters and present opponents depicted on their countenances, but looking serious and solemn and determined. The picture is one which should interest every colored loyalist in the country.”",
                        textAlign: TextAlign.justify,
                        style: template.bodyTextStyle(),
                      ),
                    ),
                    SizedBox(
                      height: template.viewHeight() * 5,
                    ),
                    Container(
                        height: template.footerTextContainerHihght(),
                        width: template.textContainerwidth() * 2,
                        child: template.footerRightText())
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
