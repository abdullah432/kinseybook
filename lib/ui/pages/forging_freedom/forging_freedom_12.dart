import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwelve extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 30,
                  ),
                  AutoSizeText(
                    "Program of the Indianapolis Negro\nSpeedway Racing Association, 1924",
                    style: TextStyle(fontWeight: FontWeight.bold),
                    maxLines: 2,
                    presetFontSizes: [24, 22, 20],
                    textAlign: TextAlign.center,
                  ),
                  AutoSizeText(
                    "Printed on paper",
                    presetFontSizes: [22, 20],
                  ),
                  AutoSizeText(
                    "6 x 9 in.",
                    presetFontSizes: [22, 20],
                  ),
                  Flexible(
                    child: AutoSizeText(
                      "The Negro Speedway Racing Association sponsored a “Gold & Glory Sweepstakes” annually between 1924 and 1936. Held at the Indianapolis Fairgrounds, it was for black drivers only. This is the original program for the first such event.\n\nFrank A. Young, the sports editor for the Chicago Defender, a leading newspaper among African Americans, wrote: “This auto race will be recognized throughout the length and breadth of the land as the single greatest sporting event to be staged annually by colored people . . . The largest purses will be posted here, and the greatest array of driving talent will be in attendance in hopes of winning gold for themselves and Glory for the race.”",
                      presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url:
                                        "assets/images/forging_freedom_12_2.jpg",
                                  )));
                    },
                    child: Container(
                        height: MediaQuery.of(context).size.height * 0.3,
                        width: MediaQuery.of(context).size.width * 0.3,
                        child: FittedBox(
                            fit: BoxFit.fitHeight,
                            child: Image.asset(
                                "assets/images/forging_freedom_12_2.jpg"))),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 3,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_12_1.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_12_1.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
