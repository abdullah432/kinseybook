import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwentyFIve extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 3,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_25.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_25.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
            Expanded(
              flex: 2,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 30,
                  ),
                  AutoSizeText(
                    "Paul Robeson in the Song of Freedom, 1936",
                    style: TextStyle(fontWeight: FontWeight.bold),
                    maxLines: 1,
                    presetFontSizes: [24, 22, 20],
                  ),
                  AutoSizeText(
                    "Song of Freedom, Inc.",
                    presetFontSizes: [22, 20],
                  ),
                  AutoSizeText(
                    "Offset lithograph",
                    presetFontSizes: [22, 20],
                  ),
                  AutoSizeText(
                    "42 1⁄2 x 29 in.",
                    presetFontSizes: [22, 20],
                  ),
                  Flexible(
                    child: AutoSizeText(
                      "Paul Robeson was an accomplished scholar, actor, singer, and international advocate for human rights. He attended Rutgers University and Columbia Law School. He acted on the stage, including his famous Broadway portrayal of Othello, and in several films, including Oscar Micheaux’s Body and Soul. His rendition of Ol’ Man River became a battle cry for workmen’s rights all over the world.\n\nRobeson was fluent in Russian and spent much time in Russia, where his name is widely recognized and praised. He was targeted by the House Un-American Activities under Joseph McCarthy, and his passport was revoked in 1950. When it was reinstated in 1958, Robeson continued his travels, lecturing and singing all over the world for peace and justice. He vowed, “I shall take my voice wherever there are those who want to hear the melody of freedom or the words that might inspire hope and courage in the face of fear. My weapons are peaceful. For it is only by peace that peace can be attained. The song of freedom must prevail.” Robeson died in 1976.",
                      presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    child:
                        AudioViewer(url: "assets/audio/forging_freedom_25.mp3"),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.play_circle_outline,
                      color: Colors.black,
                    ),
                    iconSize: 80,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => VideoView(
                                    videoLink:
                                        "assets/videos/forging_freedom_25.MP4",
                                  )));
                    },
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
