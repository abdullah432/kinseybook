import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
          height: ht,
          width: wt,
          decoration: template.viewportBorder(),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: template.textContainerheight(),
                width: template.textContainerwidth(),
                child: Padding(
                  padding: template.textcontainerPadding(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: (ht / 300) * 75,
                      ),
                      Text(
                        "The Boss, 2006",
                        style: template.textheaderStyle(),
                      ),
                      Text(
                        "Bisa Butler",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "Quilted cotton, appliqué",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "34 x 21 in.",
                        style: template.bodyTextStyle(),
                      ),
                      SizedBox(
                        height: template.viewHeight() * 20,
                      ),
                      GestureDetector(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return Dialog(
                                  child: Container(
                                    height:
                                        MediaQuery.of(context).size.height / 2,
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Stack(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 20),
                                          child: Column(
                                            children: <Widget>[
                                              SizedBox(
                                                height: 30,
                                              ),
                                              Text(
                                                "Bisa Butler (1975– )",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 24,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              Expanded(
                                                  child: AutoSizeText(
                                                "Butler, born in Orange, New Jersey, graduated from Howard University, where she studied the work of Howard professors and students, including Lois Mailou Jones and Elizabeth Catlett. While working on her MFA, Butler began to merge her love of fabric with artistic expression, creating vibrant quilts that depict black life. She notes, “With fiber art, I feel that I have finally tapped into a way to communicate emotion, art, heritage, tradition, and beauty to those who view my work.” Recently, a Butler quilt of voters in line at the polls was featured in a special exhibition at the Washington Historical Society in honor of then President- elect Barack Obama.",
                                                presetFontSizes: [
                                                  22,
                                                  21,
                                                  20,
                                                  19,
                                                  18,
                                                  17,
                                                  16,
                                                  14
                                                ],
                                                textAlign: TextAlign.justify,
                                              ))
                                            ],
                                          ),
                                        ),
                                        Positioned(
                                          child: IconButton(
                                            icon: Icon(
                                              Icons.clear,
                                              color: Colors.black,
                                              size: 30,
                                            ),
                                            onPressed: () =>
                                                Navigator.pop(context),
                                          ),
                                          top: 10,
                                          right: 10,
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              });
                        },
                        child: template.bioImg(),
                      ),
                      SizedBox(
                        height: (ht / 300) * 120,
                      ),
                      Container(
                          height: template.footerTextContainerHihght(),
                          width: template.textContainerwidth() * 3,
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: template.viewHeight() * 10),
                              child: template.footerLeftText())),
                    ],
                  ),
                ),
              ),
              SizedBox(width: template.viewWidth() * 15),
              Container(
                height: template.viewHeight() * 270,
                width: template.viewWidth() * 230,
                child: Padding(
                  padding: EdgeInsets.only(
                      top: template.viewHeight() * 10,
                      right: template.viewWidth() * 40),
                  child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GalleryView(
                                      url:
                                          "assets/images/forging_freedom_2.jpg",
                                    )));
                      },
                      child: Image.asset(
                        "assets/images/forging_freedom_2.jpg",
                        fit: BoxFit.contain,
                      )),
                ),
              )
            ],
          )),
    );
  }
}
