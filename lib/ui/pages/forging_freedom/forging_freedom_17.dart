import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomSeventeen extends StatefulWidget {
  @override
  _ForgingFreedomSeventeenState createState() =>
      _ForgingFreedomSeventeenState();
}

class _ForgingFreedomSeventeenState extends State<ForgingFreedomSeventeen> {
  int _current = 0;

  List<String> _imagesList = [
    "assets/images/forging_freedom_17_1.jpg",
    "assets/images/forging_freedom_17_2.jpg",
    "assets/images/forging_freedom_17_3.jpg",
    "assets/images/forging_freedom_17_4.jpg",
    "assets/images/forging_freedom_17_5.jpg",
    "assets/images/forging_freedom_17_6.jpg",
  ];

  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
                flex: 3,
                child: Stack(children: [
                  Center(
                    child: CarouselSlider(
                      items: _imagesList.map((i) {
                        return Builder(
                          builder: (BuildContext context) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => GalleryView(
                                              url: i,
                                            )));
                              },
                              child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.6,
                                  height:
                                      MediaQuery.of(context).size.height * 0.75,
                                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                                  child: FittedBox(
                                    fit: BoxFit.contain,
                                    child: Image.asset(i),
                                  )),
                            );
                          },
                        );
                      }).toList(),
                      onPageChanged: (index) {
                        setState(() {
                          _current = index;
                        });
                      },
                      viewportFraction: 1.0,
//                      aspectRatio: 16/9,
                      height: MediaQuery.of(context).size.height * 0.8,
                      enableInfiniteScroll: false,
                    ),
                  ),
                  Positioned(
                      bottom: 40,
                      left: 0.0,
                      right: 0.0,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.1),
                        child: Container(
                          height: 40,
                          color: Colors.black54,
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: _imagesList.map((f) {
                                var index = _imagesList.indexOf(f);
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    height: index == _current ? 14 : 10,
                                    width: index == _current ? 14 : 10,
                                    decoration: BoxDecoration(
                                        color: index == _current
                                            ? Colors.white
                                            : Colors.white30,
                                        shape: BoxShape.circle),
                                  ),
                                );
                              }).toList()),
                        ),
                      ))
                ])),
            Expanded(
                flex: 1,
                child: Padding(
                  padding: EdgeInsets.only(
                      right: MediaQuery.of(context).size.width * 0.15,
                      left: MediaQuery.of(context).size.width * 0.15,
                      bottom: 20),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AutoSizeText(
                            "Featured in the center is a portrait postcard of Lieutenant Benedict Mosley wearing the French Croix de Guerre Medal for the Battle of Argonne, which he and his fellow “Harlem Hellfighters” of the 9th Calvary 369th Infantry Regiment were awarded by the French Military.",
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AutoSizeText(
                            "The remaining four portrait postcards are unidentified members of the more than 350,000 African American soldiers and sailors who served in highly segregated units, mostly as support troops, during World War I. Lower right is a rare image of an African American woman Navy Corporal.",
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
