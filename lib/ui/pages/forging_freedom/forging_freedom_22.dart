import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwentyTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                flex: 3,
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_22.jpg",
                                )));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child:
                          Image.asset("assets/images/forging_freedom_22.jpg"),
                    ),
                  ),
                ),
              ),
              Expanded(
                  flex: 2,
                  child: Padding(
                    padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.05,
                        left: MediaQuery.of(context).size.width * 0.05,
                        bottom: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      AutoSizeText(
                                        "Panoramic photograph of the 39th Annual\nConference of the NAACP, 1938",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        maxLines: 2,
                                        presetFontSizes: [
                                          22,
                                          21,
                                          20,
                                          19,
                                          18,
                                          17,
                                          16,
                                          14,
                                          12,
                                          11
                                        ],
                                        textAlign: TextAlign.center,
                                      ),
                                      AutoSizeText(
                                        "Maker unknown",
                                        presetFontSizes: [24, 22, 20, 18],
                                        maxLines: 1,
                                      ),
                                      AutoSizeText(
                                        "Silver print",
                                        presetFontSizes: [24, 22, 20, 18],
                                        maxLines: 1,
                                      ),
                                      AutoSizeText(
                                        "8 x 29 1⁄2 in.",
                                        presetFontSizes: [24, 22, 20, 18],
                                        maxLines: 1,
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                    ],
                                  ),
                                ],
                              )),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                    width: 100,
                                    height: 50,
                                    child: Stack(
                                      children: <Widget>[
                                        Positioned(
                                          child: GestureDetector(
                                              child: Image.asset(
                                            "assets/images/magnify.png",
                                            scale: 5,
                                          )),
                                          left: -30,
                                        ),
                                      ],
                                      overflow: Overflow.visible,
                                    )),
                                Expanded(
                                    child: AutoSizeText(
                                  "Seated in the center of this group photograph is Walter White, then National Secretary of the NAACP. To the right are Assistant National Secretary Roy Wilkins, and Thurgood Marshall, then NAACP special counsel. He was appointed to this position in 1938. Individuals seated include NAACP staff: Juanita Jackson, Dorothy Lampkin, Charles Hamilton Houston, James McClendon, E. F. Morrow, and William Pickens.\n\nFounded on February 12, 1909, the centennial of Abraham Lincoln’s birth, the NAACP is",
                                  presetFontSizes: [
                                    22,
                                    21,
                                    20,
                                    19,
                                    18,
                                    17,
                                    16,
                                    14
                                  ],
                                  textAlign: TextAlign.justify,
                                )),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: AutoSizeText(
                              "the nation’s oldest, largest, and most widely recognized grassroots civil rights organization. Its more than half-million members and supporters throughout the United States and the world are the premier advocates for civil rights in their communities, campaigning for equal opportunity and conducting voter mobilization. The NAACP was formed partly in response to the horrific practice of lynching and the 1908 race riot in Springfield, the capital of Illinois and the resting place of Abraham Lincoln. Appalled at the violence that was committed against blacks, a",
                              presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                              textAlign: TextAlign.justify,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
