import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomOne extends StatefulWidget {
  @override
  _ForgingFreedomOneState createState() => _ForgingFreedomOneState();
}

class _ForgingFreedomOneState extends State<ForgingFreedomOne> {
  final textFiles = [
    "\n\nBernard and Shirley Kinsey came of age during the fight for civil rights, one of the most powerful movements in modern history. Both were actively engaged in that cause. Shirley was arrested and jailed for participating in a demonstration at Florida A&M University, and she met Bernard soon after her release. The right to citizenship, which inspired the struggles of the civil rights movement, informs their collecting efforts.\n\nFrederick Douglass, Benjamin Banneker, and Carrie Kinsey, whether famous or virtually unknown, all contributed to the cause of social justice and fought for the rights and agency of those who could not speak for themselves. The Kinseys continue to assemble artifacts that tell the story of forging freedom, beginning with Reconstruction.\n\nReconstruction saw the end of slavery, the temporary equalization of black and white men, and the establishment of provisions for reparations to freed slaves. During this time, many African Americans were elected to public office, several serving in the United States Congress, including Hiram Rhoades Revels, the first African American senator, representing Mississippi in the 41st and 42nd Congress. Bernard Kinsey has",
    "collected the images and signatures of these men, who are often forgotten or even parodied in modern references to this era.\n\nAfrican Americans faced formidable challenges at this time, including the dissolution of the principles of Reconstruction and the emergence of “Black Codes,” which had their roots in the Civil War–era slave codes. In the 1870s, large groups of lawless whites terrorized blacks in the South. Toward the end of the decade, white militia groups carried out open violence against blacks. Groups such as the White League, the Ku Klux Klan, and the Red Shirts formed to provide paramilitary support for the Southern Democratic Party. These groups helped secure the near total disenfranchisement of black voters through poll taxes, literacy tests, and grandfather clauses, as well as extreme acts of violence.\n\nThere are over two hundred documented incidents of white-on-black persecution during this era. Some blacks were literally burned out of their homes. Following these incidents, the refugees sought safe haven in other places and recounted the hateful crimes committed against them in powerful oratory. One of the best documented incidents occurred in Rosewood, Florida, in 1923. Victims of that atrocity fled to nearby Fort White and Gainesville. Their oral history was among Bernard Kinsey’s earliest childhood memories, as recounted by his father, U. B. Kinsey, who remembered their arrival. His passing down of this story profoundly influenced Bernard’s commitment to the reconstruction of",
    "the past through tactile artifacts.\n\nIn the early 20th century, whites perpetuated discriminatory practices through the passage of Jim Crow laws, which imposed public segregation, essentially restoring the Black Codes. Black men and women lived with a daily bombardment of racial hatred and discrimination, yet contested their return to second-class citizenship. Every day throughout the South, individuals resisted violence and oppression.\n\nThe Kinseys honor the legacy of these 20th- century citizens, who paved the road to freedom. Along the way, some of these anonymous voices became louder than the rest, more widely recognized and more respected. The image of Paul Robeson, for example, looms large in this presentation of legacy builders, together with Malcolm X and Martin Luther King, Jr., Robeson—actor, athlete, singer, writer, Russian scholar, and resister—is one of the best-known African Americans in the world. These very well known individuals worked for something beyond their own achievements. The collection includes artists, writers, orators, and soldiers who worked for the benefit of the African American community at large, establishing the path down which the Kinseys and many others would follow.\n\nFor the Kinseys, the power of Bisa Butler’s image lies in its representation of the four million African Americans who toiled on 75,000 cotton plantations, generating billions of dollars from their free labor."
  ];
  static bool listView = false;
  ScrollController listViewController = ScrollController();
  static double fontSize = 50;
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: listView
            ? Stack(
                children: <Widget>[
                  ListView.builder(
                      controller: listViewController,
                      itemCount: textFiles.length,
                      itemBuilder: (BuildContext context, index) {
                        if (index == 0) {
                          return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  AutoSizeText.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                            text: "Forging Freedom\n\n",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 24)),
                                        TextSpan(
                                            text:
                                                "That Justice is a blind goddess\nIs a thing to which we blacks are wise:\nHer bandage hides two festering sores\nThat once perhaps were eyes.")
                                      ],
                                    ),
                                    presetFontSizes: [fontSize],
                                    textAlign: TextAlign.justify,
                                  ),
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: AutoSizeText(
                                      "— Langston Hughes",
                                      presetFontSizes: [fontSize],
                                    ),
                                  ),
                                  AutoSizeText(
                                    textFiles[0],
                                    presetFontSizes: [fontSize],
                                    textAlign: TextAlign.justify,
                                  )
                                ],
                              ));
                        }
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          child: AutoSizeText(textFiles[index],
                              minFontSize: fontSize,
                              textAlign: TextAlign.justify),
                        );
                      }),
                  Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        color: Colors.white,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            IconButton(
                              onPressed: () {
                                if (listViewController
                                        .position.minScrollExtent ==
                                    listViewController.offset) {
                                  print("on the end");
                                } else {
                                  listViewController.animateTo(
                                      listViewController.offset -
                                          listViewController
                                                  .position.maxScrollExtent /
                                              7,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.easeIn);
                                }
                              },
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.blue,
                                size: 40,
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                IconButton(
                                    icon: Icon(
                                      Icons.zoom_out,
                                      color: Colors.blue,
                                      size: 40,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        fontSize = 30;
                                      });
                                    }),
                                IconButton(
                                    icon: Icon(
                                      Icons.close,
                                      color: Colors.blue,
                                      size: 40,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        listView = false;
                                      });
                                    })
                              ],
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.blue,
                                size: 40,
                              ),
                              onPressed: () {
                                if (listViewController
                                        .position.maxScrollExtent ==
                                    listViewController.offset) {
                                  print("on the end");
                                } else {
                                  listViewController.animateTo(
                                      listViewController.offset +
                                          listViewController
                                                  .position.maxScrollExtent /
                                              7,
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.easeIn);
                                }
                              },
                            )
                          ],
                        ),
                      ))
                ],
              )
            : Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: 30,
                          ),
                          Container(
                              width: 100,
                              height: 50,
                              child: Stack(
                                children: <Widget>[
                                  Positioned(
                                    child: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            fontSize = 50;
                                            listView = true;
                                          });
                                        },
                                        child: Image.asset(
                                          "assets/images/magnify.png",
                                          scale: 4,
                                        )),
                                    left: -30,
                                  ),
                                ],
                                overflow: Overflow.visible,
                              )),
                          AutoSizeText.rich(
                            TextSpan(
                              children: [
                                TextSpan(
                                    text: "Forging Freedom\n\n",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 24)),
                                TextSpan(
                                    text:
                                        "That Justice is a blind goddess\nIs a thing to which we blacks are wise:\nHer bandage hides two festering sores\nThat once perhaps were eyes.")
                              ],
                            ),
                            textAlign: TextAlign.justify,
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: AutoSizeText(
                              "— Langston Hughes",
                              presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            ),
                          ),
                          Flexible(
                              child: AutoSizeText(
                            textFiles[0],
                            presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                            textAlign: TextAlign.justify,
                          ))
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AutoSizeText(
                      textFiles[1],
                      presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                      textAlign: TextAlign.justify,
                    ),
                  )),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 30, horizontal: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: AutoSizeText(
                          textFiles[2],
                          presetFontSizes: [22, 21, 20, 19, 18, 17, 16, 14],
                          textAlign: TextAlign.justify,
                        )),
                      ],
                    ),
                  ))
                ],
              ),
      ),
    );
  }
}
