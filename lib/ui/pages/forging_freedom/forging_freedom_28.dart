import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ForgingFreedomTwentyEight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    AutoSizeText(
                      "Brown et al vs. Board of Education of Topeka\net al, 1954",
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      presetFontSizes: [24, 22, 20],
                      textAlign: TextAlign.center,
                    ),
                    AutoSizeText(
                      "Typed on paper",
                      presetFontSizes: [22, 20],
                    ),
                    AutoSizeText(
                      "8 1⁄2 x 11 in.",
                      presetFontSizes: [22, 20],
                    ),
                    Flexible(
                      child: AutoSizeText(
                        "Brown v. Board of Education was the landmark case that overturned Plessy v. Ferguson, which in 1896 had upheld the “separate but equal” principle with respect to the right to education. The decision in Brown was bolstered by several national precedents that chipped away at the constitutionality of Plessy. Bernard Kinsey’s father, U. B. Kinsey, was involved in two of those early cases, one of them in 1941, when the Palm Beach County Teachers Association successfully sued the local board of education for equal pay. Thurgood Marshall argued the case.\n\nBrown v. Board of Education, also argued by Thurgood Marshall, was unanimously approved by the U.S. Supreme Court, Shown is the decision letter, signed by the Supreme Court Justices, and the complete decision, a bound volume of 235 pages.",
                        presetFontSizes: [24, 20, 18],
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 10),
                      child: AudioViewer(
                          url: "assets/audio/forging_freedom_28.mp3"),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/forging_freedom_28.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/forging_freedom_28.jpg",
                    fit: BoxFit.fitHeight,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
