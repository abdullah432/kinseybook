import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class AmericanWarTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: (wt / 390) * 7,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Revolutionary Soldier, 1994",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Ed Dwight",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Bronze",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "15 1⁄2 x 12 1⁄2 x 6 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    flex: 1,
                    child: Text(
                      "\nCrispus Attucks is remembered as the first American to die in the Boston Massacre of 1770. It is thought that Attucks was an escaped slave of African and Native American descent. In John Adams’ defense of the British soldiers who committed the massacre, he described the crowd as “a motley rabble of saucy boys, negroes and molattoes, Irish teagues and outlandish jack tarrs.” Despite negative categorizations and daily discrimination, African Americans’ desire to find freedom led them to fight side-by-side with those who would further oppress them. Thousands of African Americans died in the battles of the Revolution.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height / 2,
                                width: MediaQuery.of(context).size.width / 2,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                            "Ed Dwight (1933– )",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Expanded(
                                              child: AutoSizeText(
                                            "Dwight’s first major commission was to create a sculpture of George Brown, Colorado’s first black Lt. Governor. For the Colorado Centennial Commission, Dwight was asked to produce 30 bronzes depicting the contributions of African Americans to the expansion of the West. In the series “Jazz: An American Art Form,” Dwight explored the musical contributions of jazz to the fabric of American life and culture. Dwight has been involved with many public art projects, including monuments in Detroit and in Windsor, Canada, dedicated to the Underground Railroad. His largest memorial work, honoring Martin Luther King, Jr., is installed in Denver. His autobiography, Soaring on the Wings of a Dream, was recently published by Third World Press.",
                                            presetFontSizes: [
                                              22,
                                              21,
                                              20,
                                              19,
                                              18,
                                              17,
                                              16,
                                              14
                                            ],
                                            textAlign: TextAlign.justify,
                                          ))
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.clear,
                                          color: Colors.black,
                                          size: 30,
                                        ),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                      top: 10,
                                      right: 10,
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                    child: template.bioImg(),
                  ),
                  SizedBox(
                    height: (ht / 299) * 90,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth() * 3,
                      child: template.footerLeftText()),
                ],
              ),
            ),
          ),
          Container(
              height: template.viewHeight() * 270,
              width: template.viewWidth() * 230,
              child: Padding(
                padding: EdgeInsets.only(top: template.viewHeight() * 10),
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/americanwar2.jpg",
                                  )));
                    },
                    child: Image.asset(
                      "assets/images/americanwar2.jpg",
                      fit: BoxFit.contain,
                    )),
              )),
        ],
      ),
    ));
  }
}
