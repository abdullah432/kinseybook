import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class AmericanWarOne extends StatefulWidget {
  @override
  _AmericanWarOneState createState() => _AmericanWarOneState();
}

class _AmericanWarOneState extends State<AmericanWarOne> {
  final textFiles=["\n\nAfrican Americans are no strangers to war. Black men have voluntarily fought in every major battle on American soil, primarily for the cause of American liberty, despite the continuing disenfranchisement of black people.\n\nThe idea that black people would give the ultimate sacrifice for freedom to a country that did not recognize them as human beings fascinates Bernard Kinsey. It both perplexes him and resonates with his own mission to inspire and educate, in the face of the contradictory rhetoric and discriminatory practices affecting so many people of color.\n\nSo often, the altruistic acts of bravery demonstrated by black men and women, particularly during times of war, are omitted from the annals of history. This became a powerful motivation for the Kinseys to collect examples of these moments of courage and selflessness. Of",
    "special interest is black participation in the Civil War, when the United States was faced with a cultural crisis, with black people at its center, that tore the nation apart.\n\nFor over half a century before the Civil War, slavery was a highly contested, morally problematic issue for America. A country founded on freedom grew prosperous on the back of a brutal labor system that considered blacks only slightly above livestock. While many opposed slavery in theory, the practice was deeply entrenched as an American prerogative, exercised by ten of the first fifteen presidents as well as land owners who had a financial stake in the national and global commercial marketplace.\n\nAlthough Europe had instituted the trans-atlantic slave trade to expand its global power and reap riches from abroad, it struggled to end the practice in the name of morality. In 1807, the British Parliament passed “An Act for the Abolition of the Slave Trade,” which decisively ceased legal slave trading in western Europe, and in January 1808, a similar measure was to take effect in the United States. Slavery was not completely abolished in England and all of her colonies, however, until 1833, and it took until 1860 to convince African kings and European pirates and raiders to stop the now illegal slave trade. It took much more to end slavery in the United States.",
    "Only an act of war could convince white slave owners to release their claim of property on millions of African Americans in bondage. The first act of rebellion was the declared secession of seven Southern states in 1860, before Abraham Lincoln became president. In 1862, Lincoln signed the Emancipation Proclamation, declaring an official end to slavery and forcing England, which initially supported the South, to step aside. Lincoln called on the Union states to amass a volunteer army, and free black men enlisted in large numbers to support the liberation of their brethren in the South. Black slaves faced double jeopardy, as many plantation owners vowed to kill their slaves before seeing them free. Many slaves fled and joined the ranks of the Northern free men fighting for the Union. Over 180,000 African American soldiers fought in the Civil War. They earned lower wages and had to pay further monies for their accommodations, and they fought and died at a higher rate than their counterparts for the ideal of freedom. Twenty- five African Americans were awarded the Medal of Honor for their service during the Civil War."
  ];
  static bool listView=false;
  ScrollController listViewController=ScrollController();
  static double fontSize=50;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
        child: listView?Stack(
          children: <Widget>[
            ListView.builder(
                controller: listViewController,
                itemCount: textFiles.length,
                itemBuilder: (BuildContext context,index){
                  if(index==0){
                    return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                        child: Column(
                          children: <Widget>[
                            AutoSizeText.rich(
                              TextSpan(
                                children: [
                                  TextSpan(text: "African Americans and War\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                                  TextSpan(text: "I never saw such fighting as was done by the Negro regiment. . .The question that negroes will fight is settled; besides they make better solders in every respect than any troops I have ever had under my command.\n",),
                                ],

                              ),
                              presetFontSizes: [fontSize],
                              textAlign: TextAlign.justify,
                            ),
                            Align(
                              alignment: Alignment.centerRight,
                              child: AutoSizeText("— General James Blunt",presetFontSizes: [fontSize],),
                            ),
                            AutoSizeText(textFiles[0],presetFontSizes: [fontSize],textAlign: TextAlign.justify,)

                          ],
                        )
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: AutoSizeText(textFiles[index],minFontSize: fontSize,textAlign: TextAlign.justify),
                  );
                }),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height*0.1,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(onPressed: (){

                        if(listViewController.position.minScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset-listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },icon: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 40,),),
                      Row(children: <Widget>[
                        IconButton(icon: Icon(Icons.zoom_out,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            fontSize=30;
                          });
                        }),
                        IconButton(icon: Icon(Icons.close,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            listView=false;
                          });
                        })
                      ],),
                      IconButton(icon: Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 40,),onPressed: (){
                        if(listViewController.position.maxScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },)
                    ],),
                ))
          ],
        ):Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 30,),
                  Container(width:100,height: 50,child: Stack(children: <Widget>[Positioned(child: GestureDetector(
                      onTap: (){
                        setState(() {
                          fontSize=50;
                          listView=true;
                        });
                      },
                      child: Image.asset("assets/images/magnify.png",scale: 4,)),left: -30,),],overflow: Overflow.visible,)),
                  AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(text: "African Americans and War\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,)),
                        TextSpan(text: "I never saw such fighting as was done by the Negro regiment. . .The question that negroes will fight is settled; besides they make better solders in every respect than any troops I have ever had under my command.\n",),

                      ],

                    ),
                    textAlign: TextAlign.justify,
                    presetFontSizes: [22,21,20,19,18,17,16,14],
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: AutoSizeText("— General James Blunt",presetFontSizes: [22,21,20,19,18,17,16,14],),
                  ),
                  Flexible(child: AutoSizeText(textFiles[0],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,))
                ],
              ),
            ),),
            Expanded(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[1],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,),
            )),
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Expanded(
                      child: AutoSizeText(textFiles[2],presetFontSizes: [22,21,20,19,18,17,16,14],textAlign: TextAlign.justify,)),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
