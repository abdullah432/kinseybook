import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class AmericanWarNine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: template.viewWidth() * 10),
          Container(
            height: template.viewHeight() * 270,
            width: template.viewWidth() * 220,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight() * 10),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/americanwar9.jpg",
                              )));
                },
                child: Container(
                    height: template.viewHeight() * 270,
                    width: template.viewWidth() * 220,
                    child: FittedBox(
                        fit: BoxFit.fill,
                        child: Image.asset("assets/images/americanwar9.jpg",
                            fit: BoxFit.contain))),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: (ht / 300) * 118,
                ),
                Text(
                  "Union soldier, ca. 1864",
                  style: template.textheaderStyle(),
                  maxLines: 1,
                ),
                Text(
                  "Maker unknown",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "Hand-colored tintype",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "3 3⁄4 x 3 1⁄4 x 3⁄4 in.",
                  style: template.bodyTextStyle(),
                ),
                SizedBox(
                  height: (ht / 300) * 15,
                ),
                Container(
                  height: (ht / 300) * 25,
                  width: template.textContainerwidth(),
                  child: IconButton(
                    icon: Icon(
                      Icons.play_circle_outline,
                      color: Colors.black,
                    ),
                    iconSize: template.videoIconSize(),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => VideoView(
                                    videoLink: "assets/videos/americanwar9.MP4",
                                  )));
                    },
                  ),
                ),
                SizedBox(
                  height: (ht / 300) * 97,
                ),
                Container(
                    height: template.footerTextContainerHihght(),
                    width: template.textContainerwidth() * 2,
                    child: Padding(
                        padding: EdgeInsets.only(right: (wt / 390) * 9),
                        child: template.footerRightText()))
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
