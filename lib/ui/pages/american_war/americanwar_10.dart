import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class AmericanWarTen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            height: template.viewHeight() * 100,
            width: template.viewWidth() * 250,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight() * 10),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/americanwar10.jpg",
                              )));
                },
                child: Container(
                    height: template.viewHeight() * 100,
                    width: template.viewWidth() * 250,
                    child: FittedBox(
                        child: Image.asset("assets/images/americanwar10.jpg",
                            fit: BoxFit.contain))),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "\n9th Cavalry Buffalo Soldiers, Parade Flag, ca. 1889",
                  style: template.textheaderStyle(),
                  maxLines: 2,
                ),
                Text(
                  "Maker unknown",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "Fabric",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "22 1⁄2 x 32 1⁄2 in.",
                  style: template.bodyTextStyle(),
                ),
              ],
            ),
          ),
          Container(
            height: template.viewHeight() * 80,
            width: template.textContainerwidth() * 3,
            child: Row(
              children: <Widget>[
                SizedBox(width: template.viewWidth() * 40),
                Container(
                    height: template.viewHeight() * 80,
                    width: template.textContainerwidth(),
                    child: Padding(
                      padding: template.textcontainerPadding(),
                      child: Text(
                        "In 1866, General Phillip Sheridan was authorized to raise one regiment of “colored” cavalry, former Union soldiers, to be designated as the 9th Regiment. The “Buffalo Soldiers” served with distinction, from patrolling frontier towns acting as militia to fighting in direct combat.",
                        textAlign: TextAlign.justify,
                        style: template.bodyTextStyle(),
                      ),
                    )),
                Container(
                    height: template.viewHeight() * 80,
                    width: template.textContainerwidth(),
                    child: Padding(
                      padding: template.textcontainerPadding(),
                      child: Text(
                        "Six officers and thirteen enlisted men in the 9th Regiment earned the Medal of Honor during the Plains Indians Wars of the late 1860s. Another five medals were awarded during the Spanish American War, including recognition of valor at the battle of San Juan Hill.",
                        textAlign: TextAlign.justify,
                        style: template.bodyTextStyle(),
                      ),
                    )),
                Container(
                  height: template.viewHeight() * 20,
                  width: template.viewWidth() * 50,
                  child: IconButton(
                    icon: Icon(
                      Icons.play_circle_outline,
                      color: Colors.black,
                    ),
                    iconSize: template.videoIconSize(),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => VideoView(
                                    videoLink:
                                        "assets/videos/americanwar10.MP4",
                                  )));
                    },
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: (ht / 299) * 68,
          ),
          Container(
              height: template.footerTextContainerHihght(),
              width: template.textContainerwidth() * 3,
              child: template.footerLeftText()),
        ],
      ),
    ));
  }
}
