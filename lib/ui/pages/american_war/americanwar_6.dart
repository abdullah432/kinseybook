import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class AmericanWarSix extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: template.viewHeight() * 200,
            width: template.viewWidth() * 280,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight() * 10),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/americanwar6.jpg",
                              )));
                },
                child: Container(
                    height: template.viewHeight() * 270,
                    width: template.viewWidth() * 230,
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: Image.asset(
                          "assets/images/americanwar6.jpg",
                          fit: BoxFit.contain,
                        ))),
              ),
            ),
          ),
          Container(
            height: template.viewHeight() * 65,
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Presentation of Colors to the Twentieth\nUnited States Colored Infantry, March 5, 1864",
                    style: template.textheaderStyle(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "Harper’s Weekly",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Book plate etching",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "16 3⁄4 x 21 1⁄2 in.",
                    style: template.bodyTextStyle(),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: (ht / 299) * 22,
          ),
          Container(
              height: template.footerTextContainerHihght(),
              width: template.textContainerwidth() * 3,
              child: Padding(
                  padding: EdgeInsets.only(left: template.viewWidth() * 5),
                  child: template.footerLeftText())),
        ],
      ),
    ));
  }
}
