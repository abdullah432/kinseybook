import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class AmericanWarSeven extends StatefulWidget {
  _AmericanWarSeven createState() => _AmericanWarSeven();
}

class _AmericanWarSeven extends State<AmericanWarSeven> {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Column(
        children: <Widget>[
          SizedBox(height: template.viewHeight() * 15),
          Container(
            height: template.viewHeight() * 30,
            width: template.textContainerwidth() * 3,
            child: Padding(
              padding: EdgeInsets.only(
                  top: (ht / 300) * 10,
                  left: (wt / 390) * 2.5,
                  right: (wt / 390) * 2.5),
              child: Text(
                "Blacks who served in the military in this period were rarely officers, and few photographs remain to document\ntheir contributions. It is estimated that as many as 5,000 blacks fought for the Confederacy.",
                style: template.bodyTextStyle(),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            height: template.viewHeight() * 150,
            width: template.textContainerwidth() * 3,
            child: Padding(
                padding: EdgeInsets.only(
                    top: (ht / 300) * 5,
                    left: (wt / 390) * 2.5,
                    right: (wt / 390) * 2.5),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/americanwar7_1.jpg",
                                      )));
                        },
                        child: Image.asset("assets/images/americanwar7_1.jpg")),
                    Expanded(
                        flex: 1,
                        child: Padding(
                          padding: EdgeInsets.symmetric(vertical: ht / 120),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: (ht / 40) * 3.5,
                                      ),
                                      Text(
                                        "Civil War Black Corporal from Confederate\nNew Orleans, ca. 1860",
                                        style: template.textheaderStyle(),
                                        textAlign: TextAlign.center,
                                      ),
                                      Text("Carte de visite",
                                          style: template.bodyTextStyle()),
                                      Text("2 ½ x 4 in.",
                                          style: template.bodyTextStyle()),
                                    ],
                                  ),
                                  Spacer()
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Spacer(),
                                  Column(
                                    children: <Widget>[
                                      Text(
                                        "Portrait of African American, Private Nelson J.\nCampbell of the Sixth U. S. Colored Calvary,\nca. 1860",
                                        style: template.textheaderStyle(),
                                        textAlign: TextAlign.center,
                                      ),
                                      Text("HCarte de visite",
                                          style: template.bodyTextStyle()),
                                      Text("2 ½ x 4 in.",
                                          style: template.bodyTextStyle()),
                                    ],
                                  )
                                ],
                              )
                            ],
                          ),
                        )),
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/americanwar7_2.jpg",
                                      )));
                        },
                        child: Image.asset("assets/images/americanwar7_2.jpg"))
                  ],
                )),
          ),
          Container(
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: wt * 0.1, vertical: ht / 35),
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: ht / 32.0, horizontal: wt / 38),
                    child: Text(
                      "“According to the revised official data, of the slightly over two million troops in the United States Volunteers, over 316,000 died (from all causes), or 15.2%. Of the 67,000 Regular Army (white) troops, 8.6%, or not quite 6,000, died. Of the approximately 180,000 United States Colored Troops, however, over 36,000 died, or 20.5%.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  )),
                  Expanded(
                      child: Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: ht / 32.0, horizontal: wt / 38),
                    child: Text(
                      "In other words, the mortality rate amongst the United States Colored Troops in the Civil War was 35% greater than that among other troops, notwithstanding the fact that the former were not enrolled until some eighteen months after the fighting began” (Herbert Aptheker, Negro Casualties in the Civil War).",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ))
                ],
              ),
            ),
          ),
          SizedBox(
            height: (ht / 300) * 13,
          ),
          Container(
              height: template.footerTextContainerHihght(),
              width: template.textContainerwidth() * 3,
              child: Padding(
                  padding: EdgeInsets.only(right: template.viewWidth() * 5),
                  child: template.footerRightText()))
        ],
      ),
    ));
  }
}
