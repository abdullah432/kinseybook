import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class AmericanWarFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: template.viewHeight() * 15,
            ),
            Flexible(
                child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Flexible(
                    child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Flexible(
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url:
                                              "assets/images/americanwar4_1.jpg",
                                        )));
                          },
                          child:
                              Image.asset("assets/images/americanwar4_1.jpg")),
                    ),
                    SizedBox(
                      height: template.viewHeight() * 10,
                    ),
                    Text(
                      "16th Army Corps General\nOrders No. 45",
                      style: template.textheaderStyle(),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      "Major General S. A. Hurlbut",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "Printed paper",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "7 x 4 in.",
                      style: template.bodyTextStyle(),
                    ),
                  ],
                )),
                Flexible(
                    child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Flexible(
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url:
                                              "assets/images/americanwar4_2.jpg",
                                        )));
                          },
                          child:
                              Image.asset("assets/images/americanwar4_2.jpg")),
                    ),
                    SizedBox(
                      height: template.viewHeight() * 10,
                    ),
                    Text(
                      "16th Army Corps General\nOrders No. 173",
                      style: template.textheaderStyle(),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      "Major General S. A. Hurlbut",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "Printed paper",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "7 x 4 in.",
                      style: template.bodyTextStyle(),
                    ),
                  ],
                )),
                Flexible(
                    child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Flexible(
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url:
                                              "assets/images/americanwar4_3.jpg",
                                        )));
                          },
                          child:
                              Image.asset("assets/images/americanwar4_3.jpg")),
                    ),
                    SizedBox(
                      height: template.viewHeight() * 10,
                    ),
                    Text(
                      "16th Army Corps General\nOrders No. 178",
                      style: template.textheaderStyle(),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      "Major General S. A. Hurlbut",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "Printed paper",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "7 x 4 in.",
                      style: template.bodyTextStyle(),
                    ),
                  ],
                ))
              ],
            )),
            Container(
              height: template.viewHeight() * 90,
              width: template.textContainerwidth(),
              child: Padding(
                padding: template.textcontainerPadding(),
                child: Text(
                  "The decision to allow black participation in the Union Army was a contentious one. Following the establishment of the Bureau of Colored Troops, general orders were issued detailing the recruitment, formation, and commendation of colored regiments under white commissioned officers.",
                  textAlign: TextAlign.justify,
                  style: template.bodyTextStyle(),
                ),
              ),
            ),
            Container(
                height: template.footerTextContainerHihght(),
                width: template.textContainerwidth() * 3,
                child: template.footerLeftText()),
            SizedBox(
              height: (ht / 299),
            ),
          ],
        ),
      ),
    );
  }
}
