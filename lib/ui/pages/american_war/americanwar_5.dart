import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class AmericanWarFive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 15,
          ),
          Container(
            height: template.viewHeight() * 280,
            width: template.viewWidth() * 240,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight() * 10),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/americanwar5.jpg",
                              )));
                },
                child: Image.asset(
                  "assets/images/americanwar5.jpg",
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          SizedBox(
            width: template.viewWidth() * 5,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 50,
                  ),
                  Text(
                    "United States Soldiers at Camp William Penn,\n1863",
                    style: template.textheaderStyle(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "Supervisory Committee for Recruiting",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Colored Regimen",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Colored Regimen",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "22 1⁄2 x 25 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Container(
                    height: template.viewHeight() * 130,
                    width: template.textContainerwidth(),
                    child: Text(
                      "Blacks were banned from military combat until late 1862, despite pleas and petitions demanding inclusion in the war effort. Once the ban was lifted, black leaders including Frederick Douglass encouraged blacks to join the fight. The Bureau of Colored Troops was formed to recruit and register black volunteers for the Union Army.\n\nRoughly 180,000 blacks served as Union soldiers during the Civil War. Following the Emancipation Proclamation, black troops were allowed to fight as Union soldiers. This is one of the first recruiting posters used to promote black service.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 40,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
