import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class AmericanWarThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 17,
          ),
          Container(
            height: template.viewHeight() * 270,
            width: template.viewWidth() * 230,
            child: Padding(
              padding: EdgeInsets.only(top: template.viewHeight() * 5),
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/americanwar3.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/americanwar3.jpg",
                    fit: BoxFit.contain,
                  )),
            ),
          ),
          SizedBox(
            width: template.viewWidth() * 15,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Emancipation Proclamation, 1862",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Abraham Lincoln",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Printed paper",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "7 x 4 3⁄4 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Container(
                    width: template.textContainerwidth(),
                    height: template.viewHeight() * 155,
                    child: Text(
                      "Lincoln issued two Emancipation Proclamations in 1862–63; the copy shown here is of the first. Both were executive orders, issued first to the military, who were to enforce them. The first proclamation, issued September 22, granted freedom to all slaves in the Confederacy; if the Southern states returned to the Union by January 1 of the following year, however, it was not to take effect. The second proclamation, issued January 1, named the ten seceding states in which slaves were now to be freed (with exceptions too detailed to list here).\n\nNeither measure abolished slavery in the North or in border states that had not seceded, such as Missouri, Kentucky, Maryland, or Delaware. Only about 20,000 slaves were freed in 1863, in areas of the South controlled by Union soldiers. Slavery was made illegal throughout the United States when the 13th Amendment to the Constitution was passed in 1865. About four million slaves were ultimately freed.",
                      textAlign: TextAlign.justify,
                      style: template.bodyTextStyle(),
                    ),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 5,
                  ),
                  Container(
                      height: (ht / 300) * 15,
                      width: template.textContainerwidth(),
                      child: FittedBox(
                        child:
                            AudioViewer(url: "assets/audio/americanwar3.mp3"),
                        fit: BoxFit.contain,
                      )),
                  SizedBox(
                    height: template.viewHeight() * 5,
                  ),
                  Container(
                    height: (ht / 300) * 15,
                    width: template.textContainerwidth(),
                    child: IconButton(
                      icon: Icon(
                        Icons.play_circle_outline,
                        color: Colors.black,
                      ),
                      iconSize: template.videoIconSize(),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VideoView(
                                      videoLink:
                                          "assets/videos/americanwar3.MP4",
                                    )));
                      },
                    ),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 43,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
