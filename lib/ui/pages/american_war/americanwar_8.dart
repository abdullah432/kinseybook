import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class AmericanWarEight extends StatefulWidget {
  @override
  _AmericanWarEightState createState() => _AmericanWarEightState();
}

class _AmericanWarEightState extends State<AmericanWarEight> {
  final textFiles = [
    "",
    "“Things as they are . . .” A letter written by a Union soldier reports the murder of four slaves by their master. The soldier writes to his father:\n\n“On the 3rd day of this month [November] we of the fourth Division took up a line of march for some point south of this place [Bolivar, Tenn.] supposed to be Holly Springs. We moved about eight miles in the morning. Marched some ten or twelve miles that day and camped for the night being deprived of only six teams to the Regt, we were forced to leave the most of our camp equipage behind. Also knapsacks carrying our blankets across our shoulders, and haversack with three days rations which left us destitute of many of the necessities of a soldier. The weather being somewhat colder than we would have desired and like all other armies (when on like marches) as soon as we broke ranks some went to cooking while others started after Straw and something to eat such as sweet potatoes & fresh pork while others would go through the different plantations rob and plunder. Well, as I stated previously we had camped for the night and one of these squads had gone across the country to a plantation (within sight) after something for his supper. On entering the house he found it deserted but the stove was not cold yet so he being anxious to know all about the matter started for the Negro huts and what did he behold!",
    "The boddies [sic] of four Negroes, two male and two female the former were hung by the necks and the latter were beaten to death with a club. After a short visit he returned with his comrades to camp and reported the fact. Some Negroes were sent to bury them and on their way when they met another the fifth one and he the only survivor to tell the tale which was as follows: This Negro was cut across his throat until when he went to drink the water ran down the outside of his throat. I saw this with my own eyes and defy anyone to deny it. His testimony was that on the approach of our army his master . . . told the three Negro men & two women to run to the woods and not let the D----d Yankee cusses to see them, for if they did they would shoot them all, which they refused to do. And he took to the club, thinking to scare them by hitting one of the wenches which he done over the head. He carrying a Bowie knife like all others of his class. The three men piched [sic] at him to protect their wives and he struck the first, after which left and saw the overseer enter the house from the cotton gin. He saw no more of the fracas until he returned with some of our boys and there found his wife dead. Also the two others. The fact was made known and his house was burned to the ground and property all confiscated and [illegible].”"
  ];
  static bool listView = false;
  ScrollController listViewController = ScrollController();
  static double fontSize = 50;
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 10;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: template.viewWidth() * 5),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: template.viewHeight() * 200,
                    width: template.viewWidth() * 230,
                    child: Padding(
                        padding:
                            EdgeInsets.only(top: template.viewHeight() * 10),
                        child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => GalleryView(
                                            url:
                                                "assets/images/americanwar8.jpg",
                                          )));
                            },
                            child: Image.asset(
                              "assets/images/americanwar8.jpg",
                              fit: BoxFit.contain,
                            ))),
                  ),
                  Text(
                    "\nLetter recounting the murder of slaves, 1862",
                    style: template.textheaderStyle(),
                    maxLines: 2,
                  ),
                  Text(
                    "Union soldier",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Ink on paper",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "12 1⁄2 x 8 in.",
                    style: template.bodyTextStyle(),
                  ),
                  SizedBox(height: template.viewHeight() * 28),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth() * 3,
                      child: template.footerLeftText()),
                ],
              ),
            ),
          ),
          Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Padding(
                padding: EdgeInsets.only(
                    top: (ht / 300) * 33,
                    left: (wt / 390) * 2.5,
                    right: (wt / 390) * 2.5),
                child: Text(
                  textFiles[1],
                  textAlign: TextAlign.justify,
                  style: template.bodyTextStyle(),
                ),
              )),
          Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Padding(
                padding: EdgeInsets.only(
                    top: (ht / 300) * 33,
                    left: (wt / 390) * 2.5,
                    right: (wt / 390) * 2.5),
                child: Text(
                  textFiles[2],
                  textAlign: TextAlign.justify,
                  style: template.bodyTextStyle(),
                ),
              ))
        ],
      ),
    ));
  }
}
