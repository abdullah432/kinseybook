import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
// image on right, text on left.
class AfricanAmericanWomenInTheArts04 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Mr. Bernard Kinsey, 2004",
      "Buena Johnson",
      "Watercolor, pencil, collage",
      "20 x 16 in.",
      "Buena Johnson (1954 – )",
      "Johnson received her BFA in Communications Design from the Pratt Institute of Art in New York. She is a trained illustrator, teacher, photographer and fine artist. Inspired by the intricate line drawings of German Renaissance artist Albrecht Durer, Johnson draws with a photographic realism style that captures minute detail to intensify reality. Using symbolic imagery, her aim is “to present a message that will uplift, encourage and inspire the spirit of the viewer”. Johnson worked for several years in advertising and editorial work for major corporations, book publishers, magazines, and newspapers. She has lectured, done art demonstrations, and exhibited throughout the United States and Japan, and currentlyresides in Southern California. This painting was commissioned by the Torrance Chamber of Commerce as a gift to Mr. Kinsey for his outstanding community service in 2004.",
    ];
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
           Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AutoSizeText(
                  textFiles[0],
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: "MyriadPro-BoldIt"),
                  presetFontSizes: presetFonts,
                ),
                AutoSizeText(
                  textFiles[1],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[2],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[3],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                SizedBox(
                  height: 2,
                ),
                GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height / 2,
                              width: MediaQuery.of(context).size.width / 2,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        AutoSizeText(
                                          textFiles[4],
                                          presetFontSizes: presetFonts,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontFamily: "Auto2-Regular",
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Expanded(
                                            child: AutoSizeText(
                                          textFiles[5],
                                          presetFontSizes: presetFonts,
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              fontFamily: "Auto2-Regular",
                                          ),
                                        ))
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.clear,
                                        color: Colors.black,
                                        size: 30,
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                    top: 10,
                                    right: 10,
                                  )
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  child: Container(
                      height: 50,
                      width: 50,
                      child: Image.asset(
                        "assets/images/bio.png",
                        scale: 3,
                      )),
                ), 
              ],
            ),
          ),
          Expanded(
            flex: 6,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 70),
              height: MediaQuery.of(context).size.height, 
              width: MediaQuery.of(context).size.width,
              child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image.asset("assets/images/afrecian_american_women_in_the_arts_04.jpg")),
            ),
          ),],
      ),
    );
  }
}
