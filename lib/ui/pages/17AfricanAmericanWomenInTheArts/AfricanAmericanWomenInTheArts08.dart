import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

// image on left, text on right
class AfricanAmericanWomenInTheArts08 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Jackie, 1974",
      "Elizabeth Catlett",
      "Lithograph",
      "34 x 27 in.",
      "Untitled, ca. 1980",
      "Elizabeth Catlett",
      "Bronze sculpture",
      "19 x 9 x 6 in.",
      "Elizabeth Catlett (1915– 2012)",
      "Catlett, one of the most famous and prolific women sculptors of the 20th century, was the first African American to earn an MFA from the University of Iowa. In 1940, her thesis project, a limestone sculpture entitled Mother and Child, won first prize in sculpture at the Negro Exposition in Chicago. She studied ceramics at the Chicago Institute of Art and lithography at the Art Students League. She received a Rosenwald Fellowship to study wood and ceramic sculpture in Mexico, where she befriended many muralists and worked with the People’s Graphic Arts Workshop. She married artist Francisco Mora and became a Mexican citizen. Her work is included in the permanent collections of Museum of Modern Art and the Metropolitan Museum of Art in New York, as well as the National Institute of Fine Arts in Mexico City and the National Museum in Prague.",
    ];
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 9,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 70),
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          child: FittedBox(
                              fit: BoxFit.contain,
                              child: Image.asset(
                                  "assets/images/afrecian_american_women_in_the_arts_09.jpg")),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            AutoSizeText(
                              textFiles[0],
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "MyriadPro-BoldIt"),
                              presetFontSizes: presetFonts,
                            ),
                            AutoSizeText(
                              textFiles[1],
                              presetFontSizes: presetFonts,
                              style: TextStyle(fontFamily: "Auto2-Regular"),
                            ),
                            AutoSizeText(
                              textFiles[2],
                              presetFontSizes: presetFonts,
                              style: TextStyle(fontFamily: "Auto2-Regular"),
                            ),
                            AutoSizeText(
                              textFiles[3],
                              presetFontSizes: presetFonts,
                              style: TextStyle(fontFamily: "Auto2-Regular"),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(),
                      Align(
                        alignment: Alignment.center,
                        child: GestureDetector(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return Dialog(
                                    child: Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              2,
                                      width:
                                          MediaQuery.of(context).size.width / 2,
                                      child: Stack(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 10, vertical: 20),
                                            child: Column(
                                              children: <Widget>[
                                                AutoSizeText(
                                                  textFiles[8],
                                                  presetFontSizes: presetFonts,
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          "Auto2-Regular",
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Expanded(
                                                    child: AutoSizeText(
                                                  textFiles[9],
                                                  presetFontSizes: presetFonts,
                                                  textAlign: TextAlign.justify,
                                                  style: TextStyle(
                                                    fontFamily: "Auto2-Regular",
                                                  ),
                                                ))
                                              ],
                                            ),
                                          ),
                                          Positioned(
                                            child: IconButton(
                                              icon: Icon(
                                                Icons.clear,
                                                color: Colors.black,
                                                size: 30,
                                              ),
                                              onPressed: () =>
                                                  Navigator.pop(context),
                                            ),
                                            top: 10,
                                            right: 10,
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                });
                          },
                          child: Container(
                              height: 50,
                              width: 50,
                              child: Image.asset(
                                "assets/images/bio.png",
                                scale: 3,
                              )),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Column(
                          children: <Widget>[
                            AutoSizeText(
                              textFiles[4],
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "MyriadPro-BoldIt"),
                              presetFontSizes: presetFonts,
                            ),
                            AutoSizeText(
                              textFiles[5],
                              presetFontSizes: presetFonts,
                              style: TextStyle(fontFamily: "Auto2-Regular"),
                            ),
                            AutoSizeText(
                              textFiles[6],
                              presetFontSizes: presetFonts,
                              style: TextStyle(fontFamily: "Auto2-Regular"),
                            ),
                            AutoSizeText(
                              textFiles[7],
                              presetFontSizes: presetFonts,
                              style: TextStyle(fontFamily: "Auto2-Regular"),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 70),
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: Image.asset(
                            "assets/images/afrecian_american_women_in_the_arts_10.jpg")),
                  ),
                )
              ],
            ),
          ),
          AutoSizeText(
            "Narrated by: Angela Bassett",
            presetFontSizes: presetFonts,
            style: TextStyle(fontFamily: "Auto2-Regular"),
          )
        ],
      ),
    );
  }
}
