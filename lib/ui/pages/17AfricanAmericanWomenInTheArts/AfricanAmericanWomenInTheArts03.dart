import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

// image on left, text on right
class AfricanAmericanWomenInTheArts03 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Woman, 1989",
      "Artis Lane",
      "Bronze sculpture",
      "31 x 15 ½ x 8.0 in.",
      "Classic Head 1, 1994",
      "Artis Lane",
      "Bronze sculpture",
      "16 ½ x 8 ½ x 9.0 in.",
      "Artis Lane (1927– )",
      "A descendent of renowned abolitionist Mary Ann Shadd, Lane was awarded a scholarship to the Ontario College of Art in Toronto and was the first woman admitted to Cranbrook Art Academy. Her early career as a portrait painter began with Michigan Governor George Romney as her subject. Lane continued her portraiture in Los Angeles, depicting dignitaries Nelson Mandela, Gordon Getty, Ronald Reagan, Barbara Bush, and others. She used artistic expression to address social and spiritual issues with such works as The Beginning, depicting a young Rosa Parks seated on the bus. She was to become Parks’ personal portrait artist. Lane eventually shifted her focus to sculpture, creating lithe dancers, pregnant women, and her generic man/woman series. Lane’s latest series, Emerging into Spirit, includes fragmented pieces of ceramic shell left on her bronzes that symbolize man’s spiritual journey to enlightenment. Most recently in 2009, her bust of Sojourner Truth was unveiled by Michelle Obama in the Capitol Visitors Center and is now on permanent display.",
    ];
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 30),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image.asset(
                      "assets/images/afrecian_american_women_in_the_arts_02.jpg")),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Align(
                  alignment: Alignment.topLeft,
                                  child: Column(
                    children: <Widget>[
                      AutoSizeText(
                        textFiles[0],
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: "MyriadPro-BoldIt"),
                        presetFontSizes: presetFonts,
                      ),
                      AutoSizeText(
                        textFiles[1],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                      AutoSizeText(
                        textFiles[2],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                      AutoSizeText(
                        textFiles[3],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Align(
                  alignment: Alignment.center,
                                  child: GestureDetector(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height / 2,
                                width: MediaQuery.of(context).size.width / 2,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          AutoSizeText(
                                            textFiles[8],
                                            presetFontSizes: presetFonts,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontFamily: "Auto2-Regular",
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Expanded(
                                              child: AutoSizeText(
                                            textFiles[9],
                                            presetFontSizes: presetFonts,
                                            textAlign: TextAlign.justify,
                                            style: TextStyle(
                                              fontFamily: "Auto2-Regular",
                                            ),
                                          ))
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.clear,
                                          color: Colors.black,
                                          size: 30,
                                        ),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                      top: 10,
                                      right: 10,
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                    child: Container(
                        height: 50,
                        width: 50,
                        child: Image.asset(
                          "assets/images/bio.png",
                          scale: 3,
                        )),
                  ),
                ),

                
                Align(
                  alignment: Alignment.bottomRight,
                                  child: Column(
                    children: <Widget>[
                      AutoSizeText(
                        textFiles[4],
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: "MyriadPro-BoldIt"),
                        presetFontSizes: presetFonts,
                      ),
                      AutoSizeText(
                        textFiles[5],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                      AutoSizeText(
                        textFiles[6],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                      AutoSizeText(
                        textFiles[7],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 30),
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image.asset(
                      "assets/images/afrecian_american_women_in_the_arts_03.jpg")),
            ),
          )
        ],
      ),
    );
  }
}
