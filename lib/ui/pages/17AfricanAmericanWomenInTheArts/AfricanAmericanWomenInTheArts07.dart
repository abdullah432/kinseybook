import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

// image on top, text on bottom
class AfricanAmericanWomenInTheArts07 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "The Faces of My People, 1990",
      "Margaret Burroughs",
      "Woodcut",
      "28 x 32 ¾ in.",
      "Margaret Burroughs (1917–2010 )",
      "Burroughs was born in St. Rose, Louisiana, in 1917, and moved with her family to the south side of Chicago in 1922. She graduated from Chicago Teachers College in 1937, studied at Chicago State, and became an art teacher at DuSable High School. She married fellow artist Bernard Goss in 1939, and their coach-house flat became a social center, dubbed “little Bohemia,” for a wide and interracial circle of friends and colleagues. Burroughs worked tirelessly to establish the South Side Community Art Center, which opened in 1940, and became the youngest member of its board. She later studied at the Art Institute of Chicago, receiving the MFA in 1948. Burroughs worked in many media, showing special facility in watercolors and linocut printmaking. An accomplished poet, she has also written children’s books. In 1961, with Charles Burroughs, her second husband, she founded the DuSable Museum of African American History in Chicago, which stands as one of the premier institutions for documenting and displaying the African American experience.",
    ];
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 6,
            child: Container(
              padding: EdgeInsets.only(top: 30),
              height: MediaQuery.of(context).size.height, 
              width: MediaQuery.of(context).size.width,
              child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image.asset("assets/images/afrecian_american_women_in_the_arts_08.jpg")),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              children: <Widget>[
                AutoSizeText(
                  textFiles[0],
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: "MyriadPro-BoldIt"),
                  presetFontSizes: presetFonts,
                ),
                AutoSizeText(
                  textFiles[1],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[2],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[3],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                SizedBox(
                  height: 2,
                ),
                GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height / 2,
                              width: MediaQuery.of(context).size.width / 2,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        AutoSizeText(
                                          textFiles[4],
                                          presetFontSizes: presetFonts,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontFamily: "Auto2-Regular",
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Expanded(
                                            child: AutoSizeText(
                                          textFiles[5],
                                          presetFontSizes: presetFonts,
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              fontFamily: "Auto2-Regular",
                                          ),
                                        ))
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.clear,
                                        color: Colors.black,
                                        size: 30,
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                    top: 10,
                                    right: 10,
                                  )
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  child: Container(
                      height: 50,
                      width: 50,
                      child: Image.asset(
                        "assets/images/bio.png",
                        scale: 3,
                      )),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
