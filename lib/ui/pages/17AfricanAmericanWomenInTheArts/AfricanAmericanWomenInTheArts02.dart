import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
// image on right, text on left.
class AfricanAmericanWomenInTheArts02 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Portrait Bust of an African, 1899–1900",
      "May Howard Jackson",
      "Bronze",
      "21 x 12 ¾ in.",
      "May Howard Jackson (1877–1931)",
      "Jackson was born and educated in Philadelphia. In 1895, she became the first African American woman to be accepted to the Pennsylvania Academy of the Fine Arts, where she studied with William Merritt Chase and John Joseph Boyle. African Americans, particularly those of mixed ancestry, are the focus of her sculptures. Jackson created portrait busts of Paul Lawrence Dunbar and W. E. B. Dubois, among others. She also painted abstract portraits, such as Head of Negro Child and Mulatto Mother and Child. She declined invitations to study in Paris, and while her work reflects the expressive style of the times, her perspective as a black woman is evident. Jackson joined the art department at Howard University in 1922. Her work was exhibited at the Corcoran Gallery in Washington, D.C., in 1915, and at the National Academy of Design in 1916 and 1928.",
    ];
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
           Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AutoSizeText(
                  textFiles[0],
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: "MyriadPro-BoldIt"),
                  presetFontSizes: presetFonts,
                ),
                AutoSizeText(
                  textFiles[1],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[2],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                AutoSizeText(
                  textFiles[3],
                  presetFontSizes: presetFonts,
                  style: TextStyle(fontFamily: "Auto2-Regular"),
                ),
                SizedBox(
                  height: 2,
                ),
                GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Dialog(
                            child: Container(
                              height: MediaQuery.of(context).size.height / 2,
                              width: MediaQuery.of(context).size.width / 2,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 20),
                                    child: Column(
                                      children: <Widget>[
                                        AutoSizeText(
                                          textFiles[4],
                                          presetFontSizes: presetFonts,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontFamily: "Auto2-Regular",
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Expanded(
                                            child: AutoSizeText(
                                          textFiles[5],
                                          presetFontSizes: presetFonts,
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              fontFamily: "Auto2-Regular",
                                          ),
                                        ))
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.clear,
                                        color: Colors.black,
                                        size: 30,
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                    top: 10,
                                    right: 10,
                                  )
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  child: Container(
                      height: 50,
                      width: 50,
                      child: Image.asset(
                        "assets/images/bio.png",
                        scale: 3,
                      )),
                ), 
              ],
            ),
          ),
          Expanded(
            flex: 6,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 70),
              height: MediaQuery.of(context).size.height, 
              width: MediaQuery.of(context).size.width,
              child: FittedBox(
                  fit: BoxFit.contain,
                  child: Image.asset("assets/images/afrecian_american_women_in_the_arts_01.jpg")),
            ),
          ),],
      ),
    );
  }
}
