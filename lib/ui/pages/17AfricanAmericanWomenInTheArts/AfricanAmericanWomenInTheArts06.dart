import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

// image on left, text on right
class AfricanAmericanWomenInTheArts06 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Untitled, n.d.",
      "Alma Thomas",
      "Oil on canvas",
      "30 x 25.½ in.",
      "Untitled, 1970",
      "Alma Thomas",
      "Acrylic on paper",
      "18 x 22 in.",
      "Alma Thomas (1891–1978)",
      "Alma Thomas was the first graduate of the Howard University art department, and the first black woman to earn an MFA from Columbia University. After teaching in the Washington, D.C., public schools for 35 years, she began her professional career in earnest. She was among the color-field painters active in Washington and shared a collaborative relationship with friends Morris Louis, Kenneth Nolan and Gene Davis. In 1943, she was asked by James Vernon Herring and Alonzo Aden to join them in establishing the Barnett-Aden Gallery, the first African American gallery in Washington. In 1971, when she was 80 years old, she was the first female African American artist to be recognized in a solo exhibition at the Whitney Museum of American Art.",
    ];
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 30),
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: Image.asset(
                            "assets/images/afrecian_american_women_in_the_arts_06.jpg")),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(
                    children: <Widget>[
                      AutoSizeText(
                        textFiles[0],
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: "MyriadPro-BoldIt"),
                        presetFontSizes: presetFonts,
                      ),
                      AutoSizeText(
                        textFiles[1],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                      AutoSizeText(
                        textFiles[2],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                      AutoSizeText(
                        textFiles[3],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      GestureDetector(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return Dialog(
                                  child: Container(
                                    height:
                                        MediaQuery.of(context).size.height / 2,
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Stack(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 20),
                                          child: Column(
                                            children: <Widget>[
                                              AutoSizeText(
                                                textFiles[8],
                                                presetFontSizes: presetFonts,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontFamily: "Auto2-Regular",
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              Expanded(
                                                  child: AutoSizeText(
                                                textFiles[9],
                                                presetFontSizes: presetFonts,
                                                textAlign: TextAlign.justify,
                                                style: TextStyle(
                                                  fontFamily: "Auto2-Regular",
                                                ),
                                              ))
                                            ],
                                          ),
                                        ),
                                        Positioned(
                                          child: IconButton(
                                            icon: Icon(
                                              Icons.clear,
                                              color: Colors.black,
                                              size: 30,
                                            ),
                                            onPressed: () =>
                                                Navigator.pop(context),
                                          ),
                                          top: 10,
                                          right: 10,
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              });
                        },
                        child: Container(
                            height: 50,
                            width: 50,
                            child: Image.asset(
                              "assets/images/bio.png",
                              scale: 3,
                            )),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      AutoSizeText(
                        textFiles[4],
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: "MyriadPro-BoldIt"),
                        presetFontSizes: presetFonts,
                      ),
                      AutoSizeText(
                        textFiles[5],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                      AutoSizeText(
                        textFiles[6],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                      AutoSizeText(
                        textFiles[7],
                        presetFontSizes: presetFonts,
                        style: TextStyle(fontFamily: "Auto2-Regular"),
                      ),
                    ],
                  ),
                ),
                
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 30),
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: Image.asset(
                            "assets/images/afrecian_american_women_in_the_arts_07.jpg")),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
