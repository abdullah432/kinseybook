import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';

// image on left, text on right
class AfricanAmericanWomenInTheArts09 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Piano, Bass and Drums, 2000",
      "Phoebe Beasley",
      "Mixed media",
      "25 ½ x 25 ½ in.",
      "As Violence, 1973",
      "Phoebe Beasley",
      "Watercolor on paper",
      "20 ½ x 28 ½ in.",
      "Shirley at Sixty, 2006",
      "Phoebe Beasley",
      "Mixed media",
      "20 x 24 in.",
      "Phoebe Beasley (1943– )",
      "Beasley was born in Cleveland and earned a BFA from Ohio State University. After moving to Los Angeles, she sustained dual careers as an artist and a media executive. Beasley is the only artist to have earned a Presidential Seal on two occasions, for commissions from Presidents Clinton and George H. W. Bush. Beasley’s work has been shown at the Kansas African American Museum and in a solo exhibition at the M. Hanks Gallery in Santa Monica, California. Another project was a collaboration with Maya Angelou, in which Beasley created several serigraphs based on poems by Langston Hughes for a limited edition book, Sunrise is Coming after While. Beasley’s work has recently been featured in the touring exhibition “Portraying Lincoln: Man of Many Faces.”",
    ];
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 30),
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          child: FittedBox(
                              fit: BoxFit.contain,
                              child: Image.asset(
                                  "assets/images/afrecian_american_women_in_the_arts_11.jpg")),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            Flexible(
                              child: AutoSizeText(
                                textFiles[0],
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "MyriadPro-BoldIt"),
                                presetFontSizes: presetFonts,
                              ),
                            ),
                            Flexible(
                              child: AutoSizeText(
                                textFiles[1],
                                presetFontSizes: presetFonts,
                                style: TextStyle(fontFamily: "Auto2-Regular"),
                              ),
                            ),
                            Flexible(
                              child: AutoSizeText(
                                textFiles[2],
                                presetFontSizes: presetFonts,
                                style: TextStyle(fontFamily: "Auto2-Regular"),
                              ),
                            ),
                            Flexible(
                              child: AutoSizeText(
                                textFiles[3],
                                presetFontSizes: presetFonts,
                                style: TextStyle(fontFamily: "Auto2-Regular"),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 30),
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: Image.asset(
                            "assets/images/afrecian_american_women_in_the_arts_13.jpg")),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 30),
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: Image.asset(
                            "assets/images/afrecian_american_women_in_the_arts_12.jpg")),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Column(
                          children: <Widget>[
                            AutoSizeText(
                              textFiles[8],
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "MyriadPro-BoldIt"),
                              presetFontSizes: presetFonts,
                            ),
                            AutoSizeText(
                              textFiles[9],
                              presetFontSizes: presetFonts,
                              style: TextStyle(fontFamily: "Auto2-Regular"),
                            ),
                            AutoSizeText(
                              textFiles[10],
                              presetFontSizes: presetFonts,
                              style: TextStyle(fontFamily: "Auto2-Regular"),
                            ),
                            AutoSizeText(
                              textFiles[11],
                              presetFontSizes: presetFonts,
                              style: TextStyle(fontFamily: "Auto2-Regular"),
                            ),
                            SizedBox(
                              height: 2,
                            ),
                            IconButton(
                              icon: Icon(
                                Icons.play_circle_outline,
                                color: Colors.black,
                              ),
                              iconSize: 60,
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => VideoView(
                                              videoLink:
                                                  "assets/videos/afrecian_american_women_in_the_arts_01.MP4",
                                            )));
                              },
                            )
                          ],
                        ),
                      ),
                      Container(),
                      Column(
                        children: <Widget>[
                          AutoSizeText(
                            textFiles[4],
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: "MyriadPro-BoldIt"),
                            presetFontSizes: presetFonts,
                          ),
                          AutoSizeText(
                            textFiles[5],
                            presetFontSizes: presetFonts,
                            style: TextStyle(fontFamily: "Auto2-Regular"),
                          ),
                          AutoSizeText(
                            textFiles[6],
                            presetFontSizes: presetFonts,
                            style: TextStyle(fontFamily: "Auto2-Regular"),
                          ),
                          AutoSizeText(
                            textFiles[7],
                            presetFontSizes: presetFonts,
                            style: TextStyle(fontFamily: "Auto2-Regular"),
                          ),
                          SizedBox(
                            height: 2,
                          ),
                          GestureDetector(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Dialog(
                                      child: Container(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                2,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                2,
                                        child: Stack(
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 10,
                                                      vertical: 20),
                                              child: Column(
                                                children: <Widget>[
                                                  AutoSizeText(
                                                    textFiles[12],
                                                    presetFontSizes:
                                                        presetFonts,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontFamily:
                                                            "Auto2-Regular",
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  Expanded(
                                                      child: AutoSizeText(
                                                    textFiles[13],
                                                    presetFontSizes:
                                                        presetFonts,
                                                    textAlign:
                                                        TextAlign.justify,
                                                    style: TextStyle(
                                                      fontFamily:
                                                          "Auto2-Regular",
                                                    ),
                                                  ))
                                                ],
                                              ),
                                            ),
                                            Positioned(
                                              child: IconButton(
                                                icon: Icon(
                                                  Icons.clear,
                                                  color: Colors.black,
                                                  size: 30,
                                                ),
                                                onPressed: () =>
                                                    Navigator.pop(context),
                                              ),
                                              top: 10,
                                              right: 10,
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  });
                            },
                            child: Container(
                                height: 50,
                                width: 50,
                                child: Image.asset(
                                  "assets/images/bio.png",
                                  scale: 3,
                                )),
                          )
                        ],
                      ),
                      Container(),
                      Container(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
