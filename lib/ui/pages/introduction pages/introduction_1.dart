// TODO Implement this library.
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class IntroductionOne extends StatefulWidget {
  @override
  _IntroductionOneState createState() => _IntroductionOneState();
}

class _IntroductionOneState extends State<IntroductionOne> {
  final textFiles = [
    "We strive to live guided by two principles: To whom much is given, much is required; and “no regrets.” We also believe in the importance of giving back, so we have decided to share our thirty-five-year history of collecting documents, manuscripts, photographs, paintings, and sculpture exploring African American experience and culture.\n\nOur collecting has evolved extensively over this period. Shirley has been clear from the start that we should tell a positive story of African American accomplishments and triumphs rather than concentrating on the despicable history of oppression that haunts black people and this country. We have kept our primary focus on creating educational opportunities by displaying the complexity and beauty of African American life that our collection encompasses.\n\nWe believe strongly that vibrant cultural communities need venues for displaying and sharing art as well as patrons to support them. Museums provide extraordinary opportunities to explore the intersections of art and history, and we are grateful to the institutions that have proudly exhibited our collection. This has enabled over six million people to encounter for the first time artifacts and art that have helped shape a nation.\n\nThe journey of our exhibition began in Los Angeles at the California African American",
    "Museum with “In the Hands of African American Collectors: The Personal Treasures of Bernard and Shirley Kinsey.” Over the past ten years we have exhibited at twenty-three museums around the world, including the Smithsonian National Museum of American History in Washington, D.C., where over two million people viewed the collection. In the American Adventure Pavilion of Epcot Theme Park in Walt Disney World Resort, a showing of “Re-Discovering America: Family Treasures from the Kinsey Collection” from 2013 to 2018 will welcome an estimated twenty million visitors. Finally, we are most proud of our recent exhibition at Hong Kong University Museum. We have shaped our story to appeal to large, diverse audiences, and we have now published catalogs and the exhibit in Spanish and Chinese.\n\nIn response to feedback from visitors and readers, and in concert with our own evolving purpose, we have decided to shift the emphasis of the narrative to the African American experience as shaped by our perspective during our more than fifty years of marriage. We have expanded and redesigned the exhibit and the accompanying volume, now titled, The Kinsey African American Art & History Collection. This book presents pieces that are not shown in the exhibit, but are very important to us and to the story.\n\nShirley and I have been blessed beyond measure, and have always felt our obligation to share our blessings to help others. This moral compass has driven our collecting and our desire to share this",
    "work. We know that in order for history to have social value it also needs to be personal, revealing the passions and problems of people in the past so that it connects them with the present, enabling history to come alive. Our collection highlights what we have learned about ourselves and our history—particularly that black people have been a part of everything that has happened in the Americas from the beginning. The impact and influence of African Americans on American history are often taken for granted or overlooked entirely. African Americans literally built America over two centuries. The ultimate example is our nation’s capital in Washington, D.C., where African Americans built the White House and the Capitol and contributed to the construction of the entire city.\n\nWe have worked hard to add our voice to the many voices dedicated to shining a light on the wonderful story of the African American experience. Our intention is that this book inspire a deeper relationship to this history through the pieces in our collection. Having that kind of connection to the past creates a sense of strength and identity, of a lineage that is so powerful in us. Over the past thirty-five years we have become caretakers of this body of work, and we share with you our thoughts, aspirations, and perceptions at this intersection of art and history. This book is part of an ever-evolving road of discovery for us, and we invite you to join us on the journey.\n\n"
  ];

  bool listView = false;

  ScrollController listViewController = ScrollController();

  double fontSize = 50;

  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 9;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Center(
          child: listView
              ? Stack(
                  children: <Widget>[
                    ListView.builder(
                        controller: listViewController,
                        itemCount: textFiles.length,
                        itemBuilder: (BuildContext context, index) {
                          if (index == 0) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  AutoSizeText(
                                    "Introduction",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                    presetFontSizes: [fontSize],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  AutoSizeText(textFiles[index],
                                      minFontSize: fontSize,
                                      textAlign: TextAlign.justify)
                                ],
                              ),
                            );
                          }
                          if (index == 1) {
                            return Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 20),
                                child: AutoSizeText.rich(
                                  TextSpan(
                                    children: [
                                      TextSpan(
                                          text:
                                              "Museum with “In the Hands of African American Collectors: The Personal Treasures of Bernard and Shirley Kinsey.” Over the past ten years we have exhibited at twenty-three museums around the world, including the Smithsonian National Museum of American History in Washington, D.C., where over two million people viewed the collection. In the American Adventure Pavilion of Epcot Theme Park in Walt Disney World Resort, a showing of “Re-Discovering America: Family Treasures from the Kinsey Collection” from 2013 to 2018 will welcome an estimated twenty million visitors. Finally, we are most proud of our recent exhibition at Hong Kong University Museum. We have shaped our story to appeal to large, diverse audiences, and we have now published catalogs and the exhibit in Spanish and Chinese.\n\nIn response to feedback from visitors and readers, and in concert with our own evolving purpose, we have decided to shift the emphasis of the narrative to the African American experience as shaped by our perspective during our more than fifty years of marriage. We have expanded and redesigned the exhibit and the accompanying volume, now titled, "),
                                      TextSpan(
                                          text:
                                              "The Kinsey African American Art & History Collection.",
                                          style: TextStyle(
                                              fontStyle: FontStyle.italic)),
                                      TextSpan(
                                          text:
                                              " This book presents pieces that are not shown in the exhibit, but are very important to us and to the story.\n\nShirley and I have been blessed beyond measure, and have always felt our obligation to share our blessings to help others. This moral compass has driven our collecting and our desire to share this")
                                    ],
                                  ),
                                  presetFontSizes: listView
                                      ? [fontSize]
                                      : [22, 21, 20, 19, 18, 17, 16, 14],
                                  textAlign: TextAlign.justify,
                                ));
                          }
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            child: Column(
                              children: <Widget>[
                                AutoSizeText(textFiles[index],
                                    minFontSize: fontSize,
                                    textAlign: TextAlign.justify),
                                Container(
                                    width: double.infinity,
                                    child: AutoSizeText(
                                      "— Bernard W. & Shirley Pooler Kinsey",
                                      textAlign: TextAlign.right,
                                      presetFontSizes: [fontSize],
                                      maxLines: 1,
                                    ))
                              ],
                            ),
                          );
                        }),
                    Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          color: Colors.white,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              IconButton(
                                onPressed: () {
                                  if (listViewController
                                          .position.minScrollExtent ==
                                      listViewController.offset) {
                                    print("on the end");
                                  } else {
                                    listViewController.animateTo(
                                        listViewController.offset -
                                            listViewController
                                                    .position.maxScrollExtent /
                                                7,
                                        duration: Duration(milliseconds: 500),
                                        curve: Curves.easeIn);
                                  }
                                },
                                icon: Icon(
                                  Icons.arrow_back_ios,
                                  color: Colors.blue,
                                  size: 40,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  IconButton(
                                      icon: Icon(
                                        Icons.zoom_out,
                                        color: Colors.blue,
                                        size: 40,
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          fontSize = 30;
                                        });
                                      }),
                                  IconButton(
                                      icon: Icon(
                                        Icons.close,
                                        color: Colors.blue,
                                        size: 40,
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          listView = false;
                                        });
                                      })
                                ],
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.arrow_forward_ios,
                                  color: Colors.blue,
                                  size: 40,
                                ),
                                onPressed: () {
                                  if (listViewController
                                          .position.maxScrollExtent ==
                                      listViewController.offset) {
                                    print("on the end");
                                  } else {
                                    listViewController.animateTo(
                                        listViewController.offset +
                                            listViewController
                                                    .position.maxScrollExtent /
                                                7,
                                        duration: Duration(milliseconds: 500),
                                        curve: Curves.easeIn);
                                  }
                                },
                              )
                            ],
                          ),
                        ))
                  ],
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(height: template.viewHeight() * 9),
                    Container(
                      height: template.viewHeight() * 2,
                      width: wt,
                      child: Padding(
                        padding: EdgeInsets.only(right: (wt / 390) * 8),
                        child: Container(
                          color: template.boxColor(),
                        ),
                      ),
                    ),
                    Container(
                      height: template.viewHeight() * 268,
                      width: template.viewWidth() * 399,
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: (wt / 390) * 5,
                          ),
                          Container(
                            height: template.viewHeight() * 268,
                            width: template.textContainerwidth(),
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: (ht / 300) * 2,
                                  left: (wt / 390) * 2.5,
                                  right: (wt / 390) * 2.5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                      width: template.textContainerwidth(),
                                      height: template.viewHeight() * 15,
                                      child: Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    fontSize = 50;
                                                    listView = true;
                                                  });
                                                },
                                                child: RotatedBox(
                                                  quarterTurns: 1,
                                                  child: Image.asset(
                                                    "assets/images/magnify.png",
                                                    height: template
                                                        .magnifyingGlass(),
                                                  ),
                                                )),
                                            left: -template.viewWidth() * 10,
                                          ),
                                        ],
                                        overflow: Overflow.visible,
                                      )),
                                  Text(
                                    "Introduction",
                                    style: template.textheaderStyle(),
                                  ),
                                  SizedBox(
                                    height: template.viewHeight() * 3,
                                  ),
                                  Expanded(
                                      child: Text(
                                    textFiles[0],
                                    textAlign: TextAlign.justify,
                                    style: template.bodyTextStyle(),
                                  ))
                                ],
                              ),
                            ),
                          ),
                          Container(
                            height: template.viewHeight() * 268,
                            width: template.textContainerwidth(),
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: (ht / 300) * 20,
                                  left: (wt / 390) * 2.5,
                                  right: (wt / 390) * 2.5),
                              child: Text(
                                "Museum with “In the Hands of African American Collectors: The Personal Treasures of Bernard and Shirley Kinsey.” Over the past ten years we have exhibited at twenty-three museums around the world, including the Smithsonian National Museum of American History in Washington, D.C., where over two million people viewed the collection. In the American Adventure Pavilion of Epcot Theme Park in Walt Disney World Resort, a showing of “Re-Discovering America: Family Treasures from the Kinsey Collection” from 2013 to 2018 will welcome an estimated twenty million visitors. Finally, we are most proud of our recent exhibition at Hong Kong University Museum. We have shaped our story to appeal to large, diverse audiences, and we have now published catalogs and the exhibit in Spanish and Chinese\n\nIn response to feedback from visitors and readers, and in concert with our own evolving purpose, we have decided to shift the emphasis of the narrative to the African American experience as shaped by our perspective during our more than fifty years of marriage. We have expanded and redesigned the exhibit and the accompanying volume, now titled, The Kinsey African American Art & History Collection. This book presents pieces that are not shown in the exhibit, but are very important to us and to the story. \n\nShirley and I have been blessed beyond measure, and have always felt our obligation to share our blessings to help others. This moral compass has driven our collecting and our desire to share this ",
                                textAlign: TextAlign.justify,
                                style: template.bodyTextStyle(),
                              ),
                            ),
                          ),
                          Container(
                              height: template.viewHeight() * 268,
                              width: template.textContainerwidth(),
                              child: Padding(
                                padding: EdgeInsets.only(
                                  top: (ht / 300) * 19,
                                  left: (wt / 390) * 2.5,
                                  right: (wt / 390) * 2.5,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                        height: template.viewHeight() * 233,
                                        width: template.viewWidth() * 132,
                                        child: Text(
                                          textFiles[2],
                                          style: template.bodyTextStyle(),
                                          textAlign: TextAlign.justify,
                                        )),
                                    SizedBox(height: (ht / 300) * 5),
                                    Container(
                                      height: template.viewHeight() * 9,
                                      width: template.textContainerwidth(),
                                      child: Text(
                                        "— Bernard W. & Shirley Pooler Kinsey",
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                          fontFamily: 'Auto2-Light',
                                          fontSize: (ht / 300) * 7,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                    SizedBox(height: template.viewHeight() * 5),
                    Container(
                      height: template.viewHeight() * 2,
                      width: wt,
                      child: Padding(
                        padding: EdgeInsets.only(left: (wt / 390) * 8),
                        child: Container(
                          color: template.boxColor(),
                        ),
                      ),
                    ),
                    SizedBox(height: template.viewHeight()),
                    Container(
                        height: template.footerTextContainerHihght(),
                        width: template.textContainerwidth(),
                        child: Padding(
                            padding: EdgeInsets.only(
                              right: (wt / 390) * 10,
                            ),
                            child: template.footerRightText()))
                  ],
                )),
    ));
  }
}
