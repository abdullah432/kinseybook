import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class ContentPageList extends StatelessWidget {
  final List<ContentData> dataList = ContentData.getAllData();

  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: AutoSizeText(
                "CONTENTS",
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 50,
                    letterSpacing: 5,
                    color: Colors.grey),
              ),
            ),
          ),
          Expanded(
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, childAspectRatio: 11 / 1),
                  itemCount: dataList.length,
                  itemBuilder: (BuildContext context, index) {
                    return Container(
                      child: ListTile(
                        title: Text(
                          dataList[index].title,
                          style: template.textheaderStyle(),
                        ),
                        trailing: Text(
                          dataList[index].number.toString(),
                          style: template.textheaderStyle(),
                        ),
                      ),
                    );
                  }))
        ],
      ),
    );
  }
}

class ContentData {
  final String title;
  final int number;
  ContentData({this.title, this.number});

  static getAllData() {
    return [
      ContentData(title: "Foreword", number: 7),
      ContentData(title: "European Influences", number: 134),
      ContentData(title: "Introduction", number: 9),
      ContentData(title: "A New Generation of Masters", number: 140),
      ContentData(
          title: "Understanding the Past Part I: A Personal Perspective",
          number: 10),
      ContentData(title: "African American Women in the Arts", number: 151),
      ContentData(
          title: "Understanding the Past Part II: Collecting History",
          number: 11),
      ContentData(title: "Southern Home", number: 160),
      ContentData(title: "Lineage", number: 14),
      ContentData(title: "African American Artists of the West", number: 166),
      ContentData(title: "Early Genius", number: 31),
      ContentData(
          title: "Afterthoughts: The Tradition of African American Art",
          number: 184),
      ContentData(title: "Slavery", number: 45),
      ContentData(title: "The Americas + 1", number: 186),
      ContentData(title: "African Americans and War", number: 65),
      ContentData(title: "Living with Art", number: 192),
      ContentData(title: "Hearts and Souls", number: 75),
      ContentData(
          title:
              "Re-Discovering America:\nFamily Treasures from The Kinsey Collection",
          number: 196),
      ContentData(title: "Forging Freedom", number: 81),
      ContentData(title: "Recommended Reading", number: 198),
      ContentData(title: "Reflections", number: 116),
      ContentData(title: "Narrator Biographies", number: 202),
      ContentData(title: "Translating Inspiration", number: 118),
      ContentData(title: "Acknowledgments", number: 205),
      ContentData(title: "The Birth of an Aesthetic", number: 120),
      ContentData(title: "Family and Friends", number: 208)
    ];
  }
}
