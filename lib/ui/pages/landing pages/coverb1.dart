import 'package:flutter/material.dart';
import 'package:zoom_widget/zoom_widget.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class CoverBOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      decoration: template.viewportBorder(),
      height: ht,
      width: wt,
      child: FittedBox(
          fit: BoxFit.contain, child: Image.asset("assets/images/coverb1.png")),
    ));
  }
}
