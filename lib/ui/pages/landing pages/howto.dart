import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';


class HowToView extends StatefulWidget {
  @override
  _HowToViewState createState() => _HowToViewState();
}

class _HowToViewState extends State<HowToView> {
  ScrollController listViewController=ScrollController();
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Stack(
          children: <Widget>[
            ListView.builder(
              controller: listViewController,
                itemCount: 7,
                itemBuilder: (BuildContext context,int index){
                  if (index ==0){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: AutoSizeText("How to use the navigation and features of\n The Kinsey Collection",textAlign: TextAlign.end,minFontSize: 40,maxFontSize: 44,style: TextStyle(fontWeight: FontWeight.bold),),
                    );
                  }
                  return Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/howto$index.png"),
                    ),
                  );
                }),
            Positioned(
                bottom: 20,
                right: 20,
                child: GestureDetector(
                  onTap: (){
                    if(listViewController.position.maxScrollExtent==listViewController.offset){
                      print("on the end");
                    }
                    else{
                      listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/5, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                    }
                  },
                  child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
              Text("Scroll\nfor More",style: TextStyle(fontSize: 24,color: Colors.black),textAlign: TextAlign.center,),
              Icon(Icons.keyboard_arrow_down,color: Colors.black,size: 40,)
            ],),
                ))
          ],
        )
      ),
    );
  }
}
