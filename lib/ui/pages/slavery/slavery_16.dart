import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaverySixteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 50;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.sizeBoxWidth(),
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 20,
                  ),
                  Text(
                    "Slave Songs, 1867",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "A. Simpson & Co.",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Book",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "6 ¼ x 9 ¼ x ¾ in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    flex: 1,
                    child: Text(
                      "Published shortly after slavery was abolished in the United States, this book was intended to record and preserve the songs of enslaved people at the time of their freedom. Most of the songs are primarily religious in nature. Lyrics such as “going to the promised land,” however, had connotations that were powerfully linked to the hardships of enslaved people.\n\n\nSlave songs had important cultural roles that extended into the future. Music and song were a significant element of the various African cultures that were forcibly migrated to the Americas. Gospel music, the blues, and jazz are among the descendants of the slave song.\n",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/slavery16_2.jpg",
                                  )));
                    },
                    child: Container(
                        height: template.viewHeight() * 80,
                        width: template.viewWidth() * 60,
                        child: Image.asset("assets/images/slavery16_2.jpg")),
                  ),
                  SizedBox(
                    height: (ht / 300) * 10,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerLeftText())
                ],
              ),
            ),
          ),
          Container(
            height: template.viewHeight() * 280,
            width: template.viewWidth() * 240,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => GalleryView(
                              url: "assets/images/slavery16_1.jpg",
                            )));
              },
              child: Image.asset(
                "assets/images/slavery16_1.jpg",
                fit: BoxFit.contain,
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
