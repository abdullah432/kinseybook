import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 46;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/slavery2.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/slavery2.jpg",
                    fit: BoxFit.fill,
                  )),
            ),
            Expanded(
              flex: 1,
              child: Column(
                children: <Widget>[
                  Text(
                    "Female slave shackles, ca. 1850",
                    style: template.textheaderStyle(),
                  ),
                  Text(
                    "Maker unknown",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Iron",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "14 ¼ x 21 ½ x 3 in.",
                    style: template.bodyTextStyle(),
                  ),
                  SizedBox(
                    height: (ht / 300) * 20,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: (ht / 299),
            ),
            Container(
                height: template.footerTextContainerHihght(),
                width: template.textContainerwidth() * 3,
                child: template.footerLeftText()),
          ],
        ),
      ),
    );
  }
}
