import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 47;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: (ht / 300) * 28,
                  ),
                  Container(
                    height: (ht / 300) * 250,
                    width: template.textContainerwidth() * 2,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GalleryView(
                                      url: "assets/images/slavery3.jpg",
                                    )));
                      },
                      child: Image.asset(
                        "assets/images/slavery3.jpg",
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: (ht / 300) * 37,
                  ),
                  Text(
                    "“Any Person May Kill and Destroy Said\nSlaves,” 1798",
                    style: template.textheaderStyle(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "Arrest Proclamation for Jem and Mat, escaped",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "slaves issued by Warren County, NC",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Ink on paper",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "19 x 17 ¼ in.",
                    style: template.bodyTextStyle(),
                  ),
                  Container(
                    height: (ht / 300 * 150),
                    width: template.textContainerwidth(),
                    child: Text(
                      "\nThis arrest proclamation for two escaped slaves, issued by the state of North Carolina, amounted to a death sentence. Were they able to escape to the North, however, they would still have been in jeopardy: Article 4 of the U.S. Constitution required that runaway slaves had to be returned to their masters, and a measure signed by George Washington in 1793, “An act respecting fugitives from justice, and persons escaping from the service of their masters”—usually referred to as the Fugitive Slave Law—made it a federal crime to assist an escaped slave. In 1842, the U.S. Supreme Court weakened this provision, ruling that state governments need not assist in repatriating slaves to their Southern masters. But the Fugitive Slave Act of 1850 superseded that ruling, fining any federal marshall who did not arrest an escaped slave (and rewarding officials who did). Blacks so detained had no legal recourse.",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  SizedBox(
                    height: (ht / 300) * 56,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
