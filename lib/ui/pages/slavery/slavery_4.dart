import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 48;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: template.sizeBoxWidth(),
              ),
              Container(
                height: template.textContainerheight(),
                width: template.textContainerwidth(),
                child: Padding(
                  padding: template.textcontainerPadding(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        height: template.sizeBoxWidth(),
                      ),
                      Text(
                        "“New York Census, 1801",
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        style: template.textheaderStyle(),
                      ),
                      Text(
                        "State of New York",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "Printed paper",
                        style: template.bodyTextStyle(),
                      ),
                      Text(
                        "8 ¾	x 5 ¾ in",
                        style: template.bodyTextStyle(),
                      ),
                      Container(
                        width: template.textContainerwidth(),
                        child: Padding(
                          padding: template.textcontainerPadding(),
                          child: Text(
                            "According to the 1787 Philadelphia Convention and the compromise between Northern and Southern states, slaves were to be counted in the census as three-fifths of a person. The compromise, which was reached in the effort to unite the Northern and Southern states, was reflected in the Constitution in Article 1, demonstrating that slavery was codified in the country’s foundational documents. The compromise gave the white voters of the South a disproportionately larger political voice than their counterparts in the North—though the white population of the Southern states was some 50 percent smaller.\n\nThe first U.S. census took place in 1790. This document shows the “three-fifths compromise” taking effect in the New York state census of 1801.",
                            style: template.bodyTextStyle(),
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: (ht / 300) * 75,
                      ),
                      Container(
                          height: template.footerTextContainerHihght(),
                          width: template.textContainerwidth(),
                          child: template.footerLeftText()),
                    ],
                  ),
                ),
              ),
              Column(
                children: <Widget>[
                  SizedBox(
                    height: (ht / 300) * 24,
                  ),
                  Container(
                    height: (ht / 300) * 250,
                    width: template.textContainerwidth() * 2,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => GalleryView(
                                      url: "assets/images/slavery4.jpg",
                                    )));
                      },
                      child: Image.asset("assets/images/slavery4.jpg"),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
