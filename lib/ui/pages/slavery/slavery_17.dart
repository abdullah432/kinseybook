import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaverySeventeen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 60;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: template.sizeBoxWidth()),
          Container(
            height: template.viewHeight() * 260,
            width: template.textContainerwidth() * 2,
            child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/slavery17.jpg",
                              )));
                },
                child: Image.asset(
                  "assets/images/slavery17.jpg",
                  fit: BoxFit.contain,
                )),
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 40,
                  ),
                  Text(
                    "Marriage license, 1868",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Printed on paper",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "8 ¼ x 10 in.k",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    flex: 1,
                    child: Text(
                      "Following the Civil War and Emancipation, ex- slaves sought formal marriage licenses, as this certificate between Isaac Owen and Margaret Robinson attests. It is important to note that her surname, formerly that of her owner, has apparently been restored in this document. Prior to Emancipation, slaves might be married by mutual consent, and even receive sanctification from their master’s minister. But formal marriage licenses were important not only for religious and spiritual reasons once slaves were emancipated. Only by means of such a document could property be handed down to heirs or other designated relatives.",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  SizedBox(
                    height: (ht / 300) * 103,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
