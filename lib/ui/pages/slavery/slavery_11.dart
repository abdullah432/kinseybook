import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryEleven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 55;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                height: (ht / 300) * 240,
                width: template.textContainerwidth() * 2,
                child: Padding(
                  padding: EdgeInsets.only(
                    top: (ht / 300),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/slavery11.jpg",
                                  )));
                    },
                    child: Container(
                      height: (ht / 300) * 150,
                      width: (wt / 390) * 200,
                      child: FittedBox(
                        child: Image.asset("assets/images/slavery11.jpg"),
                      ),
                    ),
                  ),
                )),
            Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Padding(
                padding: template.textcontainerPadding(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: (ht / 300) * 50,
                    ),
                    Container(
                        height: (ht / 300) * 40,
                        width: template.textContainerwidth(),
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Frederick Douglass, 2003",
                              style: template.textheaderStyle(),
                            ),
                            Text(
                              "Tina Allen",
                              style: template.bodyTextStyle(),
                            ),
                            Text(
                              "Bronze",
                              style: template.bodyTextStyle(),
                            ),
                            Text(
                              "23 x 17 ¼	 x 12 in.",
                              style: template.bodyTextStyle(),
                            ),
                          ],
                        )),
                    SizedBox(
                      height: (ht / 300),
                    ),
                    Container(
                      height: (ht / 300) * 20,
                      width: template.footerContainerWidth(),
                      child: GestureDetector(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return Dialog(
                                  child: Container(
                                    height:
                                        MediaQuery.of(context).size.height / 2,
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Stack(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10, vertical: 20),
                                          child: Column(
                                            children: <Widget>[
                                              Text(
                                                "Tina Allen (1955–2008)",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 24,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              Expanded(
                                                  child: AutoSizeText(
                                                "Born in 1955, in Hempstead, New York, Allen was mentored by sculptor William Zorach and attended Pratt Institute and the New York School of Visual Arts. She also studied art at the University of South Alabama. In 1986, Allen won a competition for a commission to create a memorial statue of African American labor activist A. Philip Randolph, who founded a union for train porters in 1925. Allen has created memorial statues of distinguished Africans and African Americans, including Malcolm X and Nelson Mandela.",
                                                presetFontSizes: [
                                                  22,
                                                  21,
                                                  20,
                                                  19,
                                                  18,
                                                  17,
                                                  16,
                                                  14
                                                ],
                                                textAlign: TextAlign.justify,
                                              ))
                                            ],
                                          ),
                                        ),
                                        Positioned(
                                          child: IconButton(
                                            icon: Icon(
                                              Icons.clear,
                                              color: Colors.black,
                                              size: 30,
                                            ),
                                            onPressed: () =>
                                                Navigator.pop(context),
                                          ),
                                          top: 10,
                                          right: 10,
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              });
                        },
                        child: Container(
                            height: (ht / 300) * 20,
                            width: (wt / 390) * 20,
                            child: Image.asset(
                              "assets/images/bio.png",
                              height: (ht / 300) * 10,
                            )),
                      ),
                    ),
                    SizedBox(
                      height: (ht / 300) * 10,
                    ),
                    Container(
                      height: (ht / 300) * 15,
                      width: template.textContainerwidth(),
                      child: FittedBox(
                        child: AudioViewer(url: "assets/audio/slavery11.mp3"),
                        fit: BoxFit.contain,
                      ),
                    ),
                    SizedBox(
                      height: (ht / 300) * 10,
                    ),
                    Container(
                      width: template.footerContainerWidth(),
                      height: (ht / 300) * 20,
                      child: IconButton(
                        icon: Icon(
                          Icons.play_circle_outline,
                          color: Colors.black,
                        ),
                        iconSize: (ht / 300) * 25,
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VideoView(
                                        videoLink:
                                            "assets/videos/slavery11.MP4",
                                      )));
                        },
                      ),
                    ),
                    SizedBox(
                      height: (ht / 300) * 100,
                    ),
                    Container(
                        height: template.footerTextContainerHihght(),
                        width: template.textContainerwidth(),
                        child: template.footerRightText())
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
