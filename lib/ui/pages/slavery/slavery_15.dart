import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryFifteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 59;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.sizeBoxWidth(),
          ),
          Container(
            height: template.viewHeight() * 280,
            width: template.viewWidth() * 240,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => GalleryView(
                              url: "assets/images/slavery15.jpg",
                            )));
              },
              child: Image.asset(
                "assets/images/slavery15.jpg",
                fit: BoxFit.contain,
              ),
            ),
          ),
          SizedBox(
            width: template.viewWidth() * 15,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 35,
                  ),
                  Text(
                    "“Mammy of Ye Olden Times,” ca. 1860",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "Rare Large Format Albumen Photograph",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "10 x 15 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Container(
                    width: template.textContainerwidth(),
                    height: template.viewHeight() * 85,
                    child: Text(
                      "This photograph shows an enslaved older black woman sitting on the porch next to a small basket, with a set of keys attached to her coat. She probably worked inside the house. Inscribed by hand with the title “Mammy of Ye Olden Times.”",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  SizedBox(
                    height: (ht / 300) * 120,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
