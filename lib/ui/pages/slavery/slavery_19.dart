import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryNinteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 62;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 4,
          ),
          Container(
            height: template.viewHeight() * 265,
            width: template.textContainerwidth() * 2,
            child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/slavery19.jpg",
                              )));
                },
                child: Image.asset(
                  "assets/images/slavery19.jpg",
                  fit: BoxFit.contain,
                )),
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 30,
                  ),
                  Text(
                    "What Mrs. Fisher Knows About Old\nSouthern Cooking, Soups, Pickles, Preserves,\netc., 1881",
                    style: template.textheaderStyle(),
                    maxLines: 3,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "Abby Fisher",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Original publisher’s brown pebbled-cloth book,",
                    style: template.bodyTextStyle(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "stamped in giltr",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "7 ¾ x 5 ½ x ¾ in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    flex: 1,
                    child: Text(
                      "Abby Fisher, born a slave, could not read or write. In the 1880 census, Mrs. Fisher is listed by race—mulatto—and profession—cook. Her husband, also a mulatto, Alexander Fisher, born in Mobile, Alabama, is listed as well. The couple arrived in San Francisco around 1870, with their eleven children, and Mrs. Fisher began to gain a reputation as a cook and as a pickle and preserve maker. She was awarded a diploma at the Sacramento State Fair in 1879. At the San Francisco Mechanics Institute Fair of 1889, she won medals for best pickles and sauces and for best assortment of jellies and preserves.\n\nThis cookbook is the first by an African American. Nine prominent white San Franciscans, three men and six women, transcribed Fisher’s recipes and published the cookbook through the prolific Women’s Co-operative Printing Union.",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  SizedBox(
                    height: (ht / 300) * 44,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
