import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryNine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 53;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth() * 2,
              child: Padding(
                padding: EdgeInsets.only(
                    top: template.viewHeight() * 10,
                    left: (wt / 390) * 2.5,
                    right: (wt / 390) * 2.5),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: template.viewHeight() * 220,
                      width: template.textContainerwidth() * 2,
                      child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => GalleryView(
                                          url: "assets/images/slavery9.jpg",
                                        )));
                          },
                          child: Image.asset("assets/images/slavery9.jpg")),
                    ),
                    SizedBox(
                      height: template.viewHeight() * 14,
                    ),
                    Text(
                      "Supreme Court of the United States:\nDred Scott Decision, 1857",
                      style: template.textheaderStyle(),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      "Printed paper",
                      style: template.bodyTextStyle(),
                    ),
                    Text(
                      "5 ½ x 9 x ½	 in.",
                      style: template.bodyTextStyle(),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: (template.viewHeight() * 14),
                  ),
                  Container(
                    width: template.textContainerwidth(),
                    height: template.viewHeight() * 268,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: template.viewWidth() * 2.5,
                      ),
                      child: Text(
                        "This document from 1857 is the original printing of the opinions and the votes of each justice of the Supreme Court of the United States in the Dred Scott case.\n\nDred Scott (1799 – September 17, 1858) was an African American slave who sued unsuccessfully for his freedom in the infamous Dred Scott v. Sandford case of 1857. Although he and his wife, Harriet, were slaves, the premise of the suit was that he had lived with his master, Dr. John Emerson, in Illinois and Minnesota (then part of the Wisconsin Territory), where both state laws and the Northwest Ordinance of 1787 had made slavery illegal. The Supreme Court ruled seven to two against Scott.\n\nChief Justice Taney’s court made two sweeping rulings. The first was that Dred Scott had no right to sue in federal court because neither slaves nor free blacks were citizens of the United States. At the time the Constitution was adopted, the chief justice wrote, blacks had been “regarded as beings of an inferior order” with “no rights which the white man was bound to respect.” Second, Taney declared that Congress had no right to exclude slavery from the federal territories since the Fifth Amendment prohibited seizure of property without due process. Taney himself was the second-largest slaveholder in Virginia and should have recused himself from deliberations.\n\nFor the first time since Marbury v. Madison in 1803, the Supreme Court declared an act of Congress unconstitutional. And its advocacy of this extreme pro-slavery position accelerated the start of the Civil War.",
                        textAlign: TextAlign.justify,
                        style: template.bodyTextStyle(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: (template.viewHeight() * 5.5),
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
