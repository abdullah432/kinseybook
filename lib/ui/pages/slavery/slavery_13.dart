import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryThirteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 57;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 7,
          ),
          Container(
            height: (ht / 300) * 260,
            width: (wt / 399) * 240,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => GalleryView(
                              url: "assets/images/slavery13.jpg",
                            )));
              },
              child: Image.asset(
                "assets/images/slavery13.jpg",
                fit: BoxFit.contain,
              ),
            ),
          ),
          SizedBox(
            width: template.sizeBoxWidth() * 2,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: (ht / 300) * 90,
                ),
                Text(
                  "Slave Insurance, ca. 1859",
                  style: template.textheaderStyle(),
                  maxLines: 1,
                ),
                Text(
                  "Albemarle Insurance Company",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "Printed paper",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "9 ¾ x 8 in.",
                  style: template.bodyTextStyle(),
                ),
                Container(
                  width: template.textContainerwidth(),
                  height: (ht / 300) * 160,
                  child: Text(
                    "\nSlaves were a valuable commodity to their owners, and like all assets they were insured. Frances, the young slave referred to in the letter on the previous page, was most likely insured by the Albemarle Company, located in the same county as her master’s plantation.",
                    style: template.bodyTextStyle(),
                    textAlign: TextAlign.justify,
                  ),
                ),
                SizedBox(
                  height: (ht / 300) * 7,
                ),
                Container(
                    height: template.footerTextContainerHihght(),
                    width: template.textContainerwidth(),
                    child: template.footerRightText())
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
