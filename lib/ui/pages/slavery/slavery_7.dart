import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaverySeven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 51;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: (ht / 300) * 250,
            width: template.textContainerwidth() * 2,
            child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/slavery7.jpg",
                              )));
                },
                child: Image.asset(
                  "assets/images/slavery7.jpg",
                )),
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: (ht / 300) * 75,
                ),
                Text(
                  "Doc. No 225: Negroes &c. Captured from\nIndians in Florida, 1839",
                  style: template.textheaderStyle(),
                  maxLines: 2,
                  textAlign: TextAlign.center,
                ),
                Text(
                  "J. R. Poinsett, Secretary of War",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "Book",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "9 ¼ x 5 ¾ x 1 ½	 in.",
                  style: template.bodyTextStyle(),
                ),
                Container(
                  height: (ht / 300) * 125,
                  width: template.footerContainerWidth(),
                  child: Text(
                    "This official publication was generated as a result of the campaigns of 1837–38 in Florida. President Jackson spent \$15 million appropriating slaves from the Seminoles and sending them to Leon and Gasden Counties, which had the largest slave holdings in all of Florida. The legacy of slavery in these counties is still evident to Bernard and Shirley Kinsey, as Floridians and American citizens.",
                    style: template.bodyTextStyle(),
                    textAlign: TextAlign.justify,
                  ),
                ),
                SizedBox(
                  height: (ht / 300) * 49,
                ),
                Container(
                    height: template.footerTextContainerHihght(),
                    width: template.textContainerwidth(),
                    child: template.footerRightText())
              ],
            ),
          ),
        ],
      ),
    ));
  }
}
