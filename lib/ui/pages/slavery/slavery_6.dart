import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaverySix extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 50;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: (ht / 300) * 20,
            ),
            Container(
              height: (ht / 300) * 160,
              width: (wt / 390) * 255,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/slavery6.jpg",
                                )));
                  },
                  child: Image.asset("assets/images/slavery6.jpg")),
            ),
            Container(
              width: template.textContainerwidth(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: template.sizeBoxWidth(),
                  ),
                  Text(
                    "Henry Butler buys the freedom of his wife and four\nchildren for \$100, 1839",
                    style: template.textheaderStyle(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "Henry Butler",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Ink on paper",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "9 x 14 1⁄2 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Container(
                    child: Text(
                      "During this period, healthy and child-bearing slaves often sold for as much as \$1,800. Mary Ann Graham sold Henry Butler his family at a significant loss. This letter is important to the Kinseys because it shows the compassion of the female slave owner. It offers a glimpse of humane conduct in an otherwise inhumane time.",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: (ht / 300) * 14,
            ),
            Container(
                width: template.textContainerwidth() * 3,
                child: template.footerLeftText())
          ],
        ),
      ),
    );
  }
}
