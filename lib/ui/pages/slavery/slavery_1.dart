import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryOne extends StatefulWidget {
  @override
  _SlaveryOneState createState() => _SlaveryOneState();
}

class _SlaveryOneState extends State<SlaveryOne> {
  final textFiles = [
    "For over 258 years, American slavery was a harsh reality for the majority of the African Americans living in the United States. While many whites served as indentured servants in the colonies, they were able to work off their servitude after a number of years. Black slaves and their progeny had no such recourse. The Kinseys have amassed formidable evidence of the scope of slavery in a nation founded on liberty.\n\nSlavery is often seen as an incomprehensible part of American culture, to the point that many refuse to or simply cannot discuss it. Thomas Jefferson acknowledged the misery of black slaves, and even mused in Notes of the State of Virginia (1781) that the risk of emancipation was the “ten thousand recollections . . . of the injuries [blacks] have sustained.” This understanding did not stop him from fathering and freeing his children",
    "by his slave, Sally Hemings, but did express his conflicted feelings on the abolition of slavery.\n\nThe dichotomy of African American identity is written large in the life of Frederick Douglass. Born a slave in Maryland in 1818, Douglass escaped from his owner to travel to New York and onward to become the pre-eminent African American spokesman for abolition. In Massachusetts he met abolitionist leader William Lloyd Garrison, who encouraged him to become active in the movement. Before he was 30, Douglass had become an accomplished orator and writer, lecturing throughout the world and advocating for the equality of all people.\n\nDouglass’ story inspires all audiences to recognize that vast potential lies in the minds of all individuals, who can achieve greatness if given the opportunity. His message changed the lives of his own people, but it also reached those who may not have realized their complicity in the sub- jugation of others. Douglass’ English audiences were so swayed by his words that they bought his freedom for 700 pounds, securing his ability to spread his message.\n\nThe Kinseys are drawn to these ambiguities,",
    "stumbling blocks between compassion and complacency. They collect provocative ephemera as evidence that things are never simple or absolute. In these intersections, the Kinseys seek the vestiges of slavery that document African American endurance. Whether delivering silent resistance or booming oratory on the evils of slavery, blacks survived and thrived."
  ];
  static bool listView = false;
  ScrollController listViewController = ScrollController();
  static double fontSize = 50;
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 45;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Center(
            child: listView
                ? Stack(
                    children: <Widget>[
                      ListView.builder(
                          controller: listViewController,
                          itemCount: textFiles.length,
                          itemBuilder: (BuildContext context, index) {
                            if (index == 0) {
                              return Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20, vertical: 20),
                                  child: Column(
                                    children: <Widget>[
                                      AutoSizeText.rich(
                                        TextSpan(
                                          children: [
                                            TextSpan(
                                                text: "Slavery\n\n",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 24)),
                                            TextSpan(
                                              text:
                                                  "In every human breast, God has implanted a principle which we call love of freedom; it is impatient of oppression and pants for deliverance.\n",
                                            ),
                                          ],
                                        ),
                                        presetFontSizes: [fontSize],
                                        textAlign: TextAlign.justify,
                                      ),
                                      Align(
                                        alignment: Alignment.centerRight,
                                        child: AutoSizeText(
                                          "— Phillis Wheatley",
                                          presetFontSizes: [fontSize],
                                        ),
                                      ),
                                      AutoSizeText(
                                        textFiles[0],
                                        presetFontSizes: [fontSize],
                                        textAlign: TextAlign.justify,
                                      )
                                    ],
                                  ));
                            }
                            return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              child: AutoSizeText(textFiles[index],
                                  minFontSize: fontSize,
                                  textAlign: TextAlign.justify),
                            );
                          }),
                      Positioned(
                          bottom: 0,
                          left: 0,
                          right: 0,
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.1,
                            color: Colors.white,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                IconButton(
                                  onPressed: () {
                                    if (listViewController
                                            .position.minScrollExtent ==
                                        listViewController.offset) {
                                      print("on the end");
                                    } else {
                                      listViewController.animateTo(
                                          listViewController.offset -
                                              listViewController.position
                                                      .maxScrollExtent /
                                                  7,
                                          duration: Duration(milliseconds: 500),
                                          curve: Curves.easeIn);
                                    }
                                  },
                                  icon: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.blue,
                                    size: 40,
                                  ),
                                ),
                                Row(
                                  children: <Widget>[
                                    IconButton(
                                        icon: Icon(
                                          Icons.zoom_out,
                                          color: Colors.blue,
                                          size: 40,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            fontSize = 30;
                                          });
                                        }),
                                    IconButton(
                                        icon: Icon(
                                          Icons.close,
                                          color: Colors.blue,
                                          size: 40,
                                        ),
                                        onPressed: () {
                                          setState(() {
                                            listView = false;
                                          });
                                        })
                                  ],
                                ),
                                IconButton(
                                  icon: Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.blue,
                                    size: 40,
                                  ),
                                  onPressed: () {
                                    if (listViewController
                                            .position.maxScrollExtent ==
                                        listViewController.offset) {
                                      print("on the end");
                                    } else {
                                      listViewController.animateTo(
                                          listViewController.offset +
                                              listViewController.position
                                                      .maxScrollExtent /
                                                  7,
                                          duration: Duration(milliseconds: 500),
                                          curve: Curves.easeIn);
                                    }
                                  },
                                )
                              ],
                            ),
                          ))
                    ],
                  )
                : Column(
                    children: <Widget>[
                      SizedBox(height: template.viewHeight() * 20),
                      Container(
                        height: template.viewHeight() * 2,
                        width: wt,
                        child: Padding(
                          padding: EdgeInsets.only(right: (wt / 390) * 8),
                          child: Container(
                            color: template.boxColor(),
                          ),
                        ),
                      ),
                      Container(
                        height: template.viewHeight() * 240,
                        width: template.textContainerwidth() * 3,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: template.viewHeight() * 240,
                              width: template.textContainerwidth(),
                              child: Padding(
                                padding: EdgeInsets.only(
                                    top: template.viewHeight() * 5,
                                    left: template.viewWidth() * 2.5,
                                    right: template.viewWidth() * 2.5),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      width: template.textContainerwidth(),
                                      height: template.viewHeight() * 12,
                                      child: Stack(
                                        children: <Widget>[
                                          Positioned(
                                            child: GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    fontSize = 50;
                                                    listView = true;
                                                  });
                                                },
                                                child: RotatedBox(
                                                  quarterTurns: 1,
                                                  child: Image.asset(
                                                    "assets/images/magnify.png",
                                                    height: template
                                                        .magnifyingGlass(),
                                                  ),
                                                )),
                                            left:
                                                template.magnifyGlassPosition(),
                                          ),
                                        ],
                                        overflow: Overflow.visible,
                                      ),
                                    ),
                                    Column(
                                      children: <Widget>[
                                        Container(
                                          width: template.textContainerwidth(),
                                          child: Text(
                                            "Slavery",
                                            style: template.textheaderStyle(),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                        Text(
                                            "\nIn every human breast, God has implanted a principle which we call love of freedom; it is impatient of oppression and pants for deliverance.\n",
                                            textAlign: TextAlign.justify,
                                            style: template.bodyTextStyle()),
                                      ],
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          width: template.textContainerwidth(),
                                          height: template.viewHeight() * 6,
                                          child: Text(
                                            "— Phillis Wheatley",
                                            textAlign: TextAlign.right,
                                            style: TextStyle(
                                                fontFamily: 'Auto2-Light',
                                                fontSize:
                                                    template.viewHeight() * 7),
                                          ),
                                        ),
                                        SizedBox(
                                            height: template.viewHeight() * 17),
                                        Container(
                                          height: template.viewHeight() * 150,
                                          width: template.textContainerwidth(),
                                          child: Text(
                                            textFiles[0],
                                            style: template.bodyTextStyle(),
                                            textAlign: TextAlign.justify,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: (ht / 300) * 10,
                            ),
                            Container(
                                height: template.viewHeight() * 235,
                                width: template.textContainerwidth(),
                                child: Padding(
                                  padding: template.textcontainerPadding(),
                                  child: Text(
                                    textFiles[1],
                                    textAlign: TextAlign.justify,
                                    style: template.bodyTextStyle(),
                                  ),
                                )),
                            Column(
                              children: <Widget>[
                                Container(
                                  height: template.viewHeight() * 235,
                                  width: (template.textContainerwidth()),
                                  child: Padding(
                                    padding: template.textcontainerPadding(),
                                    child: Text(
                                      textFiles[2],
                                      textAlign: TextAlign.justify,
                                      style: template.bodyTextStyle(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: template.viewHeight() * 2,
                        width: wt,
                        child: Padding(
                          padding: EdgeInsets.only(left: (wt / 390) * 8),
                          child: Container(
                            color: template.boxColor(),
                          ),
                        ),
                      ),
                      SizedBox(height: template.viewHeight() * 25),
                      Container(
                          height: template.footerTextContainerHihght(),
                          width: template.textContainerwidth() * 3,
                          child: template.footerRightText())
                    ],
                  )),
      ),
    );
  }
}
