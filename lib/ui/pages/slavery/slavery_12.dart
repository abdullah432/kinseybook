import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryTwelve extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 56;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Center(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: template.sizeBoxWidth(),
              ),
              Container(
                height: template.textContainerheight(),
                width: template.textContainerwidth(),
                child: Padding(
                  padding: EdgeInsets.only(left: (wt / 390) * 2.5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: (ht / 300) * 128,
                        width: template.textContainerwidth(),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 35),
                            Text(
                              "Oration Delivered in Corinthian Hall Roches-\nter, 1852",
                              style: template.textheaderStyle(),
                              maxLines: 2,
                              textAlign: TextAlign.center,
                            ),
                            Text(
                              "Frederick Douglass",
                              style: template.bodyTextStyle(),
                            ),
                            Text(
                              "Pamphlet",
                              style: template.bodyTextStyle(),
                            ),
                            Text(
                              "10 ¼	x 7 x 1 ¼ in.",
                              style: template.bodyTextStyle(),
                            ),
                            Container(
                              height: (ht / 300) * 50,
                              child: Text(
                                "Frederick Douglass demonstrated his commitment to the pursuit of freedom while challenging America’s celebration of liberty in a speech to the Rochester Ladies Anti-Slavery Society: “What have I, or those I represent, to do with your national independence? . . . This Fourth of July is yours, not mine. You may rejoice, I must mourn.”",
                                style: template.bodyTextStyle(),
                                textAlign: TextAlign.justify,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                          height: (ht / 300) * 90,
                          width: template.textContainerwidth(),
                          child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => GalleryView(
                                              url:
                                                  "assets/images/slavery12_1.jpg",
                                            )));
                              },
                              child: Image.asset(
                                  "assets/images/slavery12_1.jpg"))),
                      Container(
                          height: (ht / 300) * 70,
                          width: template.textContainerwidth(),
                          child: Column(
                            children: <Widget>[
                              Text(
                                "\nThe Life and Times of Frederick Douglass\nfrom 1817 to 1832, 1882",
                                style: template.textheaderStyle(),
                                textAlign: TextAlign.center,
                              ),
                              Text(
                                "Frederick Douglass",
                                style: template.bodyTextStyle(),
                              ),
                              Text(
                                "Book",
                                style: template.bodyTextStyle(),
                              ),
                              Text(
                                "8 1⁄2 x 6 1⁄2 x 1 3⁄4 in.",
                                style: template.bodyTextStyle(),
                              ),
                              SizedBox(
                                height: template.viewHeight() * 16,
                              ),
                              Container(
                                  width: template.textContainerwidth(),
                                  child: template.footerLeftText())
                            ],
                          )),
                    ],
                  ),
                ),
              ),
              Container(
                width: template.textContainerwidth() * 2,
                height: (ht / 300) * 280,
                child: Padding(
                  padding: template.textcontainerPadding(),
                  child: Container(
                    child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GalleryView(
                                        url: "assets/images/slavery12.jpg",
                                      )));
                        },
                        child: Image.asset(
                          "assets/images/slavery12.jpg",
                          fit: BoxFit.contain,
                        )),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
