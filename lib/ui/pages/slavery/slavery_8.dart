import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryEight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 52;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: (ht / 300) * 2,
            ),
            Container(
              height: (ht / 300) * 150,
              width: (wt / 390) * 255,
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/slavery8.jpg",
                              )));
                },
                child: Container(
                    height: (ht / 300) * 150,
                    width: (wt / 390) * 255,
                    child: FittedBox(
                        child: Image.asset("assets/images/slavery8.jpg"))),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Slave Tags, 1841, 1862",
                  style: template.textheaderStyle(),
                  maxLines: 1,
                ),
                Text(
                  "Metal; copper",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "1 ½	x 1 ½ in., each",
                  style: template.bodyTextStyle(),
                ),
              ],
            ),
            Container(
              height: (ht / 300) * 120,
              width: template.textContainerwidth() * 3,
              child: Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: (ht / 300) * 110,
                        width: template.textContainerwidth(),
                        child: Padding(
                          padding: template.textcontainerPadding(),
                          child: Text(
                            "According to the first U.S. Census in 1790, Charleston, South Carolina, was the fourth largest city in the newly formed United States. It was also the city where the most Africans were transported into the country to become enslaved people. As such, Charleston developed a sophisticated culture and economy supported by enslaved labor.\n\nSlave badges, commonly referred to today as slave tags, were codified and came into use around 1800 only in the Charleston area to formalize a slave-",
                            style: template.bodyTextStyle(),
                            textAlign: TextAlign.justify,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: (ht / 300) * 3,
                      ),
                      Container(
                          width: template.textContainerwidth(),
                          child: template.footerLeftText())
                    ],
                  ),
                  Container(
                      height: (ht / 300) * 100,
                      width: template.textContainerwidth(),
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: (ht / 300) * 10,
                            left: (wt / 390) * 2.5,
                            right: (wt / 390) * 2.5),
                        child: Text(
                          "hire system. After paying the city’s annual license fee, an owner was issued a slave badge stamped with a number for a specific slave with a named skill. This badge had to be worn at all times by the enslaved person, and allowed the owner to hire the enslaved person out to others. Frequently a portion of the wages obtained were split with the enslaved person. With a slave badge, an enslaved person could also legally work after hours or on free time to garner additional personal income. The practice of slaves for hire was controversial with poorer whites competing for work. Enslaved",
                          style: template.bodyTextStyle(),
                          textAlign: TextAlign.justify,
                        ),
                      )),
                  Container(
                      height: (ht / 300) * 100,
                      width: template.textContainerwidth(),
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: (ht / 300) * 10,
                            left: (wt / 390) * 2.5,
                            right: (wt / 390) * 2.5),
                        child: Text(
                          "people working for hire without a badge could be heavily fined and most often punished with 20 or more lashes.\n\nAs these badges were re-issued annually, they were disposed of regularly, and few remain in existence today. Most have been found by treasure hunters with metal detectors and are highly sought after by collectors of slave ephemera.",
                          style: template.bodyTextStyle(),
                          textAlign: TextAlign.justify,
                        ),
                      ))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
