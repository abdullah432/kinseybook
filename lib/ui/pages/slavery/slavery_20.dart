import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryTwenty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 63;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 8,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 10,
                  ),
                  Text(
                    "The Souls of Black Folk, 1903",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "W.E.B DuBois",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Book, first edition, signed",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "5 ¼ x 7 ½ x 1 ¼ in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    flex: 1,
                    child: Text(
                      "\nThe Souls of Black Folk, published in 1903, is a classic work of American literature and a cornerstone of African American literary history. The book contains several essays on race, some of which had been previously published in the Atlantic Monthly. Du Bois drew from his own experiences as an African American living in America, and beyond its importance as a historical source, The Souls of Black Folk is a landmark early work in the field of sociology. After graduating from Harvard, where he was the first African American to earn a doctorate, Du Bois became a professor of history, sociology, and economics at Atlanta University. In 1909, he was one of the co-founders of the National Association for the Advancement of Colored People (NAACP).\n",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GalleryView(
                                    url: "assets/images/slavery20_2.jpg",
                                  )));
                    },
                    child: Container(
                        height: template.viewHeight() * 85,
                        width: template.viewWidth() * 75,
                        child: Image.asset(
                          "assets/images/slavery20_2.jpg",
                          fit: BoxFit.contain,
                        )),
                  ),
                  SizedBox(
                    height: (ht / 300) * 10,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerLeftText())
                ],
              ),
            ),
          ),
          Container(
            height: template.viewHeight() * 180,
            width: template.textContainerwidth() * 2,
            child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/slavery20_1.jpg",
                              )));
                },
                child: Image.asset(
                  "assets/images/slavery20_1.jpg",
                  fit: BoxFit.fill,
                )),
          ),
        ],
      ),
    ));
  }
}
