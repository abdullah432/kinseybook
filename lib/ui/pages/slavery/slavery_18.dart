import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryEighteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 61;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 10,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: (ht / 300) * 50,
                ),
                Text(
                  "The Freeman: A National Colored Weekly\nNewspaper, 1889",
                  style: template.textheaderStyle(),
                  maxLines: 2,
                  textAlign: TextAlign.center,
                ),
                Text(
                  "Printed paper",
                  style: template.bodyTextStyle(),
                ),
                Text(
                  "14 ½	x 21 ½	 in.",
                  style: template.bodyTextStyle(),
                ),
                Flexible(
                  flex: 1,
                  child: Text(
                    "The first illustrated black newspaper,The Freeman, was published in Indianapolis by Edward Elder Cooper. The issue shown here, from March 23, 1889, featured H. C. Smith, the editor of the Cleveland Gazette, and J. Hugo Johnson, president of the Virginia Normal and Collegiate Institute for Colored Youths at Petersburg. The masthead featured images of a black soldier, Lincoln emancipating a slave, and black voters and sharecroppers. In addition to portraits of African Americans who held prominent positions at the time, the paper contained jokes and political cartoons, and a serialized excerpt from H. Rider Haggard’s King Solomon’s Mines, a popular work of fiction that was the first adventure story set in Africa. This copy bears a stamp and a label indicating that was once the property of the Indianapolis Public Library.",
                    style: template.bodyTextStyle(),
                    textAlign: TextAlign.justify,
                  ),
                ),
                SizedBox(
                  height: (ht / 300) * 81,
                ),
                Container(
                    height: template.footerTextContainerHihght(),
                    width: template.textContainerwidth(),
                    child: template.footerLeftText())
              ],
            ),
          ),
          Container(
            height: template.viewHeight() * 265,
            width: template.textContainerwidth() * 2,
            child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/slavery18.jpg",
                              )));
                },
                child: Image.asset("assets/images/slavery18.jpg",
                    fit: BoxFit.contain)),
          ),
        ],
      ),
    ));
  }
}
