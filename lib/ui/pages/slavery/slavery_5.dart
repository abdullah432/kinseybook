import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryFive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
      child: Container(
        height: ht,
        width: wt,
        decoration: template.viewportBorder(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: (ht / 300) * 250,
              width: template.textContainerwidth() * 2,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GalleryView(
                                  url: "assets/images/slavery5.jpg",
                                )));
                  },
                  child: Image.asset(
                    "assets/images/slavery5.jpg",
                  )),
            ),
            Container(
              height: template.textContainerheight(),
              width: template.textContainerwidth(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: (ht / 300) * 75,
                  ),
                  Text(
                    "Handwritten slave document, 1832",
                    style: template.textheaderStyle(),
                    maxLines: 1,
                  ),
                  Text(
                    "William Johnson",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Ink on paper",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "15 ½	x 10 ½	 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    child: Text(
                      "William Johnson’s 1832 bill of sale for an unnamed slave sold for \$550, the equivalent of \$14,000 today.\n\nGiven to Bernard and Shirley Kinsey as a gift, this document was one of the first to be included in their collection. It inspired Bernard’s quest to discover both his personal history and African American historical documentation.",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  SizedBox(
                    height: (ht / 300) * 125,
                  ),
                  Container(
                      height: template.footerTextContainerHihght(),
                      width: template.textContainerwidth(),
                      child: template.footerRightText())
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
