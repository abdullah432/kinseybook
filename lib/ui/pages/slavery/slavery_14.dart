import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryFourteen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 58;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: template.viewWidth() * 10,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: template.textcontainerPadding(),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: template.viewHeight() * 85,
                  ),
                  Text(
                    "A Letter from Hutson Lee to\nC. T. Pollard, Esq.,1860",
                    style: template.textheaderStyle(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "Hutson Lee",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Ink on paper",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "8 ¼	 x 5 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Container(
                    width: template.textContainerwidth(),
                    height: template.viewHeight() * 40,
                    child: Text(
                      "\n\nThis letter documents the profitable business of trading slaves. It includes sale price, commission, and delivery fees.",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  SizedBox(
                    height: template.viewHeight() * 100,
                  ),
                  Container(
                    height: template.footerTextContainerHihght(),
                    width: template.textContainerwidth(),
                    child: template.footerLeftText(),
                  )
                ],
              ),
            ),
          ),
          Container(
            height: template.viewHeight() * 280,
            width: template.viewWidth() * 230,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => GalleryView(
                              url: "assets/images/slavery14.jpg",
                            )));
              },
              child: Image.asset(
                "assets/images/slavery14.jpg",
                fit: BoxFit.contain,
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
