import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/audio_viewer.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

import 'package:kinsey/ui/pages/style/Template.dart';

class SlaveryTen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 54;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: (wt / 390) * 10,
          ),
          Container(
            height: template.textContainerheight(),
            width: template.textContainerwidth(),
            child: Padding(
              padding: EdgeInsets.only(
                top: (ht / 300) * 15,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "A slave carrying her fate in her hands, 1854",
                    style: template.textheaderStyle(),
                    maxLines: 2,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    "A. M. F. Crawford",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "Printed paper",
                    style: template.bodyTextStyle(),
                  ),
                  Text(
                    "19 ¾ x 12 ½	 in.",
                    style: template.bodyTextStyle(),
                  ),
                  Flexible(
                    child: Text(
                      "\nThis letter documents the commodification of blacks in stark terms. Frances, the young girl carrying her fate in her hands but helpless to change it, is being sold away from her family so that her owner can use the proceeds for other purchases. The owner, meanwhile, though aware of Frances’ abilities and value, is entirely concerned with her own economic and emotional circumstances:\n\n“This will be handed you by my servant Frances. I am told that it is useless to give the capabilities of a servant, that it depends altogather [sic] on there [sic] personal appearance; be that as it may. I say positively that she is the finest chamber-maid I have ever seen in my life, she is a good washer, but at house cleaning she has perfect slight of hand. She is 17teen [sic] years old the eleventh of this month.\n\n“She does not know that she is to be sold, I couldn’t tell her; I own all her family and the leave-taking would be so distressing that I could not. Please say to her that that was my reason, and that I was compelled to sell her to pay for the horses that I have bought, and to build my stable. I believe I have said all that is necessary, but I am so nervous that I hardly know what I have written. Respectfully Yours AMF Crawford”",
                      style: template.bodyTextStyle(),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  SizedBox(
                    height: (ht / 300) * 5,
                  ),
                  Container(
                    height: (ht / 300) * 15,
                    width: template.textContainerwidth(),
                    child: FittedBox(
                      child: AudioViewer(url: "assets/audio/slavery10.mp3"),
                      fit: BoxFit.contain,
                    ),
                  ),
                  Text(
                    'Narrated by: Hattie Winston',
                    style: TextStyle(fontSize: (ht / 300) * 5),
                  ),
                  SizedBox(
                    height: (ht / 300) * 14,
                  ),
                  Container(
                      width: template.textContainerwidth() * 3,
                      child: template.footerLeftText())
                ],
              ),
            ),
          ),
          Container(
            height: (ht / 300) * 260,
            width: (wt / 390) * 250,
            child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GalleryView(
                                url: "assets/images/slavery10.jpg",
                              )));
                },
                child: Image.asset(
                  "assets/images/slavery10.jpg",
                )),
          ),
          SizedBox(height: (ht / 300) * 5)
        ],
      ),
    ));
  }
}
