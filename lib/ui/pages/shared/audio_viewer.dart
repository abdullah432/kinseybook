import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

class AudioViewer extends StatefulWidget {
  final String url;
  AudioViewer({this.url});
  @override
  _AudioViewerState createState() => _AudioViewerState();
}

class _AudioViewerState extends State<AudioViewer> {
  AssetsAudioPlayer _assetAudioPlayer = AssetsAudioPlayer();
  bool playing = false;
  int progress = 0;
  int totalValue = 1;
  @override
  void initState() {
    //  implement initState
    super.initState();
  }

  @override
  void dispose() {
    // implement dispose
    _assetAudioPlayer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 45;
    return Container(
      width: 500,
      height: 60,
      decoration: BoxDecoration(
          color: Colors.black, borderRadius: BorderRadius.circular(5)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: IconButton(
                icon: Icon(
                  Icons.refresh,
                  color: Colors.white,
                  size: 40,
                ),
                onPressed: () {
                  _assetAudioPlayer.open(Audio(widget.url));
                  setState(() {
                    playing = true;
                  });
                }),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
            child: IconButton(
              icon: Icon(
                playing ? Icons.pause : Icons.play_arrow,
                color: Colors.white,
                size: 40,
              ),
              onPressed: () {
                if (!_assetAudioPlayer.isPlaying.value) {
                  if (_assetAudioPlayer.current.value == null) {
                    _assetAudioPlayer.open(Audio(widget.url));
                    _assetAudioPlayer.current.listen((data) {
                      setState(() {
                        totalValue = data.audio.duration.inSeconds;
                      });
                    });
                    _assetAudioPlayer.playlistFinished.listen((play) {
                      if (play) {
                        setState(() {
                          playing = false;
                          progress = 0;
                        });
                      }
                    });
                    _assetAudioPlayer.currentPosition.listen((data) {
                      setState(() {
                        progress = data.inSeconds;
                      });
                    });
                    setState(() {
                      playing = true;
                    });
                  } else {
                    _assetAudioPlayer.play();
                    setState(() {
                      playing = true;
                    });
                  }
                } else {
                  _assetAudioPlayer.pause();
                  setState(() {
                    playing = false;
                  });
                }
              },
            ),
          ),
          Expanded(
              flex: 1,
              child: FlutterSlider(
                  disabled: !playing,
                  trackBar: FlutterSliderTrackBar(
                    activeTrackBar: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.white),
                  ),
                  min: 0,
                  max: totalValue.toDouble(),
                  handler: FlutterSliderHandler(child: SizedBox()),
                  onDragCompleted: (index, a, b) {
                    setState(() {
                      print(a);
                      progress = a.toInt();
                    });
                    _assetAudioPlayer.seek(Duration(seconds: a.toInt()));
                  },
                  values: [progress.toDouble()])),
        ],
      ),
    );
  }
}
