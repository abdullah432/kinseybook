import 'package:chewie/chewie.dart';
import "package:flutter/material.dart";
import 'package:video_player/video_player.dart';

class VideoView extends StatefulWidget {
  final String videoLink;

  VideoView({this.videoLink});
  @override
  _VideoViewState createState() => _VideoViewState();
}

class _VideoViewState extends State<VideoView> {
  VideoPlayerController videoPlayerController;

  ChewieController chewieController;
  void dispose() {
    videoPlayerController.dispose();
    chewieController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    videoPlayerController = VideoPlayerController.asset(widget.videoLink);
    chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      aspectRatio: 3 / 2,
      autoPlay: true,
      showControls: true,
    );
    videoPlayerController.addListener((){
      if(videoPlayerController.value.position==videoPlayerController.value.duration){
        if(chewieController.isFullScreen){
          Navigator.pop(context);
          Navigator.pop(context);
        }
        else{
          Navigator.pop(context);
        }
      }
    });


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: <Widget>[
          Center(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.6,
              width: MediaQuery.of(context).size.width * 0.7,
              child: Chewie(
                controller: chewieController,
              ),
            ),
          ),
          Positioned(
            right: 15,
            top: 30,
            child: GestureDetector(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                ),
                padding: EdgeInsets.all(5),
                child: Icon(
                  Icons.close,
                  color: Colors.black,
                  size: 30,
                ),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }
}
