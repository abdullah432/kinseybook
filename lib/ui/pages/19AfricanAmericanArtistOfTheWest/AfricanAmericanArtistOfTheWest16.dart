import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

// image on top, text on bottom
class AfricanAmericanArtistOfTheWest16 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "The Gambler, 1990",
      "William Sylvester Carter",
      "Oil on canvas",
      "18 ½ x 16 ½ in.",
      "William Sylvester Carter (1909-1996)",
      "Carter, a nationally recognized and award-winning artist, lived on Chicago’s south side for most of his life and contributed extensively to the African American art culture of the city. Carter mastered all painting mediums to convey a poignant visual journal of a people confined within the American closet. Most of all, Carter leads the viewer to a profound appreciation of the irony and complexity of their experience, balanced by admiration for their cohesive urban spirit and the vibrant hope that tomorrow will be brighter.",
    ];
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 6,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/african_american_artists_of_the_west_15.jpg",)));
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/african_american_artists_of_the_west_15.jpg")),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Column(
                children: <Widget>[
                  AutoSizeText(
                    textFiles[0],
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: "MyriadPro-BoldIt"),
                    presetFontSizes: presetFonts,
                  ),
                  AutoSizeText(
                    textFiles[1],
                    presetFontSizes: presetFonts,
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  AutoSizeText(
                    textFiles[2],
                    presetFontSizes: presetFonts,
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  AutoSizeText(
                    textFiles[3],
                    presetFontSizes: presetFonts,
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height / 2,
                                width: MediaQuery.of(context).size.width / 2,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          AutoSizeText(
                                            textFiles[4],
                                            presetFontSizes: presetFonts,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontFamily: "Auto2-Regular",
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Expanded(
                                              child: AutoSizeText(
                                            textFiles[5],
                                            presetFontSizes: presetFonts,
                                            textAlign: TextAlign.justify,
                                            style: TextStyle(
                                                fontFamily: "Auto2-Regular",
                                            ),
                                          ))
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.clear,
                                          color: Colors.black,
                                          size: 30,
                                        ),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                      top: 10,
                                      right: 10,
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                    child: Container(
                        height: 80,
                        width: 80,
                        child: Image.asset(
                          "assets/images/bio.png",
                          scale: 3,
                        )),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
