import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';
import 'package:kinsey/ui/pages/shared/video_view.dart';

class AfricanAmericanArtistOfTheWest5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Old Maasai Woman, 1986",
      "Ed Dwight",
      "Bronze",
      "38 x 14 x 10 ½ in.",
      "Ed Dwight (1933– )",
      "Dwight’s first major commission was to create a sculpture of George Brown, Colorado’s first black Lt. Governor. For the Colorado Centennial Commission, Dwight was asked to produce 30 bronzes depicting the contributions of African Americans to the expansion of the West. In the series “Jazz: An American Art Form,” Dwight explored the musical contributions of jazz to the fabric of American life and culture. Dwight has been involved with many public art projects, including monuments in Detroit and in Windsor, Canada, dedicated to the Underground Railroad. His largest memorial work, honoring Martin Luther King, Jr., is installed in Denver. His autobiography, Soaring on the Wings of a Dream, was recently published by Third World Press.",
    ];
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  AutoSizeText(
                    textFiles[0],
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: "MyriadPro-BoldIt"),
                    presetFontSizes: presetFonts,
                  ),
                  AutoSizeText(
                    textFiles[1],
                    presetFontSizes: presetFonts,
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  AutoSizeText(
                    textFiles[2],
                    presetFontSizes: presetFonts,
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  AutoSizeText(
                    textFiles[3],
                    presetFontSizes: presetFonts,
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  SizedBox(
                    height:20,
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height / 2,
                                width: MediaQuery.of(context).size.width / 2,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          AutoSizeText(
                                            textFiles[4],
                                            presetFontSizes: presetFonts,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontFamily: "Auto2-Regular",
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Expanded(
                                              child: AutoSizeText(
                                            textFiles[5],
                                            presetFontSizes: presetFonts,
                                            textAlign: TextAlign.justify,
                                            style: TextStyle(
                                                fontFamily: "Auto2-Regular",
                                            ),
                                          ))
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.clear,
                                          color: Colors.black,
                                          size: 30,
                                        ),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                      top: 10,
                                      right: 10,
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                    child: Container(
                        height: 80,
                        width: 80,
                        child: Image.asset(
                          "assets/images/bio.png",
                          scale: 3,
                        )),
                  ),
                  SizedBox(height: 20,),
                      IconButton(
                        icon: Icon(
                          Icons.play_circle_outline,
                          color: Colors.black,
                        ),
                        iconSize: 80,
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VideoView(
                                        videoLink:
                                            "assets/videos/african_american_artists_of_the_west_01.mp4",
                                      )));
                        },
                      )
                ],
              ),
            ),
            Expanded(
              flex: 6,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/african_american_artists_of_the_west_04.png",)));
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 70),
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                      fit: BoxFit.fitHeight,
                      child: Image.asset("assets/images/african_american_artists_of_the_west_04.png")),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
