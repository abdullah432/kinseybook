import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kinsey/ui/pages/shared/gallery_view.dart';

// image on top, text on bottom
class AfricanAmericanArtistOfTheWest2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<double> presetFonts = [28, 24, 20, 16, 14];
    final textFiles = [
      "Kenyatta Center, Nairobi, Kenya\nHarambi Street, 1987-1991",
      "John Biggers",
      "Pen and pencil on paper",
      "10 ¼ x 20 ¼ in.",
      "John Biggers (1924–2001)",
      "Biggers attended Hampton Institute, where his talent was encouraged by Viktor Loweb-feld. In 1943, Biggers’ mural Dying Soldier was featured in the exhibition “Young Negro Art” at the Museum of Modern Art in New York. Biggers was influenced by Harlem Renaissance thinkers and artists, including W. E. B. DuBois and Charles White, who became his mentor. Biggers enrolled at Pennsylvania State University and was awarded a doctorate in 1954. He was the founding chairman of the art department at Texas Southern University and initiated a mural program there. In 1957, Biggers received a UNESCO fellowship to study art in Africa, the basis of Ananse: The Web of Life in Africa, a visual diary of his experiences there.",
    ];
    return Padding(
      padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 6,
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>GalleryView(url: "assets/images/african_american_artists_of_the_west_01.jpg",)));
                },
                child: Container(
                  padding: EdgeInsets.only(top: 30),
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                      fit: BoxFit.contain,
                      child: Image.asset("assets/images/african_american_artists_of_the_west_01.jpg")),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Column(
                children: <Widget>[
                  AutoSizeText(
                    textFiles[0],
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontFamily: "MyriadPro-BoldIt"),
                    presetFontSizes: presetFonts,
                  ),
                  AutoSizeText(
                    textFiles[1],
                    presetFontSizes: presetFonts,
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  AutoSizeText(
                    textFiles[2],
                    presetFontSizes: presetFonts,
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  AutoSizeText(
                    textFiles[3],
                    presetFontSizes: presetFonts,
                    style: TextStyle(fontFamily: "Auto2-Regular"),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  GestureDetector(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Dialog(
                              child: Container(
                                height: MediaQuery.of(context).size.height / 2,
                                width: MediaQuery.of(context).size.width / 2,
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 20),
                                      child: Column(
                                        children: <Widget>[
                                          AutoSizeText(
                                            textFiles[4],
                                            presetFontSizes: presetFonts,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontFamily: "Auto2-Regular",
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Expanded(
                                              child: AutoSizeText(
                                            textFiles[5],
                                            presetFontSizes: presetFonts,
                                            textAlign: TextAlign.justify,
                                            style: TextStyle(
                                                fontFamily: "Auto2-Regular",
                                            ),
                                          ))
                                        ],
                                      ),
                                    ),
                                    Positioned(
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.clear,
                                          color: Colors.black,
                                          size: 30,
                                        ),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                      top: 10,
                                      right: 10,
                                    )
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                    child: Container(
                        height: 80,
                        width: 80,
                        child: Image.asset(
                          "assets/images/bio.png",
                          scale: 3,
                        )),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
