import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';


class AfricanAmericanArtistOfTheWestOne extends StatefulWidget {
  @override
  _AfricanAmericanArtistOfTheWestOneState createState() => _AfricanAmericanArtistOfTheWestOneState();
}

class _AfricanAmericanArtistOfTheWestOneState extends State<AfricanAmericanArtistOfTheWestOne> {
  final textFiles=["Art is the only thing you cannot punch a button for. You must do it the old-fashioned way. Stay up and really burn the midnight oil. There are no compromises.\n\n— Leontyne Price\n\nBernard and Shirley Kinsey moved to Los Angeles in 1967 and bought their first house in 1971. Their son Khalil was born soon after. As their careers flourished, so did their disposable income, much of which they saved. However, early in their marriage they discovered the joy of collecting art and artifacts, which helped them document their lives together and also served as reminders of their beginnings, goals, and dreams.\n\nThrough hard work, determination, and strong faith, Bernard and Shirley Kinsey prospered and found themselves looking backward, seeking to coalesce their experiences, travels, and memories through their collection. Now surrounded by ocean vistas and canyon landscapes, the Kinseys have embraced their adopted Southern California home and have forged a special bond with black artists whose lives and work in the West resonate with their spirit.",
    "African Americans have long been able to explore their creativity in the West, but often without the recognition they deserve. A 1933 Harmon Foundation catalogue claimed that Sargent Johnson was the only recognized African American artist in California. Johnson, like Grafton Tyler Brown before him, skillfully melded craft and fine art. By the time that Thelma Johnson Streat joined Johnson on Works Progress Administration projects in the Bay Area, African American heritage was recognized as a primary source for realizing a middle ground in the debate between high and low culture. California gave black artists the freedom to investigate the relationship between their identity and their art.\n\nThroughout the 20th century, black artists in California continued to challenge conventional notions of fine art and evolve new ones. The assemblage movement of the 1960s placed California artists such as Betye Saar, David Hammonds, and Noah Purifoy on the international stage. Artists active in the West, notably Charles White, created a distinctive representation of black experience in America and new art forms. They inspired a generation of African American artists to explore the cultural and visual landscape of the region, producing work richly layered with meaning.",
    "The Kinseys have personally met many artists represented in their collection, through travel in the West and through California gallery owners, Thelma Harris and Alitash Kebede. Kebede introduced them to William Pajaud, among others, who motivated Shirley to champion living black artists whenever she could. They met John Biggers, who has fostered the success of many African American art students, as have a number of artists represented in the Kinsey Collection. The Kinseys commissioned John Biggers to travel to Kenya for the painting pictured on the opposite page, which was then given as a gift to Florida A&M. The artists represented in the following pages are living, working, or were trained in California, Colorado, Texas, and throughout the West."
  ];
  static bool listView=false;
  ScrollController listViewController=ScrollController();
  static double fontSize=50;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: MediaQuery.of(context).size.height*0.04, horizontal: MediaQuery.of(context).size.width*0.05),
        child: listView?Stack(
          children: <Widget>[
            ListView.builder(
                controller: listViewController,
                itemCount: textFiles.length,
                itemBuilder: (BuildContext context,index){
                  if(index==0){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                      child: AutoSizeText.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: "African American Artists of the West\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                            TextSpan(text: textFiles[0])
                          ],

                        ),
                        presetFontSizes: [fontSize],
                        textAlign: TextAlign.justify,
                      ),
                    );
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                    child: AutoSizeText(textFiles[index],minFontSize: fontSize,textAlign: TextAlign.justify),
                  );
                }),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: MediaQuery.of(context).size.height*0.1,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(onPressed: (){

                        if(listViewController.position.minScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset-listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },icon: Icon(Icons.arrow_back_ios,color: Colors.blue,size: 40,),),
                      Row(children: <Widget>[
                        IconButton(icon: Icon(Icons.zoom_out,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            fontSize=30;
                          });
                        }),
                        IconButton(icon: Icon(Icons.close,color: Colors.blue,size: 40,), onPressed: (){
                          setState(() {
                            listView=false;
                          });
                        })
                      ],),
                      IconButton(icon: Icon(Icons.arrow_forward_ios,color: Colors.blue,size: 40,),onPressed: (){
                        if(listViewController.position.maxScrollExtent==listViewController.offset){
                          print("on the end");
                        }
                        else{
                          listViewController.animateTo(listViewController.offset+listViewController.position.maxScrollExtent/7, duration: Duration(milliseconds: 500), curve: Curves.easeIn);
                        }
                      },)
                    ],),
                ))
          ],
        ):Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 30,),
                  Container(width:100,height: 50,child: Stack(children: <Widget>[Positioned(child: GestureDetector(
                      onTap: (){
                        setState(() {
                          fontSize=50;
                          listView=true;
                        });
                      },
                      child: Image.asset("assets/images/magnify.png",scale: 4,)),left: -30,),],overflow: Overflow.visible,)),
                  AutoSizeText.rich(
                    TextSpan(
                      children: [
                        TextSpan(text: "African American Artists of the West\n\n",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24)),
                      ],

                    ),
                    textAlign: TextAlign.justify,
                  ),
                  Expanded(child: AutoSizeText(textFiles[0],presetFontSizes: [22,16,14],textAlign: TextAlign.justify,))
                ],
              ),
            ),),
            Expanded(child:Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(textFiles[1],presetFontSizes: [22,18,16,14],textAlign: TextAlign.justify,),
            )),
            Expanded(child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: AutoSizeText(textFiles[2],presetFontSizes: [20,16,14],textAlign: TextAlign.justify,)),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
