import 'dart:convert';
import 'package:kinsey/ui/pages/style/Template.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:kinsey/ui/pages/18SouthernHome/SouthernHome1.dart';
import 'package:kinsey/ui/pages/18SouthernHome/SouthernHome2.dart';
import 'package:kinsey/ui/pages/18SouthernHome/SouthernHome3.dart';
import 'package:kinsey/ui/pages/18SouthernHome/SouthernHome4.dart';
import 'package:kinsey/ui/pages/18SouthernHome/SouthernHome5.dart';
import 'package:kinsey/ui/pages/18SouthernHome/SouthernHome6.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest10.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest11.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest12.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest13.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest14.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest15.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest16.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest17.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest18.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest2.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest3.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest4.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest5.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest6.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest7.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest8.dart';
import 'package:kinsey/ui/pages/19AfricanAmericanArtistOfTheWest/AfricanAmericanArtistOfTheWest9.dart';
import 'package:kinsey/ui/pages/20TheAmericas/TheAmericas1.dart';
import 'package:kinsey/ui/pages/20TheAmericas/TheAmericas2.dart';
import 'package:kinsey/ui/pages/20TheAmericas/TheAmericas3.dart';
import 'package:kinsey/ui/pages/20TheAmericas/TheAmericas4.dart';
import 'package:kinsey/ui/pages/20TheAmericas/TheAmericas5.dart';
import 'package:kinsey/ui/pages/20TheAmericas/TheAmericas6.dart';
import 'package:kinsey/ui/pages/21LivingWithArt/LivingWithArt1.dart';
import 'package:kinsey/ui/pages/21LivingWithArt/LivingWithArt2.dart';
import 'package:kinsey/ui/pages/21LivingWithArt/LivingWithArt3.dart';
import 'package:kinsey/ui/pages/21LivingWithArt/LivingWithArt4.dart';
import 'package:kinsey/ui/pages/22AReDiscoveringAmerica/ReDiscoveringAmerica1.dart';
import 'package:kinsey/ui/pages/22AReDiscoveringAmerica/ReDiscoveringAmerica2.dart';
import 'package:kinsey/ui/pages/23Bigbliography/Bibliography1.dart';
import 'package:kinsey/ui/pages/23Bigbliography/Bibliography2.dart';
import 'package:kinsey/ui/pages/23Bigbliography/Bibliography3.dart';
import 'package:kinsey/ui/pages/23Bigbliography/Bibliography4.dart';
import 'package:kinsey/ui/pages/24NaratorBios/NaratorBios1.dart';
import 'package:kinsey/ui/pages/24NaratorBios/NaratorBios2.dart';
import 'package:kinsey/ui/pages/24NaratorBios/NaratorBios3.dart';
import 'package:kinsey/ui/pages/25Acknowledgements/Acknowledgements1.dart';
import 'package:kinsey/ui/pages/25Acknowledgements/Acknowledgements2.dart';
import 'package:kinsey/ui/pages/25Acknowledgements/Acknowledgements3.dart';
import 'package:kinsey/ui/pages/26FriendsAndFamily/FamilyAndFriends1.dart';
import 'package:kinsey/ui/pages/26FriendsAndFamily/FamilyAndFriends2.dart';
import 'package:kinsey/ui/pages/afterthoughts/afterthoughts_1.dart';
import 'package:kinsey/ui/pages/afterthoughts/afterthoughts_2.dart';
import 'package:kinsey/ui/pages/american_war/americanwar_1.dart';
import 'package:kinsey/ui/pages/american_war/americanwar_10.dart';
import 'package:kinsey/ui/pages/american_war/americanwar_2.dart';
import 'package:kinsey/ui/pages/american_war/americanwar_3.dart';
import 'package:kinsey/ui/pages/american_war/americanwar_4.dart';
import 'package:kinsey/ui/pages/american_war/americanwar_5.dart';
import 'package:kinsey/ui/pages/american_war/americanwar_6.dart';
import 'package:kinsey/ui/pages/american_war/americanwar_7.dart';
import 'package:kinsey/ui/pages/american_war/americanwar_8.dart';
import 'package:kinsey/ui/pages/american_war/americanwar_9.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_1.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_10.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_11.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_12.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_13.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_14.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_2.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_3.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_4.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_5.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_6.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_7.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_8.dart';
import 'package:kinsey/ui/pages/birth_aesthetic/birth_aesthetic_9.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_1.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_10.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_11.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_12.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_13.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_14.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_2.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_3.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_4.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_5.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_6.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_7.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_8.dart';
import 'package:kinsey/ui/pages/early_genius/early_genius_9.dart';
import 'package:kinsey/ui/pages/european_influences/european_influence_3.dart';
import 'package:kinsey/ui/pages/european_influences/european_influences_1.dart';
import 'package:kinsey/ui/pages/european_influences/european_influences_2.dart';
import 'package:kinsey/ui/pages/european_influences/european_influences_4.dart';
import 'package:kinsey/ui/pages/european_influences/european_influences_5.dart';
import 'package:kinsey/ui/pages/european_influences/european_influences_6.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_1.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_10.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_11.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_12.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_13.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_14.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_15.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_16.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_17.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_18.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_19.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_2.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_20.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_21.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_22.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_23.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_24.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_25.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_26.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_27.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_28.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_29.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_3.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_30.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_31.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_32.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_33.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_34.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_35.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_4.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_5.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_6.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_7.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_8.dart';
import 'package:kinsey/ui/pages/forging_freedom/forging_freedom_9.dart';
import 'package:kinsey/ui/pages/generation_masters/generation_masters_1.dart';
import 'package:kinsey/ui/pages/generation_masters/generation_masters_10.dart';
import 'package:kinsey/ui/pages/generation_masters/generation_masters_11.dart';
import 'package:kinsey/ui/pages/generation_masters/generation_masters_2.dart';
import 'package:kinsey/ui/pages/generation_masters/generation_masters_3.dart';
import 'package:kinsey/ui/pages/generation_masters/generation_masters_4.dart';
import 'package:kinsey/ui/pages/generation_masters/generation_masters_5.dart';
import 'package:kinsey/ui/pages/generation_masters/generation_masters_6.dart';
import 'package:kinsey/ui/pages/generation_masters/generation_masters_7.dart';
import 'package:kinsey/ui/pages/generation_masters/generation_masters_8.dart';
import 'package:kinsey/ui/pages/generation_masters/generation_masters_nine.dart';
import 'package:kinsey/ui/pages/heartandsoul/heartandsoul_1.dart';
import 'package:kinsey/ui/pages/heartandsoul/heartandsoul_2.dart';
import 'package:kinsey/ui/pages/heartandsoul/heartandsoul_4.dart';
import 'package:kinsey/ui/pages/heartandsoul/heartandsoul_5.dart';
import 'package:kinsey/ui/pages/heartandsoul/heartandsoul_6.dart';
import 'package:kinsey/ui/pages/introduction%20pages/introduction_1.dart';
import 'package:kinsey/ui/pages/landing%20pages/cover1.dart';
import 'package:kinsey/ui/pages/landing%20pages/cover2.dart';
import 'package:kinsey/ui/pages/landing%20pages/coverb1.dart';
import 'package:kinsey/ui/pages/landing%20pages/coverb2.dart';
import 'package:kinsey/ui/pages/forward%20pages/forward_2.dart';
import 'package:kinsey/ui/pages/landing%20pages/howto.dart';
import 'package:kinsey/ui/pages/forward%20pages/forward_1.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage2.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_1.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_10.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_11.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_12.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_13.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_14.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_15.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_16.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_17.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_3.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_4.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_5.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_6.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_7.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_8.dart';
import 'package:kinsey/ui/pages/lineage%20pages/lineage_9.dart';
import 'package:kinsey/ui/pages/reflections/reflection_1.dart';
import 'package:kinsey/ui/pages/reflections/reflection_2.dart';
import 'package:kinsey/ui/pages/slavery/slavery_1.dart';
import 'package:kinsey/ui/pages/slavery/slavery_10.dart';
import 'package:kinsey/ui/pages/slavery/slavery_11.dart';
import 'package:kinsey/ui/pages/slavery/slavery_12.dart';
import 'package:kinsey/ui/pages/slavery/slavery_13.dart';
import 'package:kinsey/ui/pages/slavery/slavery_14.dart';
import 'package:kinsey/ui/pages/slavery/slavery_15.dart';
import 'package:kinsey/ui/pages/slavery/slavery_16.dart';
import 'package:kinsey/ui/pages/slavery/slavery_17.dart';
import 'package:kinsey/ui/pages/slavery/slavery_18.dart';
import 'package:kinsey/ui/pages/slavery/slavery_19.dart';
import 'package:kinsey/ui/pages/slavery/slavery_2.dart';
import 'package:kinsey/ui/pages/slavery/slavery_20.dart';
import 'package:kinsey/ui/pages/slavery/slavery_3.dart';
import 'package:kinsey/ui/pages/slavery/slavery_4.dart';
import 'package:kinsey/ui/pages/slavery/slavery_5.dart';
import 'package:kinsey/ui/pages/slavery/slavery_6.dart';
import 'package:kinsey/ui/pages/slavery/slavery_7.dart';
import 'package:kinsey/ui/pages/slavery/slavery_8.dart';
import 'package:kinsey/ui/pages/slavery/slavery_9.dart';
import 'package:kinsey/ui/pages/translating_inspiration/translating_inspiration_1.dart';
import 'package:kinsey/ui/pages/translating_inspiration/translating_inspiration_2.dart';
import 'package:kinsey/ui/pages/understanding_pages/understanding_1.dart';
import 'package:kinsey/ui/pages/understanding_pages/understanding_2.dart';
import 'package:kinsey/ui/pages/understanding_pages/understanding_3.dart';
import 'package:kinsey/ui/pages/understanding_pages/understanding_4.dart';
import 'package:kinsey/ui/pages/women_in_art/women_in_art_1.dart';
import 'package:kinsey/ui/pages/women_in_art/women_in_art_2.dart';
import 'package:kinsey/ui/pages/women_in_art/women_in_art_3.dart';
import 'package:kinsey/ui/pages/women_in_art/women_in_art_4.dart';
import 'package:kinsey/ui/pages/women_in_art/women_in_art_5.dart';
import 'package:kinsey/ui/pages/women_in_art/women_in_art_6.dart';
import 'package:kinsey/ui/pages/women_in_art/women_in_art_7.dart';
import 'package:kinsey/ui/pages/women_in_art/women_in_art_8.dart';
import 'package:kinsey/ui/pages/women_in_art/women_in_art_9.dart';
import 'package:overlay_container/overlay_container.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:intl/intl.dart';

import 'pages/heartandsoul/heartandsoul_3.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final FocusScopeNode _focusScopeNOde = FocusScopeNode();
  List<String> bookmarks = [];
  List<ContentData> dataList = ContentData.getAllData();
  PageController controller;
  Widget contents;
  double sliderValue = 1;
  bool showGridView = false;
  bool showControls = false;
  List<Widget> _pages = [];
  Database database;
  List<Map> notesData = [];
  bool showNotesBookMarks = false;
  bool showSearchedArea = false;
  int activated = 0;
  GlobalKey<FormState> _formKey = GlobalKey();
  List<Map> searchedNotesData = [];
  TextEditingController _titleController = TextEditingController();
  TextEditingController _noteController = TextEditingController();
  List<Map> searchedResult = [];
  List _pagesSearchData;

  getPagesDataForSearch() async {
    _pagesSearchData =
        json.decode(await rootBundle.loadString("assets/data/data.json"));
  }

  getSearchResults(String val) {
    searchedResult.clear();
    val = val.toLowerCase();
    if (_pagesSearchData != null) {
      _pagesSearchData.forEach((f) {
        String text = f['data'].toLowerCase();
        Map data = {};
        if (text.contains(val)) {
          print(val.allMatches(text));
          if (text.indexOf(val) > 20 &&
              text.indexOf(val) + val.length + 20 < text.length) {
            data['text'] = text.substring(
                text.indexOf(val) - 20, text.indexOf(val) + val.length + 20);
            data['page'] = f['page'];
            data['occurence'] = val.allMatches(text).length;
          } else if (text.indexOf(val) > 20) {
            data['text'] = text.substring(
                text.indexOf(val) - 20, text.indexOf(val) + val.length);
            data['page'] = f['page'];
            data['occurence'] = val.allMatches(text).length;
          } else if (f['data'].indexOf(val) + val.length + 20 <
              f['data'].length) {
            data['text'] = text.substring(
                text.indexOf(val), text.indexOf(val) + val.length + 20);
            data['page'] = f['page'];
            data['occurence'] = val.allMatches(text).length;
          } else {
            data['text'] = text.substring(
                text.indexOf(val), text.indexOf(val) + val.length);
            data['page'] = f['page'];
            data['occurence'] = val.allMatches(text).length;
          }
          setState(() {
            searchedResult.add(data);
          });
        }
      });
      print(searchedResult);
    } else {
      print("data not loaded");
    }
  }

  getBookmarkData() async {
    SharedPreferences _sharedPrefs = await SharedPreferences.getInstance();

    if (_sharedPrefs.getStringList("bookmarks") != null) {
      bookmarks = _sharedPrefs.getStringList("bookmarks");
    }
  }

  @override
  void dispose() {
    _focusScopeNOde.dispose();
    super.dispose();
  }

  getDataBaseConnection() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'kinsey.db');
    database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      // When creating the db, create the table
      await db.execute(
          'CREATE TABLE IF NOT EXISTS Notes (id INTEGER primary key autoincrement, title TEXT, note Text,page INTEGER,date TEXT)');
      print("table created");
    });
    getNotesData();
  }

  getNotesData() async {
    notesData.clear();
    List<Map> list = await database.rawQuery('SELECT * FROM Notes');
    list.forEach((f) {
      notesData.add(f);
    });
    searchedNotesData = notesData;
  }

  @override
  void initState() {
    super.initState();
    getPagesDataForSearch();
    _pages = [
      CoverOne(),
      CoverTwo(),
      HowToView(),
      CoverBOne(),
      CoverBTwo(),
      SizedBox(),
      ForwardOne(),
      ForwardTwo(),
      IntroductionOne(),
      UnderstandingOne(),
      UnderstandingTwo(),
      UnderstandingThree(),
      UnderstandingFour(),
      LineageOne(),
      LineageTwo(),
      LineageThree(),
      LineageFour(),
      LineageFive(),
      LineageSix(),
      LineageSeven(),
      LineageEight(),
      LineageNine(),
      LineageTen(),
      LineageEleven(),
      LineageTwelve(),
      LineageThirteen(),
      LineageFourteen(),
      LineageFifteen(),
      LineageSixteen(),
      LineageSeventeen(),
      EarlyGeniusOne(),
      EarlyGeniusTwo(),
      EarlyGeniusThree(),
      EarlyGeniusFour(),
      EarlyGeniusFive(),
      EarlyGeniusSix(),
      EarlyGeniusSeven(),
      EarlyGeniusEight(),
      EarlyGeniusNine(),
      EarlyGeniusTen(),
      EarlyGeniusEleven(),
      EarlyGeniusTwelve(),
      EarlyGeniusThirteen(),
      EarlyGeniusFourteen(),
      SlaveryOne(),
      SlaveryTwo(),
      SlaveryThree(),
      SlaveryFour(),
      SlaveryFive(),
      SlaverySix(),
      SlaverySeven(),
      SlaveryEight(),
      SlaveryNine(),
      SlaveryTen(),
      SlaveryEleven(),
      SlaveryTwelve(),
      SlaveryThirteen(),
      SlaveryFourteen(),
      SlaveryFifteen(),
      SlaverySixteen(),
      SlaverySeventeen(),
      SlaveryEighteen(),
      SlaveryNinteen(),
      SlaveryTwenty(),
      AmericanWarOne(),
      AmericanWarTwo(),
      AmericanWarThree(),
      AmericanWarFour(),
      AmericanWarFive(),
      AmericanWarSix(),
      AmericanWarSeven(),
      AmericanWarEight(),
      AmericanWarNine(),
      AmericanWarTen(),
      HeartAndSoulOne(),
      HeartAndSoulTwo(),
      HeartAndSoulThree(),
      HeartAndSoulFour(),
      HeartAndSoulFive(),
      HeartAndSoulSix(),
      ForgingFreedomOne(),
      ForgingFreedomTwo(),
      ForgingFreedomThree(),
      ForgingFreedomFour(),
      ForgingFreedomFive(),
      ForgingFreedomSix(),
      ForgingFreedomSeven(),
      ForgingFreedomEight(),
      ForgingFreedomNine(),
      ForgingFreedomTen(),
      ForgingFreedomEleven(),
      ForgingFreedomTwelve(),
      ForgingFreedomThirteen(),
      ForgingFreedomFourteen(),
      ForgingFreedomFifteen(),
      ForgingFreedomSixteen(),
      ForgingFreedomSeventeen(),
      ForgingFreedomEighteen(),
      ForgingFreedomNinteen(),
      ForgingFreedomTwenty(),
      ForgingFreedomTwentyOne(),
      ForgingFreedomTwentyTwo(),
      ForgingFreedomTwentyThree(),
      ForgingFreedomTwentyFour(),
      ForgingFreedomTwentyFIve(),
      ForgingFreedomTwentySix(),
      ForgingFreedomTwentySeven(),
      ForgingFreedomTwentyEight(),
      ForgingFreedomTwentyNine(),
      ForgingFreedomThirty(),
      ForgingFreedomThirtyOne(),
      ForgingFreedomThirtyTwo(),
      ForgingFreedomThirtyThree(),
      ForgingFreedomThirtyFour(),
      ForgingFreedomThirtyFive(),
      ReflectionOne(),
      ReflectionTwo(),
      TranslatingInspirationOne(),
      TranslatingInspirationTwo(),
      BirthAestheticOne(),
      BirthAestheticTwo(),
      BirthAestheticThree(),
      BirthAestheticFour(),
      BirthAestheticFive(),
      BirthAestheticSix(),
      BirthAestheticSeven(),
      BirthAestheticEight(),
      BirthAestheticNine(),
      BirthAestheticTen(),
      BirthAestheticEleven(),
      BirthAestheticTwelve(),
      BirthAestheticThirteen(),
      BirthAestheticFourteen(),
      EuropeanInfluenceOne(),
      EuropeanInfluenceTwo(),
      EuropeanInfluenceThree(),
      EuropeanInfluenceFour(),
      EuropeanInfluenceFive(),
      EuropeanInfluenceSix(),
      GenerationMasterOne(),
      GenerationMasterTwo(),
      GenerationMasterThree(),
      GenerationMastersFour(),
      GenerationMastersFive(),
      GenerationMastersSix(),
      GenerationMastersSeven(),
      GenerationMastersEight(),
      GenerationMastersNine(),
      GenerationMAstersTen(),
      GenerationMastersEleven(),
      WomenInArtOne(),
      WomenInArtTwo(),
      WomenInArtThree(),
      WomenInArtFour(),
      WomenInArtFive(),
      WomenInArtSix(),
      WomenInArtSeven(),
      WomenInArtEight(),
      WomenInArtNine(),
      SouthernHome(),
      SouthernHome2(),
      SouthernHome3(),
      SouthernHome4(),
      SouthernHome5(),
      SouthernHome6(),
      AfricanAmericanArtistOfTheWestOne(),
      AfricanAmericanArtistOfTheWest2(),
      AfricanAmericanArtistOfTheWest3(),
      AfricanAmericanArtistOfTheWest4(),
      AfricanAmericanArtistOfTheWest5(),
      AfricanAmericanArtistOfTheWest6(),
      AfricanAmericanArtistOfTheWest7(),
      AfricanAmericanArtistOfTheWest8(),
      AfricanAmericanArtistOfTheWest9(),
      AfricanAmericanArtistOfTheWest10(),
      AfricanAmericanArtistOfTheWest11(),
      AfricanAmericanArtistOfTheWest12(),
      AfricanAmericanArtistOfTheWest13(),
      AfricanAmericanArtistOfTheWest14(),
      AfricanAmericanArtistOfTheWest15(),
      AfricanAmericanArtistOfTheWest16(),
      AfricanAmericanArtistOfTheWest17(),
      AfricanAmericanArtistOfTheWest18(),
      AfterThoughtsOne(),
      AfterThoughtsTwo(),
      TheAmericas1(),
      TheAmericas2(),
      TheAmericas3(),
      TheAmericas4(),
      TheAmericas5(),
      TheAmericas6(),
      LivingWithArt1(),
      LivingWithArt2(),
      LivingWithArt3(),
      LivingWithArt4(),
      ReDiscoveringAmerica1(),
      ReDiscoveringAmerica2(),
      Bibliography1(),
      Bibliography2(),
      Bibliography3(),
      Bibliography4(),
      NaratorBios1(),
      NaratorBios2(),
      NaratorBios3(),
      Acknowledgements1(),
      Acknowledgements2(),
      Acknowledgements3(),
      FamilyAndFriends1(),
      FamilyAndFriends2(),
    ];
    controller = PageController();
    controller.addListener(() {
      setState(() {
        sliderValue = controller.page + 1;
      });
    });

    getBookmarkData();
    getDataBaseConnection();
  }

  Widget getContentPage(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Center(
        child: Container(
      height: ht,
      width: wt,
      decoration: template.viewportBorder(),
      child: Padding(
        padding: EdgeInsets.only(
            left: template.viewWidth() * 5, right: template.viewWidth() * 5),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: template.viewHeight() * 10,
            ),
            Padding(
              padding: EdgeInsets.only(left: template.viewWidth() * 10),
              child: Container(
                width: wt,
                child: AutoSizeText(
                  "CONTENTS",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: template.viewHeight() * 25,
                      letterSpacing: template.viewWidth() * 19,
                      color: Colors.grey),
                ),
              ),
            ),
            Container(
                width: wt,
                height: template.viewHeight() * 250,
                child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio:
                            template.viewHeight() * 10 / template.viewHeight()),
                    itemCount: dataList.length,
                    itemBuilder: (BuildContext context, index) {
                      return Container(
                        child: GestureDetector(
                          onTap: () {
                            controller.animateToPage(dataList[index].number - 1,
                                duration: Duration(seconds: 2),
                                curve: Curves.easeIn);
                          },
                          child: ListTile(
                            title: Text(
                              dataList[index].title,
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Auto2-Bold',
                                fontSize: (ht / 300) * 6,
                              ),
                              maxLines: 2,
                            ),
                            trailing: Text(
                              dataList[index].number.toString(),
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'Auto2-Bold',
                                fontSize: (ht / 300) * 6,
                              ),
                            ),
                          ),
                        ),
                      );
                    }))
          ],
        ),
      ),
    ));
  }

  getShowDialog(BuildContext context, bool hasData, data) {
    if (hasData) {
      _titleController.text = data['title'];
      _noteController.text = data['note'];
    } else {
      _titleController.text = "";
      _noteController.text = "";
    }

    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Dialog(
              child: Container(
                height: MediaQuery.of(context).size.height * 0.7,
                width: MediaQuery.of(context).size.width * 0.5,
                color: Colors.white,
                child: Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            hasData
                                ? FlatButton(
                                    onPressed: () async {
                                      int count = await database.rawUpdate(
                                          'UPDATE Notes SET title = ?, note = ? WHERE id = ?',
                                          [
                                            _titleController.text,
                                            _noteController.text,
                                            data['id']
                                          ]);
                                      print('updated: $count');
                                      getNotesData();
                                      Navigator.pop(context);
                                    },
                                    child: Text(
                                      "Save",
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold),
                                    ))
                                : FlatButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text(
                                      "Close",
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold),
                                    )),
                            hasData
                                ? FlatButton(
                                    onPressed: () async {
                                      int count = await database.rawDelete(
                                          'DELETE FROM Notes WHERE id = ?',
                                          [data['id']]);
                                      print(count);
                                      getNotesData();
                                      Navigator.pop(context);
                                    },
                                    child: Text(
                                      "Delete",
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold),
                                    ))
                                : FlatButton(
                                    onPressed: () async {
                                      if (_formKey.currentState.validate()) {
                                        await database.transaction((txn) async {
                                          int id2 = await txn.rawInsert(
                                              'INSERT INTO Notes(title, note, page,date) VALUES(?, ?, ?,?)',
                                              [
                                                _titleController.text,
                                                _noteController.text,
                                                int.parse(controller.page
                                                    .toStringAsFixed(0)),
                                                DateTime.now().toIso8601String()
                                              ]);
                                          print('inserted2: $id2');
                                        });
                                        getNotesData();
                                        Navigator.pop(context);
                                      }
                                    },
                                    child: Text(
                                      "Ok",
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold),
                                    ))
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 20, horizontal: 10),
                          child: TextFormField(
                            controller: _titleController,
                            validator: (val) {
                              if (val.isEmpty) {
                                return "Enter note title";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                hintText: "Enter title here"),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 20, horizontal: 10),
                          child: TextFormField(
                            controller: _noteController,
                            validator: (val) {
                              if (val.isEmpty) {
                                return "Enter note description";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              hintText: "Enter note here",
                            ),
                            maxLines: 12,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          });
        });
  }

  Widget getStackView(BuildContext context, Widget child, bool bookmarked) {
    return showGridView
        ? getGridView()
        : Stack(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  setState(() {
                    showControls = !showControls;
                    showNotesBookMarks = false;
                    showSearchedArea = false;
                  });
                },
                child: child,
              ),
              FocusScope(
                node: _focusScopeNOde,
                child: OverlayContainer(
                  show: showNotesBookMarks,
                  // Let's position this overlay to the right of the button.
                  position: OverlayContainerPosition(
                      // Left position.
                      0,
                      // Bottom position.
                      0),
                  // The content inside the overlay.
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.8,
                    width: MediaQuery.of(context).size.width * 0.5,
                    padding: const EdgeInsets.all(20),
                    margin: const EdgeInsets.only(top: 5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: Colors.grey[300],
                          blurRadius: 3,
                          spreadRadius: 6,
                        )
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(3),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.blue)),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          activated = 0;
                                        });
                                      },
                                      child: AnimatedContainer(
                                        height: 100,
                                        duration: Duration(milliseconds: 500),
                                        color: activated == 0
                                            ? Colors.blue
                                            : Colors.white,
                                        child: Center(
                                          child: Text(
                                            "Bookmarks",
                                            style: TextStyle(
                                                color: activated == 0
                                                    ? Colors.white
                                                    : Colors.blue,
                                                fontSize: 30),
                                          ),
                                        ),
                                      ))),
                              Expanded(
                                  child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          activated = 1;
                                        });
                                      },
                                      child: AnimatedContainer(
                                        height: 100,
                                        duration: Duration(milliseconds: 500),
                                        color: activated == 1
                                            ? Colors.blue
                                            : Colors.white,
                                        child: Center(
                                          child: Text(
                                            "Notes",
                                            style: TextStyle(
                                                color: activated == 1
                                                    ? Colors.white
                                                    : Colors.blue,
                                                fontSize: 30),
                                          ),
                                        ),
                                      )))
                            ],
                          ),
                        ),
                        Expanded(
                          child: activated == 0
                              ? GridView.builder(
                                  shrinkWrap: true,
                                  itemCount: bookmarks.length,
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2,
                                          crossAxisSpacing: 30,
                                          mainAxisSpacing: 30),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Column(
                                      children: <Widget>[
                                        Expanded(
                                          child: GestureDetector(
                                            onTap: () {
                                              showNotesBookMarks = false;
                                              controller.animateToPage(
                                                  int.parse(bookmarks[index]),
                                                  duration:
                                                      Duration(seconds: 5),
                                                  curve: Curves.ease);
                                            },
                                            child: Container(
                                                child: Image.asset(
                                                    "assets/images/navigation_images/${int.parse(bookmarks[index]) + 1}.png")),
                                          ),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              "Page ${int.parse(bookmarks[index]) + 1}",
                                              style: TextStyle(fontSize: 30),
                                            ),
                                            IconButton(
                                                icon: Icon(
                                                  Icons.delete,
                                                  size: 40,
                                                ),
                                                color: Colors.blue,
                                                onPressed: () async {
                                                  SharedPreferences
                                                      _sharedPrefs =
                                                      await SharedPreferences
                                                          .getInstance();
                                                  setState(() {
                                                    bookmarks.removeAt(index);
                                                  });
                                                  _sharedPrefs.setStringList(
                                                      "bookmarks", bookmarks);
                                                })
                                          ],
                                        )
                                      ],
                                    );
                                  })
                              : Column(
                                  children: <Widget>[
//                            Row(children: <Widget>[
//                              Expanded(
//                                child: Builder(
//                                  builder: (BuildContext context){
//                                    return Material(
//                                      type: MaterialType.transparency,
//                                      child: Container(
//                                        width:MediaQuery.of(context).size.width,
//                                        padding:EdgeInsets.all(5),
//                                        color: Colors.grey,
//                                        child: TextField(
//                                          onChanged: (val){
//                                            if(val.isEmpty){
//                                              searchedNotesData=notesData;
//                                            }
//                                            else{
//                                              searchedNotesData.clear();
//                                              notesData.forEach((f){
//                                                if(f['title'].contains(val)|| f['note'].contains(val)){
//                                                  searchedNotesData.add(f);
//                                                }
//                                              });
//                                            }
//                                          },
//                                          decoration: InputDecoration(
//                                              border: OutlineInputBorder(
//                                                borderRadius: BorderRadius.circular(20),
//                                              ),
//                                              filled: true,
//                                              fillColor: Colors.white,
//                                              hintText: "Enter text to filter annotations"
//                                          ),
//                                        ),
//                                      ),
//                                    );
//                                  },
//                                ),
//                              )
//                            ],),
                                    Expanded(
                                      child: ListView.separated(
                                          shrinkWrap: true,
                                          separatorBuilder:
                                              (BuildContext context, index) {
                                            return Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Divider(
                                                height: 5,
                                                color: Colors.black,
                                              ),
                                            );
                                          },
                                          itemCount: searchedNotesData.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  showNotesBookMarks = false;
                                                });
                                                getShowDialog(context, true,
                                                    searchedNotesData[index]);
                                              },
                                              child: Container(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.2,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Expanded(
                                                      flex: 1,
                                                      child: Column(
                                                        children: <Widget>[
                                                          Expanded(
                                                              child: Container(
                                                                  child: Image
                                                                      .asset(
                                                            "assets/images/navigation_images/${searchedNotesData[index]['page'] + 1}.png",
                                                            fit: BoxFit.fill,
                                                          ))),
                                                          Center(
                                                              child: Text(
                                                            "Page ${searchedNotesData[index]['page'] + 1}",
                                                            style: TextStyle(
                                                                fontSize: 20,
                                                                color: Colors
                                                                    .black),
                                                          ))
                                                        ],
                                                      ),
                                                    ),
                                                    Expanded(
                                                      flex: 2,
                                                      child: Container(
                                                        margin: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 20,
                                                                vertical: 0),
                                                        decoration:
                                                            BoxDecoration(
                                                          border: Border.all(
                                                              color:
                                                                  Colors.grey,
                                                              width: 3),
                                                        ),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            SizedBox(
                                                              height: 15,
                                                            ),
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 20),
                                                              child: Container(
                                                                  width: MediaQuery.of(
                                                                              context)
                                                                          .size
                                                                          .width *
                                                                      0.16,
                                                                  child:
                                                                      AutoSizeText(
                                                                    "${searchedNotesData[index]['title']} :",
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                          .black,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    ),
                                                                    minFontSize:
                                                                        22,
                                                                    maxLines: 2,
                                                                  )),
                                                            ),
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left: 20),
                                                              child:
                                                                  AutoSizeText(
                                                                "${DateFormat("MMM d,yyyy").format(DateTime.parse(searchedNotesData[index]['date']))}",
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        20),
                                                                minFontSize: 16,
                                                              ),
                                                            ),
                                                            Expanded(
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            20),
                                                                child:
                                                                    Container(
                                                                        width: MediaQuery.of(context).size.width *
                                                                            0.22,
                                                                        child:
                                                                            AutoSizeText(
                                                                          "${searchedNotesData[index]['note']}",
                                                                          style:
                                                                              TextStyle(
                                                                            color:
                                                                                Colors.black,
                                                                          ),
                                                                          minFontSize:
                                                                              22,
                                                                        )),
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            );
                                          }),
                                    )
                                  ],
                                ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              showControls
                  ? Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        child: Row(
                          children: <Widget>[
                            IconButton(
                              icon: Icon(
                                Icons.dashboard,
                                color: Colors.black,
                                size: 40,
                              ),
                              onPressed: () {
                                setState(() {
                                  showGridView = true;
                                });
                              },
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Expanded(
                                child: FlutterSlider(
                              min: 1,
                              max: _pages.length.toDouble(),
                              onDragCompleted: (index, a, b) {
                                setState(() {
                                  sliderValue = a;
                                  showControls = false;
                                });
                                controller.animateToPage(a.toInt() - 1,
                                    duration: Duration(
                                      seconds: 2,
                                    ),
                                    curve: Curves.ease);
                              },
                              handler: FlutterSliderHandler(child: SizedBox()),
                              values: [sliderValue],
                              tooltip: FlutterSliderTooltip(custom: (value) {
                                return Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.2,
                                    width: 500,
                                    color: Colors.white,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            AutoSizeText(
                                              "${value.toInt()} to ${_pages.length}",
                                              minFontSize: 20,
                                            ),
                                          ],
                                        ),
                                        Expanded(
                                          child: Image.asset(
                                            "assets/images/navigation_images/${value.toInt()}.png",
                                            fit: BoxFit.fill,
                                          ),
                                        )
                                      ],
                                    ));
                              }),
                            ))
                          ],
                        ),
                      ),
                    )
                  : SizedBox(),
              showControls
                  ? Positioned(
                      top: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: MediaQuery.of(context).size.height * 0.1,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        child: Row(
                          children: <Widget>[
                            FlatButton(
                                onPressed: () {
                                  setState(() {
                                    showControls = false;
                                    showSearchedArea = false;
                                    showNotesBookMarks = false;
                                  });
                                },
                                child: Text(
                                  "Close",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 25),
                                )),
                            Spacer(
                              flex: 1,
                            ),
                            AutoSizeText(
                              "Page ${(controller.page + 1).toStringAsFixed(0)} of ${_pages.length}",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25),
                            ),
                            Spacer(
                              flex: 1,
                            ),
                            Row(
                              children: <Widget>[
                                IconButton(
                                  icon: Icon(
                                    Icons.add_comment,
                                    color: Colors.black,
                                    size: 40,
                                  ),
                                  onPressed: () {
                                    getShowDialog(context, false, []);
                                  },
                                ),
                                IconButton(
                                  icon: Icon(
                                    bookmarked
                                        ? Icons.bookmark
                                        : Icons.bookmark_border,
                                    color:
                                        bookmarked ? Colors.blue : Colors.black,
                                    size: 40,
                                  ),
                                  onPressed: () async {
                                    SharedPreferences _sharedPrefs =
                                        await SharedPreferences.getInstance();

                                    if (bookmarks.contains(
                                        controller.page.toStringAsFixed(0))) {
                                      setState(() {
                                        bookmarked = false;
                                        bookmarks.remove(
                                            controller.page.toStringAsFixed(0));
                                      });
                                    } else {
                                      setState(() {
                                        bookmarked = true;
                                        bookmarks.add(
                                            controller.page.toStringAsFixed(0));
                                      });
                                    }
                                    _sharedPrefs.setStringList(
                                        "bookmarks", bookmarks);
                                  },
                                ),
                                IconButton(
                                  icon: Icon(Icons.collections_bookmark),
                                  onPressed: () {
                                    setState(() {
                                      showNotesBookMarks = !showNotesBookMarks;
                                    });
                                  },
                                ),
                                FlatButton(
                                    onPressed: () {
                                      setState(() {
                                        showSearchedArea = !showSearchedArea;
                                        searchedResult.clear();
                                      });
                                    },
                                    child: Text(
                                      "Search",
                                      style: TextStyle(
                                          color: Colors.blue, fontSize: 30),
                                    ))
                              ],
                            )
                          ],
                        ),
                      ),
                    )
                  : SizedBox(),
              showSearchedArea
                  ? Positioned(
                      top: MediaQuery.of(context).size.height * 0.1,
                      right: 0,
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.35,
                        height: MediaQuery.of(context).size.height * 0.7,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              blurRadius:
                                  20.0, // has the effect of softening the shadow
                              spreadRadius:
                                  5.0, // has the effect of extending the shadow
                              offset: Offset(
                                -10.0, // horizontal, move right 10
                                10.0, // vertical, move down 10
                              ),
                            )
                          ],
                        ),
                        child: Column(
                          children: <Widget>[
                            TextField(
                              onChanged: getSearchResults,
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  prefixIcon: Icon(
                                    Icons.search,
                                    size: 30,
                                  ),
                                  filled: true,
                                  fillColor: Colors.grey[400]),
                            ),
                            Expanded(
                                child: ListView.separated(
                                    shrinkWrap: true,
                                    itemCount: searchedResult.length,
                                    separatorBuilder:
                                        (BuildContext context, index) {
                                      return Divider();
                                    },
                                    itemBuilder: (BuildContext context, index) {
                                      return GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            showSearchedArea = false;
                                          });
                                          controller.animateToPage(
                                              searchedResult[index]['page'],
                                              duration: Duration(seconds: 2),
                                              curve: Curves.ease);
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex: 1,
                                                child: Container(
                                                  child: Image.asset(
                                                      "assets/images/navigation_images/${searchedResult[index]['page'] + 1}.png"),
                                                )),
                                            Expanded(
                                              flex: 2,
                                              child: Column(
                                                children: <Widget>[
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceAround,
                                                    children: <Widget>[
                                                      AutoSizeText(
                                                        "Page: ${searchedResult[index]['page'] + 1}",
                                                        minFontSize: 20,
                                                        style: TextStyle(
                                                            color: Colors.blue,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      AutoSizeText(
                                                        "Occurence: ${searchedResult[index]['occurence']}",
                                                        minFontSize: 20,
                                                        style: TextStyle(
                                                            color: Colors.blue,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      )
                                                    ],
                                                  ),
                                                  Container(
                                                      child: AutoSizeText(
                                                    "${searchedResult[index]['text']}",
                                                    minFontSize: 30,
                                                  ))
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      );
                                    }))
                          ],
                        ),
                      ),
                    )
                  : SizedBox()
            ],
          );
  }

  @override
  Widget build(BuildContext context) {
    double mydata = (MediaQuery.of(context).size.height / 100) * 7;
    double ht = MediaQuery.of(context).size.height - mydata;
    double wt = ht * 1.3;
    int page = 49;
    Template template = new Template(ht, wt, page);
    return Scaffold(
        body: Center(
      child: Container(
        child: Center(
            child: Padding(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * 0.0,
                    horizontal: MediaQuery.of(context).size.width * 0.0),
                child: Container(
                    decoration: BoxDecoration(border: Border.all(width: 1)),
                    child: Container(
                        child: PageView.builder(
                            controller: controller,
                            itemCount: _pages.length,
                            itemBuilder: (BuildContext context, int index) {
                              if (index == 5) {
                                return getStackView(
                                    context,
                                    getContentPage(context),
                                    bookmarks.contains(index.toString()));
                              }
                              return getStackView(context, _pages[index],
                                  bookmarks.contains(index.toString()));
                            }))))),
      ),
    ));
  }

  Widget getGridView() {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        backgroundColor: Colors.white,
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                setState(() {
                  showGridView = false;
                });
              },
              child: Text(
                "Close",
                style: TextStyle(color: Colors.blue, fontSize: 35),
              ))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(0),
        child: GridView.builder(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4, crossAxisSpacing: 20, mainAxisSpacing: 10),
            itemCount: _pages.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () async {
                  setState(() {
                    showGridView = false;
                  });
                  await Future.delayed(Duration(milliseconds: 500));
                  controller.animateToPage(index,
                      duration: Duration(
                          seconds: (index - controller.page).toInt().abs()),
                      curve: Curves.ease);
                },
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                            padding: const EdgeInsets.all(0),
                            color: controller.page == index
                                ? Colors.blue
                                : Colors.white,
                            child: Image.asset(
                                "assets/images/navigation_images/${index + 1}.png")),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "${index + 1}",
                            textAlign: TextAlign.end,
                            style: TextStyle(fontSize: 30),
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.end,
                      )
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }
}

class ContentData {
  final String title;
  final int number;
  ContentData({this.title, this.number});

  static getAllData() {
    return [
      ContentData(title: "Foreword", number: 7),
      ContentData(title: "European Influences", number: 134),
      ContentData(title: "Introduction", number: 9),
      ContentData(title: "A New Generation of Masters", number: 140),
      ContentData(
          title: "Understanding the Past Part I: A Personal Perspective",
          number: 10),
      ContentData(title: "African American Women in the Arts", number: 151),
      ContentData(
          title: "Understanding the Past Part II: Collecting History",
          number: 11),
      ContentData(title: "Southern Home", number: 160),
      ContentData(title: "Lineage", number: 14),
      ContentData(title: "African American Artists of the West", number: 166),
      ContentData(title: "Early Genius", number: 31),
      ContentData(
          title: "Afterthoughts: The Tradition of African American Art",
          number: 184),
      ContentData(title: "Slavery", number: 45),
      ContentData(title: "The Americas + 1", number: 186),
      ContentData(title: "African Americans and War", number: 65),
      ContentData(title: "Living with Art", number: 192),
      ContentData(title: "Hearts and Souls", number: 75),
      ContentData(
          title:
              "Re-Discovering America:\nFamily Treasures from The Kinsey Collection",
          number: 196),
      ContentData(title: "Forging Freedom", number: 81),
      ContentData(title: "Recommended Reading", number: 198),
      ContentData(title: "Reflections", number: 116),
      ContentData(title: "Narrator Biographies", number: 202),
      ContentData(title: "Translating Inspiration", number: 118),
      ContentData(title: "Acknowledgments", number: 205),
      ContentData(title: "The Birth of an Aesthetic", number: 120),
      ContentData(title: "Family and Friends", number: 208)
    ];
  }
}
